//////////////////////////////////////////////////////////////////////////////////
// Company: ITESO
// Engineer: Gustavo Araiza Obeso ie699209@iteso.mx
// 			 
// Create Date:  20/04/2019  
// Design Name:  
// Module Name:  UART
// Project Name: Sequential multiplier
// Target Devices: FPGA ALTERA DE2-115
// Tool versions: Quartus Prime
//
// Revision: 
// Revision 0.01 - File Created
// Additional Comments: 
////////////////////////////////////////////////////////////////////////////////////
module UART
import nBitsPackage::*;
(
	 // inputs
    input clk,
	 input rst,
    input data8_t dataToTransmit_in,
	 input serialData_RX_in,
	 input clearInterrupt_in,
	 input transmit_in,
	 //Outputs
	 output data8_t receivedData_o,
	 output parity_RX_o,
    output RX_INT_o,
	 output serialOutput_TX_o
	 
);



logic RX_INT_l;
logic RX_interruptEnable_l;
// RX
UART_RX Receive(
	.clk(clk),
	.rst(rst),
	.RX_in(serialData_RX_in),
	.RX_INT_o(RX_interruptEnable_l),
	.parity_RX_o(parity_RX_o),
	.Q_o(receivedData_o)
);

// Tx
UART_TX Transmit(
	
	.clk_UART_in(clk),
	.rst(rst),
	.EN(transmit_in),
	.data_in(dataToTransmit_in),
	.Data_o(serialOutput_TX_o)
);

/*1 Bit register*/ 




//////////////////////////////// Assigns
assign RX_INT_o = RX_INT_l;

endmodule 
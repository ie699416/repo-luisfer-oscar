/*
*************************************************************************************
*	Coder:	OSCAR MAISTERRA FLORES	 		[ ie699416]
*	Coder: 	LUIS FERNANDO MORENO ALONzO	[ ie693314] 
*	Date:          Jun 4th, 2019	
*  Name:          Counter_SM.sv      
*************************************************************************************
*/

module Counter_SM
import nBitsPackage::*;
(
	// Input Ports
	input clk,
	input reset,
	input enable,
	
	// Output Ports
	output flag,
	output dataN_t countValue_o
	
);

logic MaxValue_Bit;
dataCount_t Count_logic;

	always_ff@(posedge clk or negedge reset) begin: counter
		if (reset == FALSE)
			Count_logic <= ZERON;
		else begin
				if(enable == TRUE)
					Count_logic <= Count_logic + UNIT;
				else
					Count_logic <= ZERON;
						
		end
	end: counter

//--------------------------------------------------------------------------------------------

always_comb
	if(Count_logic ==  N - UNIT)
		MaxValue_Bit = HIGH;
	else
		MaxValue_Bit = LOW;

assign flag = MaxValue_Bit;
assign countValue_o = Count_logic;

endmodule

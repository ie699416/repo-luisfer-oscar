//////////////////////////////////////////////////////////////////////////////////
// Company: ITESO
// Engineer: Gustavo Araiza Obeso ie699209@iteso.mx
// 			 
// Create Date:  10/05/2019  
// Design Name:  
// Module Name:  Top level Practica 3 MxV
// Project Name: Sequential multiplier
// Target Devices: FPGA ALTERA DE2-115
// Tool versions: Quartus Prime
//
// Revision: 
// Revision 0.01 - File Created
// Additional Comments: 
////////////////////////////////////////////////////////////////////////////////////
/**********************************************************************************/

module Top_level
import nBitsPackage::*;

(
	//inputs
	input clk,
	input reset_pb_in,
	input serial_input_RX_in,	// PIN_G12
	input data8_t data_result_l,
	input logic enable_TX_l,
	//outputs
	output serial_output_TX_o,	// 
	//output logic_enum_state_t state_o
	
			
		/* 7 SEGM */
		output [6:0] unidades_7seg
);

/*
*************************************************************************************
* Logics definitions
*************************************************************************************
*/
logic interrupt_l;
//logic enable_TX_l;
logic serial_out_l;
logic clear_interrupt_l;

//data8_t data_result_l; 
data8_t received_data_l;



/*

*************************************************************************************
* Instances definitions
*************************************************************************************
*/

UART uart_module
(
    .dataToTransmit_in(data_result_l),
	 .serialData_RX_in(serial_input_RX_in),
    .rst(reset_pb_in),
    .clk(clk),
	 .clearInterrupt_in(clear_interrupt_l),
	 .transmit_in(enable_TX_l),
	 .receivedData_o(received_data_l),
    .RX_INT_o(interrupt_l),
	 .serialOutput_TX_o(serial_output_TX_o) 
	
);


Conv_ASCII_7seg segmentos
(
	.ASCII_in(received_data_l),
	.segmentos(unidades_7seg)	
);



endmodule


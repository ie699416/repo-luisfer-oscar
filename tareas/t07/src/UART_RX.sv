//////////////////////////////////////////////////////////////////////////////////
// Company: ITESO
// Engineer: Gustavo Araiza Obeso ie699209@iteso.mx
// 			 
// Create Date:  21/04/2019  
// Design Name:  
// Module Name:  UART RX
// Project Name: Sequential multiplier
// Target Devices: FPGA ALTERA DE2-115
// Tool versions: Quartus Prime
//
// Revision: 
// Revision 0.01 - File Created
// Additional Comments: 
////////////////////////////////////////////////////////////////////////////////////
module UART_RX
import nBitsPackage::*;
(
    input clk,
    input rst,
    input RX_in,
	 output parity_RX_o,
    output data8_t Q_o,
	 output logic RX_INT_o
);

/*
*************************************************************************************
* Logics definitions
*************************************************************************************
*/


logic enableFlag_l;
logic Counter_flag;
logic Start_counter;
logic data_ready_flag_l;
logic REG_UART_l;
logic sync_rst_l;
logic CLK_UART_l;

data11_t Q_l;

/*

*************************************************************************************
* Instances definitions
*************************************************************************************
*/

shiftRegister_Q UART_Register
(
	.clk(CLK_UART_l),
	.rst(rst),
	.EN(data_ready_flag_l),
	.D_in(RX_in),
	.Q_o(Q_l)
);

Register_With_Sync_Reset INT_register
(
	.clk(CLK_UART_l), 
	.reset(rst),
	.enable(REG_UART_l),
	.Sync_Reset(sync_rst_l),					//Flush
	.Data_Input( Q_l[10:2]),
	.parity_RX_o(parity_RX_o),
	.Data_Output(Q_o)
);
	 
UART_FSM_RX UART_CONTROL 
(
	.CLK_UART(CLK_UART_l),
	.rst(rst),
	.RX_INT(RX_in),
	.Counter_flag(Counter_flag),
	.start_counter(Start_counter),
	.receiving_flag(enableFlag_l),
	.data_ready_flag(data_ready_flag_l),
	.REG_UART_flag(REG_UART_l), 
	.sync_rst_flag(sync_rst_l)	 
);

CLK_115200 RX_sampler
(
	.clk(clk),
	.rst(rst),
	.EN(enableFlag_l),
	.clk_115200(CLK_UART_l)	
);

Counter_SM counter
(
	// Input Ports
	.clk(CLK_UART_l),
	.reset(rst),
	.enable(Start_counter),
	
	// Output Ports
	.flag(Counter_flag),
	.countValue_o()
);

	 
	 
endmodule

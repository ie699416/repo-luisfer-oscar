
`ifndef MYPACKAGE_DONE
   `define MYPACKAGE_DONE
	 package nBitsPackage; // package name

		parameter WORD_LENGTH = 8;
		parameter N = 8;
		parameter BD_COUNT_VALUE = /*216*/ 9'b011011000;
		parameter DELAY_COUNT_VALUE = /*108 + 216*/ 9'b101000100;		
		parameter ZERO8 = {WORD_LENGTH{1'b0}};
		parameter ZERO9 = {9{1'b0}};
		parameter ZERO11 = {11{1'b0}};
		parameter BD_115200 = 115200; 
		parameter FPGA_CCLK =50000000;	
		parameter UNIT = 1'b1;	
		parameter HIGH = 1'b1;
		parameter LOW  = 1'b0;
		parameter TRUE = 1'b1;
		parameter FALSE = 1'b0;
		parameter ZERO = 1'b0;
		parameter ZERON = {N{1'b0}};
		parameter TOP_VAL	= {N{1'b1}};
		
			
		typedef logic [WORD_LENGTH-1:0] data8_t;
		typedef logic [WORD_LENGTH:0] data9_t;
		typedef logic [1:0] data2_t;		
		typedef logic [10:0] data11_t;
		typedef logic [5:0] dataCount_t;
		typedef logic [N-1:0] dataN_t;
		
		typedef enum logic [3:0] {IDLE, STARTBIT,PROCESS, PARITY, STOPBIT} UART_STATE_en;	
		
		
	endpackage
`endif
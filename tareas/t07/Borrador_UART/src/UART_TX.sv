//////////////////////////////////////////////////////////////////////////////////
// Company: ITESO
// Engineer: Gustavo Araiza Obeso ie699209@iteso.mx
// 			 
// Create Date:  23/04/2019  
// Design Name:  
// Module Name:  UART Tx (transmition)
// Project Name: Sequential multiplier
// Target Devices: FPGA ALTERA DE2-115
// Tool versions: Quartus Prime
//
// Revision: 
// Revision 0.01 - File Created
// Additional Comments: 
////////////////////////////////////////////////////////////////////////////////////
module UART_TX
import nBitsPackage::*;(
    input clk_UART_in,
    input rst,
	 input EN,						// Enable 
    input data8_t data_in,	 
    output Data_o
);

/*
*************************************************************************************
* Logics definitions
*************************************************************************************
*/

dataN_t Wire_count;

logic CLK_UART_l;
logic Start_counter;
logic Counter_flag;
logic Start_Reg;
logic Start_clk_115;
logic l_s_wire;

UART_FSM_TX Tx_control
(
	 .CLK_UART(CLK_UART_l),
    .rst(rst),	 
	 .Counter_flag(Counter_flag),
	 .Start(EN),
	 
	 .start_counter(Start_counter),
	 .TX_enable(Start_clk_115),
	 .Load_flag(l_s_wire),
    .TX_flag(Start_Reg)
);




Counter_SM counter
(
	// Input Ports
	.clk(CLK_UART_l),
	.reset(rst),
	.enable(Start_counter),
	
	// Output Ports
	.flag(Counter_flag),
	.countValue_o()
);

Register
TX_register
(
	.clk(CLK_UART_l),
	.rst(rst),
	.enable(Start_Reg),
	.data_in(data_in),
	.l_s(l_s_wire),
	.data_o(Data_o)
);

CLK_115200 RX_sampler
(
	.clk(clk_UART_in),
	.rst(rst),
	.EN(Start_clk_115),
	.clk_115200(CLK_UART_l)	
);

                                                                                                                                              
endmodule 
module UART_FSM_TX
import nBitsPackage::*;
(
    input CLK_UART,
    input rst,	 
	 input Counter_flag,
	 input logic Start,
	 
	 //////////
	 output logic start_counter,
	 output logic TX_enable,
	 output logic Load_flag,
    output logic TX_flag
	 
);


/*state enum definition*/
UART_STATE_en UART_currentState; 


always_ff@(posedge CLK_UART or negedge rst ) 
	begin
		if(rst == FALSE)
		 UART_currentState <= IDLE;
		else 
			begin
				case (UART_currentState)
					IDLE: 
						begin
							if(Start == TRUE)
								UART_currentState <= STARTBIT;
							else 
								UART_currentState <= IDLE;
						end
					STARTBIT:
						UART_currentState <= PROCESS;
					PROCESS:
						begin
							if(Counter_flag == TRUE)
								UART_currentState <= PARITY;
							else
								UART_currentState <= UART_currentState;
						end
					PARITY:
						UART_currentState <= STOPBIT;
					STOPBIT :
						UART_currentState <= IDLE;
					default:
						UART_currentState <= IDLE;
				endcase
			end
	end


/*
*************************************************************************************
* Combinational processes
*************************************************************************************
*/


always@(*)
begin
	case(UART_currentState)
		IDLE: 
			begin
				TX_flag = 1'b0;; 
				start_counter = 1'b0; 
				TX_enable  = 1'b1; 
				Load_flag = 1'b0; 
			end
		STARTBIT:
			begin
				TX_flag = 1'b1;
				start_counter = 1'b1; 
				TX_enable  = 1'b1; 
				Load_flag = 1'b1;
			end
		PROCESS:
			begin
				TX_flag = 1'b1;
				start_counter = 1'b1; 
				TX_enable  = 1'b1; 
				Load_flag = 1'b0;
			end
		PARITY:
			begin
				TX_flag = 1'b1;
				start_counter = 1'b0; 
				TX_enable  = 1'b1; 	
				Load_flag = 1'b0;
			end
		STOPBIT:	
			begin
				TX_flag = 1'b0;
				start_counter = 1'b0; 
				TX_enable  = 1'b1; 
				Load_flag = 1'b0;
			end
		default: 
			begin
				TX_flag = 1'b0;
				start_counter = 1'b0; 
				TX_enable  = 1'b0;
				Load_flag = 1'b0;	
			end
	endcase

end


endmodule

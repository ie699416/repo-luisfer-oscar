 //////////////////////////////////////////////////////////////////////////////////
// Company: ITESO
// Engineer: Gustavo Araiza Obeso ie699209@iteso.mx
// 			 
// Create Date:  25/04/2019  
// Design Name:  
// Module Name:  register
// Project Name: Sequential multiplier
// Target Devices: FPGA ALTERA DE2-115
// Tool versions: Quartus Prime
//
// Revision: 
// Revision 0.01 - File Created
// Additional Comments: 
////////////////////////////////////////////////////////////////////////////////////
 module Register
import nBitsPackage::*;
(

	// Input Ports
	input clk,
	input rst,
	input enable,
	input data8_t data_in,	
	input l_s,    // load or shift
	// Output Ports
	output logic data_o
);

data8_t register;

always_ff@(posedge clk or negedge rst) 
begin: Register8Bits
	 if(rst == FALSE)
        register  <= ZERO;
    else if (enable == TRUE) begin
        if (l_s == TRUE)
            register  <= data_in;
        else 
            register  <= {register[N-2:0], register[N-1]};
		  end
	else
		register  <= ZERO;
	
end: Register8Bits

assign data_o = register[N-1];

endmodule

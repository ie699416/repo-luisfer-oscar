

module CLK_change
import nBitsPackage::*;

(
	
	// Input Ports
	input clk_500,
	input clk_115,
	input rst,
	input Data_in, 
	// Output Ports
	output logic Enable_out
	
);


dataCount_t Count_logic;
dataCount_t Count_reg;
logic Flag_count;

always_ff@(posedge clk_500 or negedge rst) 
begin
		if (rst == FALSE)
			begin
					Count_logic <= ZERO8;		 				
			end
		else
			begin
				if(Data_in == Flag_count)
				   Count_reg <= Count_logic
					Count_logic <= ZERO8;
					
				else
					begin
						Count_logic <= Count_logic + UNIT;			
					end
			end
	end
	
always_comb
	if(Count_logic ==  N - UNIT)
		MaxValue_Bit = HIGH;
	else
		MaxValue_Bit = LOW;

assign flag = MaxValue_Bit;
assign countValue_o = Count_logic;

endmodule

//////////////////////////////////////////////
//														  //
//	17/febrero/2019								  //	
//														  //
//Felipe Rivera									  //	
//Oscar Maisterra 								  //
//														  //
//														  //
//Modulo contador con función			        //
//														  //
//														  //
//														  //
//														  //
//														  //
//////////////////////////////////////////////
module CLK_115200
import nBitsPackage::*;

(
	
	// Input Ports
	input clk,
	input rst,
	input EN, 
	// Output Ports
	output logic clk_115200
	
);

data9_t Count_logic; // Max count 436

bit MaxValue_Bit = 1'b0;
bit clk_output = 1'b0;


always_ff@(posedge clk or negedge rst) 
	begin
		if (rst == 1'b0)
			begin
					Count_logic <= ZERO8;
					clk_output <= 1'b0;						
			end
		else
			begin
				if(EN == 1'b1)
					begin
						if(MaxValue_Bit == 1'b1)
							begin							
								clk_output <= ~clk_output;
								Count_logic <= ZERO8;		
							end
						else 
							Count_logic <= Count_logic + 1'b1;
					end
				else
					begin
						clk_output <= 1'b0;
						Count_logic <= ZERO8;			
					end
			end
	end
	
	
	
always@(*)
			begin
				if(Count_logic == BD_COUNT_VALUE - 1)
					MaxValue_Bit = 1'b1;
				else
					MaxValue_Bit = 1'b0;
			end

assign clk_115200 = clk_output;




endmodule 

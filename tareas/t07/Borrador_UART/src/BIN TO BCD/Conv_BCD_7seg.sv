module Conv_BCD_7seg
(
	// Input Ports
	input [3:0]BCD_in,

	// Output Ports
	output [6:0]segmentos

);

 logic[6:0]segment;

//Case encargado de mostrar el numero correspondiente de la entra al display
always_comb
begin
//Solamente tomamos los casos desde 0 hasta 9
	case (BCD_in)
	
		0 : segment = 7'b1000000; 
      1 : segment = 7'b1111001;
      2 : segment = 7'b0100100;
      3 : segment = 7'b0110000;
		4 : segment = 7'b0011001;
      5 : segment = 7'b0010010;
      6 : segment = 7'b0000010;
      7 : segment = 7'b1111000;
      8 : segment = 7'b0000000;
      9 : segment = 7'b0010000;
      default : segment = 7'b1111111; //Si no esta dentro del rango, se apaga el display
	
	
    endcase
end

assign segmentos = segment;

endmodule
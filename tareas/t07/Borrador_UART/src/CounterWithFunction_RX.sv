//////////////////////////////////////////////////////////////////////////////////
// Company: ITESO
// Engineer: Gustavo Araiza Obeso ie699209@iteso.mx
// 			 
// Create Date:  21/04/2019  
// Design Name:  
// Module Name:  contador para RX
// Project Name: Sequential multiplier
// Target Devices: FPGA ALTERA DE2-115
// Tool versions: Quartus Prime
//
// Revision: 
// Revision 0.01 - File Created
// Additional Comments: 
////////////////////////////////////////////////////////////////////////////////////
module CounterWithFunction_RX
import nBitsPackage::*;
(
	// Input Ports
	input clk,
	input rst,
	input EN_max_in,
	input EN_half_in,
	
	// Output Ports
	output CounterFlag_o
);


/*
*************************************************************************************
* Logics definitions
*************************************************************************************
*/

bit MaxValue_Bit;

logic [NBITS_FOR_COUNTER-1 : 0] Count_logic;
logic [NBITS_FOR_COUNTER-1 : 0] limit;

assign limit = (EN_half_in) ? HALF_VALUE[NBITS_FOR_COUNTER - 1:0] : MAXIMUM_VALUE[NBITS_FOR_COUNTER - 1:0];


/*
*************************************************************************************
* Sequential Processes 
*************************************************************************************
*/


always_ff@(posedge clk or negedge rst)
	begin
		if (rst == 1'b0)
			Count_logic <= {NBITS_FOR_COUNTER{1'b0}};
		else begin
				if(EN_max_in | EN_half_in)
					if(Count_logic == limit - 1'b1)
						Count_logic <= 1'b0;
					else
						Count_logic <= Count_logic + 1'b1;
		end
	end

/*
*************************************************************************************
* Combinational Processes 
*************************************************************************************
*/

always_comb
	if(Count_logic == limit-1'b0)
		MaxValue_Bit = 1'b1;
	else
		MaxValue_Bit = 1'b0;

/*Continuous assing*/		

assign CounterFlag_o = MaxValue_Bit;

endmodule

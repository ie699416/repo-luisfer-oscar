module Conv_ASCII_7seg
(
	// Input Ports
	input [7:0]ASCII_in,

	// Output Ports
	output  logic [6:0]segmentos

);

 logic[6:0]segment;

//Case encargado de mostrar el numero correspondiente de la entra al display
always_comb
begin
//Solamente tomamos los casos desde 0 hasta 9
	case (ASCII_in)
	
		48 : segmentos = 7'b1000000; 
      49 : segmentos = 7'b1111001;
      50 : segmentos = 7'b0100100;
      51 : segmentos = 7'b0110000;
		52 : segmentos = 7'b0011001;
      53 : segmentos = 7'b0010010;
      54 : segmentos = 7'b0000010;
      55 : segmentos = 7'b1111000;
      56 : segmentos = 7'b0000000;
      57 : segmentos = 7'b0010000;
      default : segmentos = 7'b1000000; //Si no esta dentro del rango, se prende el display en 0
	
	
    endcase
end



endmodule
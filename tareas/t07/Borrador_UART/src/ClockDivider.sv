//////////////////////////////////////////////
//														  //
//	17/febrero/2019								  //	
//														  //
//Felipe Rivera									  //	
//Oscar Maisterra 								  //
//														  //
//														  //
//Modulo contador con función			        //
//														  //
//														  //
//														  //
//														  //
//														  //
//////////////////////////////////////////////
module CLK_GEN #(
import nBitsPackage::*;
(
	input clk_FPGA,
	input rst,
	input EN,
	output clk_115200
	
);

data8_t Count_logic; 	//Max count = 217 

bit MaxValue_Bit;
bit clk_output = 1'b0;


always_ff@(posedge clk_FPGA or negedge rst) 
	begin
		if (rst == 1'b0)
			begin
					Count_logic <= {WORD_LENGTH{1'b0}};
					clk_output <= 1'b0;
					
			end
		else 
			begin
				if (EN == 1'b1)
					begin
						if(MaxValue_Bit == 1'b1)
							begin
								clk_output <= ~clk_output;
								Count_logic <= 1'b0;
							end
						else 
							Count_logic <= Count_logic + 1'b1;	
					end	
				else
					begin
						Count_logic <= {WORD_LENGTH{1'b0}};
						clk_output <= 1'b0;
					end
			end
	end
	
always_comb

	if(Count_logic == MAXIMUM_VALUE - 1)
		MaxValue_Bit = 1;
	else
		MaxValue_Bit = 0;

assign clk_115200 = clk_output;


endmodule 




 

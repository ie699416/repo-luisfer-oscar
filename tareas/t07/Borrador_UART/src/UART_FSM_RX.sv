//////////////////////////////////////////////////////////////////////////////////
// Company: ITESO
// Engineer: Gustavo Araiza Obeso ie699209@iteso.mx
// 			 
// Create Date:  21/04/2019  
// Design Name:  
// Module Name:  UART FSM (para RX)
// Project Name: Sequential multiplier
// Target Devices: FPGA ALTERA DE2-115
// Tool versions: Quartus Prime
//
// Revision: 
// Revision 0.01 - File Created
// Additional Comments: 
////////////////////////////////////////////////////////////////////////////////////
module UART_FSM_RX
import nBitsPackage::*;
(
    input CLK_UART,
    input rst,	 
	 input RX_INT,
	 input Counter_flag,
	 output logic start_counter,
    output logic receiving_flag,
	 output logic data_ready_flag,
	 output logic REG_UART_flag, 
	 output logic sync_rst_flag
	 
);


/*state enum definition*/
UART_STATE_en UART_currentState; 
logic RX_INT_l = 1'b0;

/*
*************************************************************************************
* Continuous assign
*************************************************************************************
*/

always@(*) 
	begin
		if (RX_INT_l == 1'b0)
			begin
				if(RX_INT == 1'b0)
					begin
						RX_INT_l = 1'b1;
					end
				else
					begin
						RX_INT_l = 1'b0;
					end
			end
		else
			begin
				if (data_ready_flag == 1'b1)
					RX_INT_l = 1'b0;
				else 
					RX_INT_l = 1'b1;
			end			
	end


always_ff@(posedge CLK_UART or negedge rst or posedge RX_INT_l ) 
	begin
		if(rst == 1'b0)
		 UART_currentState <= IDLE;
		else 
			begin
				case (UART_currentState)
					IDLE: 
						begin
							if(RX_INT_l == TRUE)
								UART_currentState <= STARTBIT;
							else 
								UART_currentState <= IDLE;
						end
					STARTBIT:
						UART_currentState <= PROCESS;
					PROCESS:
						begin
							if(Counter_flag == TRUE)
								UART_currentState <= PARITY;
							else
								UART_currentState <= UART_currentState;
						end
					PARITY:
						UART_currentState <= STOPBIT;
					STOPBIT :
						UART_currentState <= IDLE;
					default:
						UART_currentState <= IDLE;
				endcase
			end
	end


/*
*************************************************************************************
* Combinational processes
*************************************************************************************
*/


always@(*)
begin
	case(UART_currentState)
		IDLE: 
			begin
				receiving_flag = 1'b1;
				data_ready_flag = 1'b1;
				REG_UART_flag = 1'b0; 
				sync_rst_flag = 1'b0; 
				start_counter = 1'b0; 
			end
		STARTBIT:
			begin
				receiving_flag = 1'b1;
				data_ready_flag = 1'b0;
				REG_UART_flag = 1'b0; 
				sync_rst_flag = 1'b0; 
				start_counter = 1'b1; 
			end
		PROCESS:
			begin
				receiving_flag = 1'b1;
				data_ready_flag = 1'b0;
				REG_UART_flag = 1'b0; 
				sync_rst_flag = 1'b0; 
				start_counter = 1'b1; 
			end
		PARITY:
			begin
				receiving_flag = 1'b1;
				data_ready_flag = 1'b0;
				REG_UART_flag = 1'b0; 
				sync_rst_flag = 1'b0;
				start_counter = 1'b0; 	
			end
		STOPBIT:	
			begin
				receiving_flag = 1'b1;
				data_ready_flag = 1'b1;
				REG_UART_flag = 1'b1; 
				sync_rst_flag = 1'b0; 
				start_counter = 1'b0; 
			end
		default: 
			begin
				receiving_flag = 1'b0;
				data_ready_flag = 1'b0;
				REG_UART_flag = 1'b0; 
				sync_rst_flag = 1'b1; 
				start_counter = 1'b0; 
			end
	endcase

end


endmodule

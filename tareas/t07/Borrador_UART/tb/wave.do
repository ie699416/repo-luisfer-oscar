onerror {resume}
quietly WaveActivateNextPane {} 0
add wave -noupdate /Top_level_tb/DUT/uart_module/Receive/clk
add wave -noupdate /Top_level_tb/DUT/uart_module/Receive/rst
add wave -noupdate /Top_level_tb/DUT/uart_module/Receive/enableFlag_l
add wave -noupdate /Top_level_tb/DUT/uart_module/Receive/UART_CONTROL/UART_currentState
add wave -noupdate -radix decimal /Top_level_tb/DUT/uart_module/receivedData_o
add wave -noupdate /Top_level_tb/DUT/uart_module/Receive/UART_Register/Q_o
add wave -noupdate /Top_level_tb/DUT/uart_module/Receive/UART_Register/EN
add wave -noupdate /Top_level_tb/TX_l
add wave -noupdate /Top_level_tb/DUT/uart_module/Receive/CLK_UART_l
add wave -noupdate /Top_level_tb/DUT/uart_module/Receive/UART_Register/D_in
add wave -noupdate /Top_level_tb/DUT/uart_module/Receive/UART_CONTROL/REG_UART_flag
add wave -noupdate /Top_level_tb/DUT/segmentos/ASCII_in
add wave -noupdate /Top_level_tb/DUT/segmentos/segmentos
add wave -noupdate /Top_level_tb/DUT/uart_module/Receive/Counter_flag
add wave -noupdate /Top_level_tb/DUT/uart_module/Receive/Start_counter
add wave -noupdate /Top_level_tb/DUT/uart_module/Receive/counter/MaxValue_Bit
add wave -noupdate /Top_level_tb/DUT/uart_module/Receive/counter/Count_logic
add wave -noupdate -color Magenta /Top_level_tb/Start
add wave -noupdate -color Magenta /Top_level_tb/data_result_l
add wave -noupdate /Top_level_tb/DUT/uart_module/Transmit/clk_UART_in
add wave -noupdate /Top_level_tb/DUT/uart_module/Transmit/rst
add wave -noupdate /Top_level_tb/DUT/uart_module/Transmit/EN
add wave -noupdate /Top_level_tb/DUT/uart_module/Transmit/data_in
add wave -noupdate /Top_level_tb/DUT/uart_module/Transmit/Data_o
add wave -noupdate /Top_level_tb/DUT/uart_module/Transmit/Start_counter
add wave -noupdate /Top_level_tb/DUT/uart_module/Transmit/Counter_flag
add wave -noupdate /Top_level_tb/DUT/uart_module/Transmit/Start_Reg
add wave -noupdate /Top_level_tb/DUT/uart_module/Transmit/Start_clk_115
add wave -noupdate /Top_level_tb/DUT/uart_module/Transmit/CLK_UART_l
add wave -noupdate /Top_level_tb/DUT/uart_module/serialOutput_TX_o
add wave -noupdate /Top_level_tb/DUT/uart_module/Transmit/Tx_control/UART_currentState
add wave -noupdate /Top_level_tb/DUT/uart_module/Transmit/Tx_control/Counter_flag
add wave -noupdate /Top_level_tb/DUT/uart_module/Transmit/counter/flag
TreeUpdate [SetDefaultTree]
WaveRestoreCursors {{Cursor 1} {73830000 ps} 0} {{Cursor 2} {873431612 ps} 0}
quietly wave cursor active 1
configure wave -namecolwidth 395
configure wave -valuecolwidth 100
configure wave -justifyvalue left
configure wave -signalnamewidth 0
configure wave -snapdistance 10
configure wave -datasetprefix 0
configure wave -rowmargin 4
configure wave -childrowmargin 2
configure wave -gridoffset 0
configure wave -gridperiod 1
configure wave -griddelta 40
configure wave -timeline 0
configure wave -timelineunits ps
update
WaveRestoreZoom {0 ps} {877618280 ps}

/** Defines Timescale #1 as 10 ns **/
`timescale 10ns/1ps


module Top_level_tb;
import nBitsPackage::*;


/*
*************************************************************************************
* Logics definitions
*************************************************************************************
*/

logic clk_top=1'b0;
logic reset_top; 			//limpiar asincrono
logic RX_l;
logic TX_l;
logic Start;
logic [6:0] unidades_7seg_l;

data8_t data_result_l; 
data8_t received_data_l;

Top_level 
DUT
(
	.clk(clk_top),
	.reset_pb_in(reset_top),
	.serial_input_RX_in(TX_l),
	.data_result_l(data_result_l),
	.enable_TX_l(Start),
	
	.serial_output_TX_o(RX_l),
	
	.unidades_7seg(unidades_7seg_l)
	

		
);

/*********************************************************/
initial // Clock generator
  begin
    forever #1 clk_top = !clk_top;
  end
/*********************************************************/
initial begin // reset generator

/******************
* Multiply 
******************/

	
	#0 reset_top = 1'b0;
	
	#0 TX_l = 1'b1;	//IDLE  = 1
	
	#40 reset_top = 1'b1;

	
	#5 Start = 1'b1;
	
	#6 data_result_l = 8'b11010101;

//	/*8.68 us = 8680 ps*/
//	#20000 TX_l = 1'b1;	//IDLE  = 1
//	
//	#868 TX_l = 1'b0;	//STARTBIT = 0
//	
//	#868 TX_l = 1'b1;	//BIT0 = 1 // LSB
//	
//	#868 TX_l = 1'b0;	//BIT1 = 0
//	
//	#868 TX_l = 1'b1;	//BIT2 = 1
//	
//	#868 TX_l = 1'b0;	//BIT3 = 0
//	
//	#868 TX_l = 1'b1;	//BIT4 = 1
//	
//	#868 TX_l = 1'b0;	//BIT5 = 0
//	
//	#868 TX_l = 1'b1;	//BIT6 = 1
//	
//	#868 TX_l = 1'b0;	//BIT7 = 0 // MSB
//	
//	#868 TX_l = 1'b1;	// (even) PARITY = 0
//	
//	#868 TX_l = 1'b1;	//STOPBIT = 1
//	
//	#868 TX_l = 1'b1;	//IDLE = 1
//	
//	/*Registered Data = DEC 0 */
//	
//	#868 TX_l = 1'b0;	//STARTBIT = 0
//	
//	#868 TX_l = 1'b1;	//BIT0 = 1 // LSB
//	
//	#868 TX_l = 1'b0;	//BIT1 = 0
//	
//	#868 TX_l = 1'b0;	//BIT2 = 1
//	
//	#868 TX_l = 1'b0;	//BIT3 = 0
//	
//	#868 TX_l = 1'b1;	//BIT4 = 1
//	
//	#868 TX_l = 1'b1;	//BIT5 = 0
//	
//	#868 TX_l = 1'b0;	//BIT6 = 1
//	
//	#868 TX_l = 1'b0;	//BIT7 = 0 // MSB
//	
//	#868 TX_l = 1'b1;	// (even) PARITY = 0
//	
//	#868 TX_l = 1'b1;	//STOPBIT = 1
//	
//	#868 TX_l = 1'b1;	//IDLE = 1
	
	/*Registered Data = DEC 1 */
	


	
end

/*********************************************************/


endmodule


/*
*************************************************************************************
*	Coder:	OSCAR MAISTERRA FLORES	 		[ ie699416]
*	Coder: 	LUIS FERNANDO MORENO ALONSO	[ ie693314] 
*	Date : 	June 8th, 2019
*  Name :   Bin to Gray 
*
*	Description: 	 
*		 this code decodifies the data in binary to data with the logarith gray
* 
*************************************************************************************
*/


module bin2gray
import FIFO_pkg::*;
(
	input  valueN_t data_bin,

	output valueN_t data_gray
);


    generate 
        genvar i;
        for ( i = ZERO ; i < ( VALUE_B_G - TWO ) ; i = ( i + UNIT ))begin: bin2gray
            xclusive_xor XOR(
				.b_in(data_bin[i]),
				.a_in(data_bin[i + UNIT]),

				.xor_o(data_gray[i])
			);
        end: bin2gray
    endgenerate

    assign data_gray[VALUE_B_G - UNIT] = data_bin[VALUE_B_G - UNIT];


endmodule


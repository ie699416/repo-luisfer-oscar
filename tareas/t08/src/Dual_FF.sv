
/*
*************************************************************************************
*	Coder:	OSCAR MAISTERRA FLORES	 		[ ie699416]
*	Coder: 	LUIS FERNANDO MORENO ALONSO	[ ie693314] 
*	Date : 	June 8th, 2019
*  Name :   DFF
*
*	Description: 	 
*		 
* 
*************************************************************************************
*/

module Dual_FF
import FIFO_pkg::*;
(
	input  clk,
	input  rst,
	input  addrN_t data_in,

	output addrN_t data_out
);

addrN_t ffd;

always_ff@(posedge clk, negedge rst) 
begin

    if(rst == FALSE)
        ffd  <= ZERO;
    else 
        ffd  <= data_in;
end

assign data_out  = ffd;

endmodule




module gray2bin
import FIFO_pkg::*;
(
	input  valueN_t data_gray,

	output valueN_t data_bin
);


genvar i;
    generate  
        for (i = ( VALUE_B_G - TWO ) ; i > ZERO ; i = ( i - UNIT ))begin: gray2bin	  
		 
            xclusive_xor XOR(
				.b_in(data_gray[i]),
				.a_in(data_bin[i + UNIT]),

				.xor_o(data_bin[i])
			);
        end: gray2bin
		  
    endgenerate
	 
	 
assign data_bin[VALUE_B_G - UNIT] = data_gray[VALUE_B_G - UNIT];
	 

endmodule




module T08_FIFO
import FIFO_pkg::*;
(	
	input clk_wr,
	input clk_rd,
	input rst,

	input dataN_t data_in,
	input logic  push,
	input logic  pop,

	output dataN_t data_out,
	output logic empty_flag,
	output logic full_flag
);


FIFO_st fifo_wires;
logic Flag_wr;
logic Flag_rd;
dataN_t addr_wr_pt;
dataN_t addr_rd_pt;

Mem_RAM RAM(

	.clk_wr(clk_wr),   
	.clk_rd(clk_rd),   
	.wr_enable(fifo_wires.enable_ram_wr),    
	.rd_enable(fifo_wires.enable_ram_rd),    
	.data_wr(data_in),  
	.addr_wr({Flag_wr,addr_wr_pt}),  
	.addr_dr({Flag_rd,addr_rd_pt}),  

	.data_rd(data_out)  
);

Pointer_wr pn_wr
(	
	.clk_wr(clk_wr),
	.rst(rst),
	.push(push),
	.full_flag(full_flag),

	.WR_enable(fifo_wires.enable_ram_wr),
	.addr_wr(addr_wr_pt),
	.Flag_wr(Flag_wr),
	.error_flag()

);

Pointer_rd pn_rd
(	
	.clk_rd(clk_rd),
	.rst(rst),
	.pop(pop),
	.empty_flag(empty_flag),

	.RD_enable(fifo_wires.enable_ram_rd),
	.addr_rd(addr_rd_pt),
	.Flag_rd(Flag_rd),
	.error_flag()

);

sync_addr Full_module(
	.clk(clk_wr),
	.rst(rst),
	.addr_1({Flag_rd,addr_rd_pt}), 
	.addr_2({Flag_wr,addr_wr_pt}), 
	
	.flag_sync(full_flag)
);

sync_addr Empty_module(
	.clk(clk_rd),
	.rst(rst),
	.addr_1({ZERO,addr_wr_pt}), 
	.addr_2({ZERO,addr_rd_pt}),

	.flag_sync(empty_flag)
);


endmodule

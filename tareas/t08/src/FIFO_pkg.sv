
`ifndef MYPACKAGE_DONE
   `define MYPACKAGE_DONE
package FIFO_pkg;

    localparam  DATA_N  = 5;
    localparam  ADDR_N  = 6;
	 localparam  VALUE_B_G  = 7;
    localparam  DEPTH_N = 10;
	 
	 parameter TRUE = 1'b1;
	 parameter FALSE = 1'b0;
	 parameter UNIT = 1'b1;
	 parameter ZERO = 1'b0;
	 parameter TWO = 2'b10;
	 parameter ZERON = {6{1'b0}};


    typedef logic [DATA_N-1:0]  dataN_t;
    typedef logic [ADDR_N-1:0]  addrN_t;
	 typedef logic [VALUE_B_G-1:0]  valueN_t;

	 typedef struct{
	    logic  enable_ram_wr;
	    addrN_t addr_ram_wr;
	    ////////////////////////
	    logic  enable_ram_rd;
	    addrN_t addr_ram_rd;
	}FIFO_st;
     
	  
	typedef struct{
    	addrN_t addr_bin_sync;
    	addrN_t addr_gray_sync;
    	addrN_t addr_gray_comp;
    }addr_sync;



endpackage

`endif

module sync_DFF
import FIFO_pkg::*; 
(
	input clk, 
	input rst,
	input addrN_t data_in, 
	
	output addrN_t data_out
);

addrN_t data_w;

Dual_FF FF_D_1(

	.clk(clk),
	.rst(rst),
	.data_in(data_in),

	.data_out(data_w)
);


Dual_FF FF_D_2(

	.clk(clk),
	.rst(rst),
	.data_in(data_w),

	.data_out(data_out)
);


endmodule


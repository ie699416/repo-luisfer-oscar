

module xclusive_conditional
import FIFO_pkg::*;
(
	input addrN_t  data_in_1,
	input addrN_t  data_in_2,

	output logic  flag_out
);

assign flag_out = ( data_in_1 == data_in_2 )? UNIT : ZERO;

endmodule



module sync_addr
import FIFO_pkg::*;
(
	input logic clk,
	input logic rst,

	input addrN_t addr_1,
	input addrN_t addr_2,

	output logic flag_sync
);

//addr_sync wires_struct;

valueN_t addr_bin_sync;
addrN_t addr_gray_sync;
valueN_t addr_gray_comp;


bin2gray bin_gray(

	.data_bin({ZERO,addr_1}), 

	.data_gray(addr_bin_sync) 
);

sync_DFF DFF(

	.clk(clk), 
	.rst(rst),
	.data_in(addr_bin_sync[ADDR_N-UNIT:0]), 

	.data_out(addr_gray_sync) 
);

gray2bin gray_bin(

    .data_gray({addr_gray_sync,ZERO}), 

    .data_bin(addr_gray_comp) 
);

xclusive_conditional conditional(
	.data_in_1(addr_gray_comp[ADDR_N:UNIT]), 
	.data_in_2(addr_2), 

	.flag_out(flag_sync)
);

endmodule





module Pointer_rd
import FIFO_pkg::*;
(	
	input clk_rd,
	input rst,
	input         pop,
	input logic   empty_flag,

	output logic  RD_enable,
	output dataN_t addr_rd,
	output logic Flag_rd,
	output logic error_flag

);

addrN_t  queue = ZERON; //= ADDR_N - UNIT;

always_ff @(posedge clk_rd or negedge rst ) begin 

	if (rst == FALSE)begin
		RD_enable    <= ZERO;
		error_flag <= ZERO; 
	end
	else
		begin
			if (empty_flag == TRUE) 
			
					error_flag <= TRUE;

			else if(pop == TRUE) 
				begin 
					error_flag <= ZERO;
					RD_enable    <= TRUE;
					queue    <= (queue + UNIT);
				end

			else 
				begin
					RD_enable    <= ZERO;
					error_flag <= ZERO;	
				end
		end
		
end

assign addr_rd = queue [4:0];
assign Flag_rd = queue[ADDR_N - UNIT];

endmodule
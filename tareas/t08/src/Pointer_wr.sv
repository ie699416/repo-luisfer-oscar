



module Pointer_wr
import FIFO_pkg::*;
(	
	input clk_wr,
	input rst,

	//input DATA_N data_addr,
	input         push,
	input logic   full_flag,

	output logic  WR_enable,
	output dataN_t addr_wr,
	output logic Flag_wr,
	output logic error_flag

);

addrN_t  queue = ZERON; //= ADDR_N - UNIT;

always_ff @(posedge clk_wr or negedge rst ) begin 

	if (rst == FALSE)begin
		WR_enable   <= ZERO;
		error_flag <= ZERO; 
	end
	else
		begin
			if (full_flag == TRUE) 
			
					error_flag <= TRUE;

			else if(push == TRUE) 
				begin 
					error_flag <= ZERO;
					WR_enable    <= TRUE;
					queue    <= (queue + UNIT);
				end

			else 
				begin
					WR_enable    <= ZERO;
					error_flag <= ZERO;	
				end
		end

	
end

assign addr_wr = queue [4:0];
assign Flag_wr = queue[ADDR_N - UNIT];

endmodule

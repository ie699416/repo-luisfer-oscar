onerror {resume}
quietly WaveActivateNextPane {} 0
add wave -noupdate /TB_fifo/clk_wr
add wave -noupdate /TB_fifo/clk_rd
add wave -noupdate /TB_fifo/rst
add wave -noupdate /TB_fifo/data_in
add wave -noupdate /TB_fifo/push
add wave -noupdate /TB_fifo/pop
add wave -noupdate /TB_fifo/data_out
add wave -noupdate /TB_fifo/empty
add wave -noupdate /TB_fifo/full
add wave -noupdate /TB_fifo/TB_t08/RAM/clk_wr
add wave -noupdate /TB_fifo/TB_t08/RAM/clk_rd
add wave -noupdate /TB_fifo/TB_t08/RAM/wr_enable
add wave -noupdate /TB_fifo/TB_t08/RAM/rd_enable
add wave -noupdate /TB_fifo/TB_t08/RAM/data_wr
add wave -noupdate /TB_fifo/TB_t08/RAM/addr_wr
add wave -noupdate /TB_fifo/TB_t08/RAM/addr_dr
add wave -noupdate /TB_fifo/TB_t08/RAM/data_rd
add wave -noupdate /TB_fifo/TB_t08/Full_module/addr_1
add wave -noupdate /TB_fifo/TB_t08/Full_module/addr_2
add wave -noupdate /TB_fifo/TB_t08/Full_module/flag_sync
add wave -noupdate /TB_fifo/TB_t08/Empty_module/clk
add wave -noupdate /TB_fifo/TB_t08/Empty_module/rst
add wave -noupdate /TB_fifo/TB_t08/Empty_module/addr_1
add wave -noupdate /TB_fifo/TB_t08/Empty_module/addr_2
add wave -noupdate /TB_fifo/TB_t08/Empty_module/flag_sync
add wave -noupdate /TB_fifo/TB_t08/Full_module/conditional/data_in_1
add wave -noupdate /TB_fifo/TB_t08/Full_module/conditional/data_in_2
add wave -noupdate /TB_fifo/TB_t08/Full_module/conditional/flag_out
add wave -noupdate /TB_fifo/TB_t08/Full_module/bin_gray/data_bin
add wave -noupdate /TB_fifo/TB_t08/Full_module/bin_gray/data_gray
add wave -noupdate /TB_fifo/TB_t08/Full_module/DFF/clk
add wave -noupdate /TB_fifo/TB_t08/Full_module/DFF/rst
add wave -noupdate /TB_fifo/TB_t08/Full_module/DFF/data_in
add wave -noupdate /TB_fifo/TB_t08/Full_module/DFF/data_out
add wave -noupdate /TB_fifo/TB_t08/Full_module/DFF/data_w
add wave -noupdate /TB_fifo/TB_t08/Full_module/gray_bin/data_gray
add wave -noupdate /TB_fifo/TB_t08/Full_module/gray_bin/data_bin
TreeUpdate [SetDefaultTree]
WaveRestoreCursors {{Cursor 1} {8 ps} 0}
quietly wave cursor active 1
configure wave -namecolwidth 106
configure wave -valuecolwidth 50
configure wave -justifyvalue left
configure wave -signalnamewidth 1
configure wave -snapdistance 10
configure wave -datasetprefix 0
configure wave -rowmargin 4
configure wave -childrowmargin 2
configure wave -gridoffset 0
configure wave -gridperiod 1
configure wave -griddelta 40
configure wave -timeline 0
configure wave -timelineunits ps
update
WaveRestoreZoom {0 ps} {124 ps}

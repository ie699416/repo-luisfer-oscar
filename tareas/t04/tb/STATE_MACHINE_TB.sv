/*
*************************************************************************************
*	Coder:	OSCAR MAISTERRA FLORES	 		[ ie699416]
*	Coder: 	LUIS FERNANDO MORENO ALONzO	[ ie693314] 
*	Date:          Jun 4th, 2019	
*  Name:          STATE_MACHINE_TB.sv      
*************************************************************************************
*/
//`timescale 1ps/1ps 	/*5MHZ*/
module STATE_MACHINE_TB;

 // Input Ports
logic clk_FPGA = 0;
logic reset;
logic start=0;
	
  // Output Ports
logic LED_o;
logic clk_1Hz;


Maquina_de_estado
DUT
(
	// Input Ports
	.clk_FPGA(clk_FPGA),
	.reset(reset),
	.start(start),
	
	// Output Ports
	.LED_o(LED_o), 
	.clk_1Hz(clk_1Hz)

);	

/*********************************************************/
initial // Clock generator
  begin
    forever #1 clk_FPGA = !clk_FPGA;
  end
/*********************************************************/
initial begin // reset generator
	#20 reset = 0;
	#20 reset = 1;


	#20 start = 0;
	
	#0 start = 1;
	/*Pulse 1 ts*/
	#20 start = 0;
	
	
	
	
	
	#1000 start = 1;
	/*Pulse 1 ts*/
	#20 start = 0;
end


endmodule 
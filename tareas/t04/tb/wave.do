onerror {resume}
quietly WaveActivateNextPane {} 0
add wave -noupdate /STATE_MACHINE_TB/clk_FPGA
add wave -noupdate /STATE_MACHINE_TB/clk_1Hz
add wave -noupdate /STATE_MACHINE_TB/reset
add wave -noupdate /STATE_MACHINE_TB/start
add wave -noupdate /STATE_MACHINE_TB/DUT/mean_machine/LED_o
add wave -noupdate /STATE_MACHINE_TB/DUT/mean_machine/readyFlag_o
add wave -noupdate /STATE_MACHINE_TB/DUT/mean_machine/state
add wave -noupdate /STATE_MACHINE_TB/DUT/countlogic/counterFlag_o
add wave -noupdate -radix decimal -childformat {{{/STATE_MACHINE_TB/DUT/countlogic/Count_logic[6]} -radix decimal} {{/STATE_MACHINE_TB/DUT/countlogic/Count_logic[5]} -radix decimal} {{/STATE_MACHINE_TB/DUT/countlogic/Count_logic[4]} -radix decimal} {{/STATE_MACHINE_TB/DUT/countlogic/Count_logic[3]} -radix decimal} {{/STATE_MACHINE_TB/DUT/countlogic/Count_logic[2]} -radix decimal} {{/STATE_MACHINE_TB/DUT/countlogic/Count_logic[1]} -radix decimal} {{/STATE_MACHINE_TB/DUT/countlogic/Count_logic[0]} -radix decimal}} -subitemconfig {{/STATE_MACHINE_TB/DUT/countlogic/Count_logic[6]} {-height 15 -radix decimal} {/STATE_MACHINE_TB/DUT/countlogic/Count_logic[5]} {-height 15 -radix decimal} {/STATE_MACHINE_TB/DUT/countlogic/Count_logic[4]} {-height 15 -radix decimal} {/STATE_MACHINE_TB/DUT/countlogic/Count_logic[3]} {-height 15 -radix decimal} {/STATE_MACHINE_TB/DUT/countlogic/Count_logic[2]} {-height 15 -radix decimal} {/STATE_MACHINE_TB/DUT/countlogic/Count_logic[1]} {-height 15 -radix decimal} {/STATE_MACHINE_TB/DUT/countlogic/Count_logic[0]} {-height 15 -radix decimal}} /STATE_MACHINE_TB/DUT/countlogic/Count_logic
add wave -noupdate /STATE_MACHINE_TB/DUT/countlogic/machineEnFlag_o
add wave -noupdate /STATE_MACHINE_TB/DUT/countlogic/cycleComparator_l
add wave -noupdate -radix unsigned /STATE_MACHINE_TB/DUT/countlogic/machineCount_logic
TreeUpdate [SetDefaultTree]
WaveRestoreCursors {{Cursor 1} {2000 ps} 0}
quietly wave cursor active 1
configure wave -namecolwidth 437
configure wave -valuecolwidth 100
configure wave -justifyvalue left
configure wave -signalnamewidth 0
configure wave -snapdistance 10
configure wave -datasetprefix 0
configure wave -rowmargin 4
configure wave -childrowmargin 2
configure wave -gridoffset 0
configure wave -gridperiod 1
configure wave -griddelta 40
configure wave -timeline 0
configure wave -timelineunits ps
update
WaveRestoreZoom {0 ps} {2688 ps}

/*
*************************************************************************************
*	Coder:	OSCAR MAISTERRA FLORES	 		[ ie699416]
*	Coder: 	LUIS FERNANDO MORENO ALONzO	[ ie693314] 
*	Date:          Jun 4th, 2019	
*  Name:          Counter_SM.sv      
*************************************************************************************
*/

module Counter_SM
import nBitsPackage::*;
(
	// Input Ports
	input clk,
	input reset,
	input start,
	input readyFlag_in,	
	// Output Ports
	output logic counterFlag_o,
	output logic machineEnFlag_o
);


logic cycleComparator_l;
dataCountFSM_t machineCount_logic; 
dataCountFSM_t Count_logic;


always_ff@(posedge clk or negedge reset)
begin
	if (reset == FALSE)
		begin
			Count_logic <= ZERO_COUNT_FSM; 
		end
	else
		begin
			if (readyFlag_in == FALSE)
				begin			
					if(Count_logic == MAXIMUM_VALUE)
						Count_logic <= ONE;
					else
						begin
							Count_logic <= Count_logic + UNIT;			
						end
				end
			else
				begin		
					Count_logic <= ZERO_COUNT_FSM;			
				end
		end
end

//--------------------------------------------------------------------------------------------

always_comb
begin
	if (Count_logic > SIX && Count_logic <= MAXIMUM_VALUE )
		counterFlag_o = LOW;
	else if (Count_logic == ZERO_COUNT_FSM )
		counterFlag_o = LOW;
	else
		counterFlag_o = HIGH;
end
		
//---------------------------------------------------------------------------------------------




always_ff@(posedge clk or negedge reset)
begin
	if (reset == FALSE)
		begin
			machineCount_logic <= ZERO_COUNT_FSM; 
		end
	else
		begin
			if (start == TRUE)
				begin	
					machineCount_logic <= ZERO_COUNT_FSM;
				end
			else
				begin					
					if (cycleComparator_l == TRUE)
							begin			
								if(machineCount_logic == NCYCLES)
									machineCount_logic <= machineCount_logic;
								else
									begin
										machineCount_logic <= machineCount_logic + UNIT;			
									end
							end
						else
							begin		
								machineCount_logic <= machineCount_logic;			
							end						
				end
		end
end

//--------------------------------------------------------------------------------------------

always_comb
begin
	if (Count_logic == MAXIMUM_VALUE )
		cycleComparator_l = HIGH;
	else
		cycleComparator_l = LOW;
end
		
//---------------------------------------------------------------------------------------------


//--------------------------------------------------------------------------------------------

always_comb
begin
	if (machineCount_logic == NCYCLES )
		machineEnFlag_o = LOW;
	else
		machineEnFlag_o = HIGH;
end
		
//---------------------------------------------------------------------------------------------






endmodule

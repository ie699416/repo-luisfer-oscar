/*
*************************************************************************************
*	Coder:	OSCAR MAISTERRA FLORES	 		[ ie699416]
*	Coder: 	LUIS FERNANDO MORENO ALONzO	[ ie693314] 
*	Date:          Jun 4th, 2019	
*  Name:          CLK_GEN.sv      
*************************************************************************************
*/
module Maquina_de_estado
import nBitsPackage::*;
(
  //input ports
  input clk_FPGA,
  input reset,
  input start,
  
  //output ports
  output logic LED_o, // PIN_H15
  output logic clk_1Hz
  
);

//wire assigment
logic newCLK_l;
logic readyFlag_l;
logic counterFlag_l;
logic machineEnFlag_l;

CLK_GEN Clock_Generator_1KH
(
	.clk_FPGA(clk_FPGA),
	.reset(reset),
	.clk_salida(newCLK_l)
);

StateMachine mean_machine
(
	
	 .clk(newCLK_l),
	 .reset(reset),
	 .Start(start),
	 .counterFlag_in(counterFlag_l),
	 .machine_en(machineEnFlag_l),
	 .readyFlag_o(readyFlag_l),
	 .LED_o(LED_o)
	
);

Counter_SM countlogic
(
	// Input Ports
	.clk(newCLK_l),
	.reset(reset),
	.start(start),
	.readyFlag_in(readyFlag_l),	
	.counterFlag_o(counterFlag_l),
	.machineEnFlag_o(machineEnFlag_l)
	
	
);

assign clk_1Hz = newCLK_l;

endmodule 
/*
*************************************************************************************
*	Coder:	OSCAR MAISTERRA FLORES	 		[ ie699416]
*	Coder: 	LUIS FERNANDO MORENO ALONzO	[ ie693314] 
*	Date:          Jun 4th, 2019	
*  Name:          CLK_GEN.sv      
*************************************************************************************
*/


module StateMachine
import nBitsPackage::*;
(
	// Input Ports
	input clk,
	input reset,
	input Start,
	input counterFlag_in,
	input machine_en,
	

	// Output Ports
	output logic LED_o,
	output logic readyFlag_o
	
);


 

//initial state = STATE_READY;

state_en state;

always_ff@(posedge clk or negedge reset) 
	begin
		if (reset == FALSE)
			begin
				//STATE_READY
				state <= STATE_READY;				
			end 
		else 
			begin
				if(machine_en == TRUE || Start == TRUE )			   	/*External Flag for three iterations*/
					begin
						case(state)	
							STATE_READY:						
								begin
									if(Start == TRUE)
										state <= STATE_WAIT_COUNTERFLAG;
									else
										state <= STATE_READY;
								end								
							STATE_WAIT_COUNTERFLAG:						
								begin
									if(counterFlag_in == TRUE)
										state <= STATE_HIGH;
									else
										state <= STATE_WAIT_COUNTERFLAG;
								end
							STATE_HIGH:
								begin
									if(counterFlag_in == TRUE) 			/*External flag turns to LOW when count value = 6*/
											state <= STATE_HIGH;
									else
											state <= STATE_LOW;			
								end		
							STATE_LOW:
								begin
									if (counterFlag_in == FALSE) 			/*External Counter turns to HIGH when count value = 10*/
											state <= STATE_LOW;				
									else 
											state <= STATE_HIGH;
								end					
							default:
											state <= STATE_READY;
						endcase
					end
				else
					begin
						state <= STATE_READY;
					end
			end	
	end 


		
always_comb 
begin
 case(state)
		STATE_READY: 			
				begin
					LED_o = LOW;
					readyFlag_o = HIGH;
				end
		STATE_WAIT_COUNTERFLAG: 			
				begin
					LED_o = LOW;
					readyFlag_o = LOW;
				end 
		STATE_HIGH: 
				begin
					LED_o = HIGH;					
					readyFlag_o = LOW;
				end
				
		STATE_LOW: 
				begin
					LED_o = LOW;
					readyFlag_o = LOW;
				end		
		default: 
				begin
					LED_o = LOW; 
					readyFlag_o = LOW;
				end
	endcase
end 






endmodule

/*
*************************************************************************************
*	Coder:	OSCAR MAISTERRA FLORES	 		[ ie699416]
*	Coder: 	LUIS FERNANDO MORENO ALONzO	[ ie693314] 
*	Date:          Jun 4th, 2019	
*  Name:          CLK_GEN.sv      
*************************************************************************************
*/

module CLK_GEN 
import nBitsPackage::*;
(
	
	// Input Ports
	input clk_FPGA,
	input reset,
	// Output Ports
	output clk_salida
	
);

dataCount_t Count_logic;

logic MaxValue_Bit = LOW;
logic clk_output = LOW;


always_ff@(posedge clk_FPGA or negedge reset) 
	begin
		if (reset == FALSE)
			begin
				Count_logic <= ZERO_COUNT;
				clk_output <= LOW;					
			end
		else 
			begin
				if(MaxValue_Bit == HIGH)
					begin
						clk_output <= ~clk_output;
						Count_logic <= ZERO_COUNT;
					end
				else 
					Count_logic <= Count_logic + UNIT;		
			end
	end
	
always@(*)
	begin
		if(Count_logic == DELAY_COUNT_VALUE - UNIT)
			MaxValue_Bit = HIGH;
		else
			MaxValue_Bit = LOW;
	end

assign clk_salida = clk_output;



endmodule 




 
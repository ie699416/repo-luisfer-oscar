/*
*************************************************************************************
*	Coder:	OSCAR MAISTERRA FLORES	 		[ ie699416]
*	Coder: 	LUIS FERNANDO MORENO ALONSO	[ ie693314] 
*	Date : 	June 11th, 2019
*  Name :   Register_SISO.sv  
*
* 
*************************************************************************************
*/

module SISO
import nBitsPackage::*;
(
	// inputs 
	input clk,
	input rst,	
	input Enable,
	input logic Selector,
	input logic D1_in,		    	
	// outputs 
	output dataN_t Q1_o					
);

dataN_t Register ;


always_ff@(posedge clk, negedge rst) 
	begin: bit_register
	
		if (!rst)
			Register <= D1_in;
				
		else if(Enable == TRUE)
			begin
				if(Selector == TRUE)
					Register = {Register[N-1:0],D1_in};
				
				else
					Register = {D1_in,Register[N-1:0]};
			end
	end:  bit_register

assign Q1_o = Register;
	
endmodule






module Global_Register
import nBitsPackage::*;
(
	// inputs 
	input clk,
	input rst,	
	input Enable,
	input data2_t Selector,
	input dataN_t D1_in,		    		
	// outputs 
	output dataN_t Q1_o					
);

dataN_t Register ;



always_ff@(posedge clk or negedge rst) begin: Register_label
    if(!rst)
        Register  <= '0;
		  
    else if (Enable) begin
	 
        if (Selector == ONE)
            Register  <= D1_in[0];
				
        else if(Selector == TWO)
			
				Register  <= D1_in[N-1];
			
		  else if(Selector == TWO)
		  
           Register  <= {Register[N-1],Register[N-2:0]};
			  
		  else
				
			  Register  <= {Register[N-2:0],Register[N-1]};	
		
    end
	 
end:Register_label

assign Q1_o  = Register;
	
endmodule

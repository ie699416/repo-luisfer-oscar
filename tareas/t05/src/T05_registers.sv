/*
*************************************************************************************
*	Coder:	OSCAR MAISTERRA FLORES	 		[ ie699416]
*	Coder: 	LUIS FERNANDO MORENO ALONSO	[ ie693314] 
*	Date : 	June 11th, 2019
*  Name :   Top_register.sv  
*
* 
*************************************************************************************
*/

module T05_Register
import nBitsPackage::*;
(
	// inputs 
	input clk_l,
	input rst_l,	
	input Enable_l,
	input data2_t Selector_in,
	input dataN_t D1_in_l,		    	
	// outputs 
	output dataN_t Q1_o_l,
	output dataN_t Q2_o_l,	
	output dataN_t Q3_o_l,	
	output dataN_t Q4_o_l,
	output dataN_t Q5_o_l		
);



Global_Register Global_reg
(
	// inputs 
	.clk(clk_l),
	.rst(rst_l),	
	.Enable(Enable_l),
	.Selector(Selector_in),
	.D1_in(D1_in_l),		    	
	// outputs 
	.Q1_o(Q1_o_l)						
);

PIPO PIPO_reg
(
	// inputs 
	.clk(clk_l),
	.rst(rst_l),	
	.Selector(Selector_in),
	.D1_in(D1_in_l),		    		
	// outputs 
	.Q1_o(Q2_o_l)						
	
);

SISO SISO_reg
(
	// inputs 
	.clk(clk_l),
	.rst(rst_l),	
	.Enable(Enable_l),
	.Selector(Selector_in),
	.D1_in(D1_in_l),		    		
	// outputs 
	.Q1_o(Q3_o_l)						
);

PISO PISO_reg
(
	// inputs 
	.clk(clk_l),
	.rst(rst_l),	
	.enable(Enable_l),
	.Selector(Selector_in),
	.D1_in(D1_in_l),		    		
	// outputs 
	 .Q1_o(Q4_o_l)							
);



SIPO SIPO_reg
 (
		// inputs 
		.clk(clk_l),    
		.rst(rst_l),   
		.enable(Enable_l),   
		.Selector(Selector_in),    
		.D1_in(D1_in_l),    
		// outputs 
		.Q1_o(Q5_o_l)     
);


endmodule
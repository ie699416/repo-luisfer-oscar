/*
*************************************************************************************
*	Coder:	OSCAR MAISTERRA FLORES	 		[ ie699416]
*	Coder: 	LUIS FERNANDO MORENO ALONSO	[ ie693314] 
*	Date : 	June 11th, 2019
*  Name :   Register_PISO.sv  
*
* 
*************************************************************************************
*/

module PISO
import nBitsPackage::*;
(
	// inputs 
	input clk,
	input rst,	
	input enable,
	input data2_t Selector,
	input dataN_t D1_in,		   
	// outputs 
	output logic Q1_o						
);

dataN_t Register;

always_ff@(posedge clk or negedge rst) begin: Register_label
    if(!rst)
        Register  <= '0;
    else if (enable) begin
        if (Selector == ONE)
            Register  <= D1_in;
        else if(Selector == TWO)
			Register  <= {Register[N-1],Register[N-2:0]};
		  else
            Register  <= {Register[N-2:0], Register[N-1]};
    end
	 
end:Register_label

assign Q1_o  = Register;
	
endmodule



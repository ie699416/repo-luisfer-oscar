
/*
*************************************************************************************
*	Coder:	OSCAR MAISTERRA FLORES	 		[ ie699416]
*	Coder: 	LUIS FERNANDO MORENO ALONSO	[ ie693314] 
*	Date : 	June 11th, 2019
*  Name :   Register_SIPO.sv  
*
*************************************************************************************
*/
module SIPO
import nBitsPackage::*;
 (
input            clk,    
input            rst,   
input            enable,   
input            Selector,    
input    		  D1_in,    
output dataN_t   Q1_o     
);


dataN_t Register ;

always_ff@(posedge clk or negedge rst) begin: rgstr_label
    if(!rst)
        Register  <= '0;
    else if (enable)begin
		if (Selector == TRUE)
        Register  <= {Register[N-2:0], D1_in};
		else 
		  Register  <= {D1_in, Register[N-2:0]};
		  end
end:rgstr_label

assign Q1_o  = Register;

	
endmodule
/*
*************************************************************************************
*	Coder:	OSCAR MAISTERRA FLORES	 		[ ie699416]
*	Coder: 	LUIS FERNANDO MORENO ALONSO	[ ie693314] 
*	Date : 	June 8th, 2019
*  Name :   bit_register.sv  
*
*	Description: 	 
*		 This module operates the FF_D for one bit
* 
*************************************************************************************
*/

module Register_PIPO
import nBitsPackage::*;

(
	// inputs 
	input clk,
	input rst,								
	input dataN_t D1_in,		    		// One bit reg
	input dataN_t D2_in,
	// outputs 
	output dataN_t Q1_o,					// One bit reg	
	output dataN_t Q2_o	
	
);

always_ff@(posedge clk, negedge rst) 
	begin: bit_register
		if (rst == FALSE)
			begin
				Q1_o <= CERO;
				Q2_o <= CERO;
			end
		else 
			begin
				Q1_o <= D1_in;
				Q2_o <= D2_in;
			end
			
	end:  bit_register

endmodule



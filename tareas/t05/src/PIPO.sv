/*
*************************************************************************************
*	Coder:	OSCAR MAISTERRA FLORES	 		[ ie699416]
*	Coder: 	LUIS FERNANDO MORENO ALONSO	[ ie693314] 
*	Date : 	June 11th, 2019
*  Name :   Pipo_register.sv  
*
* 
*************************************************************************************
*/

module PIPO
import nBitsPackage::*;

(
	// inputs 
	input clk,
	input rst,	
	input logic Selector,
	input dataN_t D1_in,		    		
	// outputs 
	output dataN_t Q1_o					
	
);

dataN_t Register_1 ;

always_ff@(posedge clk, negedge rst) 
	begin: bit_register
		if (rst == FALSE)
				Q1_o <= CERO;

		else if(Selector == TRUE) 
			begin
				Register_1 <= D1_in;
			
			end
			
	end:  bit_register

	assign  Q1_o = Register_1;
	
endmodule



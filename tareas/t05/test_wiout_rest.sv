module PIPO
import nBitsPackage::*;

(
	// inputs 
	input clk,
	input rst,	
	input logic Selector
	input dataN_t D1_in,		    		// One bit reg
	// outputs 
	output dataN_t Q1_o,					// One bit reg	
	
);

dataN_t Register_1 ;


always_ff@(posedge clk, negedge rst) 
	begin: bit_register

		else if(Selector == TRUE) 
			begin
				Register_1 <= D1_in;
			
			end
			
	end:  bit_register

	assign  Q1_o = Register_1;
	
endmodule


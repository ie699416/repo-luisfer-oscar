/*
*************************************************************************************
*	Coder:	OSCAR MAISTERRA FLORES	 		[ ie699416]
*	Coder: 	LUIS FERNANDO MORENO ALONzO	[ ie693314] 
*	Date:          Jun 12th, 2019	
*  Name:          Register_TB.sv      
*************************************************************************************
*/

module Register_TB;

 // Input Ports
reg clk_FPGA = 0;
reg RST;
reg start=0;
reg Selec=0;
reg data  = 4'b1101;
	
  // Output Ports
wire out_1;
wire out_2;
wire out_3;
wire out_4;
wire out_5;


T05_Register Top_test
(
	// inputs 
	.clk_l(clk_FPGA),
	.rst_l(RST),	
	.Enable_l(start),
	.Selector_in(Selec),
	.D1_in_l(data),		    	
	// outputs 
	.Q1_o_l(out_1),
	.Q2_o_l(out_2),	
	.Q3_o_l(out_3),	
	.Q4_o_l(out_4),
	.Q5_o_l(out_5)		
);

/*********************************************************/
initial // Clock generator
  begin
    forever #2 clk_FPGA = !clk_FPGA;
  end
/*********************************************************/
initial begin // reset generator
	#0 RST = 0;
	#5 RST = 1;
end

/*********************************************************/
initial begin // enable

	#5 start = 1;
	#5 Selec = 1;	
	
end

endmodule
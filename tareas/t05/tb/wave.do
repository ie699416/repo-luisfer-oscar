onerror {resume}
quietly WaveActivateNextPane {} 0
add wave -noupdate /STATE_MACHINE_TB/clk_FPGA
add wave -noupdate /STATE_MACHINE_TB/RST
add wave -noupdate /STATE_MACHINE_TB/start
add wave -noupdate /STATE_MACHINE_TB/out
add wave -noupdate /STATE_MACHINE_TB/clk_1Hz
add wave -noupdate /STATE_MACHINE_TB/DUT/mean_machine/Start
add wave -noupdate /STATE_MACHINE_TB/DUT/mean_machine/state
add wave -noupdate /STATE_MACHINE_TB/DUT/mean_machine/Count_logic
TreeUpdate [SetDefaultTree]
WaveRestoreCursors {{Cursor 1} {998222 ps} 0}
quietly wave cursor active 1
configure wave -namecolwidth 330
configure wave -valuecolwidth 100
configure wave -justifyvalue left
configure wave -signalnamewidth 0
configure wave -snapdistance 10
configure wave -datasetprefix 0
configure wave -rowmargin 4
configure wave -childrowmargin 2
configure wave -gridoffset 0
configure wave -gridperiod 1
configure wave -griddelta 40
configure wave -timeline 0
configure wave -timelineunits ps
update
WaveRestoreZoom {998063 ps} {999781 ps}

onerror {resume}
quietly WaveActivateNextPane {} 0
add wave -noupdate -radix decimal /tb_P02_borrador/P02_test/Segments/Switches_in
add wave -noupdate -radix decimal /tb_P02_borrador/P02_test/Segments/result_in
add wave -noupdate /tb_P02_borrador/P02_test/Segments/ready_in
add wave -noupdate /tb_P02_borrador/P02_test/Segments/Display_1
add wave -noupdate /tb_P02_borrador/P02_test/Segments/Display_2
add wave -noupdate /tb_P02_borrador/P02_test/Segments/Display_3
add wave -noupdate /tb_P02_borrador/P02_test/Segments/Display_4
add wave -noupdate /tb_P02_borrador/P02_test/Segments/Display_5
add wave -noupdate /tb_P02_borrador/P02_test/Segments/Display_6
add wave -noupdate /tb_P02_borrador/P02_test/Segments/Display_7
add wave -noupdate /tb_P02_borrador/P02_test/Segments/Display_8
add wave -noupdate /tb_P02_borrador/P02_test/Start_in
add wave -noupdate /tb_P02_borrador/P02_test/clk_FPGA
add wave -noupdate /tb_P02_borrador/P02_test/CU/clk
add wave -noupdate /tb_P02_borrador/P02_test/CU/rst
add wave -noupdate /tb_P02_borrador/P02_test/CU/Start
add wave -noupdate /tb_P02_borrador/P02_test/CU/Flag_counter
add wave -noupdate /tb_P02_borrador/P02_test/CU/ASR_flag
add wave -noupdate /tb_P02_borrador/P02_test/CU/A_flag
add wave -noupdate /tb_P02_borrador/P02_test/CU/Q_flag
add wave -noupdate /tb_P02_borrador/P02_test/CU/Q0_flag
add wave -noupdate /tb_P02_borrador/P02_test/CU/Mtr_selec_in
add wave -noupdate /tb_P02_borrador/P02_test/CU/A_reg_selec_in
add wave -noupdate /tb_P02_borrador/P02_test/CU/Rest_flag
add wave -noupdate /tb_P02_borrador/P02_test/CU/Ready_flag
add wave -noupdate /tb_P02_borrador/P02_test/CU/Enable_count
add wave -noupdate /tb_P02_borrador/P02_test/CU/Actual_state
add wave -noupdate /tb_P02_borrador/P02_test/CC/Select_op
add wave -noupdate /tb_P02_borrador/P02_test/CC/flag
add wave -noupdate /tb_P02_borrador/P02_test/CC/MaxValue_Bit
add wave -noupdate /tb_P02_borrador/P02_test/CC/Count_logic
add wave -noupdate /tb_P02_borrador/P02_test/mul/Result_H_o
add wave -noupdate /tb_P02_borrador/P02_test/mul/Result_L_o
add wave -noupdate /tb_P02_borrador/P02_test/adder/A_fromAdder_o
TreeUpdate [SetDefaultTree]
WaveRestoreCursors {{Cursor 1} {78531 ps} 0}
quietly wave cursor active 1
configure wave -namecolwidth 389
configure wave -valuecolwidth 100
configure wave -justifyvalue left
configure wave -signalnamewidth 0
configure wave -snapdistance 10
configure wave -datasetprefix 0
configure wave -rowmargin 4
configure wave -childrowmargin 2
configure wave -gridoffset 0
configure wave -gridperiod 1
configure wave -griddelta 40
configure wave -timeline 0
configure wave -timelineunits ps
update
WaveRestoreZoom {121020 ps} {414684 ps}

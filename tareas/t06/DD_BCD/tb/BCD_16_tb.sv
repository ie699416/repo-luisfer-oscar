/*
*************************************************************************************
*	Coder:	OSCAR MAISTERRA FLORES	 		[ ie699416]
*	Coder: 	LUIS FERNANDO MORENO ALONSO	[ ie693314] 
*	Date : 	June 16th, 2019
* 	Name :   boothMultiplication_tb.sv  
*
*	Description: TB file 
*					 
*************************************************************************************
*/

`timescale 1ns/1ps

module BCD_16_tb;
import nBitsPackage::*;


/*
*************************************************************************************
* Interconneciton wires/logic
*************************************************************************************
*/

logic clk_top = HIGH;

data16_t A_l;
dataSeg_en Sign_l;
dataSeg_en Uni_Mill_l;
dataSeg_en Dec_Mill_l;	
dataSeg_en Cen_l;
dataSeg_en Dec_l;
dataSeg_en Uni_l;

/*
*************************************************************************************
* Instances definition  
*************************************************************************************
*/

BCD_16 TB
(
	.A_in(A_l),
	.Sign_o(Sign_l),	
	.Uni_Mill_o(Uni_Mill_l),
	.Dec_Mill_o(Dec_Mill_l),
	.Cen_o(Cen_l),
	.Dec_o(Dec_l),
	.Uni_o(Uni_l)
);

/*********************************************************/
initial // Clock generator
  begin
    forever #1 clk_top = !clk_top;
  end
/*********************************************************/
initial begin // reset generator

#0 A_l = 1;


#4 A_l = 15689;


#4 A_l = 2338;


#4 A_l = -12598;
	
end

/*********************************************************/


endmodule
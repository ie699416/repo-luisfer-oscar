`timescale 1ns/1ps
/*
*************************************************************************************
*	Coder:	OSCAR MAISTERRA FLORES	 		[ ie699416]
*	Coder: 	LUIS FERNANDO MORENO ALONSO	[ ie693314] 
*	Date : 	June 16th, 2019
*  Name :   boothMultiplication_tb.sv  
*
*	Description: TB file 
*					 
*************************************************************************************
*/

module BCD_8_tb;
import nBitsPackage::*;


/*
*************************************************************************************
* Interconneciton wires/logic
*************************************************************************************
*/

logic clk_top = HIGH;

data8_t A_l = 22;
dataSeg_en Sign_l;	
dataSeg_en Cen_l;
dataSeg_en Dec_l;
dataSeg_en Uni_l;

/*
*************************************************************************************
* Instances definition  
*************************************************************************************
*/

BCD_8 TB
(
	.A_in(A_l),
	.Sign_o(Sign_l),	
	.Cen_o(Cen_l),
	.Dec_o(Dec_l),
	.Uni_o(Uni_l)
);

/*********************************************************/
initial // Clock generator
  begin
    forever #1 clk_top = !clk_top;
  end
/*********************************************************/
initial begin // reset generator

#0 A_l = 22;


#4 A_l = 128;


#4 A_l = 255;


#4 A_l = -122;
	
end

/*********************************************************/


endmodule
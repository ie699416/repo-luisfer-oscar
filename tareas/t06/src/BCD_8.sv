/*
*************************************************************************************
*	Coder:	OSCAR MAISTERRA FLORES	 		[ ie699416]
*	Coder: 	LUIS FERNANDO MORENO ALONSO	[ ie693314] 
*	Date:          May 30th, 2019
*  Name:          BCD_8.sv
*	Description:   Top file for BCD hw #2. The module test interconects the BCD 
*						decoder and the 7 segment modules.
*************************************************************************************
*/

`timescale 1ns/1ps

module BCD_8

import nBitsPackage::*;
(
	// Input Ports
	input data8_t A_in,
	output dataSeg_en Sign_o,	
	output dataSeg_en Cen_o,
	output dataSeg_en Dec_o,
	output dataSeg_en Uni_o
);

/*
*************************************************************************************
* Interconneciton wires/logic
*************************************************************************************
*/


decoder_en Sign_l;
decoder_en Cen_l;
decoder_en Dec_l;
decoder_en Uni_l;

data8_t complement_l;



/*
*************************************************************************************
* Instances definition  
*************************************************************************************
*/

Complement_Neg twosComp
(
	.data_in(A_in),
	.comp2_o(complement_l),
	.dataSign_o(Sign_l)
);


DD_BCD decoder
(
	.A_in({ZERO8,complement_l}), 		// 16 bit input
	.Cen_o(Cen_l),
	.Dec_o(Dec_l),
	.Uni_o(Uni_l)
);

Conv_BCD_7seg Sign
(
	.BCD_in(Sign_l),
	.segments_o(Sign_o)
);

Conv_BCD_7seg Cen
(
	.BCD_in(Cen_l),
	.segments_o(Cen_o)
);

Conv_BCD_7seg Dec
(
	.BCD_in(Dec_l),
	.segments_o(Dec_o)
);


Conv_BCD_7seg Uni
(
	.BCD_in(Uni_l),
	.segments_o(Uni_o)
);

endmodule
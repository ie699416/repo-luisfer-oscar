/*
*************************************************************************************
*	Coder:	OSCAR MAISTERRA FLORES	 		[ ie699416]
*	Coder: 	LUIS FERNANDO MORENO ALONSO	[ ie693314] 
*	Date:          May 30th, 2019
*  Name:          Corrimiento.sv
*	Description:   Shift logic file for BCD hw #2. 
*
*	Combiational processes description:
*
*		This counter modifies the order of the bits covering form 0 - 4 and skipping 
*		until the sequence 8 - 12 (C) for the algorithmim used for the BCD converter.
*
*************************************************************************************
*/
module Corrimiento

import nBitsPackage::*;

(
	// Input Ports
	input dataNum_t A_input,

	// Output Ports
	output dataNum_t B_output
);


/*
*************************************************************************************
*	Combiational processes
*************************************************************************************
*/

always_comb begin
 case (decoder_en'(A_input))
	ZERO 		: B_output = dataNum_t'(ZERO);
   ONE 		: B_output = dataNum_t'(ONE);
	TWO		: B_output = dataNum_t'(TWO);
   THREE 	: B_output = dataNum_t'(THREE);
	FOUR 		: B_output = dataNum_t'(FOUR);
   FIVE		: B_output = dataNum_t'(EIGHT);
   SIX		: B_output = dataNum_t'(NINE);
   SEVEN		: B_output = dataNum_t'(A);
   EIGHT		: B_output = dataNum_t'(B);
   NINE		: B_output = dataNum_t'(C);	
   default	: B_output = dataNum_t'(ZERO);
 endcase
end




endmodule

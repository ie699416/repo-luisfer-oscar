onerror {resume}
quietly WaveActivateNextPane {} 0
add wave -noupdate /BCD_8_tb/clk_top
add wave -noupdate -radix decimal /BCD_8_tb/data_l
add wave -noupdate /BCD_8_tb/sign_7seg_l
add wave -noupdate /BCD_8_tb/centenas_7seg_l
add wave -noupdate /BCD_8_tb/decenas_7seg_l
add wave -noupdate /BCD_8_tb/unidades_7seg_l
add wave -noupdate -radix decimal /BCD_8_tb/DUT/decoder/A_in
add wave -noupdate /BCD_8_tb/DUT/decoder/Cen_o
add wave -noupdate /BCD_8_tb/DUT/decoder/Dec_o
add wave -noupdate /BCD_8_tb/DUT/decoder/Uni_o
TreeUpdate [SetDefaultTree]
WaveRestoreCursors {{Cursor 1} {67 ps} 0}
quietly wave cursor active 1
configure wave -namecolwidth 309
configure wave -valuecolwidth 100
configure wave -justifyvalue left
configure wave -signalnamewidth 0
configure wave -snapdistance 10
configure wave -datasetprefix 0
configure wave -rowmargin 4
configure wave -childrowmargin 2
configure wave -gridoffset 0
configure wave -gridperiod 1
configure wave -griddelta 40
configure wave -timeline 0
configure wave -timelineunits ps
update
WaveRestoreZoom {0 ps} {220 ps}

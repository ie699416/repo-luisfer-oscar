/*
*************************************************************************************
*	Coder:	OSCAR MAISTERRA FLORES	 		[ ie699416]
*	Coder: 	LUIS FERNANDO MORENO ALONSO	[ ie693314] 
*	Date:          May 30th, 2019
*  Name:          BCD_8_tb.sv
*	Description:   Test bench file for BCD hw #2. The module test 5 cases for
*						3 digit numbers within the 8 bits scope.
*************************************************************************************
*/
/** Defines Timescale #1 as 1 ns **/
`timescale 1ns/1ps

module BCD_8_tb;
import nBitsPackage::*;



logic clk_top=1'b0;
	
	

data8_t data_l;


dataSeg_en unidades_7seg_l;		
dataSeg_en decenas_7seg_l;	
dataSeg_en centenas_7seg_l;
dataSeg_en sign_7seg_l;	

BCD_8
DUT
(		
	.A_in(data_l),
	.Sign_o(sign_7seg_l),
	.Cen_o(centenas_7seg_l),
	.Dec_o(decenas_7seg_l),
	.Uni_o(unidades_7seg_l)		
);

/*********************************************************/
initial // Clock generator
  begin
    forever #1 clk_top = !clk_top;
  end
/*********************************************************/
initial begin // reset generator

#2 data_l = 27;

#2 data_l = 127;

#2 data_l = -127;

#2 data_l = 1;
	
end

/*********************************************************/


endmodule

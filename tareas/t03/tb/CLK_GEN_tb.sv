/*
*************************************************************************************
*	Coder:	OSCAR MAISTERRA FLORES	 		[ ie699416]
*	Coder: 	LUIS FERNANDO MORENO ALONSO	[ ie693314] 
*	Date:          Jun 3th, 2019
*  Name:          ClkGen_tb;.sv
*	Description:   
*************************************************************************************
*/
`timescale 1ns/1ps

module CLK_GEN_tb;
import nBitsPackage::*;

logic clk_top = LOW;
logic reset = LOW;
logic clk_new;

/*
	
data8_t data_l;

dataSeg_en unidades_7seg_l;		
dataSeg_en decenas_7seg_l;	
dataSeg_en centenas_7seg_l;
dataSeg_en sign_7seg_l;	
*/

CLK_GEN DUT
(
	.clk_FPGA(clk_top),
	.reset(reset),
	.clk_salida(clk_new)	
);


/*********************************************************/
initial // Clock generator
  begin
    forever #2 clk_top = !clk_top;
  end
/*********************************************************/
initial begin // reset generator

#5 reset = HIGH;
	
end

/*********************************************************/


endmodule

// Coder:           DSc Abisai Ramirez Perez
// Date:            June 4th, 2019
// Name:            mux_2_1_op_ternario.sv
// Description:     This is a multiplexer using ternary operator.

module mux_2_1_op_ternario
import mux_pkg::*;
(
input   dtwidth_t   i_a,
input   dtwidth_t   i_b,
input   selectr_t   i_sel,
output  dtwidth_t   o_sltd
);

// All combinational logic must be modeled using ternary operator, CASE or IF-ELSE statements.

//   SALIDA = (Condicion) ? (Valor si verdadero) : (Valor si falso);
//   output = (conditional) ? (true) : (false);
assign  o_sltd = (i_sel) ? (i_b): (i_a);

endmodule


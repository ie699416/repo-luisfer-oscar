// Coder:           DSc Abisai Ramirez Perez
// Date:            June 4th, 2019
// Name:            mux_2_1_case.sv
// Description:     This is a multiplexer using case statement.

module mux_2_1_case
import mux_pkg::*;
(
input   dtwidth_t   i_a,
input   dtwidth_t   i_b,
input   selectr_t   i_sel,
output  dtwidth_t   o_sltd
);

//TODO: this code should generate a warning in the simulation, could you correct the code?
// If you cannnot see the warning, simulate the code.

// All combinational logic must be modeled using ternary operator, CASE or IF-ELSE statements.


// Description of a multiplexer two to one using a case statement
always_comb begin
    case(i_sel)
    0:          o_sltd = i_a;   // Review that for combinational logic 
    default:    o_sltd = i_b;   // the usage of blocking assigment
    endcase
end

endmodule


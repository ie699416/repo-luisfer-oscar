// Coder:           DSc Abisai Ramirez Perez
// Date:            June 4th, 2019
// Name:            mux_4_1_if_else.sv
// Description:     This is a multiplexer using if-else.

module mux_4_1_if_else
import mux_pkg::*;
(
input   dtwidth_t   i_a,
input   dtwidth_t   i_b,
input   dtwidth_t   i_c,
input   dtwidth_t   i_d,
input   selectr_t   i_sel,
output  dtwidth_t   o_sltd
);

// All combinational logic must be modeled using ternary operator, CASE or IF-ELSE statements.

// Description of a multiplexer two to one using a if-else statement
always_comb begin
if(i_sel == ONE)
    o_sltd = i_a;
else if(i_sel == TWO)
    o_sltd = i_b;
else if(i_sel == THREE)
    o_sltd = i_c;
else 
    o_sltd = i_d;
end

// Question: difference between types int and interger?
endmodule


`timescale 1ns / 1ps
//`include "../src/mux_pkg.sv"
module tb_mux_param;

    import mux_pkg::*;

in_bus_t    datain;
selectr_t   select;
dtwidth_t   sltd_o;

mux_param uut(
.datain(datain),     // Incoming data bus 
.select(select),     // Selector
.sltd_o(sltd_o)      // Selected data
);

initial begin
        datain  = {$random(),$random(),$random(),$random()};
        select  = 0;
    #2  select  = 1; 
    #2  select  = 2; 
    #2  select  = 3;
    #2  datain = {$random(),$random(),$random(),$random()};
        select  = 0;
    #2  select  = 1; 
    #2  select  = 2; 
    #2  select  = 3;
    $display("\n\n#########################################################################");
    $display("EJERCICIO: Simula el restante de los multiplexores en el folder de src");
    $display("#########################################################################\n\n");

    #100
    $stop;
end

endmodule

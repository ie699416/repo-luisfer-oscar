/*
*************************************************************************************
*	Coder:	OSCAR MAISTERRA FLORES	 		[ ie699416]
*	Coder: 	LUIS FERNANDO MORENO ALONSO	[ ie693314] 
*	Date : 	June 8th, 2019
*  Name :   mux_2_1.sv  
*
*	Description: Multiplexer parametric N bus value
*
* 
*************************************************************************************
*/

module mux_2_1
import nBitsPackage::*;
(
	// Input Ports
	input Selector,
	input data0_in,
	input data1_in,
	
	// Output Ports
	
	output logic mux_o

);


/*
*************************************************************************************
* Combiational processes 
*************************************************************************************
*/

always_comb 
	begin: MUX
		if (Selector == TRUE)
			mux_o = data1_in;
		else
			mux_o = data0_in;
	end: MUX// end always

endmodule
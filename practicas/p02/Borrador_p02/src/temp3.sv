/*
****************************************************************
* This module defines the 2 bits shift operation for temp 3
*	-	Also selects if the adder will receive a negative value
****************************************************************
*/

module temp3
import nBitsPackage::*;
(
	// Input Ports
	input data16_t Q_in,			// Data input from X PIPO register
	input R15_in,					// Selector for adder
	
	// Output Ports
	output data16_t temp3_o

);

data16_t dataShift_l;
data16_t temp3_l;

always @(*) begin: OR

dataShift_l = Q_in << (2);

	if(R15_in == 1'b1)
		begin
			temp3_l = {dataShift_l[N-1:2], (dataShift_l[1:0] | 2'b11)};	
		end
	else
		begin
			/*Two's complement operation*/
			temp3_l = (~({dataShift_l[N-1:1], (dataShift_l[0] | 1'b1)}) ) + 1'b1;	
		end

end: OR// end always

assign temp3_o = temp3_l;

endmodule
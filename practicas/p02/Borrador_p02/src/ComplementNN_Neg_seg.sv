/*
*************************************************************************************
*	Coder:	OSCAR MAISTERRA FLORES	 		[ ie699416]
*	Coder: 	LUIS FERNANDO MORENO ALONSO	[ ie693314] 
*	Date:          May 30th, 2019
*  Name:          Complement_Neg.sv
*	Description:   Two's complement file for BCD hw #2.
*
*  Combiational processes description:
*
*		The process below will perform the two's complement only if the data input
*		MSB is a logic  1'b1.  Other way  the data input is already on the desired 
*		format.
*
*		Also, in the continious combinational assignment the a data sign flag with
*		a decoder value for later conversion for the 7 segment display.
*
*************************************************************************************
*/
module ComplementNN_Neg_seg
import nBitsPackage::*;
(
	input dataNN_t	data_in,
	output dataNN_t comp2_o,
	output decoder_en dataSign_o
);



/*
*************************************************************************************
* Combiational processes 
*************************************************************************************
*/

always_comb begin: comp2_always
	
	if(data_in[2*N-1] == TRUE)
		begin		
			comp2_o = (~data_in) + UNIT;
			dataSign_o = SIGN;
		end
	else
		begin
			comp2_o = data_in;
			dataSign_o = DEFAULT;
		end

end: comp2_always

endmodule 
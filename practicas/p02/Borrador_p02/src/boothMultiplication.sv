/*
*************************************************************************************
*	Coder:	OSCAR MAISTERRA FLORES	 		[ ie699416]
*	Coder: 	LUIS FERNANDO MORENO ALONSO	[ ie693314] 
*	Date : 	June 4th, 2019
*  Name :   boothMultiplication.sv  
*
*	Description: Top module for booth multiplication algorithim.
*					 
*************************************************************************************
*/
module boothMultiplication

import nBitsPackage::*;

(
	// inputs for multiplication SR register
	input clk,
	input rst,
	input en_A, 	
	input en_Q,	
	input en_q0,								
	input	ASR_flag_in,						// Shift Control
	input multiplier_selector_in,			// MUX control
	input A_reg_selector_in,				// MUX control
	input dataN_t multiplier_in,			// Q <- Multiplier
	input dataN_t multiplicand_in,		// M <- Multiplicand
	
	// outputs from multiplication 					
	output dataNN_t result_o			   // Result <- Q from SR_register 
	
);

/*
*************************************************************************************
* Interconneciton wires/logic
*************************************************************************************
*/

dataN_t A_fromAdder_l;
dataN_t A_result_High_l;
dataN_t Q_result_Low_l;
dataN_t B_l;
logic Q0_sel_l;

/*
*************************************************************************************
* Instances definition 
*************************************************************************************
*/



mul mul
(
	.clk(clk),
	.rst(rst),
	.en_A(en_A), 	
	.en_Q(en_Q),	
	.en_q0(en_q0),								
	.ASR_flag_in(ASR_flag_in),									// Shift Control
	.multiplier_selector_in(multiplier_selector_in),	// MUX control
	.A_reg_selector_in(A_reg_selector_in),					// MUX control
	.A_from_adder_in(A_fromAdder_l),							// After Adder
	.multiplier_in(multiplier_in),							// Q <- Multiplier
	.multiplicand_in(multiplicand_in),						// M <- Multiplicand
	.Q_o(Q0_sel_l),												// Q_o
	.M_o(B_l),														// M <- Multiplicand
	.Result_H_o(A_result_High_l),								// Result_H <- A
	.Result_L_o(Q_result_Low_l)								// Result_L <- Q
	

);



adderN adder(
	.A_in(A_result_High_l),     
	.B_in(B_l),
	.Q0_sel(Q0_sel_l),
	.A_fromAdder_o(A_fromAdder_l)
);

assign result_o = {A_result_High_l,Q_result_Low_l};







endmodule 
/*
*************************************************************************************
*	Coder:	OSCAR MAISTERRA FLORES	 		[ ie699416]
*	Coder: 	LUIS FERNANDO MORENO ALONSO	[ ie693314] 
*	Date:          Jun 22th, 2019
*  Name:          processing_unit.sv
*  Description:   
*	-	The processing unit includes the algorithims implementation for the MUL, DIV 
*		and ROOT modules. 
*	-	The module implements a MUX selection for the algorithim to work accordingly.
*	-	TODO: implement a shared 33 BIT register to work with efficency. 
*
*************************************************************************************
*/

module processing_unit 
import nBitsPackage::*;
(
	// inputs for multiplication SR register
	input clk,
	input rst,	
	input dataN_t registerX_in,								// Q <- Multiplier
	input dataN_t registerY_in,								// M <- Multiplicand
	
	
	// MUL control input signals 
	input en_mul_A, 	
	input en_mul_Q,	
	input en_mul_q0,								
	input	ASR_flag_in,											// Shift Control
	input mul_mux_selector_in,									// MUX control
	input A_reg_selector_in,									// MUX control
	
	//Output ports
	
	output dataN_t Result_H_o,									// Result_H <- A
	output dataN_t Result_L_o									// Result_L <- Q
	

);

/*
*************************************************************************************
* Interconneciton wires/logic
*************************************************************************************
*/


logic AU_sel_l;
dataN_t A_fromAdder_l;


/*
*************************************************************************************
* Instances definition  
*************************************************************************************
*/


adderN AU(
	.A_in(Result_H_o),     
	.B_in(registerX_in),
	.AU_sel(AU_sel_l),
	.A_fromAdder_o(A_fromAdder_l)
);



mul mul
(
	.clk(clk),
	.rst(rst),
	.en_A(en_mul_A), 	
	.en_Q(en_mul_Q),	
	.en_q0(en_mul_q0),								
	.ASR_flag_in(ASR_flag_in),									// Shift Control
	.multiplier_selector_in(mul_mux_selector_in),		// MUX control
	.A_reg_selector_in(A_reg_selector_in),					// MUX control
	.A_from_adder_in(A_fromAdder_l),							// After Adder
	.multiplier_in(registerY_in),								// Q <- Multiplier
	.AU_Flag_o(AU_sel_l),										// Q[0] when HIGH AU will SUBSTRACT
	.Result_H_o(Result_H_o),									// Result_H <- A
	.Result_L_o(Result_L_o)										// Result_L <- Q
);





endmodule

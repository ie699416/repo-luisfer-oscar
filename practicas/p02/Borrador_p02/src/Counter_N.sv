module Counter_N
import nBitsPackage::*;
(
	// Input Ports
	input clk,
	input reset,
	input enable,
	
	// Output Ports
	output dataN_t N_counter
	
);

logic MaxValue_Bit;
dataN_t Count_logic;

always_ff@(posedge clk or negedge reset)
	begin: counter
		if (reset == FALSE)
			Count_logic <= UNIT16;
		else 
			begin
				if(enable == TRUE) 				
					Count_logic <= Count_logic - UNIT;						
				else
					Count_logic <= UNIT16;			
			end
	end: counter
	
	
assign N_counter = Count_logic;


endmodule

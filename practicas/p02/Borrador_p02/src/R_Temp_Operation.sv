/*
****************************************************************
* This module concatenates the temp values for the new R
*	- 	The shifted value of temp2 is assigned to the output with
*		the first two LSB bits from the temp1 operation
****************************************************************
*/

module R_Temp_Operation
import nBitsPackage::*;
(
	input data2_t temp1_in,		
	input data16_t temp2_in,			
	
	// Output Ports
	output data16_t R_temp_o

);


/*
********************************
* Interconneciton wires/logic
********************************
*/


data16_t bitwise_l;


/*
********************************
* Sequential operation
********************************
*/

always @(*)	begin: bitwise
		bitwise_l = {temp2_in[15:2],(temp2_in [1:0] | temp1_in)};	
end: bitwise// end always

assign R_temp_o =  bitwise_l;

endmodule
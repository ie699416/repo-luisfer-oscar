/*
*************************************************************************************
*	Coder:	OSCAR MAISTERRA FLORES	 		[ ie699416]
*	Coder: 	LUIS FERNANDO MORENO ALONSO	[ ie693314] 
*	Date:          May 30th, 2019
*  Name:          Conv_BCD_7seg.sv
*	Description:   Converter file for BCD hw #2. The module performs the combinational
*						process described below.
*
*	Combiational processes description:
*
*		The nBitsPackage pakage defines the  bits  relation  for  the  inverted  logic 
*		required to turn on the leds in  the  7 segment display with an enum data type 
*		definition.
*************************************************************************************
*/
module Conv_BCD_7seg_sign

import nBitsPackage::*;

(
	// Input Ports
	input sign_in,

	// Output Ports
	output dataSeg_en segments_o

);


/*
*************************************************************************************
* Combiational processes 
*************************************************************************************
*/


always_comb
begin
	if (sign_in == TRUE)
		segments_o = _min;
	else
		segments_o = _X;
end


endmodule
module mul 
import nBitsPackage::*;
(
	// inputs for multiplication SR register
	input clk,
	input rst,
	input en_A, 	
	input en_Q,	
	input en_q0,								
	input	ASR_flag_in,						// Shift Control
	input multiplier_selector_in,			// MUX control
	input A_reg_selector_in,				// MUX control
	input dataN_t A_from_adder_in,		// After Adder
	input dataN_t multiplier_in,			// Q <- Multiplier
	
	//Output ports
	output AU_Flag_o,							// Q_o
	output dataN_t Result_H_o,				// Result_H <- A
	output dataN_t Result_L_o				// Result_L <- Q
	
);

/*
*************************************************************************************
* Interconneciton wires/logic
*************************************************************************************
*/

logic Q_reg_outputBit_l;
logic q0_l;
logic xOr_l;
logic mux_A_l;

dataN_t Q_result_Low_l;
dataN_t Q_from_mux_l;
dataN_t A_from_mux_l;

/*
*************************************************************************************
* Instances definition  
*************************************************************************************
*/

Booth_SR_register SR_AQq0(
	.clk(clk),
	.rst(rst),
	.en_A(en_A), 	
	.en_Q(en_Q),	
	.en_q0(en_q0),
	.ASR_flag_in(ASR_flag_in),
	.multiplier_in(Q_from_mux_l),		// Q <- Multiplier 
	.A_fromAdder_in(A_from_mux_l),		// A <- A + M_reg	
	.Q_reg_outputBit_o(AU_Flag_o),
	.q0_o(q0_l),
	.Result_H_o(Result_H_o),			// Result_H from A_reg	
	.Result_L_o(Result_L_o)			// Result_L from Q_reg
);

xclusive_or xOr(
	.a_in(AU_Flag_o),				//Defined in booth algorithm
	.b_in(q0_l),
	.xOr_o(xOr_l)
);

muxN_2_1 mux_Q
(
	.Selector(multiplier_selector_in),
	.data0_in(Result_L_o),
	.data1_in(multiplier_in), 
	.mux_o(Q_from_mux_l)  
);

xclusive_AND AND
(
	.a_in(xOr_l),
	.b_in(A_reg_selector_in),
	.and_o(mux_A_l)
);
 
muxN_2_1 mux_A
(
	.Selector(mux_A_l),							// When HIGH store the adder result
	.data0_in(Result_H_o),
	.data1_in(A_from_adder_in),
	.mux_o(A_from_mux_l)  
);

endmodule

/*
****************************************************************
* This module defines de top-block for a booth multiplication
****************************************************************
*/
module boothRoot

import nBitsPackage::*;

(
	// inputs 
	input cclk,
	input rst,
	input data16_t X_in,					// D <- Data from X register	
	input dataN_t n_in,
	input logic Idel_flag,
	input logic Load_flag,
	input logic Process_flag,
	input logic Ready_flag,
	
	// outputs					
	output data16_t result_o,			// Result <- Q 
	output data16_t remainder_o		// Remainder <- R
	
);

/*
********************************
* Interconneciton wires/logic
********************************
*/

data16_t R_temp_l;
data16_t M_fromTemp3_l;			//temp value for adder, internal two's complement combinational logic
data17_t R_fromAdder17_l;


R_Operation 
newR
(
	.clk(cclk),
	.reset(rst),
	.R_in(R_fromAdder17_l[N-1:0]),			
	.D_in(X_in),
	.n_in(n_in),
	.Idel_flag(Idel_flag),
	.Load_flag(Load_flag),
	.Process_flag(Process_flag),
	.Ready_flag(Ready_flag),
	.R_temp_o(R_temp_l),			//R previous add operation
	.remainder_o(remainder_o)  //R previous add operation
);

Q_Operation 
newQ
(
	.clk(cclk),
	.reset(rst),
	.Idel_flag(Idel_flag),
	.Load_flag(Load_flag),
	.Process_flag(Process_flag),
	.Ready_flag(Ready_flag),
	.R15_in(R_fromAdder17_l[N-1]),
	.Q_o(result_o)
);

temp3
M
(
	.Q_in(result_o),			
	.R15_in(R_temp_l[13]),			
	.temp3_o(M_fromTemp3_l)

);

adderN AU(
	.A_in({CERO,R_temp_l}),     
	.B_in({CERO,M_fromTemp3_l}),
	.AU_sel(CERO),
	.A_fromAdder_o(R_fromAdder17_l)
);

endmodule 
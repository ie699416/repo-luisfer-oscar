/*
****************************************************************
* This module concatenates the temp values for the new R
*	- 	The shifted value of temp2 is assigned to the output with
*		the first two LSB bits from the temp1 operation
*	-	The value is stored in a FF
****************************************************************
*/

module R_Operation
import nBitsPackage::*;
(
	// Input Ports
	input clk,
	input reset,
	input data16_t R_in,				// R input from adder
	input data16_t D_in,				// D input from adder
	input dataN_t n_in,	
	input logic Idel_flag,
	input logic Load_flag,
	input logic Process_flag,
	input logic Ready_flag,
	
	
	// Output Ports
	output data16_t R_temp_o,		//R previous add operation
	output data16_t remainder_o	//Final R stored value

);

/*
********************************
* Interconneciton wires/logic
********************************
*/


logic_enum_state_t state; /* DEFINICIÓN DE ESTADOS DE LA MÁQUINA */    

data2_t temp1_l;
data16_t temp2_l;
data16_t R_temp_l;
data16_t R_regsiter_l;
data16_t R_temp_regsiter_l;

/*
*********************
* Modules instances
*********************
*/

temp1 D_SR_and_3
(

	.D_in(D_in),
	.n_in(n_in),
	.temp1_o(temp1_l)

);

temp2 R_SL
(

	.R_in(R_in),
	.enable(Process_flag),
	.temp2_o(temp2_l)

);


R_Temp_Operation
bitwiseOR
(
	.temp1_in(temp1_l),	
	.temp2_in(temp2_l),			
	.R_temp_o(R_temp_l)
);


/*
********************************
* Sequential operations
********************************
*/


always_ff@(posedge clk, negedge reset)
	begin: R_register
	
		if(reset == 1'b0)
			begin
				R_regsiter_l <= ZERO16;
				R_temp_regsiter_l <= ZERO16;
			end
		else 
			begin
				if(Idel_flag == TRUE)begin
							R_regsiter_l <= ZERO16;
							R_temp_regsiter_l <= ZERO16;
				 end
				 
				else if(Load_flag == TRUE)begin
					R_regsiter_l <= R_in;						
					R_temp_regsiter_l <= R_temp_l;
				 end
				 
				 else if(Process_flag == TRUE)begin
						R_regsiter_l <= R_in;							
						R_temp_regsiter_l <= R_temp_l;
					end
				 else if(Ready_flag == TRUE)begin
						R_regsiter_l <= R_regsiter_l;						
						R_temp_regsiter_l <= R_temp_regsiter_l;
					end
				else
					begin	
						R_regsiter_l <= R_regsiter_l;
						R_temp_regsiter_l <= R_temp_regsiter_l;
					end
			end			
	end: R_register

assign remainder_o = R_regsiter_l;
assign R_temp_o = R_temp_regsiter_l;

endmodule
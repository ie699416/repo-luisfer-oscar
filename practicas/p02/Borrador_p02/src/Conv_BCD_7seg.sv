/*
*************************************************************************************
*	Coder:	OSCAR MAISTERRA FLORES	 		[ ie699416]
*	Coder: 	LUIS FERNANDO MORENO ALONSO	[ ie693314] 
*	Date:          May 30th, 2019
*  Name:          Conv_BCD_7seg.sv
*	Description:   Converter file for BCD hw #2. The module performs the combinational
*						process described below.
*
*	Combiational processes description:
*
*		The nBitsPackage pakage defines the  bits  relation  for  the  inverted  logic 
*		required to turn on the leds in  the  7 segment display with an enum data type 
*		definition.
*************************************************************************************
*/
module Conv_BCD_7seg

import nBitsPackage::*;

(
	// Input Ports
	input decoder_en BCD_in,

	// Output Ports
	output dataSeg_en segments_o

);


/*
*************************************************************************************
* Combiational processes 
*************************************************************************************
*/


always @(*)
begin
	case (BCD_in)
		ZERO 		: segments_o = _0;
      ONE 		: segments_o = _1;
      TWO 		: segments_o = _2;
      THREE 	: segments_o = _3;
		FOUR 		: segments_o = _4;
		FIVE 		: segments_o = _5;
      SIX 		: segments_o = _6;
      SEVEN 	: segments_o = _7;
      EIGHT		: segments_o = _8;
      NINE		: segments_o = _9;
		SIGN		: segments_o = _min;
		DEFAULT	: segments_o = _X;
      default 	: segments_o = _X; 
    endcase
end


endmodule
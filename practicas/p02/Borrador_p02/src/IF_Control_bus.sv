`ifndef CONTROL_BUS_IF
    `define CONTROL_BUS_IF

interface Control_bus
import nBitsPackage::*; //define the interface

	logic ASR_flag;			//Signal that sends the start process for the multiplication process 
	logic A_flag;
	logic Q_flag;
	logic Q0_flag;
	logic Mtr_selec_in;
	logic A_reg_selec_in;
	logic Rest_flag;		//Signal that send the flag of restart to the other modules
	logic Ready_flag;	   //Signal that indicates the state of the process 
	logic Enable_count;	//Signal that enables the counter
	logic Flag_op;
	logic load_X;
	logic load_Y;
	logic	Mul_enable;
	logic	Div_enable;
	logic	Root_enable;
	
modport Operations(

	input clk,
	input rst,
	input Control_start Start,					// Signal that determines the Start of the proyect, by a push botton
	input Control_flag Flag_counter,			//This flag determines when the counter finished counting 
	input data2_t Select_op,
	input Control_flag Flag_load_x,
	input Control_flag Flag_load_y,
	
	// outputs from Control Unit						
	output Control_flag ASR_flag,			//Signal that sends the start process for the multiplication process 
	output Control_flag A_flag,
	output Control_flag Q_flag,
	output Control_flag Q0_flag,
	output Control_flag Mtr_selec_in,
	output Control_flag A_reg_selec_in,
	output Control_flag Rest_flag,		//Signal that send the flag of restart to the other modules
	output Control_ready Ready_flag,	   //Signal that indicates the state of the process 
	output Control_flag Enable_count,	//Signal that enables the counter
	output Control_flag Flag_op,
	output Control_flag load_X,
	output Control_flag load_Y,
	output Control_flag 	Mul_enable,
	output Control_flag 	Div_enable,
	output Control_flag 	Root_enable
);	
	
	
endinterface: Control_bus
`endif




	

module R
import nBitsPackage::*;

(
	// inputs 
	input clk,
	input rst,
	input en,								// Enable for M register
	input dataN_t Root_in,	// M <- Multiplicand	
	// outputs 
	output dataN_t Root_o					// M <- value for adder	
	
);


N_register M_reg
(
	.clk(clk),
	.rst(rst),
	.en(en),								
	.data_in(Root_in),	
	.data_o(Root_o)					// M <- value for adder	
);



endmodule 
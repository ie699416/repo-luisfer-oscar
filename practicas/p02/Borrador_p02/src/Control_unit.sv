
/*
****************************************************************
* This module defines de top-block for the practice 
****************************************************************
*/
module Control_unit
import nBitsPackage::*;
(
	// inputs for the control unit
	input clk,
	input rst,
	input PB start_in,		
	input PB load_in,	
	input data2_t OP_in,	
	input Control_flag Flag_counter,			//This flag determines when the counter finished counting 
	
	// outputs from Control Unit						
	output Control_flag ASR_flag,				//Signal that sends the start process for the multiplication process 
	output Control_flag A_flag,	
	output Control_flag Q_flag,
	output Control_flag Q0_flag,
	output Control_flag Mtr_selec_in,
	output Control_flag A_reg_selec_in,
	output Control_flag Ready_flag,	   	//Signal that indicates the state of the process 
	output Control_flag Enable_count,			//Signal that enables the counter
	output Control_flag Xreg_flag,
	output Control_flag Yreg_flag
);


State_e Actual_state;	

always_ff@(posedge clk or negedge rst) // Circuito Secuenicial en un proceso always.
	begin
		if (rst == FALSE)
			Actual_state <= STATE_IDLE;			
		else 
			begin		
				case(Actual_state)				
						STATE_IDLE:
							begin 						
								if (start_in == TRUE)
										Actual_state <= STATE_LOAD_X; 									
								else 
										Actual_state <= STATE_IDLE; 							
							end							
						STATE_LOAD_X: 
							begin
								if (load_in == TRUE)
									Actual_state <= STATE_LOAD_Y;
								else 
									Actual_state <= STATE_LOAD_X;
							end
						
						STATE_LOAD_Y: 
							begin
								if (load_in == TRUE)
									Actual_state <= STATE_SET;
								else 
									Actual_state <= STATE_LOAD_Y;
							end
							
						STATE_SET: 
								Actual_state <= STATE_PROCESS;
								
						STATE_PROCESS:
							begin 						
								if (Flag_counter==TRUE)
									Actual_state<=STATE_PREREADY;
								else 
									Actual_state<=STATE_PROCESS;
						end
							
						STATE_PREREADY:
							Actual_state <= STATE_READY;
							
						STATE_READY:begin 
						
							if (start_in==TRUE)
								Actual_state<=STATE_LOAD_X;
							else 
								Actual_state<=STATE_READY;							
							end
							
						default: Actual_state<=STATE_IDLE;
							
				 endcase	
			end
	end

// CTO combinacional de salida.
always_comb 
	begin
		case(Actual_state)
			 STATE_IDLE:
				 begin 
					A_reg_selec_in = LOW;
					Ready_flag = LOW;
					A_flag = LOW ;
					Q_flag =  LOW;
					Q0_flag = LOW;
					Mtr_selec_in = HIGH;
					ASR_flag = LOW; 
					Enable_count = LOW;
					Xreg_flag = LOW;
					Yreg_flag = LOW;
				 end
					
			STATE_LOAD_X:
				begin
					A_reg_selec_in = HIGH;
					A_flag = LOW ;
					Q_flag =  HIGH;
					Q0_flag = LOW;
					Mtr_selec_in = HIGH;
					Ready_flag = LOW;
					ASR_flag = LOW; 
					Enable_count = LOW;
					Xreg_flag = HIGH;
					Yreg_flag = LOW;
				end
				
			 STATE_LOAD_Y:
				begin
					A_reg_selec_in = HIGH;
					A_flag = LOW ;
					Q_flag =  HIGH;
					Q0_flag = LOW;
					Mtr_selec_in = HIGH;
					Ready_flag = LOW;
					ASR_flag = LOW; 
					Enable_count = LOW;
					Xreg_flag = LOW;
					Yreg_flag = HIGH;
				end
				
			 STATE_SET:			 
			 	begin
					A_reg_selec_in = HIGH;
					A_flag = LOW ;
					Q_flag =  HIGH;
					Q0_flag = LOW;
					Mtr_selec_in = HIGH;
					Ready_flag = LOW;
					ASR_flag = LOW; 
					Enable_count = LOW;
					Xreg_flag = LOW;
					Yreg_flag = HIGH;
				end
			 
			 STATE_PROCESS:
			 begin
				case(OP_en'(OP_in))
					MUL:
						begin
					 		A_reg_selec_in = HIGH;
							A_flag = HIGH ;
							Q_flag =  HIGH;
							Q0_flag = HIGH;
							Mtr_selec_in = LOW;
							Ready_flag = LOW;
							ASR_flag = HIGH; 
							Enable_count = HIGH;
							Xreg_flag = LOW;
							Yreg_flag = LOW;
						end
					DIV:
						begin
					 		A_reg_selec_in = HIGH;
							A_flag = HIGH ;
							Q_flag =  HIGH;
							Q0_flag = HIGH;
							Mtr_selec_in = LOW;
							Ready_flag = LOW;
							ASR_flag = HIGH; 
							Enable_count = HIGH;
							Xreg_flag = LOW;
							Yreg_flag = LOW;
						end
					ROOT: 						
						begin
					 		A_reg_selec_in = HIGH;
							A_flag = HIGH ;
							Q_flag =  HIGH;
							Q0_flag = HIGH;
							Mtr_selec_in = LOW;
							Ready_flag = LOW;
							ASR_flag = HIGH; 
							Enable_count = HIGH;
							Xreg_flag = LOW;
							Yreg_flag = LOW;
						end
					NA: 					
						begin
					 		A_reg_selec_in = HIGH;
							A_flag = HIGH ;
							Q_flag =  HIGH;
							Q0_flag = HIGH;
							Mtr_selec_in = LOW;
							Ready_flag = LOW;
							ASR_flag = HIGH; 
							Enable_count = HIGH;
							Xreg_flag = LOW;
							Yreg_flag = LOW;
						end
					default:					
						begin
					 		A_reg_selec_in = HIGH;
							A_flag = HIGH ;
							Q_flag =  HIGH;
							Q0_flag = HIGH;
							Mtr_selec_in = LOW;
							Ready_flag = LOW;
							ASR_flag = HIGH; 
							Enable_count = HIGH;
							Xreg_flag = LOW;
							Yreg_flag = LOW;
						end				
				 endcase
			 end
			 
			 STATE_PREREADY:
				begin
					A_reg_selec_in = LOW;
					A_flag = LOW ;
					Q_flag =  LOW;
					Q0_flag = LOW;
					Mtr_selec_in = HIGH;
					Ready_flag = LOW;
					ASR_flag = HIGH; 
					Enable_count = HIGH;
					Xreg_flag = LOW;
					Yreg_flag = LOW;
				end
			 
			 STATE_READY:
				begin
					A_reg_selec_in = LOW;
					A_flag = LOW ;
					Q_flag =  LOW;
					Q0_flag = LOW;
					Mtr_selec_in = LOW;
					Ready_flag = HIGH;
					ASR_flag = LOW; 
					Enable_count = LOW;
					Xreg_flag = LOW;
					Yreg_flag = LOW;
				end
				
			 default:
				begin
					A_reg_selec_in = LOW;
					A_flag = LOW ;
					Q_flag =  LOW;
					Q0_flag = LOW;
					Mtr_selec_in = LOW;
					Ready_flag = LOW;
					ASR_flag = LOW; 
					Enable_count = LOW;
					Xreg_flag = LOW;
					Yreg_flag = LOW;
				end
	 
	endcase
end

endmodule
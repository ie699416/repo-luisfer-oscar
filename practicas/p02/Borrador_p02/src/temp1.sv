/*
****************************************************************
* This module defines the n-shift operation for temp 1
****************************************************************
*/

module temp1
import nBitsPackage::*;
(
	// Input Ports
	input data16_t D_in,			// Data input from X PIPO register
	input dataN_t n_in,	// Iterative counter for shift
	
	// Output Ports
	output data2_t temp1_o

);

/*
********************************
* Interconneciton wires/logic
********************************
*/

shift_enum_t nCase;

data16_t dataShift_l;

/*
********************************
* Combinational operation
********************************
*/


always @(*) begin: shift_case
	case (n_in)
		zero_s:
			begin
				dataShift_l = D_in; // dataShift_l = D_in >> 1'b0;
			end
		one_s:
			begin
				dataShift_l = D_in >> (2);
			end
		two_s:
			begin
				dataShift_l = D_in >> (4);
			end
		three_s:
			begin
				dataShift_l = D_in >> (6);
			end
		four_s:
			begin
				dataShift_l = D_in >> (8);
			end
		five_S:
			begin
				dataShift_l = D_in >> (10);
			end
		six_s:
			begin
				dataShift_l = D_in >> (12);
			end
		seven_s:
			begin
				dataShift_l = D_in >> (14);
			end
		default:
			begin
				dataShift_l = ZERO16; 
			end
	endcase
	

end: shift_case// end always

assign temp1_o = dataShift_l[1:0] & LSB_MASK;

endmodule
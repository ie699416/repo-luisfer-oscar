/*
*************************************************************************************
*	Coder:	OSCAR MAISTERRA FLORES	 		[ ie699416]
*	Coder: 	LUIS FERNANDO MORENO ALONzO	[ ie693314] 
*	Date:          Jun 4th, 2019	
*  Name:          Counter_SM.sv      
*************************************************************************************
*/

module Counter_SM
import nBitsPackage::*;
(
	// Input Ports
	input clk,
	input reset,
	input enable,
	input data2_t OP_in,
	
	// Output Ports
	output flag
	
);

logic MaxValue_Bit;
dataCount_t Count_logic;

always_ff@(posedge clk or negedge reset)
	begin: counter
		if (reset == FALSE)
			Count_logic <= ZERO_COUNT;
		else 
			begin
				if(enable == TRUE) 				
					Count_logic <= Count_logic + UNIT;						
				else
					Count_logic <= ZERO_COUNT;			
			end
	end: counter

//--------------------------------------------------------------------------------------------

always_comb
begin
	case (OP_en'(OP_in))
		MUL: 
			if(Count_logic ==  N - UNIT)
				MaxValue_Bit = HIGH;
			else
				MaxValue_Bit = LOW;
		DIV: 	
			if(Count_logic ==  (N+UNIT) - UNIT)
				MaxValue_Bit = HIGH;
			else
				MaxValue_Bit = LOW;
		ROOT:
			if(Count_logic ==  EIGHT)
				MaxValue_Bit = HIGH;
			else
				MaxValue_Bit = LOW;
		NA:
			MaxValue_Bit = HIGH;
			
		default: MaxValue_Bit = LOW;
	
	endcase
end
		
//---------------------------------------------------------------------------------------------
assign flag = MaxValue_Bit;


endmodule

/*
*************************************************************************************
*	Coder:	OSCAR MAISTERRA FLORES	 		[ ie699416]
*	Coder: 	LUIS FERNANDO MORENO ALONSO	[ ie693314] 
*	Date:          May 30th, 2019
*  Name:          BCD_shift.sv
*	Description:   Shift logic for double dabble algorithm
*	-	DD_en, flag for activate the 
*	-	The module receives a 1-bit input (OVF after shift)
*	-	The 4-bit value to shift. 
*	-	The  4-bit  output after shift & Double dabble 
*
*************************************************************************************
*/
module DD_shift
import nBitsPackage::*;
(
	input LSB_in,
	input DD_en,
	input  dataNum_t A_in,
	output dataNum_t B_o
);

/*
*************************************************************************************
*	Combiational processes
*************************************************************************************
*/


dataNum_t shift_l;


always_comb 
	begin
		shift_l = {A_in[2:0],LSB_in};
			if(DD_en == TRUE)
				begin				
					case (decoder_en'(shift_l))
						ZERO 		: B_o = dataNum_t'(ZERO);
						ONE 		: B_o = dataNum_t'(ONE);
						TWO		: B_o = dataNum_t'(TWO);
						THREE 	: B_o = dataNum_t'(THREE);
						FOUR 		: B_o = dataNum_t'(FOUR);
						FIVE		: B_o = dataNum_t'(EIGHT);
						SIX		: B_o = dataNum_t'(NINE);
						SEVEN		: B_o = dataNum_t'(A);
						EIGHT		: B_o = dataNum_t'(B);
						NINE		: B_o = dataNum_t'(C);	
						default	: B_o = dataNum_t'(ZERO);
					 endcase
				 end
			else
				begin
					B_o = shift_l;
				end
	end


endmodule

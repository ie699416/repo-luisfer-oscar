/*
*************************************************************************************
*	Coder:	OSCAR MAISTERRA FLORES	 		[ ie699416]
*	Coder: 	LUIS FERNANDO MORENO ALONSO	[ ie693314] 
*	Date:          Jun 8th, 2019
*  Name:          adderN.sv
*  Description:   The process below performs the arithmetic SUM between two N-bits
*
*************************************************************************************
*/

module adderN
import nBitsPackage::*;
(
	input dataN_t A_in,     
	input dataN_t B_in,
	input Q0_sel,
	output dataN_t A_fromAdder_o
);

/*
*************************************************************************************
* Combiational processes 
*************************************************************************************
*/


always_comb 
	begin: adder
		if (Q0_sel == TRUE)
			A_fromAdder_o = A_in - B_in;
		else 
			A_fromAdder_o = A_in + B_in;
	end: adder


endmodule 
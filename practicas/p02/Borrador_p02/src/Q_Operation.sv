/*
****************************************************************
* This shifts the current value of Q and modify the LSB
*	-	The value is stored in a FF
****************************************************************
*/

module Q_Operation
import nBitsPackage::*;
(
	// Input Ports
	input clk,
	input reset,
	input R15_in,					// R input from adder
	input logic Idel_flag,
	input logic Load_flag,
	input logic Process_flag,
	input logic Ready_flag,
	// Output Ports
	output data16_t Q_o			// Result 

);

/*
********************************
* Interconneciton wires/logic
********************************
*/


logic_enum_state_t state; /* DEFINICIÓN DE ESTADOS DE LA MÁQUINA */    

data16_t Q_register_l;

/*
*********************
* Modules instances
*********************
*/


/*
********************************
* Sequential operations
********************************
*/


always_ff@(posedge clk, negedge reset)
	begin: Q_register
	
		if(reset == 1'b0)
			Q_register_l <= ZERO16;
		else 
			begin
				if(Idel_flag == TRUE)
					Q_register_l <= ZERO16;
					
				else if(Load_flag == TRUE)
					Q_register_l <= ZERO16;
					
				 else if(Process_flag == TRUE)
						begin
							Q_register_l = (Q_register_l << 1 );
							if(R15_in == 1'b1)
								begin
								/*bitwise OR: LSB | 0*/
								Q_register_l[0] <= Q_register_l[0] | 1'b0;
								
								end
							else
								begin
								/*bitwise OR: LSB | 1*/
								Q_register_l[0] <= Q_register_l[0] | 1'b1;
								end
						end
						
					else if(Ready_flag == TRUE)					
						Q_register_l <= Q_register_l;
						
					else
						Q_register_l <= Q_register_l;
			end			
	end: Q_register


assign Q_o = Q_register_l;

endmodule
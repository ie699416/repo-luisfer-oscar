/*
*************************************************************************************
*	Coder:	OSCAR MAISTERRA FLORES	 		[ ie699416]
*	Coder: 	LUIS FERNANDO MORENO ALONSO	[ ie693314] 
*	Date : 	June 11th, 2019
*  Name :   DISPLAY_data.sv  
*
*	Description: 	 
*	-	This module receives swithes or Result Data selected by the ready flag.
*	-	It has been designed for N = 16
*		 
*************************************************************************************
*/

module DISPLAY_data
import nBitsPackage::*;

(
	input dataN_t	Switches_in,
	input dataN_t	result_in,
	input sign_xor_in, 
	input sign_sw_in, 
	input ready_in,						// Ready_flag
	input error_flag,
		// outputs from Control Unit						
	output dataSeg_en Display_1,			
	output dataSeg_en Display_2,
	output dataSeg_en Display_3, 
	output dataSeg_en Display_4,
	output dataSeg_en Display_5,			
	output dataSeg_en Display_6
	
);

/*
*************************************************************************************
* Interconneciton wires/logic
*************************************************************************************
*/


wire sign_l;
decoder_en Dec_Mill_l;
decoder_en Uni_Mill_l;
decoder_en Cen_l;
decoder_en Dec_l;
decoder_en Uni_l;
data16_t result_fromMux_l;

dataSeg_en Display_1_wire;			
dataSeg_en Display_2_wire;
dataSeg_en Display_3_wire; 
dataSeg_en Display_4_wire;
dataSeg_en Display_5_wire;			
dataSeg_en Display_6_wire;

dataSeg_en Display_1_output;			
dataSeg_en Display_2_output;
dataSeg_en Display_3_output; 
dataSeg_en Display_4_output;
dataSeg_en Display_5_output;			
dataSeg_en Display_6_output;



decoder_en Wire_result_sign;			//sign
/*
*************************************************************************************
* Instances definitions
*************************************************************************************
*/

mux_2_1 muxSign
(
	.Selector(ready_in),
	.data0_in(sign_sw_in),
	.data1_in(sign_xor_in),
	.mux_o(sign_l)

);


BCD_16 BCD_Result
(
	// Input Ports
	.A_in(result_fromMux_l),
	.sign_in(sign_l),
	.sign_o(Display_6_wire),
	.Dec_Mill_o(Display_5_wire),
	.Uni_Mill_o(Display_4_wire),
	.Cen_o(Display_3_wire),
	.Dec_o(Display_2_wire),
	.Uni_o(Display_1_wire)
);


muxN_2_1 muxN
(
	.Selector(ready_in),
	.data0_in(Switches_in),
	.data1_in(result_in),
	.mux_o(result_fromMux_l)
);

Mux_result_error Mux_error_1
(
	// Input Ports
	.data0_in(Display_1_wire),
	.data1_in(_F),
	.sel(error_flag),

	// Output Ports
	.data_o(Display_1_output)

);

Mux_result_error Mux_error_2
(
	// Input Ports
	.data0_in(Display_2_wire),
	.data1_in(_F),
	.sel(error_flag),

	// Output Ports
	.data_o(Display_2_output)

);

Mux_result_error Mux_error_3
(
	// Input Ports
	.data0_in(Display_3_wire),
	.data1_in(_F),
	.sel(error_flag),

	// Output Ports
	.data_o(Display_3_output)

);
Mux_result_error Mux_error_4
(
	// Input Ports
	.data0_in(Display_4_wire),
	.data1_in(_F),
	.sel(error_flag),

	// Output Ports
	.data_o(Display_4_output)

);

Mux_result_error Mux_error_5

(
	// Input Ports
	.data0_in(Display_5_wire),
	.data1_in(_F),
	.sel(error_flag),

	// Output Ports
	.data_o(Display_5_output)

);

Mux_result_error Mux_error_6
(
	// Input Ports
	.data0_in(Display_6_wire),
	.data1_in(_F),
	.sel(error_flag),

	// Output Ports
	.data_o(Display_6_output)

);

assign Display_1 = Display_1_output ;
assign Display_2 = Display_2_output ;
assign Display_3 = Display_3_output ;
assign Display_4 = Display_4_output ;
assign Display_5 = Display_5_output ;
assign Display_6 = Display_6_output ;


endmodule
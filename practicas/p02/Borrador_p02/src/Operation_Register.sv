/*
*************************************************************************************
*	Coder:	OSCAR MAISTERRA FLORES	 		[ ie699416]
*	Coder: 	LUIS FERNANDO MORENO ALONSO	[ ie693314] 
*	Date:          Jun 22th, 2019
*  Name:          Operation_Register.sv
*  Description:   Two bits register for SW OPeration selector
*
*************************************************************************************
*/
module OP_rRegister
import nBitsPackage::*;
(
	// Input Ports
	input clk,
	input reset,
	input enable,
	input data2_t SW,
	
	// Output Ports
	output data2_t OP_o
	
);


/*
*************************************************************************************
* Sequential processes 
*************************************************************************************
*/

OP_en OP_l;


always_ff@(posedge clk or negedge reset) 
	begin: Operation
		if (reset == FALSE)
			OP_l <= MUL; 	//MUL default
		else 
			begin
				if(enable == TRUE) 
					begin
						case(SW)
							MUL:
								OP_l <= MUL;
							DIV:
								OP_l <= DIV;
							ROOT:
								OP_l <= ROOT;
							NA:
								OP_l <= NA;
							default: OP_l <= NA;
						endcase
					end
				else
					OP_l <= OP_l;
			end
	end: Operation
	
assign OP_o = data2_t'(OP_l);
	
endmodule
	
	
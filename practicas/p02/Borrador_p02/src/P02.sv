/*
*************************************************************************************
*	Coder:	OSCAR MAISTERRA FLORES	 		[ ie699416]
*	Coder: 	LUIS FERNANDO MORENO ALONSO	[ ie693314] 
*	Date:          Jun 21th, 2019
*  Name:          P02_borrador.sv
*  Description:   First sinthetization for overall TOP bock connections
*
*************************************************************************************
*/
module P02
import nBitsPackage::*;
(
	input clk_FPGA,
	input reset,
	input data2_t OP_in,
	input data16_t Switches_in,
	input PB start_in,		
	input PB load_in,	
	output LED ready_o,
	output LED loadX_o,
	output LED loadY_o,	
	output dataSeg_en Display_1,			
	output dataSeg_en Display_2,
	output dataSeg_en Display_3, 
	output dataSeg_en Display_4,
	output dataSeg_en Display_5,			
	output dataSeg_en Display_6,
	output data16_t remainder_o
);


/*
*************************************************************************************
* Interconneciton wires/logic
*************************************************************************************
*/
logic new_clk_l;
logic Q0_sel_l;
logic SignxOr_l;
logic Regsiter_X_en;
logic Regsiter_Y_en;

PB start_l;
PB load_l;

Control_flag Wire_Control_counter;
Control_flag Wire_Counter_control;
Control_flag Wire_Reset;
Control_flag Wire_Control_A_flag;
Control_flag Wire_Control_Q_flag;
Control_flag Wire_Control_Q0_flag;
Control_flag Wire_Control_ASR;
Control_flag Wire_Control_Mtr_flag;
Control_flag Wire_Control_Reg_flag;
Control_flag wire_control_Xreg_flag_l;
Control_flag wire_control_Yreg_flag_l;
Control_flag wire_error_flag;

data2_t OP_l;
dataN_t A_fromAdder_l;
dataN_t A_result_High_l;
dataN_t Q_result_Low_l;
dataN_t Mux_result_Low_l;
dataN_t B_l;
dataN_t comp2_l;		
dataN_t Regsiter_X_l;				
dataN_t Regsiter_X_sign;		    		
dataN_t Regsiter_Y_l;				
dataN_t Regsiter_Y_sign;


/*
*************************************************************************************
* Instances definition  
*************************************************************************************
*/

CLK_GEN new_clk
(
	.clk_FPGA(clk_FPGA),
	.reset(reset),
	.clk_salida(new_clk_l)
	
);

OP_register OP_Reg
(
	.clk(new_clk_l),
	.reset(reset),
	.enable(start_l),			//Debouncer Start enable
	.SW(OP_in),
	.OP_o(OP_l)	
);

one_shot start_debouncer(
	.clk(new_clk_l),
	.reset(reset),
	.Start(start_in),
	.Shot(start_l)
);

one_shot load_debouncer(
	.clk(new_clk_l),
	.reset(reset),
	.Start(load_in),
	.Shot(load_l)
);

Control_unit CU
(
	.clk(new_clk_l),
	.rst(reset),
	.start_in(start_l),	
	.load_in(load_l),
	.OP_in(OP_l),	
	.Flag_counter(Wire_Counter_control),	
	.A_flag(Wire_Control_A_flag),
	.Q_flag(Wire_Control_Q_flag),
	.Q0_flag(Wire_Control_Q0_flag),
	.Mtr_selec_in(Wire_Control_Mtr_flag),
	.A_reg_selec_in(Wire_Control_Reg_flag),	
	.ASR_flag(Wire_Control_ASR),
   .Ready_flag(ready_o),	  
	.Enable_count(Wire_Control_counter),
	.Xreg_flag(wire_control_Xreg_flag_l),
	.Yreg_flag(wire_control_Yreg_flag_l)

);

Counter_SM CC
(
	// Input Ports
	.clk(new_clk_l),
	.reset(reset),
	.enable(Wire_Control_counter),
	.OP_in(OP_l),
	
	// Output Ports
	.flag(Wire_Counter_control)
);

Complement_Neg comp2
(
	.complement_flag_in(Switches_in[15]),
	.data_in(Switches_in),
	.comp2_o(comp2_l)
);

xclusive_AND AND_Xreg
(
	.a_in(load_l),
	.b_in(wire_control_Xreg_flag_l),
	.and_o(Regsiter_X_en)
);

Sign_N_register Regsiter_X
(
	.clk(new_clk_l),
	.rst(reset),
	.en(Regsiter_X_en),								
	.MSB_in(Switches_in[15]),		    						
	.data_in(comp2_l),		    		
	.data_o(Regsiter_X_l),					
	.sign_o(Regsiter_X_sign)					
	
);

xclusive_AND AND_Yreg
(
	.a_in(load_l),
	.b_in(wire_control_Yreg_flag_l),
	.and_o(Regsiter_Y_en)
);

Sign_N_register Regsiter_Y
(
	.clk(new_clk_l),
	.rst(reset),
	.en(Regsiter_Y_en),								
	.MSB_in(Switches_in[15]),		    						
	.data_in(comp2_l),		    		
	.data_o(Regsiter_Y_l),					
	.sign_o(Regsiter_Y_sign)					
	
);

processing_unit PU
(
	.clk(new_clk_l),
	.rst(reset),	
	.registerX_in(Regsiter_X_l),							// Q <- Multiplier
	.registerY_in(Regsiter_Y_l),							// M <- Multiplicand
	.en_mul_A(Wire_Control_A_flag), 	
	.en_mul_Q(Wire_Control_Q_flag),	
	.en_mul_q0(Wire_Control_Q0_flag),								
	.ASR_flag_in(Wire_Control_ASR),						// Shift Control
	.mul_mux_selector_in(Wire_Control_Mtr_flag),		// MUX control
	.A_reg_selector_in(Wire_Control_Reg_flag),		// MUX control
	.Result_H_o(A_result_High_l),							// Result_H <- A
	.Result_L_o(Q_result_Low_l)							// Result_L <- Q
);

xclusive_or SignXor(
	.a_in(Regsiter_X_sign),				//Defined in booth algorithm
	.b_in(Regsiter_Y_sign),
	.xOr_o(SignxOr_l)
);

DISPLAY_data Segments (
	.Switches_in(comp2_l),
	.result_in(Q_result_Low_l),
	.ready_in(ready_o),	
	.sign_sw_in(Switches_in[15]), 		
	.sign_xor_in(SignxOr_l),
	.error_flag(wire_error_flag),	
	.Display_1(Display_1),			
	.Display_2(Display_2),
	.Display_3(Display_3),			
	.Display_4(Display_4),					
	.Display_5(Display_5),			
	.Display_6(Display_6)	
);

Error_Unit Error_flag_unit
(
	// Input Ports
	.reset(reset),
	.clk(new_clk_l),
	.Result_mul(A_result_High_l),
	.Selec_op(OP_l),
	.Register_y(Regsiter_Y_en),
	.sign_root(),
	
	// Output Ports
	.Flag_error(wire_error_flag)
	
);



assign remainder_o = A_result_High_l;
assign loadX_o = wire_control_Xreg_flag_l;
assign loadY_o = wire_control_Yreg_flag_l;

endmodule
/*
****************************************************************
* This module defines the shift left operation for R
****************************************************************
*/

module temp2
import nBitsPackage::*;
(
	// Input Ports
	input data16_t R_in,			// R input from adder	
	input logic control,
	// Output Ports
	output data16_t temp2_o

);

/*
********************************
* Interconneciton wires/logic
********************************
*/


logic_enum_state_t state; /* DEFINICIÓN DE ESTADOS DE LA MÁQUINA */  
data16_t shift_l;


/*
********************************
* Combinational operations
********************************
*/


always @(*)	begin: Shift
	if (control == TRUE) 
		shift_l = R_in << (2);	
	else
		shift_l = ZERO16;
	
end: Shift// end always

assign temp2_o = shift_l;

endmodule
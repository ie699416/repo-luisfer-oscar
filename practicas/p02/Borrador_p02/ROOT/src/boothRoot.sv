/*
****************************************************************
* This module defines de top-block for a booth multiplication
****************************************************************
*/
module boothRoot

import nBitsPackage::*;

(
	// inputs 
	input cclk,
	input rst,
	input data16_t X_in,					// D <- Data from X register	
	input dataN_t n_in,
	input root_con_t control,			// idle -> load -> Shift (multiply)-> ready
	
	// outputs					
	output data16_t result_o,			// Result <- Q 
	output data16_t remainder_o		// Remainder <- R
	
);

/*
********************************
* Interconneciton wires/logic
********************************
*/

data16_t R_temp_l;
data16_t M_fromTemp3_l;			//temp value for adder, internal two's complement combinational logic
data17_t R_fromAdder17_l;


R_Operation 
newR
(
	.clk(cclk),
	.reset(rst),
	.control(control),
	.R_in(R_fromAdder17_l[N-1:0]),			
	.D_in(X_in),
	.n_in(n_in),
	.R_temp_o(R_temp_l),			//R previous add operation
	.remainder_o(remainder_o)  //R previous add operation
);

Q_Operation 
newQ
(
	.clk(cclk),
	.reset(rst),
	.control(control),
	.R15_in(R_fromAdder17_l[N-1]),
	.Q_o(result_o)
);

temp3
M
(
	.Q_in(result_o),			
	.R15_in(R_temp_l[13]),			
	.temp3_o(M_fromTemp3_l)

);



adder17 
adder
(
	.A_fromShiftRegister_in({1'b0,R_temp_l}),	// R register equivalent to A 
	.M_in({1'b0,M_fromTemp3_l}),					
	.A_fromAdder_o(R_fromAdder17_l)
	
);

endmodule 
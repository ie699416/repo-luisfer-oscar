
module adder17
import nBitsPackage::*;
(
	input data17_t A_fromShiftRegister_in,
	input data17_t M_in,
	output data17_t A_fromAdder_o
);

data17_t adder_l;
always_comb begin: adder

	adder_l = A_fromShiftRegister_in + M_in;

end: adder

assign A_fromAdder_o = adder_l;
endmodule 
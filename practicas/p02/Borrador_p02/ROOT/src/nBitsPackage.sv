
`ifndef MYPACKAGE_DONE
   `define MYPACKAGE_DONE
	 package nBitsPackage; // package name

		parameter N = 16;
		parameter LSB_MASK = 2'b11;
		parameter ZERO16 = {N{1'b0}};
		parameter ZERO17 = {(N+1){1'b0}};		
		parameter ZERO32 = {(2*N){1'b0}};
		parameter ZERO33 = {(2*N+1){1'b0}};	
		
		typedef logic [($clog2(N)):0] dataN_t;	//Ceil log operation for bus width	
		typedef logic [($clog2(2*N)):0] dataNN_t;	//Ceil log operation for double bus width	
	   typedef logic [N-1:0] data16_t;
		typedef logic [N:0] data17_t;
		typedef logic [2*N-1:0] data32_t;
		typedef logic [2*N:0] data33_t;				
		typedef logic [1:0] data2_t;	
		typedef logic [1:0] mul_con_t;		
		typedef logic [1:0] div_con_t;
		typedef logic [1:0] root_con_t;
		typedef enum logic [1:0] {IDLE, LOAD, SHIFT, READY} logic_enum_state_t;
		typedef enum logic [1:0] {Waiting_Shot, Shot_State, Waiting_Not_Shot} logic_enum_shot_t;
		typedef enum logic [4:0] {zero_s, one_s, two_s, three_s, four_s, five_S, six_s, seven_s} shift_enum_t;
		
	endpackage
	`endif
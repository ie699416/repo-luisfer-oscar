
/*
****************************************************************
* This module defines the tb
****************************************************************
*/


module boothRoot_tb;
import nBitsPackage::*;



/*
********************************
* Interconneciton wires/logic
********************************
*/



	reg clk_top=1'b1;
	reg reset_top;			//limpiar asincrono
	reg start = 1'b1;	
	dataN_t nn_l;
	data16_t data;
	data16_t remainder_l;
	data16_t result_l;
	logic_enum_state_t tb_state;


/*
*********************
* Modules instances
*********************
*/


boothRoot 
DUT
(
	.cclk(clk_top),
	.rst(reset_top),
	.X_in(data),						// D <- Data from X register	
	.n_in(nn_l),
	.control(tb_state),				// idle -> load -> Shift (multiply)-> ready
	.result_o(result_l),				// Result <- Q 
	.remainder_o(remainder_l)		// Remainder <- R
	
);


/*********************************************************/
initial // Clock generator
  begin
    forever #2 clk_top = !clk_top;
  end
/*********************************************************/
initial begin // reset generator
	data = 30000;
	nn_l = 5'b10000;
	tb_state = IDLE;
	
	#0 reset_top = 1'b0;
	
	#4 reset_top = 1'b1;

	#4 tb_state = LOAD;
	
	#4 tb_state = SHIFT;
	
	#4 nn_l = 5'b01111;
	
	#4 nn_l = 5'b01110;
	
	#4 nn_l = 5'b01101;
	
	#4 nn_l = 5'b01100;
	
	#4 nn_l = 5'b01011;
	
	#4 nn_l = 5'b01010;
	
	#4 nn_l = 5'b01001;
	
	#4 nn_l = 5'b01000;
	
	#4 nn_l = 5'b00111;
	
	#4 nn_l = 5'b00110;
	
	#4 nn_l = 5'b00101;
	
	#4 nn_l = 5'b00100;
	
	#4 nn_l = 5'b00011;
	
	#4 nn_l = 5'b00010;
	
	#4 nn_l = 5'b00001;
	
	#4 nn_l = 5'b00000;

	#12 tb_state = READY;

	
end

/*********************************************************/


endmodule
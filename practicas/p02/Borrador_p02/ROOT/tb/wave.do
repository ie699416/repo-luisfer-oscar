onerror {resume}
quietly WaveActivateNextPane {} 0
add wave -noupdate /boothRoot_tb/clk_top
add wave -noupdate /boothRoot_tb/reset_top
add wave -noupdate -radix unsigned /boothRoot_tb/nn_l
add wave -noupdate /boothRoot_tb/data
add wave -noupdate -color Violet /boothRoot_tb/DUT/result_o
add wave -noupdate -color Violet /boothRoot_tb/DUT/remainder_o
add wave -noupdate /boothRoot_tb/tb_state
add wave -noupdate -color {Spring Green} /boothRoot_tb/DUT/newR/temp1_l
add wave -noupdate -color {Forest Green} /boothRoot_tb/DUT/newR/D_SR_and_3/dataShift_l
add wave -noupdate -color {Spring Green} /boothRoot_tb/DUT/newR/temp2_l
add wave -noupdate -color {Spring Green} /boothRoot_tb/DUT/newR/R_in
add wave -noupdate -color {Spring Green} /boothRoot_tb/DUT/newR/R_temp_o
add wave -noupdate /boothRoot_tb/DUT/newR/remainder_o
add wave -noupdate -color Cyan /boothRoot_tb/DUT/newQ/R15_in
add wave -noupdate -color Cyan /boothRoot_tb/DUT/newQ/Q_o
add wave -noupdate /boothRoot_tb/DUT/M/temp3_o
add wave -noupdate -color Gold /boothRoot_tb/DUT/adder/A_fromShiftRegister_in
add wave -noupdate -color Gold /boothRoot_tb/DUT/M/R15_in
add wave -noupdate -color Gold /boothRoot_tb/DUT/adder/M_in
add wave -noupdate -color Gold /boothRoot_tb/DUT/adder/A_fromAdder_o
TreeUpdate [SetDefaultTree]
WaveRestoreCursors {{Cursor 1} {2 ps} 0}
quietly wave cursor active 1
configure wave -namecolwidth 293
configure wave -valuecolwidth 100
configure wave -justifyvalue left
configure wave -signalnamewidth 0
configure wave -snapdistance 10
configure wave -datasetprefix 0
configure wave -rowmargin 4
configure wave -childrowmargin 2
configure wave -gridoffset 0
configure wave -gridperiod 1
configure wave -griddelta 40
configure wave -timeline 0
configure wave -timelineunits ps
update
WaveRestoreZoom {83 ps} {101 ps}

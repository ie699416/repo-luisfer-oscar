`timescale 1ps / 1ps
module tb_P02;
import nBitsPackage::*;
	// inputs for the control unit
	logic clk_FPGA_l = HIGH;
	logic reset_l = LOW;
	data2_t OP_l = 2'b00;
	dataN_t SW_l;
	dataN_t remainder_l;
	LED ready_l;
	PB start_l = LOW;				// Signal that determines the Start of the proyect, by a push botton
	PB load_l = LOW;				// Signal that determines the Start of the proyect, by a push botton
	
	// outputs from Control Unit						
	 dataSeg_en Display_1_l;			
	 dataSeg_en Display_2_l;
	 dataSeg_en Display_3_l; 
	 dataSeg_en Display_4_l;
	 dataSeg_en Display_5_l;			
	 dataSeg_en Display_6_l;


P02 P02_test
(
	.clk_FPGA(clk_FPGA_l),
	.reset(reset_l),
	.OP_in(OP_l),
	.Switches_in(SW_l),
	.start_in(start_l),		
	.load_in(load_l),	
	.ready_o(ready_l),	
	.Display_1(Display_1_l),			
	.Display_2(Display_2_l),
	.Display_3(Display_3_l), 
	.Display_4(Display_4_l),
	.Display_5(Display_5_l),			
	.Display_6(Display_6_l),
	.remainder_o(remainder_l)
);


/*********************************************************/
initial // Clock generator
  begin
    forever #1 clk_FPGA_l = !clk_FPGA_l;
  end
/*********************************************************/
initial begin // reset generator

#0 SW_l = 69;

#40 reset_l = HIGH;

#20 start_l = HIGH;

#200 load_l = HIGH;

#20 load_l = LOW;

#50 SW_l = 22;

#200 load_l = HIGH;

#20 load_l = LOW;




	
end

/*********************************************************/


endmodule



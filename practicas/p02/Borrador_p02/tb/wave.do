onerror {resume}
quietly WaveActivateNextPane {} 0
add wave -noupdate -radix decimal /tb_P02_borrador/P02_test/Segments/Switches_in
add wave -noupdate -radix decimal /tb_P02_borrador/P02_test/Segments/result_in
add wave -noupdate /tb_P02_borrador/P02_test/Segments/ready_in
add wave -noupdate /tb_P02_borrador/P02_test/Segments/Display_1
add wave -noupdate /tb_P02_borrador/P02_test/Segments/Display_2
add wave -noupdate /tb_P02_borrador/P02_test/Segments/Display_3
add wave -noupdate /tb_P02_borrador/P02_test/Segments/Display_4
add wave -noupdate /tb_P02_borrador/P02_test/Segments/Display_5
add wave -noupdate /tb_P02_borrador/P02_test/Segments/Display_6
add wave -noupdate /tb_P02_borrador/P02_test/OP_Reg/clk
add wave -noupdate /tb_P02_borrador/P02_test/OP_Reg/reset
add wave -noupdate /tb_P02_borrador/P02_test/OP_Reg/enable
add wave -noupdate /tb_P02_borrador/P02_test/OP_Reg/OP_l
add wave -noupdate /tb_P02_borrador/P02_test/CU/start_in
add wave -noupdate /tb_P02_borrador/P02_test/CU/load_in
add wave -noupdate /tb_P02_borrador/P02_test/CU/Flag_counter
add wave -noupdate /tb_P02_borrador/P02_test/CU/ASR_flag
add wave -noupdate /tb_P02_borrador/P02_test/CU/A_flag
add wave -noupdate /tb_P02_borrador/P02_test/CU/Q_flag
add wave -noupdate /tb_P02_borrador/P02_test/CU/Q0_flag
add wave -noupdate /tb_P02_borrador/P02_test/CU/Mtr_selec_in
add wave -noupdate /tb_P02_borrador/P02_test/CU/A_reg_selec_in
add wave -noupdate /tb_P02_borrador/P02_test/CU/Ready_flag
add wave -noupdate /tb_P02_borrador/P02_test/CU/Enable_count
add wave -noupdate /tb_P02_borrador/P02_test/CU/Xreg_flag
add wave -noupdate /tb_P02_borrador/P02_test/CU/Yreg_flag
add wave -noupdate /tb_P02_borrador/P02_test/CU/Actual_state
add wave -noupdate /tb_P02_borrador/P02_test/Regsiter_Y/en
add wave -noupdate -radix decimal /tb_P02_borrador/P02_test/Regsiter_Y/data_o
add wave -noupdate /tb_P02_borrador/P02_test/Regsiter_Y/sign_o
add wave -noupdate /tb_P02_borrador/P02_test/Regsiter_X/en
add wave -noupdate -radix decimal /tb_P02_borrador/P02_test/Regsiter_X/data_o
add wave -noupdate /tb_P02_borrador/P02_test/Regsiter_X/sign_o
TreeUpdate [SetDefaultTree]
WaveRestoreCursors {{Cursor 1} {3136 ps} 0}
quietly wave cursor active 1
configure wave -namecolwidth 389
configure wave -valuecolwidth 100
configure wave -justifyvalue left
configure wave -signalnamewidth 0
configure wave -snapdistance 10
configure wave -datasetprefix 0
configure wave -rowmargin 4
configure wave -childrowmargin 2
configure wave -gridoffset 0
configure wave -gridperiod 1
configure wave -griddelta 40
configure wave -timeline 0
configure wave -timelineunits ps
update
WaveRestoreZoom {0 ps} {4590 ps}

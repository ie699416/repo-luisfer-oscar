

module Mux_result_error
import nBitsPackage::*;

(
	// Input Ports
	input dataSeg_en data0_in,
	input dataSeg_en data1_in,
	input sel,

	// Output Ports
	output dataSeg_en data_o

);


/*
*************************************************************************************
* Combiational processes 
*************************************************************************************
*/


always_comb
begin
	if (sel == TRUE)
		data_o = data1_in;
	else
		data_o = data0_in;
end


endmodule
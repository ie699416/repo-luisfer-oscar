/*
*************************************************************************************
*	Coder:	OSCAR MAISTERRA FLORES	 		[ ie699416]
*	Coder: 	LUIS FERNANDO MORENO ALONSO	[ ie693314] 
*	Date:          Jun 6th, 2019
*  Name:          BCD_16.sv
*	Description:   Top file for BCD hw #2. The module test interconects the BCD 
*						decoder and the 7 segment modules.
*************************************************************************************
*/

module BCD_16

import nBitsPackage::*;
(
	// Input Ports
	input data16_t A_in,
	input sign_in, 
	output dataSeg_en sign_o,	
	output dataSeg_en Dec_Mill_o,
	output dataSeg_en Uni_Mill_o,	
	output dataSeg_en Cen_o,
	output dataSeg_en Dec_o,
	output dataSeg_en Uni_o

);

/*
*************************************************************************************
* Interconneciton wires/logic
*************************************************************************************
*/

decoder_en Dec_Mill_l;
decoder_en Uni_Mill_l;
decoder_en Cen_l;
decoder_en Dec_l;
decoder_en Uni_l;


/*
*************************************************************************************
* Instances definition  
*************************************************************************************
*/


DD_BCD decoder
(
	.A_in(A_in), 		// 16 bit input,
	.Uni_Mill_o(Uni_Mill_l),
	.Dec_Mill_o(Dec_Mill_l),
	.Cen_o(Cen_l),
	.Dec_o(Dec_l),
	.Uni_o(Uni_l)
);

Conv_BCD_7seg_sign Sign
(
	.sign_in(sign_in),
	.segments_o(sign_o)
);

Conv_BCD_7seg Dec_Mill
(
	.BCD_in(Dec_Mill_l),
	.segments_o(Dec_Mill_o)
);

Conv_BCD_7seg Uni_Mill
(
	.BCD_in(Uni_Mill_l),
	.segments_o(Uni_Mill_o)
);

Conv_BCD_7seg Cen
(
	.BCD_in(Cen_l),
	.segments_o(Cen_o)
);

Conv_BCD_7seg Dec
(
	.BCD_in(Dec_l),
	.segments_o(Dec_o)
);

Conv_BCD_7seg Uni
(
	.BCD_in(Uni_l),
	.segments_o(Uni_o)
);

endmodule
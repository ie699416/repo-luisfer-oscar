/*
****************************************************************
* This module defines the n-shift operation for temp 1
****************************************************************
*/

module temp1
import nBitsPackage::*;
(
	// Input Ports
	input data16_t D_in,			// Data input from X PIPO register
	input dataCount_t n_in,		// Iterative counter for shift
	
	// Output Ports
	output data2_t temp1_o

);

/*
********************************
* Interconneciton wires/logic
********************************
*/

shift_enum_t nCase;

/*
********************************
* Combinational operation
********************************
*/


always @(*) begin: shift_case
	case (n_in)
		ZERO_U:
			begin
				temp1_o = D_in[15:14]; // dataShift_l = D_in >> 1'b0;
			end
		ONE_U:
			begin
				temp1_o = D_in [13:12];
			end
		TWO_U:
			begin
				temp1_o = D_in [11:10];
			end
		THREE_U:
			begin
				temp1_o = D_in [9:8];
			end
		FOUR_U:
			begin
				temp1_o = D_in [7:6];
			end
		FIVE_U:
			begin
				temp1_o = D_in [5:4];
			end
		SIX_U:
			begin
				temp1_o = D_in [3:2];
			end
		SEVEN_U:
			begin
				temp1_o = D_in [1:0];
			end
		default:
			begin
				temp1_o = ZERO2; 
			end
	endcase
	

end: shift_case// end always


endmodule

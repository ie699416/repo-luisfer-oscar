/*
*************************************************************************************
*	Coder:	OSCAR MAISTERRA FLORES	 		[ ie699416]
*	Coder: 	LUIS FERNANDO MORENO ALONSO	[ ie693314] 
*	Date:          Jun 22th, 2019
*  Name:          Eror_Unit.sv
*  Description:   Enables a flag in case there is an error in the operation
*
*************************************************************************************
*/
module Error_Unit
import nBitsPackage::*;
(
	// Input Ports
	input reset,
	input clk,
	input dataN_t Result_mul,
	input data2_t Selec_op,
	input dataN_t Register_y,
	input logic sign_root,
	
	// Output Ports
	output logic Flag_error
	
);

logic error_flag;

always_ff@(posedge clk or negedge reset) 
	begin: Operation
		if (reset == FALSE)
			error_flag <= FALSE; 	//MUL default
		else
			begin
				case(Selec_op)
						MUL:begin
								if(Result_mul != ZERON)
									error_flag <= TRUE;
								else
									error_flag <= FALSE;
							end	
						DIV:
								if(Register_y == ZERO16)
									error_flag <= TRUE;
								else
									error_flag <= FALSE;
						ROOT:
								if(sign_root == UNIT)
									error_flag <= CERO;
								else
									error_flag <= FALSE;
						NA:
							error_flag <= FALSE;
							
						default: error_flag <= FALSE;
				endcase
			
			end
	end: Operation
	
assign Flag_error = error_flag;
	
endmodule
	
	
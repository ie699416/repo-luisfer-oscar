/*
*************************************************************************************
*	Coder:	OSCAR MAISTERRA FLORES	 		[ ie699416]
*	Coder: 	LUIS FERNANDO MORENO ALONSO	[ ie693314] 
*	Date : 	June 8th, 2019
*  Name :   xclusive_or.sv  
*
*	Description: 	 
*		 This module operates the logical xOr operation between single bits.
* 
*************************************************************************************
*/

module ena_or
(
	input a_in,
	input b_in,
	output or_o
);

assign or_o = a_in | b_in ;
endmodule 
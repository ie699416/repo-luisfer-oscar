/*
****************************************************************
* This module defines de top-block for the practice 
****************************************************************
*/
module Control_unit
import nBitsPackage::*;
(
	// inputs for the control unit
	input clk,
	input rst,
	input PB start_in,		
	input PB load_in,	
	input data2_t OP_in,	
	input Control_flag Flag_counter,			//This flag determines when the counter finished counting 
	
	// outputs from Control Unit						
	output Control_flag ASR_flag,				//Signal that sends the start process for the multiplication process 
	output Control_flag H_flag,	
	output Control_flag L_flag,
	output Control_flag AU_flag,
	output Control_flag Q_reg_selector_flag,
	output Control_flag A_reg_selector_flag,
	output Control_flag Ready_flag,	   	//Signal that indicates the state of the process 
	output Control_flag Enable_count,			//Signal that enables the counter
	output Control_flag Xreg_flag,
	output Control_flag Yreg_flag,
	output Control_flag flush_H_flag,			//Signal that enables the counter
	output Control_flag flush_L_flag,
	output Control_flag flush_AU_flag
);


State_e Actual_state;	

always_ff@(posedge clk or negedge rst) // Circuito Secuenicial en un proceso always.
	begin
		if (rst == FALSE)
			Actual_state <= STATE_IDLE;			
		else 
			begin		
				case(Actual_state)				
						STATE_IDLE:
							begin 						
								if (start_in == TRUE && (decoder_en'(OP_in)) != NA )
										Actual_state <= STATE_LOAD_X; 									
								else 
										Actual_state <= STATE_IDLE; 							
							end							
						STATE_LOAD_X: 
							begin
								if (load_in == TRUE && (decoder_en'(OP_in)) == ROOT )
									Actual_state <= STATE_SET;
								else if (load_in == TRUE && ((decoder_en'(OP_in)) == MUL || (decoder_en'(OP_in)) == DIV) )
									Actual_state <= STATE_LOAD_Y;
								else 
									Actual_state <= STATE_LOAD_X;
							end
						
						STATE_LOAD_Y: 
							begin
								if (load_in == TRUE)
									Actual_state <= STATE_SET;
								else 
									Actual_state <= STATE_LOAD_Y;
							end
							
						STATE_SET: 
								Actual_state <= STATE_PROCESS;
								
						STATE_PROCESS:
							begin 						
								if (Flag_counter==TRUE)
									Actual_state<=STATE_PREREADY;
								else 
									Actual_state<=STATE_PROCESS;
							end
							
						STATE_PREREADY:
							Actual_state <= STATE_READY;
							
						STATE_READY:
							begin 						
								if (start_in==TRUE)
									Actual_state<=STATE_FLUSH;
								else 
									Actual_state<=STATE_READY;							
							end
							
						STATE_FLUSH:
							begin 						
								Actual_state<=STATE_LOAD_X;						
							end
						
							
						default: Actual_state<=STATE_IDLE;
							
				 endcase	
			end
	end

// CTO combinacional de salida.
always_comb 
	begin
		case(Actual_state)
			 STATE_IDLE:
				 begin 
					Ready_flag = LOW;
					H_flag = LOW;
					L_flag = LOW;
					AU_flag = LOW;
					Q_reg_selector_flag = HIGH;					
					A_reg_selector_flag = LOW;
					ASR_flag = LOW; 
					Enable_count = LOW;
					Xreg_flag = LOW;
					Yreg_flag = LOW;
					flush_H_flag = LOW;
					flush_L_flag = LOW;
					flush_AU_flag = LOW;
				 end
					
			STATE_LOAD_X:
				begin
					case(OP_en'(OP_in))
						MUL:
							begin
								H_flag = LOW;
								L_flag = HIGH;
								AU_flag = LOW;								
								A_reg_selector_flag = HIGH;
								Q_reg_selector_flag = HIGH;
								Ready_flag = LOW;
								ASR_flag = LOW; 
								Enable_count = LOW;
								Xreg_flag = HIGH;
								Yreg_flag = LOW;
								flush_H_flag = LOW;
								flush_L_flag = LOW;
								flush_AU_flag = LOW;
							end
						DIV:
							begin								
								H_flag = LOW;
								L_flag = LOW;
								AU_flag = LOW;								
								A_reg_selector_flag = HIGH;
								Q_reg_selector_flag = HIGH;
								Ready_flag = LOW;
								ASR_flag = LOW; 
								Enable_count = LOW;
								Xreg_flag = HIGH;
								Yreg_flag = LOW;
								flush_H_flag = LOW;
								flush_L_flag = LOW;
								flush_AU_flag = LOW;
							end
						ROOT: 						
							begin
								H_flag = LOW;
								L_flag = LOW;
								AU_flag = LOW;			
								A_reg_selector_flag = LOW;
								Q_reg_selector_flag = LOW;
								Ready_flag = LOW;
								ASR_flag = LOW; 
								Enable_count = LOW;
								Xreg_flag = HIGH;
								Yreg_flag = LOW;
								flush_H_flag = LOW;
								flush_L_flag = LOW;
								flush_AU_flag = LOW;;
							end
						NA: 					
							begin
								H_flag = LOW;
								L_flag = LOW;
								AU_flag = LOW;				
								A_reg_selector_flag = LOW;
								Q_reg_selector_flag = LOW;
								Ready_flag = LOW;
								ASR_flag = HIGH; 
								Enable_count = LOW;
								Xreg_flag = LOW;
								Yreg_flag = LOW;
								flush_H_flag = LOW;
								flush_L_flag = LOW;
								flush_AU_flag = LOW;
							end
						default:					
							begin
								H_flag = HIGH;
								L_flag = HIGH;
								AU_flag = HIGH;
								A_reg_selector_flag = LOW;
								Q_reg_selector_flag = LOW;
								Ready_flag = LOW;
								ASR_flag = HIGH; 
								Enable_count = HIGH;
								Xreg_flag = LOW;
								Yreg_flag = LOW;
								flush_H_flag = LOW;
								flush_L_flag = LOW;
								flush_AU_flag = LOW;
							end				
					 endcase		
				end				
			 STATE_LOAD_Y:
				begin
					case(OP_en'(OP_in))
						MUL:
							begin
								H_flag = LOW;
								L_flag = HIGH;
								AU_flag = LOW;
								A_reg_selector_flag = HIGH;
								Q_reg_selector_flag = HIGH;
								Ready_flag = LOW;
								ASR_flag = LOW; 
								Enable_count = LOW;
								Xreg_flag = LOW;
								Yreg_flag = HIGH;
								flush_H_flag = LOW;
								flush_L_flag = LOW;
								flush_AU_flag = LOW;
							end
						DIV:
							begin
								H_flag = LOW;
								L_flag = LOW;
								AU_flag = LOW;
								A_reg_selector_flag = HIGH;
								Q_reg_selector_flag = HIGH;
								Ready_flag = LOW;
								ASR_flag = LOW; 
								Enable_count = LOW;
								Xreg_flag = LOW;
								Yreg_flag = HIGH;
								flush_H_flag = LOW;
								flush_L_flag = LOW;
								flush_AU_flag = LOW;
							end
						ROOT: 						
							begin
								H_flag = LOW;
								L_flag = LOW;
								AU_flag = LOW;
								A_reg_selector_flag = LOW;
								Q_reg_selector_flag = LOW;
								Ready_flag = LOW;
								ASR_flag = LOW; 
								Enable_count = LOW;
								Xreg_flag = LOW;
								Yreg_flag = HIGH;
								flush_H_flag = LOW;
								flush_L_flag = LOW;
								flush_AU_flag = LOW;
							end
						NA: 					
							begin
								H_flag = LOW;
								L_flag = LOW;
								AU_flag = HIGH;
								A_reg_selector_flag = HIGH;
								Q_reg_selector_flag = HIGH;
								Ready_flag = LOW;
								ASR_flag = HIGH; 
								Enable_count = LOW;
								Xreg_flag = LOW;
								Yreg_flag = LOW;
								flush_H_flag = LOW;
								flush_L_flag = LOW;
								flush_AU_flag = LOW;
							end
						default:					
							begin
								H_flag = LOW;
								L_flag = LOW;
								AU_flag = HIGH;
								A_reg_selector_flag = HIGH;
								Q_reg_selector_flag = HIGH;
								Ready_flag = LOW;
								ASR_flag = HIGH; 
								Enable_count = LOW;
								Xreg_flag = LOW;
								Yreg_flag = LOW;
								flush_H_flag = LOW;
								flush_L_flag = LOW;
								flush_AU_flag = LOW;
							end				
					 endcase
				end
				
			 STATE_SET:			 
			 	begin
					case(OP_en'(OP_in))
						MUL:
							begin								
								H_flag = LOW;
								L_flag = HIGH;
								AU_flag = LOW;
								A_reg_selector_flag = HIGH;
								Q_reg_selector_flag = HIGH;
								Ready_flag = LOW;
								ASR_flag = LOW; 
								Enable_count = LOW;
								Xreg_flag = LOW;
								Yreg_flag = LOW;
								flush_H_flag = LOW;
								flush_L_flag = LOW;
								flush_AU_flag = LOW;
							end
						DIV:
							begin
								H_flag = HIGH;
								L_flag = LOW;
								AU_flag = LOW;
								A_reg_selector_flag = LOW;
								Q_reg_selector_flag = HIGH;
								Ready_flag = LOW;
								ASR_flag = LOW; 
								Enable_count = LOW;
								Xreg_flag = LOW;
								Yreg_flag = LOW;
								flush_H_flag = LOW;
								flush_L_flag = LOW;
								flush_AU_flag = LOW;
							end
						ROOT: 						
							begin
								H_flag = HIGH;
								L_flag = HIGH;
								AU_flag = LOW;
								A_reg_selector_flag = HIGH;
								Q_reg_selector_flag = HIGH;
								Ready_flag = LOW;
								ASR_flag = LOW; 
								Enable_count = LOW;
								Xreg_flag = LOW;
								Yreg_flag = LOW;
								flush_H_flag = LOW;
								flush_L_flag = LOW;
								flush_AU_flag = LOW;
							end
						NA: 					
							begin
								H_flag = HIGH;
								L_flag = HIGH;
								AU_flag = HIGH;
								A_reg_selector_flag = HIGH;
								Q_reg_selector_flag = LOW;
								Ready_flag = LOW;
								ASR_flag = HIGH; 
								Enable_count = HIGH;
								Xreg_flag = LOW;
								Yreg_flag = LOW;
								flush_H_flag = LOW;
								flush_L_flag = LOW;
								flush_AU_flag = LOW;
							end
						default:					
							begin
								H_flag = HIGH;
								L_flag = HIGH;
								AU_flag = HIGH;
								A_reg_selector_flag = HIGH;
								Q_reg_selector_flag = LOW;
								Ready_flag = LOW;
								ASR_flag = HIGH; 
								Enable_count = HIGH;
								Xreg_flag = LOW;
								Yreg_flag = LOW;
								flush_H_flag = LOW;
								flush_L_flag = LOW;
								flush_AU_flag = LOW;
							end				
					 endcase
				end
			 
			 STATE_PROCESS:
				 begin
					case(OP_en'(OP_in))
						MUL:
							begin
								H_flag = HIGH;
								L_flag = HIGH;
								AU_flag = HIGH;
								Q_reg_selector_flag = LOW;					
								A_reg_selector_flag = HIGH;
								Ready_flag = LOW;
								ASR_flag = HIGH; 
								Enable_count = HIGH;
								Xreg_flag = LOW;
								Yreg_flag = LOW;
								flush_H_flag = LOW;
								flush_L_flag = LOW;
								flush_AU_flag = LOW;
							end
						DIV:
							begin
								H_flag = HIGH;
								L_flag = HIGH;
								AU_flag = HIGH;
								Q_reg_selector_flag = LOW;					
								A_reg_selector_flag = LOW;
								Ready_flag = LOW;
								ASR_flag = HIGH; 
								Enable_count = HIGH;
								Xreg_flag = LOW;
								Yreg_flag = LOW;
								flush_H_flag = LOW;
								flush_L_flag = LOW;
								flush_AU_flag = LOW;
							end
						ROOT: 						
							begin
								H_flag = HIGH;
								L_flag = HIGH;
								AU_flag = HIGH;
								Q_reg_selector_flag = HIGH;					
								A_reg_selector_flag = HIGH;
								Ready_flag = LOW;
								ASR_flag = HIGH; 
								Enable_count = HIGH;
								Xreg_flag = LOW;
								Yreg_flag = LOW;
								flush_H_flag = LOW;
								flush_L_flag = LOW;
								flush_AU_flag = LOW;
							end
						NA: 					
							begin
								H_flag = HIGH;
								L_flag = HIGH;
								AU_flag = HIGH;
								Q_reg_selector_flag = LOW;					
								A_reg_selector_flag = LOW;
								Ready_flag = LOW;
								ASR_flag = HIGH; 
								Enable_count = HIGH;
								Xreg_flag = LOW;
								Yreg_flag = LOW;
								flush_H_flag = LOW;
								flush_L_flag = LOW;
								flush_AU_flag = LOW;
							end
						default:					
							begin
								H_flag = HIGH;
								L_flag = HIGH;
								AU_flag = HIGH;
								Q_reg_selector_flag = LOW;					
								A_reg_selector_flag = HIGH;
								Ready_flag = LOW;
								ASR_flag = HIGH; 
								Enable_count = HIGH;
								Xreg_flag = LOW;
								Yreg_flag = LOW;
								flush_H_flag = LOW;
								flush_L_flag = LOW;
								flush_AU_flag = LOW;
							end				
					 endcase
				end
			 
			 STATE_PREREADY:				
				 begin
					case(OP_en'(OP_in))
						MUL:
							begin
								H_flag = LOW;
								L_flag = LOW;
								AU_flag = LOW;
								Q_reg_selector_flag = HIGH;					
								A_reg_selector_flag = HIGH;
								Ready_flag = LOW;
								ASR_flag = HIGH; 
								Enable_count = LOW;
								Xreg_flag = LOW;
								Yreg_flag = LOW;
								flush_H_flag = LOW;
								flush_L_flag = LOW;
								flush_AU_flag = LOW;
							end
						DIV:
							begin
								H_flag = LOW;
								L_flag = LOW;
								AU_flag = LOW;
								Q_reg_selector_flag = LOW;					
								A_reg_selector_flag = HIGH;
								Ready_flag = LOW;
								ASR_flag = HIGH; 
								Enable_count = LOW;
								Xreg_flag = LOW;
								Yreg_flag = LOW;
								flush_H_flag = LOW;
								flush_L_flag = LOW;
								flush_AU_flag = LOW;
							end
						ROOT: 						
							begin
								H_flag = LOW;
								L_flag = LOW;
								AU_flag = LOW;
								Q_reg_selector_flag = LOW;					
								A_reg_selector_flag = HIGH;
								Ready_flag = LOW;
								ASR_flag = HIGH; 
								Enable_count = LOW;
								Xreg_flag = LOW;
								Yreg_flag = LOW;
								flush_H_flag = LOW;
								flush_L_flag = LOW;
								flush_AU_flag = LOW;
							end
						NA: 					
							begin
								H_flag = LOW;
								L_flag = LOW;
								AU_flag = LOW;
								Q_reg_selector_flag = LOW;					
								A_reg_selector_flag = HIGH;
								Ready_flag = LOW;
								ASR_flag = HIGH; 
								Enable_count = LOW;
								Xreg_flag = LOW;
								Yreg_flag = LOW;
								flush_H_flag = LOW;
								flush_L_flag = LOW;
								flush_AU_flag = LOW;
							end
						default:					
							begin
								H_flag = LOW;
								L_flag = LOW;
								AU_flag = LOW;
								Q_reg_selector_flag = LOW;					
								A_reg_selector_flag = HIGH;
								Ready_flag = LOW;
								ASR_flag = HIGH; 
								Enable_count = LOW;
								Xreg_flag = LOW;
								Yreg_flag = LOW;
								flush_H_flag = LOW;
								flush_L_flag = LOW;
								flush_AU_flag = LOW;
							end				
					 endcase
				end
			 
			 STATE_READY:
				begin
					H_flag = LOW;
					L_flag = LOW;
					AU_flag = LOW;
					Q_reg_selector_flag = LOW;					
					A_reg_selector_flag = LOW;
					Ready_flag = HIGH;
					ASR_flag = LOW; 
					Enable_count = LOW;
					Xreg_flag = LOW;
					Yreg_flag = LOW;
					flush_H_flag = LOW;
					flush_L_flag = LOW;
					flush_AU_flag = LOW;
				end
				
			STATE_FLUSH:
				begin
					H_flag = LOW;
					L_flag = LOW;
					AU_flag = LOW;
					Q_reg_selector_flag = LOW;					
					A_reg_selector_flag = LOW;
					Ready_flag = LOW;
					ASR_flag = LOW; 
					Enable_count = LOW;
					Xreg_flag = LOW;
					Yreg_flag = LOW;
					flush_H_flag = HIGH;
					flush_L_flag = HIGH;
					flush_AU_flag = HIGH;
				end
				
				
			 default:
				begin
					H_flag = LOW;
					L_flag = LOW;
					AU_flag = LOW;
					Q_reg_selector_flag = LOW;					
					A_reg_selector_flag = LOW;
					Ready_flag = LOW;
					ASR_flag = LOW; 
					Enable_count = LOW;
					Xreg_flag = LOW;
					Yreg_flag = LOW;
					flush_H_flag = LOW;
					flush_L_flag = LOW;
					flush_AU_flag = LOW;
				end
	 
	endcase
end

endmodule

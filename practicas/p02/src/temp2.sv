/*
****************************************************************
* This module defines the shift left operation for R
****************************************************************
*/

module temp2
import nBitsPackage::*;
(
	// Input Ports
	input data16_t R_in,							// R input from adder	
	// Output Ports
	output data16_t temp2_o

);

/*
********************************
* Interconneciton wires/logic
********************************
*/
data16_t shift_l;


/*
********************************
* Combinational operations
********************************
*/

assign temp2_o = R_in << (2);

endmodule
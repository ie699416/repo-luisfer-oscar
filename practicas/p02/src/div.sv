module div 
import nBitsPackage::*;
(
	// inputs for multiplication SR register
	input clk,
	input rst,
	input flush_H_flag_in,
	input flush_L_flag_in,
	input flush_AU_flag_in,
	input en_H, 	
	input en_L,	
	input en_AU,	
	input counterFlag_in,
	input	ASR_flag_in,						// Shift Control
	input Q_reg_selector_in,			// MUX control
	input A_reg_selector_in,				// MUX control
	input dataN_t A_from_adder_in,		// After Adder
	input dataN_t dividend_in,				// Q <- Dividend
	input A33_in,
	
	//Output ports
	output AU_Flag_o,							// Q_o
	output dataN_t Result_H_o,				// Result_H <- A
	output dataN_t Result_L_o				// Result_L <- Q
	
);

/*
*************************************************************************************
* Interconneciton wires/logic
*************************************************************************************
*/

logic en_H_or_l;
logic ASR_L_flag_l;
dataN_t Q_from_mux_l;
dataN_t A_from_mux_l;

/*
*************************************************************************************
* Instances definition  
*************************************************************************************
*/


Booth_SR_register SR_AQq0(
	.clk(clk),
	.rst(rst),
	.flush_H_flag_in(flush_H_flag_in),
	.flush_L_flag_in(flush_L_flag_in),
	.flush_AU_flag_in(flush_AU_flag_in),
	.en_H(en_H), 	
	.en_L(en_L | AU_Flag_o ),	
	.en_AU(en_AU),
	.ASR_H_flag_in(ASR_flag_in),				
	.ASR_L_flag_in(ASR_L_flag_l),
	.Q0_inputBit_in(~A33_in),
	.AU_inputBit_in(A33_in),
	.RegisterQ_in(Q_from_mux_l),								// Q <- Dividend 
	.endianess_flag(HIGH),
	.A_fromAdder_in(A_from_mux_l),			// A <- A + M_reg	
	.LSB(),											// L_reg //A[N-1]
	.AU_selector(AU_Flag_o),
	.Result_H_o(Result_H_o),					// Result_H from A_reg	
	.Result_L_o(Result_L_o)						// Result_L from Q_reg
);



xclusive_AND AND
(
	.a_in(ASR_flag_in),
	.b_in(~counterFlag_in),
	.and_o(ASR_L_flag_l)
);



muxN_2_1 mux_Q
(
	.Selector(Q_reg_selector_in),
	.data0_in(Result_L_o),
	.data1_in(dividend_in), 
	.mux_o(Q_from_mux_l)  
);

muxN_2_1 mux_A
(
	.Selector(A_reg_selector_in),							// When HIGH store the adder result
	.data0_in(A_from_adder_in),
	.data1_in(ZERON),
	.mux_o(A_from_mux_l)  
);

endmodule

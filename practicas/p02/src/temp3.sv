/*
****************************************************************
* This module defines the 2 bits shift operation for temp 3
*	-	Also selects if the adder will receive a negative value
****************************************************************
*/

module temp3
import nBitsPackage::*;
(
	// Input Ports
	input dataN_t Q_in,			// Data input from X PIPO register
	input R15_in,					// Selector for adder
	
	// Output Ports
	output dataN_t temp3_o

);



assign temp3_o = {Q_in[N-1:2],(~R15_in),UNIT};

endmodule
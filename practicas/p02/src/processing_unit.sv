/*
*************************************************************************************
*	Coder:	OSCAR MAISTERRA FLORES	 		[ ie699416]
*	Coder: 	LUIS FERNANDO MORENO ALONSO	[ ie693314] 
*	Date:          Jun 22th, 2019
*  Name:          processing_unit.sv
*  Description:   
*	-	The processing unit includes the algorithims implementation for the MUL, DIV 
*		and ROOT modules. 
*	-	The module implements a MUX selection for the algorithim to work accordingly.
*	-	TODO: implement a shared 33 BIT register to work with efficency. 
*
*************************************************************************************
*/

module processing_unit 
import nBitsPackage::*;
(
	// inputs for multiplication SR register
	input clk,
	input rst,
	input	data2_t Sel_op,
	input flush_H_flag_in,
	input flush_L_flag_in,
	input flush_AU_flag_in,
	input dataN_t registerX_in,								// Q <- Multiplier
	input dataN_t registerY_in,								// M <- Multiplicand
	
	
	// MUL control input signals 	
	input en_H, 	
	input en_L,	
	input en_AU,	
	input counterFlag_in,
	input	ASR_flag_in,											// Shift Control
	input Q_reg_selector_in,									// MUX control
	input A_reg_selector_in,									// MUX control

	input dataCount_t n_in,	
	input countFlag_in,	
	
	//Output ports
	
	output dataN_t Result_H_o,									// Result_H <- A
	output dataN_t Result_L_o									// Result_L <- Q
	

);

/*
*************************************************************************************
* Interconneciton wires/logic
*************************************************************************************
*/

logic A33_l;
logic A33_div_l;
logic AU_sel_root_l;
logic AU_sel_l;
logic Wire_muxResult_AU;
dataN_t A_fromAdder_l;
logic Root_flag;

/////////////Result High part
dataN_t Wire_mul_h;
dataN_t Wire_div_h;
dataN_t Wire_root_h;
/////////////Result Low part
dataN_t Wire_mul_l;
dataN_t Wire_div_l;
dataN_t Wire_root_l;

/////////////Wires for the mux for ROOT case A & M;
dataN_t Wire_R_A;
dataN_t Wire_R_M;

/////////////Wires for the selected value from muxes
dataN_t Wire_muxResult_M;
dataN_t Wire_muxResult_A;



/*
*************************************************************************************
* Instances definition  
*************************************************************************************
*/

mux_3_1_AU Mux_AU_sel
(
	.OP_in(Sel_op),
	.data0_in(AU_sel_l),					// MUL HIGH substract
	.data1_in(~A33_div_l),				// DIV HIGH add
	.data2_in(LOW),						// ROOT always add
	.mux_o(Wire_muxResult_AU)  
);

adderN AU(

	.A_in(Wire_muxResult_A),
	.A33_in(A33_div_l),	
	.B_in(Wire_muxResult_M),
	.AU_sel(Wire_muxResult_AU),
	.A33_o(A33_l),
	.A_fromAdder_o(A_fromAdder_l)
);


muxN_3_1_OP Mux_MDR_M
(
	.OP_in(Sel_op),
	.data0_in(registerY_in),
	.data1_in(registerY_in),
	.data2_in(Wire_R_M),
	.mux_o(Wire_muxResult_M)  
);

muxN_3_1_OP Mux_MDR_A
(
	.OP_in(Sel_op),
	.data0_in(Wire_mul_h),
	.data1_in(Wire_div_h),
	.data2_in(Wire_R_A),	
	.mux_o(Wire_muxResult_A)

);

muxN_3_1_OP Mux_MDR_H
(
	// Input Ports
	.OP_in(Sel_op),
	.data0_in(Wire_mul_h),
	.data1_in(Wire_div_h),
	.data2_in(Wire_root_h),
	
	// Output Ports
	
	 .mux_o(Result_H_o)

);

muxN_3_1_OP Mux_MDR_L
(
	// Input Ports
	.OP_in(Sel_op),
	.data0_in(Wire_mul_l),
	.data1_in(Wire_div_l),
	.data2_in(Wire_root_l),
	
	// Output Ports
	
	 .mux_o(Result_L_o)

);

mul mul
(
	.clk(clk),
	.rst(rst),
	.flush_H_flag_in(flush_H_flag_in),
	.flush_L_flag_in(flush_L_flag_in),
	.flush_AU_flag_in(flush_AU_flag_in),
	.en_H(en_H), 	
	.en_L(en_L),	
	.en_AU(en_AU),								
	.ASR_flag_in(ASR_flag_in),									// Shift Control
	.Q_reg_selector_in(Q_reg_selector_in),		// MUX control
	.A_reg_selector_in(A_reg_selector_in),					// MUX control
	.A_from_adder_in(A_fromAdder_l),							// After Adder
	.multiplier_in(registerX_in),								// Q <- Multiplier
	.AU_Flag_o(AU_sel_l),										// Q[0] when HIGH AU will SUBSTRACT
	.Result_H_o(Wire_mul_h),									// Result_H <- A
	.Result_L_o(Wire_mul_l)										// Result_L <- Q
);


div div
(
	.clk(clk),
	.rst(rst),
	.flush_H_flag_in(flush_H_flag_in),
	.flush_L_flag_in(flush_L_flag_in),
	.flush_AU_flag_in(flush_AU_flag_in),
	.en_H(en_H), 	
	.en_L(en_L),	
	.en_AU(en_AU),	
	.counterFlag_in(counterFlag_in),
	.ASR_flag_in(ASR_flag_in),									// Shift Control
	.Q_reg_selector_in(Q_reg_selector_in),		// MUX control
	.A_reg_selector_in(A_reg_selector_in),					// MUX control
	.A_from_adder_in(A_fromAdder_l),							// After Adder
	.A33_in(A33_l),
	.dividend_in(registerX_in),								// Q <- Multiplier
	.AU_Flag_o(A33_div_l),										// Q[0] when HIGH AU will SUBSTRACT
	.Result_H_o(Wire_div_h),									// Result_H <- A
	.Result_L_o(Wire_div_l)										// Result_L <- Q
);


root root
(
	// inputs for multiplication SR register
	.clk(clk),
	.rst(rst),
	.flush_H_flag_in(flush_H_flag_in),
	.flush_L_flag_in(flush_L_flag_in),
	.flush_AU_flag_in(flush_AU_flag_in),
	.en_H(en_H), 	
	.en_L(en_L),	
	.en_AU(en_AU),								
	.ASR_flag_in(ASR_flag_in),						// Shift Control
	.Q_reg_selector_in(Q_reg_selector_in),			// MUX control
	.A_reg_selector_in(A_reg_selector_in),				// MUX control
	.A_from_adder_in(A_fromAdder_l),		// After Adder
	.n_in(n_in),
	.Data_x_in(registerX_in),
	
	//Output ports
	.Result_flag(Root_flag),
	.AU_Flag_o(AU_sel_root_l),							// Q_o
	.Result_H_o(Wire_root_h),				// Result_H <- A
	.Result_L_o(Wire_root_l),				// Result_L <- Q
	.Temp3_o(Wire_R_M),
	.TempR_o(Wire_R_A)
	
);


endmodule

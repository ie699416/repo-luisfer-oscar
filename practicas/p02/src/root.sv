module root 
import nBitsPackage::*;
(
	// inputs for multiplication SR register
	input clk,
	input rst,
	input flush_H_flag_in,
	input flush_L_flag_in,
	input flush_AU_flag_in,
	input en_H, 	
	input en_L,	
	input en_AU,								
	input	ASR_flag_in,						// Shift Control
	input Q_reg_selector_in,							
	input A_reg_selector_in,				// MUX control
	input dataN_t A_from_adder_in,		// R after adder
	input dataCount_t n_in,
	input dataN_t Data_x_in,
	
	//Output ports
	output AU_Flag_o,							// Q_o
	output Result_flag,
	output dataN_t Result_H_o,				// Result_H <- A
	output dataN_t Result_L_o,				// Result_L <- Q
	output dataN_t Temp3_o,
	output dataN_t TempR_o
	
);

/*
*************************************************************************************
* Interconneciton wires/logic
*************************************************************************************
*/

logic Q_reg_outputBit_l;
logic q0_l;
logic xOr_l;

dataN_t mux_A_l;

dataN_t mux_Q_l;
data2_t Temp_1_l;
dataN_t Temp_2_l;



/*
*************************************************************************************
* Instances definition  
*************************************************************************************
*/


Booth_SR_register SR_AQ_root(
	.clk(clk),
	.rst(rst),
	.flush_H_flag_in(flush_H_flag_in),
	.flush_L_flag_in(flush_L_flag_in),
	.flush_AU_flag_in(flush_AU_flag_in),
	.en_H(en_H), 	
	.en_L(en_L),	
	.en_AU(en_AU),
	.ASR_H_flag_in(LOW),								// FF not shifting
	.ASR_L_flag_in(ASR_flag_in),
	.Q0_inputBit_in(~A_from_adder_in[N-1]),
	.RegisterQ_in(mux_Q_l),						// Q <- Multiplier 
	.endianess_flag(LOW),
	.A_fromAdder_in(mux_A_l),						// A <- A + M_reg	
	.LSB(AU_Flag_o),
	.AU_selector(q0_l),
	.Result_H_o(Result_H_o),						// Result_H from A_reg	
	.Result_L_o(Result_L_o)							// Result_L from Q_reg
);		

temp1 comb_1
(
	// Input Ports
	.D_in(Data_x_in),									// Data input from X PIPO register
	.n_in(n_in),								// Iterative counter for shift
	
	// Output Ports
	.temp1_o(Temp_1_l)

);
temp2 comb_2
(
	// Input Ports
	.R_in(Result_H_o),								// R input from adder	
	// Output Ports
	.temp2_o(Temp_2_l)

);
temp3 comb_3
(
	// Input Ports
	.Q_in(Result_L_o),			// Data input from X PIPO register
	.R15_in(TempR_o[N-1]),					// Selector for adder
	
	// Output Ports
	.temp3_o(Temp3_o)

);
R_Temp_Operation comb_4
(
	.temp1_in(Temp_1_l),		
	.temp2_in(Temp_2_l),			
	
	// Output Ports
	.R_temp_o(TempR_o)

);


muxN_2_1 mux_Q
(
	.Selector(Q_reg_selector_in),
	.data0_in(ZERON),
	.data1_in(Result_L_o), 
	.mux_o(mux_Q_l)  
);

 
muxN_2_1 mux_A
(
	.Selector(A_reg_selector_in),							// When HIGH store the adder result
	.data0_in(ZERON),
	.data1_in(A_from_adder_in),
	.mux_o(mux_A_l)  
);

assign Result_flag = TRUE;

endmodule
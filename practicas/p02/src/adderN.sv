/*
*************************************************************************************
*	Coder:	OSCAR MAISTERRA FLORES	 		[ ie699416]
*	Coder: 	LUIS FERNANDO MORENO ALONSO	[ ie693314] 
*	Date:          Jun 8th, 2019
*  Name:          adderN.sv
*  Description:   The process below performs the arithmetic SUM between two N-bits
*
*************************************************************************************
*/

module adderN
import nBitsPackage::*;
(
	input dataN_t A_in,
	input A33_in,	
	input dataN_t B_in,
	input AU_sel,
	output A33_o,
	output dataN_t A_fromAdder_o
);

/*
*************************************************************************************
* Combiational processes 
*************************************************************************************
*/

/*local datatypes N + 1 bits*/
logic [N:0] adder17_l;
logic [N:0] A_l;
logic [N:0] M_l;

always_comb 
	begin: adder
		if (AU_sel == TRUE)
			adder17_l = A_l - M_l;
		else 
			adder17_l = A_l + M_l;
	end: adder



assign A_l = ({A33_in,A_in});
assign M_l = ({CERO,B_in});
assign A33_o = adder17_l[N];
assign A_fromAdder_o = adder17_l[N-1:0];

endmodule 
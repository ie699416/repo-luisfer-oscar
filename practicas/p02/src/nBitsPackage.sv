/*
*************************************************************************************
*	Coder:	OSCAR MAISTERRA FLORES	 		[ ie699416]
*	Coder: 	LUIS FERNANDO MORENO ALONSO	[ ie693314] 
*	Date:          May 30th, 2019
*  Name:          nBitsPackage.sv
*	Description:   Package for BCD hw #2. The package has an internal N predefined as
*						8 bits. Also list the enum values for later simulation.
*
*  WARNING: By modifing paramter N =8, the defined logic will not work.
*
*************************************************************************************
*/
 
`ifndef MYPACKAGE_DONE
   `define MYPACKAGE_DONE
	 package nBitsPackage; // package name

		parameter N = 16;	
		parameter MSB = 3;			
		parameter ZERO2 = {2{1'b0}};
		parameter ZERO4 = {4{1'b0}};
		parameter ZERO8 = {8{1'b0}};
		parameter ZERO16 = {16{1'b0}};
		parameter ZERON = {N{1'b0}};		
		parameter ZERONN = {2*N{1'b0}};
		parameter HIGH = 1'b1;
		parameter LOW  = 1'b0;
		parameter TRUE = 1'b1;
		parameter FALSE = 1'b0;
		parameter UNIT = 1'b1;
		parameter UNIT16 = {16{1'b1}};
		parameter CERO = 1'b0;
		parameter CERO_U = 2'b00;
		parameter ONE_UNIT = 2'b01;
		
			
		
		parameter MAXIMUM_VALUE = N;
		parameter NBITS_FOR_COUNTER_FSM = $clog2(MAXIMUM_VALUE);		
		parameter ZERO_COUNT_FSM = {NBITS_FOR_COUNTER_FSM{1'b0}};
		
		parameter FREQUENCY =  5000000; 	//Desired Frequency 5MHZ
		parameter SYSTEM_CLK = 50000000;	//Reference clock
		parameter DELAY_COUNT_VALUE = freq_calc(FREQUENCY);
		parameter NBITS_FOR_COUNTER = $clog2(DELAY_COUNT_VALUE);
		parameter ZERO_COUNT = {NBITS_FOR_COUNTER{1'b0}};

		typedef logic [N-1:0] dataN_t;
		typedef logic [2*N-1:0] dataNN_t;
		typedef logic [7:0] data8_t;
		typedef logic [15:0] data16_t;
		typedef logic [3:0] dataNum_t;
		typedef logic [1:0] data2_t;
		typedef logic [3:0] data4_t;
		typedef logic [5:0] dataCount_t;	
		typedef logic PB;
		typedef logic LED;
		typedef logic Control_flag; 
		typedef logic Control_ready;
		typedef enum logic [1:0] {IDLE, LOAD, SHIFT, READY} logic_enum_state_t;
		typedef enum logic [1:0] {Waiting_Shot, Shot_State, Waiting_Not_Shot} logic_enum_shot_t;
		typedef enum logic [4:0] {zero_s, one_s, two_s, three_s, four_s, five_S, six_s, seven_s} shift_enum_t;
		
		typedef enum logic [6:0] {
		_0 = 7'b1000000,
      _1 = 7'b1111001,
      _2 = 7'b0100100,
      _3 = 7'b0110000,
		_4 = 7'b0011001,
      _5 = 7'b0010010,
      _6 = 7'b0000010,
      _7 = 7'b1111000,
      _8 = 7'b0000000,
      _9 = 7'b0010000,
		_F = 7'b0001110,
		_min = 7'b0111111,
		_X = 7'b1111111
		}
		dataSeg_en;	
		
		typedef enum logic [3:0] {
		ZERO_U =  4'b0000,
      ONE_U =   4'b0001,
      TWO_U =   4'b0011,
      THREE_U = 4'b0100,
		FOUR_U =  4'b0101,
      FIVE_U =  4'b0110,
      SIX_U =   4'b0111,
      SEVEN_U = 4'b1111
		}
		dec_units;

		
		typedef enum logic [3:0] {
		ZERO, ONE, TWO, THREE, FOUR, FIVE, SIX, SEVEN, EIGHT, NINE, A, B, C, D, SIGN, DEFAULT
		} decoder_en;
		
		
		typedef enum logic [3:0]{
        STATE_IDLE    	= 4'b0000,
		  STATE_LOAD_X		= 4'b0001,
		  STATE_LOAD_Y   	= 4'b0011,
		  STATE_SET  		= 4'b0010,
		  STATE_PROCESS  	= 4'b0110,
        STATE_PREREADY 	= 4'b0111,
		  STATE_READY		= 4'b0101,  
		  STATE_FLUSH		= 4'b0100
		  
		} State_e;
		
		typedef enum logic [1:0]{
        MUL     = 2'b00,
		  DIV     = 2'b01,
        ROOT    = 2'b10,
		  NA		 = 2'b11		  
		} OP_en;
				
		/*
*************************************************************************************
*/		
	 
		function integer freq_calc;
			input integer freq_requerida;
			integer result;
			begin
				result = (SYSTEM_CLK/freq_requerida)/2;
				freq_calc = result;
			end
		endfunction
		
/*
*************************************************************************************
*/		
		
	endpackage
	`endif
/*
*************************************************************************************
*	Coder:	OSCAR MAISTERRA FLORES	 		[ ie699416]
*	Coder: 	LUIS FERNANDO MORENO ALONSO	[ ie693314] 
*	Date:          16th, 2019
*  Name:          DD_BCD.sv
*	Description:   Implementation of multiple instances using  For Generate technique
*	-	The logic instanciates the DD_shift.sv accordingly to interconect the bits
*		creating a shifting mechanich purely combinational. 
*************************************************************************************
*/
module DD_BCD

import nBitsPackage::*;
(
	// Input Ports
	input data16_t A_in,	
	output decoder_en Dec_Mill_o,
	output decoder_en Uni_Mill_o,
	output decoder_en Cen_o,
	output decoder_en Dec_o,
	output decoder_en Uni_o
);

/*
*************************************************************************************
* Interconneciton wires/logic
*************************************************************************************
*/


dataNum_t DD_N1_shift  [16:0];
dataNum_t DD_N2_shift  [16:0];
dataNum_t DD_N3_shift  [16:0];
dataNum_t DD_N4_shift  [16:0];
dataNum_t DD_U_shift	  [16:0];
dataNum_t DD_D_shift	  [16:0];
dataNum_t DD_C_shift   [16:0];
dataNum_t DD_U_M_shift [16:0];
dataNum_t DD_D_M_shift [16:0];

/*Init values*/
assign DD_U_shift[0] = ZERO4;
assign DD_D_shift[0] = ZERO4;
assign DD_C_shift[0] = ZERO4;
assign DD_U_M_shift[0] = ZERO4;
assign DD_D_M_shift[0] = ZERO4;

assign DD_N1_shift[0] = A_in [3:0];
assign DD_N2_shift[0] = A_in [7:4];
assign DD_N3_shift[0] = A_in [11:8];
assign DD_N4_shift[0] = A_in [15:12];

genvar i;
/*
*************************************************************************************
* Instances definition  
*************************************************************************************
*/


generate
	for (i = CERO; i<N ;i++)
		begin:DD_
			if (i<(N-1))
				begin
					DD_shift N1
					(
						.LSB_in(CERO),
						.DD_en(FALSE),
						.A_in(DD_N1_shift[i]),
						.B_o(DD_N1_shift[i+1])
					);

					DD_shift N2
					(
						.LSB_in(DD_N1_shift[i][MSB]),
						.DD_en(FALSE),
						.A_in(DD_N2_shift[i]),
						.B_o(DD_N2_shift[i+1])
					);
					
					DD_shift N3
					(
						.LSB_in(DD_N2_shift[i][MSB]),
						.DD_en(FALSE),
						.A_in(DD_N3_shift[i]),
						.B_o(DD_N3_shift[i+1])
					);

					DD_shift N4
					(
						.LSB_in(DD_N3_shift[i][MSB]),
						.DD_en(FALSE),
						.A_in(DD_N4_shift[i]),
						.B_o(DD_N4_shift[i+1])
					);


					DD_shift U
					(
						.LSB_in(DD_N4_shift[i][MSB]),
						.DD_en(TRUE),
						.A_in(DD_U_shift[i]),
						.B_o(DD_U_shift[i+1])
					);

					DD_shift D
					(
						.LSB_in(DD_U_shift[i][MSB]),
						.DD_en(TRUE),
						.A_in(DD_D_shift[i]),
						.B_o(DD_D_shift[i+1])
					);

					DD_shift C
					(
						.LSB_in(DD_D_shift[i][MSB]),
						.DD_en(TRUE),
						.A_in(DD_C_shift[i]),
						.B_o(DD_C_shift[i+1])
					);
					
					DD_shift U_M
					(
						.LSB_in(DD_C_shift[i][MSB]),
						.DD_en(TRUE),
						.A_in(DD_U_M_shift[i]),
						.B_o(DD_U_M_shift[i+1])
					);

					DD_shift D_M
					(
						.LSB_in(DD_U_M_shift[i][MSB]),
						.DD_en(TRUE),
						.A_in(DD_D_M_shift[i]),
						.B_o(DD_D_M_shift[i+1])
					);
				end
			else
				begin
				
					DD_shift U
					(
						.LSB_in(DD_N4_shift[i][MSB]),
						.DD_en(FALSE),
						.A_in(DD_U_shift[i]),
						.B_o(DD_U_shift[i+1])
					);

					DD_shift D
					(
						.LSB_in(DD_U_shift[i][MSB]),
						.DD_en(FALSE),
						.A_in(DD_D_shift[i]),
						.B_o(DD_D_shift[i+1])
					);

					DD_shift C
					(
						.LSB_in(DD_D_shift[i][MSB]),
						.DD_en(FALSE),
						.A_in(DD_C_shift[i]),
						.B_o(DD_C_shift[i+1])
					);
					
					DD_shift U_M
					(
						.LSB_in(DD_C_shift[i][MSB]),
						.DD_en(FALSE),
						.A_in(DD_U_M_shift[i]),
						.B_o(DD_U_M_shift[i+1])
					);

					DD_shift D_M
					(
						.LSB_in(DD_U_M_shift[i][MSB]),
						.DD_en(FALSE),
						.A_in(DD_D_M_shift[i]),
						.B_o(DD_D_M_shift[i+1])
					);
						
				end
	end:DD_
endgenerate






assign Dec_Mill_o = decoder_en'(DD_D_M_shift[N]);
assign Uni_Mill_o = decoder_en'(DD_U_M_shift[N]);
assign Cen_o = decoder_en'(DD_C_shift[N]);
assign Dec_o = decoder_en'(DD_D_shift[N]);
assign Uni_o = decoder_en'(DD_U_shift[N]);



endmodule





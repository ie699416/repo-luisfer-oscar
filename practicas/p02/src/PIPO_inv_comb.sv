/*
*************************************************************************************
*	Coder:	OSCAR MAISTERRA FLORES	 		[ ie699416]
*	Coder: 	LUIS FERNANDO MORENO ALONSO	[ ie693314] 
*	Date:          Jun 8th, 2019
*  Name:          PIPO_inv_comb.sv
*  Description:   
*	-	The process below inverts the paralell input with a control flag
*	-	Original desgin for N = 16 bits. 
*	-	IS parametrical
*
*************************************************************************************
*/
module PIPO_inv_comb
import nBitsPackage::*;
(
	input inv_flag_in,
	input dataN_t	data_in,
	output dataN_t data_o
);

genvar i;


generate
	for (i = N; i>0 ;i--)
		begin:BIT_
			mux_2_1 inv
			(
				.Selector(inv_flag_in),
				.data0_in(data_in[N-i]),
				.data1_in(data_in[i-1]),
				.mux_o(data_o[N-i])
			);
		end: BIT_
				
endgenerate  

endmodule 
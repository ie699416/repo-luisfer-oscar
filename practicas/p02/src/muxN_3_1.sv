/*
*************************************************************************************
*	Coder:	OSCAR MAISTERRA FLORES	 		[ ie699416]
*	Coder: 	LUIS FERNANDO MORENO ALONSO	[ ie693314] 
*	Date : 	June 8th, 2019
*  Name :   mux_2_1.sv  
*
*	Description: Multiplexer parametric N bus value
*
* 
*************************************************************************************
*/

module muxN_3_1_OP
import nBitsPackage::*;
(
	// Input Ports
	input data2_t OP_in,
	input dataN_t data0_in,
	input dataN_t data1_in,
	input dataN_t data2_in,
	
	// Output Ports
	
	output dataN_t mux_o

);


/*
*************************************************************************************
* Combiational processes 
*************************************************************************************
*/

always_comb 
	begin: MUX
		case(OP_en'(OP_in))
			MUL:
				mux_o = data0_in;
			DIV:
				mux_o = data1_in;				
			ROOT: 
				mux_o = data2_in;
			NA:
				mux_o = data0_in;			
			default:
				mux_o = data0_in;
						
	end: MUX// end always

endmodule
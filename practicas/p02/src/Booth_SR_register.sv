/*
*************************************************************************************
*	Coder:	OSCAR MAISTERRA FLORES	 		[ ie699416]
*	Coder: 	LUIS FERNANDO MORENO ALONSO	[ ie693314] 
*	Date : 	June 8th, 2019
*  Name :   Booth_SR_register.sv  
*
*	Description: Shift Right concatenated register.
*	- This local top-module interconects two N-bits register and one bit register
*    performing an arithmetic shift operation as a whole (2N+1) bits register. 
*  - The interconections sends the LSB directly to the MSB of the attached
*    register. 
*	- For the A register, the MSB is a duplicate of the BUS position [N-1] because of 
*    the signed bit.
*
* 
*************************************************************************************
*/
module Booth_SR_register

import nBitsPackage::*;

(
	// inputs for multiplication SR register
	input clk,
	input rst,
	input flush_H_flag_in,				
	input flush_L_flag_in,
	input flush_AU_flag_in,
	input en_H, 	
	input en_L,	
	input en_AU,
	input ASR_H_flag_in,	
	input ASR_L_flag_in,	
	input Q0_inputBit_in,				// External bit to be stored in Q[0]	
	input AU_inputBit_in,				// External bit to be stored in A[N] (Bit register)
	input dataN_t RegisterQ_in,		// Q <- Multiplier | dividend
	input dataN_t A_fromAdder_in,		// A <- A +- M
	input endianess_flag,
	output logic LSB,						//Q[0] 	| 	A[N-1]
	output logic AU_selector,			//q0 		|	A[N]				
	output dataN_t Result_H_o,			// Result_H from A_reg
	output dataN_t Result_L_o			// Result_L from Q_reg
	
);

/*
*************************************************************************************
* Interconneciton wires/logic
*************************************************************************************
*/

logic H_reg_outputBit_l;
logic Q0_mux_sel_l;
logic AU_mux_sel_l;

dataN_t muxN_H_IN_l;
dataN_t muxN_L_IN_l;


dataN_t PIPO_H_IN_l;
dataN_t PIPO_L_IN_l;

dataN_t H_reg_l;
dataN_t L_reg_l;

dataN_t PIPO_H_OUT_l;
dataN_t PIPO_L_OUT_l;



/*
*************************************************************************************
* Instances definition 
*************************************************************************************
*/

muxN_2_1 muxN_H_IN
(
	.Selector(endianess_flag),
	.data0_in(A_fromAdder_in),
	.data1_in(RegisterQ_in),
	.mux_o(muxN_H_IN_l)
);

PIPO_inv_comb PIPO_H_IN
(
	.inv_flag_in(endianess_flag),
	.data_in(muxN_H_IN_l),
	.data_o(PIPO_H_IN_l)
);


mux_2_1 mux_Q0_IN
(
	.Selector(endianess_flag),
	.data0_in(PIPO_H_IN_l[N-1]),					// duplicates the bus MSB input
	.data1_in(Q0_inputBit_in),						// External input
	.mux_o(Q0_mux_sel_l)
);



SR_N_register H_reg
(
	.clk(clk),
	.rst(rst),
	.clr(flush_H_flag_in),
	.en(en_H),
	.ASR_flag_in(ASR_H_flag_in),						
	.inputBit_in(Q0_mux_sel_l),					// Mux control for input bit 			
	.data_in(PIPO_H_IN_l),	
	.outputBit_o(H_reg_outputBit_l),							
	.data_o(H_reg_l)					
	
);

PIPO_inv_comb PIPO_H_OUT
(
	.inv_flag_in(endianess_flag),
	.data_in(H_reg_l),
	.data_o(PIPO_H_OUT_l)
);

muxN_2_1 muxN_H_OUT
(
	.Selector(endianess_flag),
	.data0_in(PIPO_H_OUT_l),
	.data1_in(PIPO_L_OUT_l),
	.mux_o(Result_H_o)
);

muxN_2_1 muxN_L_IN
(
	.Selector(endianess_flag),
	.data0_in(RegisterQ_in),
	.data1_in(A_fromAdder_in),
	.mux_o(muxN_L_IN_l)
);

PIPO_inv_comb PIPO_L_IN
(
	.inv_flag_in(endianess_flag),
	.data_in(muxN_L_IN_l),
	.data_o(PIPO_L_IN_l)
);

/*Q*/
SR_N_register L_reg
(
	.clk(clk),
	.rst(rst),
	.clr(flush_L_flag_in),
	.en(en_L),
	.ASR_flag_in(ASR_L_flag_in),						
	.inputBit_in(H_reg_outputBit_l),						
	.data_in(PIPO_L_IN_l),	
	.outputBit_o(LSB),
	.data_o(L_reg_l)					
	
);



mux_2_1 mux_AU_IN
(
	.Selector(endianess_flag),						// HIGH 
	.data0_in(LSB),									// duplicates the bus MSB input
	.data1_in(AU_inputBit_in),						// External input
	.mux_o(AU_mux_sel_l)
);

bit_register AU_reg
(
	.clk(clk),
	.rst(rst),
	.clr(flush_AU_flag_in),
	.en(en_AU),									
	.data_in(AU_mux_sel_l),	
	.data_o(AU_selector)				
	
);

PIPO_inv_comb PIPO_L_OUT
(
	.inv_flag_in(endianess_flag),
	.data_in(L_reg_l),
	.data_o(PIPO_L_OUT_l)
);

muxN_2_1 muxN_L_OUT
(
	.Selector(endianess_flag),
	.data0_in(PIPO_L_OUT_l),
	.data1_in(PIPO_H_OUT_l),
	.mux_o(Result_L_o)
);






endmodule 
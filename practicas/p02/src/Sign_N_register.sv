/*
*************************************************************************************
*	Coder:	OSCAR MAISTERRA FLORES	 		[ ie699416]
*	Coder: 	LUIS FERNANDO MORENO ALONSO	[ ie693314] 
*	Date : 	June 22th, 2019
*  Name :   Sign_N_register.sv  
*
*	Description: 	 
*	-	This module operates the FF_D for N bits
*	-	N = 16
*	-	Also operates and additional Bit 
* 
*************************************************************************************
*/

module Sign_N_register
import nBitsPackage::*;

(
	// inputs 
	input clk,
	input rst,
	input en,								
	input MSB_in,		    						
	input dataN_t data_in,		    		
	// outputs 
	output dataN_t data_o,					
	output sign_o					
	
);

/*
*************************************************************************************
* Interconneciton wires/logic
*************************************************************************************
*/



/*
*************************************************************************************
* Instances definition 
*************************************************************************************
*/


bit_register sign_reg
(
	.clk(clk),
	.rst(rst),
	.en(en),								
	.data_in(MSB_in),	
	.data_o(sign_o)					
	
);

N_register data_reg
(
	.clk(clk),
	.rst(rst),
	.en(en),								
	.data_in(data_in),	
	.data_o(data_o)					
	
);




endmodule

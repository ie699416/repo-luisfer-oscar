module mul 
import nBitsPackage::*;
(
	// inputs for multiplication SR register
	input clk,
	input rst,
	input flush_H_flag_in,
	input flush_L_flag_in,
	input flush_AU_flag_in,
	input en_H, 	
	input en_L,	
	input en_AU,									
	input	ASR_flag_in,						// Shift Control
	input Q_reg_selector_in,			// MUX control
	input A_reg_selector_in,				// MUX control
	input dataN_t A_from_adder_in,		// After Adder
	input dataN_t multiplier_in,			// Q <- Multiplier
	
	//Output ports
	output AU_Flag_o,							// Q_o
	output dataN_t Result_H_o,				// Result_H <- A
	output dataN_t Result_L_o				// Result_L <- Q
	
);

/*
*************************************************************************************
* Interconneciton wires/logic
*************************************************************************************
*/

logic xOr_l;
logic LSB_l;
logic mux_A_l;

dataN_t Q_from_mux_l;
dataN_t A_from_mux_l;

/*
*************************************************************************************
* Instances definition  
*************************************************************************************
*/



Booth_SR_register SR_AQq0(
	.clk(clk),
	.rst(rst),
	.flush_H_flag_in(flush_H_flag_in),
	.flush_L_flag_in(flush_L_flag_in),
	.flush_AU_flag_in(flush_AU_flag_in),	
	.en_H(en_H), 	
	.en_L(en_L),	
	.en_AU(en_AU),
	.ASR_H_flag_in(ASR_flag_in),					// FF not shifting
	.ASR_L_flag_in(ASR_flag_in),
	.Q0_inputBit_in(LOW),
	.RegisterQ_in(Q_from_mux_l),						// Q <- Multiplier 
	.endianess_flag(LOW),	
	.A_fromAdder_in(A_from_mux_l),				// A <- A + M_reg	
	.LSB(AU_Flag_o),									// Q[0]
	.AU_selector(LSB_l),
	.Result_H_o(Result_H_o),						// Result_H from A_reg	
	.Result_L_o(Result_L_o)							// Result_L from Q_reg
);

xclusive_or xOr(
	.a_in(AU_Flag_o),									//Defined in booth algorithm
	.b_in(LSB_l),
	.xOr_o(xOr_l)
);

muxN_2_1 mux_Q
(
	.Selector(Q_reg_selector_in),
	.data0_in(Result_L_o),
	.data1_in(multiplier_in), 
	.mux_o(Q_from_mux_l)  
);

xclusive_AND AND
(
	.a_in(xOr_l),
	.b_in(A_reg_selector_in),
	.and_o(mux_A_l)
);
 
muxN_2_1 mux_A
(
	.Selector(mux_A_l),							// When HIGH store the adder result
	.data0_in(Result_H_o),
	.data1_in(A_from_adder_in),
	.mux_o(A_from_mux_l)  
);

endmodule

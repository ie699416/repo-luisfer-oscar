/*
*************************************************************************************
*	Coder:	OSCAR MAISTERRA FLORES	 		[ ie699416]
*	Coder: 	LUIS FERNANDO MORENO ALONSO	[ ie693314] 
*	Date : 	June 4th, 2019
*  Name :   nBitsPackage.sv  
*
*	Historic review: Jun 4th, 2019
*	Additions: Enum for state machine.      
*
*	Historic review: Jun 3rd, 2019
*  
*	Additions by this modification: 
*		- CLK parametric values for new clk freq
*
*	Historic review:  May 30th, 2019 
*		-Package for CLK hw #3. The package has an internal N predefined as
*		 8 bits. Also list the enum values for later simulation.
*
*  WARNING: By modifing paramter N =8, the defined logic will not work.
*************************************************************************************
*/
 
`ifndef MYPACKAGE_DONE
   `define MYPACKAGE_DONE
	 package nBitsPackage; // package name

		parameter N = 8;
		parameter ZERON = {N{1'b0}};		
		parameter ZERONN = {2*N{1'b0}};
		parameter HIGH = 1'b1;
		parameter LOW  = 1'b0;
		parameter TRUE = 1'b1;
		parameter FALSE = 1'b0;
		parameter UNIT = 1'b1;
		parameter CERO = 1'b0;
		
		
		/*Freqcuency parameters CLK Generator  */
		parameter FREQUENCY = 1; 	//Desired Frequency 1 HZ
		parameter SYSTEM_CLK = 50000000;	//Reference clock
		parameter DELAY_COUNT_VALUE = freq_calc(FREQUENCY);
		parameter NBITS_FOR_COUNTER = $clog2(DELAY_COUNT_VALUE);
		parameter ZERO_COUNT = {NBITS_FOR_COUNTER{1'b0}};

		
	   typedef logic [N-1:0] dataN_t;
		typedef logic [2*N-1:0] dataNN_t;
		typedef logic [3:0] dataNum_t;
		typedef logic [NBITS_FOR_COUNTER - 1 : 0] dataCount_t;		
		
		typedef enum logic [6:0] {
		_0 = 7'b1000000,
      _1 = 7'b1111001,
      _2 = 7'b0100100,
      _3 = 7'b0110000,
		_4 = 7'b0011001,
      _5 = 7'b0010010,
      _6 = 7'b0000010,
      _7 = 7'b1111000,
      _8 = 7'b0000000,
      _9 = 7'b0010000,
		_min = 7'b0111111,
		_ = 7'b1111111
		}
		dataSeg_en;	

		
		typedef enum logic [3:0] {
		ZERO, ONE, TWO, THREE, FOUR, FIVE, SIX, SEVEN, EIGHT, NINE, A, B, C, D, SIGN, DEFAULT
		} decoder_en;
		
		typedef enum logic [2:0] {
		STATE_READY, STATE_HIGH, STATE_LOW
		} state_en;
		
/*
*************************************************************************************
*/		
	 
		function integer freq_calc;
			input integer freq_requerida;
			integer result;
			begin
				result = (SYSTEM_CLK/freq_requerida)/2;
				freq_calc = result;
			end
		endfunction
		
/*
*************************************************************************************
*/



	 
		
	endpackage
	`endif
	

/*
*************************************************************************************
*	Coder:	OSCAR MAISTERRA FLORES	 		[ ie699416]
*	Coder: 	LUIS FERNANDO MORENO ALONSO	[ ie693314] 
*	Date : 	June 8th, 2019
*  Name :   N_register.sv  
*
*	Description: 	 
*		 This module operates the FF_D for N bits
* 
*************************************************************************************
*/

module SR_N_register
import nBitsPackage::*;

(
	// inputs 
	input clk,
	input rst,
	input en,									// Register enable
	input ASR_flag_in,						// ASR_flag
	input inputBit_in,						// Shift concat
	input dataN_t data_in,		    		// M <- Multiplicand	
	// outputs 
	output logic outputBit_o,				// Shift concat
	output dataN_t data_o					// M <- value for adder	
	
);

/*
*************************************************************************************
* Interconneciton wires/logic
*************************************************************************************
*/


/*
*************************************************************************************
* Sequential process
*************************************************************************************
*/


always_ff@(posedge clk, negedge rst) 
	begin: SR_N_register
		if (rst == FALSE)
			begin
				data_o <= ZERON;
			end
		else 
			begin
				if(en == TRUE) 
					begin
						if(ASR_flag_in == TRUE) 		// ASR_flag
							begin
								data_o <= {inputBit_in, data_in[N-1:1]};								
							end
						else
							begin
								data_o <= data_in;
							end
					end
				else
					begin
						data_o <= data_o;		
					end
			end
			
	end:  SR_N_register
	
assign outputBit_o = data_in[0];

endmodule



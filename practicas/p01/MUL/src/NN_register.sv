/*
*************************************************************************************
*	Coder:	OSCAR MAISTERRA FLORES	 		[ ie699416]
*	Coder: 	LUIS FERNANDO MORENO ALONSO	[ ie693314] 
*	Date : 	June 8th, 2019
*  Name :   NN_register.sv  
*
*	Description: 	 
*  - 	This module operates the FF_D for 2N bits, it serves as a local-top module that
*		Instanciates two N_register concatenating the 2N bits otuput. 
*		
* 
*************************************************************************************
*/

module NN_register
import nBitsPackage::*;

(
	// inputs 
	input clk,
	input rst,
	input en,								
	input dataN_t dataH_in,		    		// Result_H <- Multiplicand	
	input dataN_t dataL_in,		    		// M <- Multiplicand	
	// outputs 
	output dataNN_t data_o					// M <- value for adder	
	
);

/*
*************************************************************************************
* Interconneciton wires/logic
*************************************************************************************
*/



/*
*************************************************************************************
* Instances definition 
*************************************************************************************
*/


N_register H_reg
(
	.clk(clk),
	.rst(rst),
	.en(en),								
	.data_in(dataH_in),	
	.data_o(data_o[2*N-1:N])					
	
);

N_register L_reg
(
	.clk(clk),
	.rst(rst),
	.en(en),								
	.data_in(dataL_in),	
	.data_o(data_o[N-1:0])					
	
);




endmodule

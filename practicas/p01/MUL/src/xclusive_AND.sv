/*
*************************************************************************************
*	Coder:	OSCAR MAISTERRA FLORES	 		[ ie699416]
*	Coder: 	LUIS FERNANDO MORENO ALONSO	[ ie693314] 
*	Date : 	June 8th, 2019
*  Name :   xclusive_AND.sv  
*
*	Description: 	 
*		 This module operates the logical AND operation between single bits.
* 
*************************************************************************************
*/

module xclusive_AND
(
	input a_in,
	input b_in,
	output and_o
);

assign and_o = a_in & b_in ;
endmodule 
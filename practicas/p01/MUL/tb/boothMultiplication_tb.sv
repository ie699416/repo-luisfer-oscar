/*
*************************************************************************************
*	Coder:	OSCAR MAISTERRA FLORES	 		[ ie699416]
*	Coder: 	LUIS FERNANDO MORENO ALONSO	[ ie693314] 
*	Date : 	June 4th, 2019
*  Name :   boothMultiplication_tb.sv  
*
*	Description: TB file 
*					 
*************************************************************************************
*/
module boothMultiplication_tb;
import nBitsPackage::*;


/*
*************************************************************************************
* Interconneciton wires/logic
*************************************************************************************
*/

logic clk_top = HIGH;
logic A_reg_selector_l = LOW; 
logic reset = LOW;
logic en_A_l = LOW;
logic en_Q_l = LOW;
logic en_q0_l = LOW;
logic ASR_flag_l = LOW;
logic multiplier_selector_l = HIGH;
dataN_t multiplier_l = 69;
dataN_t multiplicand_l = -22;
dataNN_t result_l = ZERON;



/*
*************************************************************************************
* Instances definition  
*************************************************************************************
*/

boothMultiplication uut

(
	.clk(clk_top),
	.rst(reset),
	.en_A(en_A_l),													// Register Control
	.en_Q(en_Q_l),													// Register Control
	.en_q0(en_q0_l),												// Register Control
	.ASR_flag_in(ASR_flag_l),									// Shift Control
	.multiplier_selector_in(multiplier_selector_l),		// MUX control
	.A_reg_selector_in(A_reg_selector_l),										// MUX control
	.multiplier_in(multiplier_l),								// Q <- Multiplier
	.multiplicand_in(multiplicand_l),						// M <- Multiplicand
	.result_o(result_l)			 								// Result <- Q from SR_register 
	
);


/*********************************************************/
initial // Clock generator
  begin
    forever #1 clk_top = !clk_top;
  end
/*********************************************************/
initial begin // reset generator


#6 reset = HIGH;

#0 en_A_l = LOW;
#0 en_Q_l = HIGH;
#0 en_q0_l = LOW;

#4 multiplier_selector_l = LOW;
#0 ASR_flag_l = HIGH;
#0 A_reg_selector_l = HIGH;;
#0 en_A_l = HIGH; 
#0 en_q0_l = HIGH;

#16 A_reg_selector_l = LOW;

#2 ASR_flag_l = LOW;
#0 en_A_l = LOW;
#0 en_Q_l = LOW;
#0 en_q0_l = LOW;



#0 multiplier_selector_l = LOW;


	
end

/*********************************************************/


endmodule
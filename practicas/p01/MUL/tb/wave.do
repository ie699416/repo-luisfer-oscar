onerror {resume}
quietly WaveActivateNextPane {} 0
add wave -noupdate /boothMultiplication_tb/uut/clk
add wave -noupdate /boothMultiplication_tb/uut/rst
add wave -noupdate -radix decimal /boothMultiplication_tb/uut/multiplier_in
add wave -noupdate -radix decimal /boothMultiplication_tb/uut/multiplicand_in
add wave -noupdate -radix decimal /boothMultiplication_tb/uut/result_o
add wave -noupdate /boothMultiplication_tb/uut/Q_from_mux_l
add wave -noupdate /boothMultiplication_tb/uut/A_from_mux_l
add wave -noupdate /boothMultiplication_tb/uut/A_fromAdder_l
add wave -noupdate -color {Violet Red} /boothMultiplication_tb/uut/xOr_l
add wave -noupdate -expand -group {Shift Right Register} -color {Medium Spring Green} /boothMultiplication_tb/uut/A_result_High_l
add wave -noupdate -expand -group {Shift Right Register} -color {Medium Spring Green} /boothMultiplication_tb/uut/Q_result_Low_l
add wave -noupdate -expand -group {Shift Right Register} -color {Medium Spring Green} /boothMultiplication_tb/uut/q0_l
add wave -noupdate -expand -group {Control Bus} -color Cyan /boothMultiplication_tb/en_A_l
add wave -noupdate -expand -group {Control Bus} -color Cyan /boothMultiplication_tb/en_Q_l
add wave -noupdate -expand -group {Control Bus} -color Cyan /boothMultiplication_tb/en_q0_l
add wave -noupdate -expand -group {Control Bus} -color Cyan /boothMultiplication_tb/ASR_flag_l
add wave -noupdate -expand -group {Control Bus} -color Cyan /boothMultiplication_tb/multiplier_selector_l
add wave -noupdate -expand -group {Control Bus} -color Cyan /boothMultiplication_tb/A_reg_selector_l
add wave -noupdate /boothMultiplication_tb/uut/AND/a_in
add wave -noupdate /boothMultiplication_tb/uut/AND/b_in
add wave -noupdate /boothMultiplication_tb/uut/AND/and_o
TreeUpdate [SetDefaultTree]
WaveRestoreCursors {{Cursor 1} {30 ps} 0}
quietly wave cursor active 1
configure wave -namecolwidth 389
configure wave -valuecolwidth 100
configure wave -justifyvalue left
configure wave -signalnamewidth 0
configure wave -snapdistance 10
configure wave -datasetprefix 0
configure wave -rowmargin 4
configure wave -childrowmargin 2
configure wave -gridoffset 0
configure wave -gridperiod 1
configure wave -griddelta 40
configure wave -timeline 0
configure wave -timelineunits ps
update
WaveRestoreZoom {10 ps} {42 ps}

`timescale 10ns / 1ps
module tb_P01;
import nBitsPackage::*;
	// inputs for the control unit
	logic clk_FPGA_l = HIGH;
	logic reset_l = LOW;
	dataN_t Multiplier_l;
	dataN_t Multiplicand_l;
	Control_start Start_in_l = LOW;				// Signal that determines the Start of the proyect, by a push botton
	
	// outputs from Control Unit						
	 dataSeg_en Display_1_l;			
	 dataSeg_en Display_2_l;
	 dataSeg_en Display_3_l; 
	 dataSeg_en Display_4_l;
	 dataSeg_en Display_5_l;			
	 dataSeg_en Display_6_l;
	 dataSeg_en Display_7_l; 
	 dataSeg_en Display_8_l;


P01 p01_test
(
	// inputs for the control unit
	.clk_FPGA(clk_FPGA_l),
	.reset(reset_l),
	.Switches_in({Multiplicand_l,Multiplier_l}),
	.Start_in(Start_in_l),					// Signal that determines the Start of the proyect, by a push botton
	
	// outputs from Control Unit						
	.Display_1(Display_1_l),			
	.Display_2(Display_2_l),
	.Display_3(Display_3_l), 
	.Display_4(Display_4_l),
	.Display_5(Display_5_l),			
	.Display_6(Display_6_l),
	.Display_7(Display_7_l), 
	.Display_8(Display_8_l)
);


/*********************************************************/
initial // Clock generator
  begin
    forever #1 clk_FPGA_l = !clk_FPGA_l;
  end
/*********************************************************/
initial begin // reset generator

#0 Multiplier_l = 127;
#0 Multiplicand_l = 127;

#4 reset_l = HIGH;







#400 Start_in_l = LOW;

#20 Start_in_l = HIGH;


#400 Start_in_l = LOW;

#20 Start_in_l = HIGH;

#0 Multiplier_l = -127;
#0 Multiplicand_l = -127;

#4 reset_l = HIGH;







#400 Start_in_l = LOW;

#20 Start_in_l = HIGH;

#400 Start_in_l = LOW;

#20 Start_in_l = HIGH;


#400 Start_in_l = LOW;

#20 Start_in_l = HIGH;

#0 Multiplier_l = 127;
#0 Multiplicand_l = -127;

#4 reset_l = HIGH;







#400 Start_in_l = LOW;

#20 Start_in_l = HIGH;

#400 Start_in_l = LOW;

#20 Start_in_l = HIGH;


#400 Start_in_l = LOW;

#20 Start_in_l = HIGH;

#0 Multiplier_l = -127;
#0 Multiplicand_l = 127;

#4 reset_l = HIGH;







#400 Start_in_l = LOW;

#20 Start_in_l = HIGH;





	
end

/*********************************************************/


endmodule



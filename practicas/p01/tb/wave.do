onerror {resume}
quietly WaveActivateNextPane {} 0
add wave -noupdate /tb_P01/p01_test/clk_FPGA
add wave -noupdate /tb_P01/p01_test/reset
add wave -noupdate /tb_P01/p01_test/Switches_in
add wave -noupdate /tb_P01/p01_test/Start_in
add wave -noupdate -expand -group Displays -color Gold /tb_P01/p01_test/Display_1
add wave -noupdate -expand -group Displays -color Gold /tb_P01/p01_test/Display_2
add wave -noupdate -expand -group Displays -color Gold /tb_P01/p01_test/Display_3
add wave -noupdate -expand -group Displays -color Gold /tb_P01/p01_test/Display_4
add wave -noupdate -expand -group Displays -color Gold /tb_P01/p01_test/Display_5
add wave -noupdate -expand -group Displays -color Gold /tb_P01/p01_test/Display_6
add wave -noupdate -expand -group Displays -color Gold /tb_P01/p01_test/Display_7
add wave -noupdate -expand -group Displays -color Gold /tb_P01/p01_test/Display_8
add wave -noupdate -expand -group Control -color Cyan /tb_P01/p01_test/Wire_Control_counter
add wave -noupdate -expand -group Control -color Cyan /tb_P01/p01_test/Wire_Counter_control
add wave -noupdate -expand -group Control -color Cyan /tb_P01/p01_test/Wire_Reset
add wave -noupdate -expand -group Control -color Cyan /tb_P01/p01_test/Wire_Control_A_flag
add wave -noupdate -expand -group Control -color Cyan /tb_P01/p01_test/Wire_Control_Q_flag
add wave -noupdate -expand -group Control -color Cyan /tb_P01/p01_test/Wire_Control_Q0_flag
add wave -noupdate -expand -group Control -color Cyan /tb_P01/p01_test/Wire_Control_Mtr_flag
add wave -noupdate -expand -group Control -color Cyan /tb_P01/p01_test/Wire_Control_Reg_flag
add wave -noupdate -expand -group Control -color Cyan /tb_P01/p01_test/Wire_Control_bM
add wave -noupdate /tb_P01/p01_test/new_clk_l
add wave -noupdate /tb_P01/p01_test/one_shot_l
add wave -noupdate /tb_P01/p01_test/bM_p01/multiplier_selector_in
add wave -noupdate /tb_P01/p01_test/bM_p01/A_reg_selector_in
add wave -noupdate -radix decimal /tb_P01/p01_test/bM_p01/multiplier_in
add wave -noupdate -radix decimal /tb_P01/p01_test/bM_p01/multiplicand_in
add wave -noupdate -radix decimal /tb_P01/p01_test/bM_p01/result_o
add wave -noupdate /tb_P01/p01_test/bM_p01/A_result_High_l
add wave -noupdate /tb_P01/p01_test/bM_p01/Q_result_Low_l
add wave -noupdate /tb_P01/p01_test/bM_p01/q0_l
TreeUpdate [SetDefaultTree]
WaveRestoreCursors {{Cursor 1} {120742 ps} 0}
quietly wave cursor active 1
configure wave -namecolwidth 389
configure wave -valuecolwidth 100
configure wave -justifyvalue left
configure wave -signalnamewidth 0
configure wave -snapdistance 10
configure wave -datasetprefix 0
configure wave -rowmargin 4
configure wave -childrowmargin 2
configure wave -gridoffset 0
configure wave -gridperiod 1
configure wave -griddelta 40
configure wave -timeline 0
configure wave -timelineunits ps
update
WaveRestoreZoom {9579955 ps} {11008163 ps}

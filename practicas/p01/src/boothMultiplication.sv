/*
*************************************************************************************
*	Coder:	OSCAR MAISTERRA FLORES	 		[ ie699416]
*	Coder: 	LUIS FERNANDO MORENO ALONSO	[ ie693314] 
*	Date : 	June 4th, 2019
*  Name :   boothMultiplication.sv  
*
*	Description: Top module for booth multiplication algorithim.
*					 
*************************************************************************************
*/
module boothMultiplication

import nBitsPackage::*;

(
	// inputs for multiplication SR register
	input clk,
	input rst,
	input clear,
	input en_A, 	
	input en_Q,	
	input en_q0,
	input	en_M,
	input	ASR_flag_in,						// Shift Control
	input multiplier_selector_in,			// MUX control
	input A_reg_selector_in,				// MUX control
	input dataN_t multiplier_in,			// Q <- Multiplier
	input dataN_t multiplicand_in,		// M <- Multiplicand
	
	// outputs from multiplication 					
	output dataNN_t result_o			   // Result <- Q from SR_register 
	
);

/*
*************************************************************************************
* Interconneciton wires/logic
*************************************************************************************
*/

logic Q_reg_outputBit_l;
logic q0_l;
logic xOr_l;
logic mux_A_l;

dataN_t B_l;
dataN_t A_result_High_l;
dataN_t Q_result_Low_l;
dataN_t Q_from_mux_l;
dataN_t A_from_mux_l;
dataN_t A_fromAdder_l;
dataN_t multiplier_o;
dataN_t multiplicand_o;


/*
*************************************************************************************
* Instances definition  
*************************************************************************************
*/


Booth_SR_register SR_AQq0(
	.clk(clk),
	.rst(rst),
	.clear(clear),
	.en_A(en_A), 	
	.en_Q(en_Q),	
	.en_q0(en_q0),
	.ASR_flag_in(ASR_flag_in),
	.multiplicand_in(Q_from_mux_l),		// Q <- Multiplier 
	.A_fromAdder_in(A_from_mux_l),		// A <- A + M_reg	
	.Q_reg_outputBit_o(Q_reg_outputBit_l),
	.q0_o(q0_l),
	.Result_H_o(A_result_High_l),			// Result_H from A_reg	
	.Result_L_o(Q_result_Low_l)			// Result_L from Q_reg
);



xclusive_or xOr(
	.a_in(Q_reg_outputBit_l),				//Defined in booth algorithm
	.b_in(q0_l),
	.xOr_o(xOr_l)
);

M M2(
	.clk(clk),
	.rst(rst),
	.en(en_M),									// Always enable
	.multiplicand_in(multiplicand_o),	// M <- Multiplicand	
	.Q0_flag_in(Q_reg_outputBit_l),						// When HIGH, performs twos complement	
	.M_o(B_l)	  								// M <- value for adder	
);

Complement_Neg multiplicand
(
	.data_in(multiplicand_in),
	.comp2_o(multiplicand_o),
	.complement_flag_in(TRUE)
);


adderN adder(
	.A_in(A_result_High_l),     
	.B_in(B_l),
	.A_fromAdder_o(A_fromAdder_l)
);

Complement_Neg multiplier
(
	.data_in(multiplier_in),
	.comp2_o(multiplier_o),
	.complement_flag_in(TRUE)
);

muxN_2_1 mux_Q
(
	.Selector(multiplier_selector_in),
	.data0_in(Q_result_Low_l),
	.data1_in(multiplier_o),
	.mux_o(Q_from_mux_l)  
);


xclusive_AND AND
(
	.a_in(xOr_l),
	.b_in(A_reg_selector_in),
	.and_o(mux_A_l)
);
 

muxN_2_1 mux_A
(
	.Selector(mux_A_l),							// When HIGH store the adder result
	.data0_in(A_result_High_l),
	.data1_in(A_fromAdder_l),
	.mux_o(A_from_mux_l)  
);

assign result_o = {A_result_High_l,Q_result_Low_l};







endmodule 
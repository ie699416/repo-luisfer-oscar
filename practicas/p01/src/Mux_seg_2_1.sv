

module Mux_seg_2_1
import nBitsPackage::*;
(
	// Input Ports
	input Selector,
	input decoder_en data0_in,
	input decoder_en data1_in,
	
	// Output Ports
	output decoder_en mux_o

);


always_comb begin: MUX

	if (Selector == TRUE)
		mux_o = data1_in;
	else
		mux_o = data0_in;

end: MUX// end always


endmodule
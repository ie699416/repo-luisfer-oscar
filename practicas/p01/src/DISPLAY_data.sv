/*
*************************************************************************************
*	Coder:	OSCAR MAISTERRA FLORES	 		[ ie699416]
*	Coder: 	LUIS FERNANDO MORENO ALONSO	[ ie693314] 
*	Date : 	June 11th, 2019
*  Name :   DISPLAY_data.sv  
*
*	Description: 	 
*	-	This module receives swithes or Result Data selected by the ready flag.
*	-	It has been designed for N = 8
*		 
*************************************************************************************
*/

module DISPLAY_data
import nBitsPackage::*;

(
	input dataNN_t	Switches_in,
	input dataNN_t	result_in,
	input ready_in,						// Ready_flag
		// outputs from Control Unit						
	output dataSeg_en Display_1,			
	output dataSeg_en Display_2,
	output dataSeg_en Display_3, 
	output dataSeg_en Display_4,
	output dataSeg_en Display_5,			
	output dataSeg_en Display_6,
	output dataSeg_en Display_7, 
	output dataSeg_en Display_8
	
);

/*
*************************************************************************************
* Interconneciton wires/logic
*************************************************************************************
*/


decoder_en Wire_7seg_mux8;
decoder_en Wire_7seg_mux7;
decoder_en Wire_7seg_mux6;
decoder_en Wire_7seg_mux5;
decoder_en Wire_7seg_mux4;
decoder_en Wire_7seg_mux3;
decoder_en Wire_7seg_mux2;
decoder_en Wire_7seg_mux1;

decoder_en Wire_result_sign;			//sign
decoder_en Wire_BCD_Result_Mux1;
decoder_en Wire_BCD_Result_Mux2;
decoder_en Wire_BCD_Result_Mux3;
decoder_en Wire_BCD_Result_Mux4;
decoder_en Wire_BCD_Result_Mux5;

decoder_en Wire_BCD_H3;					//sign
decoder_en Wire_BCD_H2;
decoder_en Wire_BCD_H1;
decoder_en Wire_BCD_H0;

decoder_en Wire_BCD_L3;					//sign
decoder_en Wire_BCD_L2;
decoder_en Wire_BCD_L1;
decoder_en Wire_BCD_L0;

dataNN_t Wire_bM_result;
logic Wire_Control_counter;
logic Wire_Counter_control;
logic Wire_Reset;
logic Wire_Control_A_flag;
logic Wire_Control_Q_flag;
logic Wire_Control_Q0_flag;
logic Wire_Control_Mtr_flag;
logic Wire_Control_Reg_flag;
logic Wire_Control_bM;
logic SignxOr_l;

dataNN_t Wire_sw_H;
dataNN_t Wire_sw_L;
dataNN_t Wire_result;


/*
*************************************************************************************
* Instances definitions
*************************************************************************************
*/



Decodificador_Binario_BCD BCD_Multiplicand
(
	// Input Ports
	.A_in(Wire_sw_H),

	// Output Ports

	.Cen_o(Wire_BCD_L2),
	.Dec_o(Wire_BCD_L1),
	.Uni_o(Wire_BCD_L0)
);

Decodificador_Binario_BCD BCD_Multiplier
(
	// Input Ports
	.A_in(Wire_sw_L),

	// Output Ports

	.Cen_o(Wire_BCD_H2),
	.Dec_o(Wire_BCD_H1),
	.Uni_o(Wire_BCD_H0)
);

ComplementNN_Neg_seg sw_result_sign
(
	.data_in(result_in),
	.comp2_o(Wire_result),
	.dataSign_o(wire_result_sign)
);

xclusive_or SignXor(
	.a_in(Switches_in[N-1]),				//Defined in booth algorithm
	.b_in(Switches_in[2*N-1]),
	.xOr_o(SignxOr_l)
);

Mux_seg_2_1 Mux_SWSIGN
(
	// Input Ports
	.Selector(SignxOr_l),
	.data0_in(DEFAULT),
	.data1_in(SIGN),
	
	// Output Ports
	.mux_o(Wire_result_sign)
);





Decodificador_Binario_BCD BCD_Result
(
	// Input Ports
	.A_in(Wire_result),

	// Output Ports
	.Dec_Millar_o(Wire_BCD_Result_Mux5),
	.Uni_Millar_o(Wire_BCD_Result_Mux4),
	.Cen_o(Wire_BCD_Result_Mux3),
	.Dec_o(Wire_BCD_Result_Mux2),
	.Uni_o(Wire_BCD_Result_Mux1)
);


ComplementN_Neg_seg sw_H_sign
(
	.data_in(Switches_in[N-1:0]),
	.comp2_o(Wire_sw_H),
	.dataSign_o(Wire_BCD_L3)
);

ComplementN_Neg_seg sw_L_sign
(
	.data_in(Switches_in[2*N-1:N]),
	.comp2_o(Wire_sw_L),
	.dataSign_o(Wire_BCD_H3)
);




Mux_seg_2_1 Mux8
(
	// Input Ports
	.Selector(ready_in),
	.data0_in(Wire_BCD_H3),
	.data1_in(DEFAULT),
	
	// Output Ports
	.mux_o(Wire_7seg_mux8)
);

Mux_seg_2_1 Mux7
(
	// Input Ports
	.Selector(ready_in),
	.data0_in(Wire_BCD_H2),
	.data1_in(DEFAULT),
	
	// Output Ports
	.mux_o(Wire_7seg_mux7)
);

Mux_seg_2_1 Mux6
(
	// Input Ports
	.Selector(ready_in),
	.data0_in(Wire_BCD_H1),
	.data1_in(Wire_result_sign),
	
	// Output Ports
	.mux_o(Wire_7seg_mux6)
);

Mux_seg_2_1 Mux5
(
	// Input Ports
	.Selector(ready_in),
	.data0_in(Wire_BCD_H0),
	.data1_in(Wire_BCD_Result_Mux5),
	
	// Output Ports
	.mux_o(Wire_7seg_mux5)
);

Mux_seg_2_1 Mux4
(
	// Input Ports
	.Selector(ready_in),
	.data0_in(Wire_BCD_L3),
	.data1_in(Wire_BCD_Result_Mux4),
	
	// Output Ports
	.mux_o(Wire_7seg_mux4)
);

Mux_seg_2_1 Mux3
(
	// Input Ports
	.Selector(ready_in),
	.data0_in(Wire_BCD_L2),
	.data1_in(Wire_BCD_Result_Mux3),
	
	// Output Ports
	.mux_o(Wire_7seg_mux3)
);

Mux_seg_2_1 Mux2
(
	// Input Ports
	.Selector(ready_in),
	.data0_in(Wire_BCD_L1),
	.data1_in(Wire_BCD_Result_Mux2),
	
	// Output Ports
	.mux_o(Wire_7seg_mux2)

);

Mux_seg_2_1 Mux1
(
	// Input Ports
	.Selector(ready_in),
	.data0_in(Wire_BCD_L0),
	.data1_in(Wire_BCD_Result_Mux1),
	
	// Output Ports
	.mux_o(Wire_7seg_mux1)
);

Conv_BCD_7seg Display_8_conv
(
	.BCD_in(Wire_7seg_mux8),
	.segments_o(Display_8)
);

Conv_BCD_7seg Display_7_conv
(
	.BCD_in(Wire_7seg_mux7),
	.segments_o(Display_7)
);


Conv_BCD_7seg Display_6_conv
(
	.BCD_in(Wire_7seg_mux6),
	.segments_o(Display_6)
);

Conv_BCD_7seg Display_5_conv
(
	.BCD_in(Wire_7seg_mux5),
	.segments_o(Display_5)
);

Conv_BCD_7seg Display_4_conv
(
	.BCD_in(Wire_7seg_mux4),
	.segments_o(Display_4)
);

Conv_BCD_7seg Display_3_conv
(
	.BCD_in(Wire_7seg_mux3),
	.segments_o(Display_3)
);


Conv_BCD_7seg Display_2_conv
(
	.BCD_in(Wire_7seg_mux2),
	.segments_o(Display_2)
);

Conv_BCD_7seg Display_1_conv
(
	.BCD_in(Wire_7seg_mux1),
	.segments_o(Display_1)
);


endmodule

/*
****************************************************************
* This module defines de top-block for the practice 
****************************************************************
*/
module P01
import nBitsPackage::*;
(
	input clk_FPGA,
	input reset,
	input dataNN_t Switches_in,
	input Control_start Start_in,	
	output logic ready_o,
	
	output dataSeg_en Display_1,			
	output dataSeg_en Display_2,
	output dataSeg_en Display_3, 
	output dataSeg_en Display_4,
	output dataSeg_en Display_5,			
	output dataSeg_en Display_6,
	output dataSeg_en Display_7, 
	output dataSeg_en Display_8
);


dataNN_t result_l;

dataNN_t Wire_bM_result;

logic new_clk_l;
logic one_shot_l;

logic Wire_Control_clear;
logic Wire_Control_counter;
logic Wire_Counter_control;
logic Wire_Reset;
logic Wire_Control_A_flag;
logic Wire_Control_Q_flag;
logic Wire_Control_Q0_flag;
logic Wire_Control_Mtr_flag;
logic Wire_Control_Reg_flag;
logic Wire_Control_bM;


one_shot debouncer(
	.clk(new_clk_l),
	.reset(reset),
	.Start(Start_in),
	.Shot(one_shot_l)
);

CLK_GEN new_clk
(
	.clk_FPGA(clk_FPGA),
	.reset(reset),
	.clk_salida(new_clk_l)
	
);

Control_unit CU
(
	.clk(new_clk_l),
	.rst(reset),
	.Start(one_shot_l),					
	.Flag_counter(Wire_Counter_control),
	.A_flag(Wire_Control_A_flag),
	.Q_flag(Wire_Control_Q_flag),
	.Q0_flag(Wire_Control_Q0_flag),			
	.M_flag(Wire_Control_M_flag),	
	.Mtr_selec_in(Wire_Control_Mtr_flag),
	.A_reg_selec_in(Wire_Control_Reg_flag),	
	.ASR_flag(Wire_Control_bM),	
   .Ready_flag(ready_o),	
	.clear_flag(Wire_Control_clear),
	.Enable_count(Wire_Control_counter)		
);

Counter_SM CC
(
	// Input Ports
	.clk(new_clk_l),
	.reset(reset),
	.enable(Wire_Control_counter),
	.clear(Wire_Control_clear),
	
	// Output Ports
	.flag(Wire_Counter_control)
);


boothMultiplication bM_p01
(
	// inputs for multiplication SR register
	.clk(new_clk_l),
	.rst(reset),
	.clear(Wire_Control_clear),
	.en_A(Wire_Control_A_flag), 	
	.en_Q(Wire_Control_Q_flag),	
	.en_q0(Wire_Control_Q0_flag),
	.en_M(Wire_Control_M_flag),
	.A_reg_selector_in(Wire_Control_Reg_flag),	
	.ASR_flag_in(Wire_Control_bM),						// Shift Control	
	.multiplier_selector_in(Wire_Control_Mtr_flag),			// MUX control
	.multiplier_in(Switches_in[N-1:0]),			// N = 9 
	.multiplicand_in(Switches_in[2*N-1:N]),		
	
	// outputs from multiplication 					
	.result_o(result_l)			   
);

`ifdef MODELSIM

`else

DISPLAY_data Segments (
	.Switches_in(Switches_in),
	.result_in(result_l),
	.ready_in(ready_o),					
	.Display_1(Display_1),			
	.Display_2(Display_2),
	.Display_3(Display_3),			
	.Display_4(Display_4),					
	.Display_5(Display_5),			
	.Display_6(Display_6),
	.Display_7(Display_7),			
	
	.Display_8(Display_8)	
);

`endif


endmodule
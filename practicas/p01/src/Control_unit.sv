
/*
****************************************************************
* This module defines de top-block for the practice 
****************************************************************
*/
module Control_unit
import nBitsPackage::*;
(
	// inputs for the control unit
	input clk,
	input rst,
	input Control_start Start,					// Signal that determines the Start of the proyect, by a push botton
	input Control_flag Flag_counter,			//This flag determines when the counter finished counting 
	
	// outputs from Control Unit						
	output Control_flag ASR_flag,			//Signal that sends the start process for the multiplication process 
	output Control_flag A_flag,
	output Control_flag Q_flag,
	output Control_flag Q0_flag,
	output Control_flag clear_flag,
	output Control_flag Mtr_selec_in,
	output Control_flag A_reg_selec_in,
	output Control_flag M_flag,		//Signal that send the flag of restart to the other modules
	output Control_ready Ready_flag,	   //Signal that indicates the state of the process 
	output Control_flag Enable_count		//Signal that enables the counter
);


State_e Actual_state;	

always_ff@(posedge clk or negedge rst) // Circuito Secuenicial en un proceso always.

	if (rst == FALSE)
		begin	
			Actual_state <= STATE_IDEL;		
		end
	else 
		begin	
			case(Actual_state)
			
		
					STATE_IDEL:begin 
					
						if (Start == TRUE)
							Actual_state <= STATE_LOAD; 	
						else 
								Actual_state <= STATE_IDEL; 							
						end
						
					STATE_LOAD:
						Actual_state <= STATE_PROCESS;
						
						
					STATE_PROCESS:begin 
					
						if (Flag_counter==TRUE)
							Actual_state<=STATE_PREREADY;
						else 
							Actual_state<=STATE_PROCESS;
						end
						
					STATE_PREREADY:
					Actual_state <= STATE_READY;
						
					STATE_READY:begin 
					
						if (Start==TRUE)
							Actual_state<=STATE_IDEL;
						else 
							Actual_state<=STATE_READY;							
						end
						
					default: Actual_state<=STATE_IDEL;
						
			 endcase
end


// CTO combinacional de salida.
always_comb 
	begin
		case(Actual_state)

			 STATE_IDEL:
			 begin 
				A_reg_selec_in = LOW;
				Ready_flag = LOW;
				clear_flag = LOW;
				A_flag = LOW ;
				Q_flag =  LOW;
				Q0_flag = LOW;
				Mtr_selec_in = HIGH;
				ASR_flag = LOW; 
				M_flag = HIGH;
				Enable_count = LOW;
			 end
			 
			 STATE_LOAD:
			 begin
				A_reg_selec_in = HIGH;
				Ready_flag = LOW;
				clear_flag = HIGH;
				A_flag = LOW ;
				Q_flag =  HIGH;
				Q0_flag = LOW;
				Mtr_selec_in = HIGH;
				ASR_flag = LOW; 
				M_flag = HIGH;
				Enable_count = LOW;
			end
			 
			 STATE_PROCESS:
			 begin
				A_reg_selec_in = HIGH;
				Ready_flag = LOW;
				clear_flag = HIGH;
				A_flag = HIGH ;
				Q_flag =  HIGH;
				Q0_flag = HIGH;
				Mtr_selec_in = LOW;
				ASR_flag = HIGH; 
				M_flag = HIGH;
				Enable_count = HIGH;
			 end
			 
			 STATE_PREREADY:
			 begin
				A_reg_selec_in = LOW;
				Ready_flag = LOW;
				clear_flag = HIGH;
				A_flag = LOW ;
				Q_flag =  LOW;
				Q0_flag = LOW;
				Mtr_selec_in = HIGH;
				ASR_flag = HIGH; 
				M_flag = HIGH;
				Enable_count = HIGH;
			 end
			 
			 STATE_READY:
			 begin
				A_reg_selec_in = LOW;
				Ready_flag = HIGH;
				clear_flag = HIGH;
				A_flag = LOW ;
				Q_flag =  LOW;
				Q0_flag = LOW;
				Mtr_selec_in = LOW;
				ASR_flag = LOW; 
				M_flag = HIGH;
				Enable_count = LOW;
			 end
			 
			 default:begin
				A_reg_selec_in = LOW;
				Ready_flag = LOW;
				clear_flag = LOW;
				A_flag = LOW ;
				Q_flag =  LOW;
				Q0_flag = LOW;
				Mtr_selec_in = LOW;
				ASR_flag = LOW; 
				M_flag = LOW;
				Enable_count = LOW;
			 end
	 
endcase
end

endmodule
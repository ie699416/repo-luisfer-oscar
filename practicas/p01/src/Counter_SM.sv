/*
*************************************************************************************
*	Coder:	OSCAR MAISTERRA FLORES	 		[ ie699416]
*	Coder: 	LUIS FERNANDO MORENO ALONzO	[ ie693314] 
*	Date:          Jun 4th, 2019	
*  Name:          Counter_SM.sv      
*************************************************************************************
*/

module Counter_SM
import nBitsPackage::*;
(
	// Input Ports
	input clk,
	input reset,
	input enable,
	input clear,
	
	// Output Ports
	output flag
	
);

logic MaxValue_Bit;
dataCount_t Count_logic;

	always_ff@(posedge clk or negedge reset) 
	begin: counter
		if (reset == FALSE)
			Count_logic <= ZERO_COUNT;
		else
			begin				
				if(clear == FALSE)
					begin
						Count_logic <= ZERO_COUNT;
					end
				else if(enable == TRUE)
					begin
						Count_logic <= Count_logic + UNIT;
					end
				else
					begin								
						Count_logic <= Count_logic;
					end					
			end
		
	end: counter

//--------------------------------------------------------------------------------------------

always_comb
	if(Count_logic ==  N - UNIT)
		MaxValue_Bit = HIGH;
	else
		MaxValue_Bit = LOW;

		
//---------------------------------------------------------------------------------------------
assign flag = MaxValue_Bit;


endmodule

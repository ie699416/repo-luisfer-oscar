/*
*************************************************************************************
*	Coder:	OSCAR MAISTERRA FLORES	 		[ ie699416]
*	Coder: 	LUIS FERNANDO MORENO ALONSO	[ ie693314] 
*	Date : 	June 4th, 2019
*  Name :   boothMultiplication.sv  
*
*	Description: 	 
*		 This module operates the combinational twos complement in a N bit input buffer.
*		 It sends the result adding and additional bit at the MSB position.
* 
*************************************************************************************
*/

module M

import nBitsPackage::*;

(
	// inputs 
	input clk,
	input rst,
	input en,								// Enable for M register
	input dataN_t multiplicand_in,	// M <- Multiplicand	
	input Q0_flag_in,						// When HIGH, performs twos complement	
	// outputs 
	output dataN_t M_o					// M <- value for adder	
	
);

/*
*************************************************************************************
* Interconneciton wires/logic
*************************************************************************************
*/


dataN_t M_reg_l;

/*
*************************************************************************************
* Instances definition  
*************************************************************************************
*/

N_register M_reg
(
	.clk(clk),
	.rst(rst),
	.en(en),								
	.data_in(multiplicand_in),	
	.data_o(M_reg_l)					// M <- value for adder	
	
);



Complement_Neg M_neg
(
	.complement_flag_in(Q0_flag_in),
	.data_in(M_reg_l),
	.comp2_o(M_o)
);

endmodule 
/*
*************************************************************************************
*	Coder:	OSCAR MAISTERRA FLORES	 		[ ie699416]
*	Coder: 	LUIS FERNANDO MORENO ALONzO	[ ie693314] 
*	Date:          Jun 4th, 2019	
*  Name:          Counter_SM.sv      
*************************************************************************************
*/

module Counter_SM
import nBitsPackage::*;
(
	// Input Ports
	input clk,
	input reset,
	input enable,
	input Sync_Reset,
	
	// Output Ports
	output flag
	
);

logic MaxValue_Bit;
dataCount_t Count_logic;

	always_ff@(posedge clk or negedge reset) begin: counter
		if (reset == FALSE)
			Count_logic <= ZERO_COUNT;
		else begin
				if(enable == TRUE) begin
					if(Sync_Reset == FALSE)
					
						Count_logic <= Count_logic + UNIT;
						
					else
						Count_logic <= ZERO_COUNT;
						
				end
		end
	end: counter

//--------------------------------------------------------------------------------------------

always_comb
	if(Count_logic == MAXIMUM_VALUE-UNIT)
		MaxValue_Bit = HIGH;
	else
		MaxValue_Bit = LOW;

		
//---------------------------------------------------------------------------------------------
assign flag = MaxValue_Bit;


endmodule

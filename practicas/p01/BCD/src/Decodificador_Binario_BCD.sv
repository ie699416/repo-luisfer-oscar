/*
*************************************************************************************
*	Coder:	OSCAR MAISTERRA FLORES	 		[ ie699416]
*	Coder: 	LUIS FERNANDO MORENO ALONSO	[ ie693314] 
*	Date:          May 30th, 2019
*  Name:          Decodificador_Binario_BCD.sv
*	Description:   Test bench file for BCD hw #2. The module performs an interconection 
*						described by hard combinational logic that could have been decribed
*						by case logic. 
*************************************************************************************
*/
module Decodificador_Binario_BCD

import nBitsPackage::*;
(
	// Input Ports
	input data16_t A_in,

	// Output Ports
	output decoder_en Dec_Millar_o,
	output decoder_en Uni_Millar_o,
	output decoder_en Cen_o,
	output decoder_en Dec_o,
	output decoder_en Uni_o
);

/*
*************************************************************************************
* Interconneciton wires/logic
*************************************************************************************
*/

dataNum_t C1_wire;
dataNum_t C2_wire;
dataNum_t C3_wire;
dataNum_t C4_wire;
dataNum_t C5_wire;
dataNum_t C6_wire;
dataNum_t C7_wire;
dataNum_t C8_wire;
dataNum_t C9_wire;
dataNum_t C10_wire;
dataNum_t C11_wire;
dataNum_t C12_wire;
dataNum_t C13_wire;
dataNum_t C14_wire;
dataNum_t C15_wire;
dataNum_t C16_wire;
dataNum_t C17_wire;
dataNum_t C18_wire;
dataNum_t C19_wire;
dataNum_t C20_wire;
dataNum_t C21_wire;
dataNum_t C22_wire;
dataNum_t C23_wire;
dataNum_t C24_wire;
dataNum_t C25_wire;
dataNum_t C26_wire;
dataNum_t C27_wire;
dataNum_t C28_wire;
dataNum_t C29_wire;
dataNum_t C30_wire;
dataNum_t C31_wire;
dataNum_t C32_wire;
dataNum_t C33_wire;
dataNum_t C34_wire;

/*
*************************************************************************************
* Instances definition  
*************************************************************************************
*/

Corrimiento C1(
	.A_input({1'b0,A_in[15:13]}),
	.B_output(C1_wire)
);

Corrimiento C2(
	.A_input({C1_wire[2:0],A_in[12]}),
	.B_output(C2_wire)
);

Corrimiento C3(
	.A_input({C2_wire[2:0],A_in[11]}),
	.B_output(C3_wire)
);

Corrimiento C4(
	.A_input({C3_wire[2:0],A_in[10]}),
	.B_output(C4_wire)
);

Corrimiento C5(
	.A_input({C4_wire[2:0],A_in[9]}),
	.B_output(C5_wire)
);

Corrimiento C6(
	.A_input({C5_wire[2:0], A_in[8]}),
	.B_output(C6_wire)
);

Corrimiento C7(
	.A_input({C6_wire[2:0], A_in[7]}),
	.B_output(C7_wire)
);

Corrimiento C8(
	.A_input({C7_wire[2:0], A_in[6]}),
	.B_output(C8_wire)
);

Corrimiento C9(
	.A_input({C8_wire[2:0], A_in[5]}),
	.B_output(C9_wire)
);

Corrimiento C10(
	.A_input({C9_wire[2:0], A_in[4]}),
	.B_output(C10_wire)
);

Corrimiento C11(
	.A_input({C10_wire[2:0], A_in[3]}),
	.B_output(C11_wire)
);

Corrimiento C12(
	.A_input({C11_wire[2:0], A_in[2]}),
	.B_output(C12_wire)
);

Corrimiento C13(
	.A_input({C12_wire[2:0], A_in[1]}),
	.B_output(C13_wire)
);

Corrimiento C14(
	.A_input({1'b0, C1_wire[3],C2_wire[3],C3_wire[3]}),
	.B_output(C14_wire)
);


Corrimiento C15(
	.A_input({C14_wire[2:0],C4_wire[3]}),
	.B_output(C15_wire)
);


Corrimiento C16(
	.A_input({C15_wire[2:0],C5_wire[3]}),
	.B_output(C16_wire)
);

Corrimiento C17(
	.A_input({C16_wire[2:0],C6_wire[3]}),
	.B_output(C17_wire)
);

Corrimiento C18(
	.A_input({C17_wire[2:0],C7_wire[3]}),
	.B_output(C18_wire)
);

Corrimiento C19(
	.A_input({C18_wire[2:0],C8_wire[3]}),
	.B_output(C19_wire)
);


Corrimiento C20(
	.A_input({C19_wire[2:0],C9_wire[3]}),
	.B_output(C20_wire)
);

Corrimiento C21(
	.A_input({C20_wire[2:0],C10_wire[3]}),
	.B_output(C21_wire)
);

Corrimiento C22(
	.A_input({C21_wire[2:0],C11_wire[3]}),
	.B_output(C22_wire)
);

Corrimiento C23(
	.A_input({C22_wire[2:0],C12_wire[3]}),
	.B_output(C23_wire)
);

Corrimiento C24(
	.A_input({1'b0,C14_wire[3],C15_wire[3], C16_wire[3]}),
	.B_output(C24_wire)
);

Corrimiento C25(
	.A_input({C24_wire[2:0],C17_wire[3]}),
	.B_output(C25_wire)
);

Corrimiento C26(
	.A_input({C25_wire[2:0],C18_wire[3]}),
	.B_output(C26_wire)
);

Corrimiento C27(
	.A_input({C26_wire[2:0],C19_wire[3]}),
	.B_output(C27_wire)
);


Corrimiento C28(
	.A_input({C27_wire[2:0],C20_wire[3]}),
	.B_output(C28_wire)
);

Corrimiento C29(
	.A_input({C28_wire[2:0],C21_wire[3]}),
	.B_output(C29_wire)
);

Corrimiento C30(
	.A_input({C29_wire[2:0],C22_wire[3]}),
	.B_output(C30_wire)
);

Corrimiento C31(
	.A_input({1'b0,C24_wire[3],C25_wire[3],C26_wire[3]}),
	.B_output(C31_wire)
);

Corrimiento C32(
	.A_input({C31_wire[2:0],C27_wire[3]}),
	.B_output(C32_wire)
);

Corrimiento C33(
	.A_input({C32_wire[2:0],C28_wire[3]}),
	.B_output(C33_wire)
);

Corrimiento C34(
	.A_input({C33_wire[2:0],C29_wire[3]}),
	.B_output(C34_wire)
);

/*
*************************************************************************************
* Combiational processes description:
*
*		Continuous assing with cast to the defined data type decoder_en
*
*************************************************************************************
*/

assign Cen_o = decoder_en'({C30_wire[2:0],C23_wire[3]});
assign Dec_o = decoder_en'({C23_wire[2:0],C13_wire[3]});
assign Uni_o = decoder_en'({C13_wire[2:0],A_in[0]});
assign UniMillar_o = decoder_en'({C34_wire[2:0],C30_wire[3]});
assign DecMillar_o = decoder_en'({C31_wire[3],C32_wire[3],C33_wire[3],C34_wire[3]});


endmodule





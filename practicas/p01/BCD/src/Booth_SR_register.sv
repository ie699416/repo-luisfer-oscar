/*
*************************************************************************************
*	Coder:	OSCAR MAISTERRA FLORES	 		[ ie699416]
*	Coder: 	LUIS FERNANDO MORENO ALONSO	[ ie693314] 
*	Date : 	June 8th, 2019
*  Name :   Booth_SR_register.sv  
*
*	Description: Shift Right concatenated register.
*	- This local top-module interconects two N-bits register and one bit register
*    performing an arithmetic shift operation as a whole (2N+1) bits register. 
*  - The interconections sends the LSB directly to the MSB of the attached
*    register. 
*	- For the A register, the MSB is a duplicate of the BUS position [N-1] because of 
*    the signed bit.
*
* 
*************************************************************************************
*/
module Booth_SR_register

import nBitsPackage::*;

(
	// inputs for multiplication SR register
	input clk,
	input rst,
	input en_A, 	
	input en_Q,	
	input en_q0,
	input ASR_flag_in,
	input dataN_t multiplicand_in,	// Q <- Multiplier 
	input dataN_t A_fromAdder_in,		// A <- A + M_reg	
	output logic Q_reg_outputBit_o,
	output logic q0_o,							
	output dataN_t Result_H_o,			// Result_H from A_reg
	output dataN_t Result_L_o			// Result_L from Q_reg
	
);

/*
*************************************************************************************
* Interconneciton wires/logic
*************************************************************************************
*/

logic A_reg_outputBit_l;


/*
*************************************************************************************
* Instances definition 
*************************************************************************************
*/


SR_N_register A_reg
(
	.clk(clk),
	.rst(rst),
	.en(en_A),
	.ASR_flag_in(ASR_flag_in),						
	.inputBit_in(A_fromAdder_in[N-1]),						
	.data_in(A_fromAdder_in),	
	.outputBit_o(A_reg_outputBit_l),							
	.data_o(Result_H_o)					
	
);

SR_N_register Q_reg
(
	.clk(clk),
	.rst(rst),
	.en(en_Q),
	.ASR_flag_in(ASR_flag_in),						
	.inputBit_in(A_reg_outputBit_l),						
	.data_in(multiplicand_in),	
	.outputBit_o(Q_reg_outputBit_o),
	.data_o(Result_L_o)					
	
);

bit_register q0_reg
(
	.clk(clk),
	.rst(rst),
	.en(en_q0),									
	.data_in(Q_reg_outputBit_o),	
	.data_o(q0_o)				
	
);




endmodule 
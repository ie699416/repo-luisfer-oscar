
module BCD
import nBitsPackage::*;
(
	input dataNN_t result_in,
	input dataN_t SW_H_in,
	input dataN_t SW_L_in,
	output dataSeg_en Display_1, 
	output dataSeg_en Display_2, 
	output dataSeg_en Display_3, 
	output dataSeg_en Display_4, 
	output dataSeg_en Display_5, 
	output dataSeg_en Display_6, 
	output dataSeg_en Display_7,
	output dataSeg_en Display_8
	
);
`timescale 1ns / 1ps
module tb_Control;
import nBitsPackage::*;

logic       clk_L;
logic       rst_L;
Control_start Start_L;
Control_flag Flag_counter_L;

Control_flag ASR_flag_L;
Control_flag Rest_flag_L;
Control_ready Ready_flag_L;
Control_flag Enable_count_L;

Control_unit uut(
	// inputs for the control unit
	.clk(clk_L),
	.rst(rst_L),
	.Start(Start_L),					
	.Flag_counter(Flag_counter_L),			
	
	// outputs from Control Unit						
	.ASR_flag(ASR_flag_L),			
	.Rest_flag(Ready_flag_L),		
	.Ready_flag(Rest_flag_L),			
	.Enable_count(Enable_count_L)		
	
);

initial begin
        rst_L            = LOW;
        clk_L            = LOW;
        Start_L 		    = LOW;
		  Flag_counter_L   = LOW;
		 
    #4  rst_L    		    = HIGH; 
    #5  Start_L          = HIGH;
    #5 Start_L          = LOW;
    #5 Flag_counter_L   = HIGH;
		
	
end

always begin
    #1 clk_L <= ~clk_L;
end

endmodule


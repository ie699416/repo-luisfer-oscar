onerror {resume}
quietly WaveActivateNextPane {} 0
add wave -noupdate /tb_Control/clk_L
add wave -noupdate /tb_Control/rst_L
add wave -noupdate /tb_Control/Start_L
add wave -noupdate /tb_Control/Flag_counter_L
add wave -noupdate /tb_Control/ASR_flag_L
add wave -noupdate /tb_Control/Rest_flag_L
add wave -noupdate /tb_Control/Ready_flag_L
add wave -noupdate /tb_Control/Enable_count_L
add wave -noupdate /tb_Control/uut/Actual_state
TreeUpdate [SetDefaultTree]
WaveRestoreCursors {{Cursor 1} {21523 ps} 0}
quietly wave cursor active 1
configure wave -namecolwidth 150
configure wave -valuecolwidth 67
configure wave -justifyvalue left
configure wave -signalnamewidth 0
configure wave -snapdistance 10
configure wave -datasetprefix 0
configure wave -rowmargin 4
configure wave -childrowmargin 2
configure wave -gridoffset 0
configure wave -gridperiod 1
configure wave -griddelta 40
configure wave -timeline 0
configure wave -timelineunits ps
update
WaveRestoreZoom {0 ps} {162304 ps}

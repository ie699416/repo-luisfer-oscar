
`ifndef MYPACKAGE_DONE
   `define MYPACKAGE_DONE
	 package nBitsPackage; // package name

		parameter N = 16;
		parameter LSB_MASK = 2'b11;
		parameter ZERO16 = {N{1'b0}};
		parameter ZERO17 = {(N+1){1'b0}};		
		parameter ZERO32 = {(2*N){1'b0}};
		parameter ZERO33 = {(2*N+1){1'b0}};	
		parameter HIGH = 1'b1;
		parameter LOW  = 1'b0;
		parameter TRUE = 1'b1;
		parameter FALSE = 1'b0;
		parameter UNIT = 1'b1;
		parameter CERO = 1'b0;
		
		parameter MAXIMUM_VALUE = 16;
		parameter NBITS_FOR_COUNTER_FSM = $clog2(MAXIMUM_VALUE);		
		parameter ZERO_COUNT_FSM = {NBITS_FOR_COUNTER_FSM{1'b0}};
		
		parameter FREQUENCY = 1; 	//Desired Frequency 1 HZ
		parameter SYSTEM_CLK = 50000000;	//Reference clock
		parameter DELAY_COUNT_VALUE = freq_calc(FREQUENCY);
		parameter NBITS_FOR_COUNTER = $clog2(DELAY_COUNT_VALUE);
		parameter ZERO_COUNT = {NBITS_FOR_COUNTER{1'b0}};

		
	   typedef logic [N-1:0] data8_t;
		typedef logic [2*N-1:0] data16_t;
		typedef logic [2*N:0] data18_t;
		typedef logic [3:0] dataNum_t;
		typedef logic [NBITS_FOR_COUNTER - 1 : 0] dataCount_t;		
		typedef logic [NBITS_FOR_COUNTER_FSM - 1 : 0] dataCountFSM_t;
		
		
		typedef enum logic [1:0]{
        STATE_IDEL  = 2'b00,
        STATE_PROCESS   = 2'b11,
        STATE_READY   = 2'b10,
		  STATE_LOAD = 2'b01
		} State_e;
		
		/*typedef enum logic [W_ST-1:0]{
        ZERO  = 1'b0,
        ONE   = 1'b1
		} value_e;*/
		
		typedef logic [($clog2(N)):0] dataN_t;	//Ceil log operation for bus width	
		typedef logic [($clog2(2*N)):0] dataNN_t;	//Ceil log operation for double bus width	
		typedef logic [N:0] data17_t;
		typedef logic [2*N-1:0] data32_t;
		typedef logic [2*N:0] data33_t;				
		typedef logic [2:0] data2_t;	
		typedef logic [1:0] Control_start;
		typedef logic [1:0] Control_flag; 
		typedef logic [1:0] Control_ready;
		typedef enum logic [1:0] {IDLE, LOAD, SHIFT, READY} logic_enum_state_t;
		typedef enum logic [1:0] {Waiting_Shot, Shot_State, Waiting_Not_Shot} logic_enum_shot_t;
		typedef enum logic [4:0] {zero_s, one_s, two_s, three_s, four_s, five_S, six_s, seven_s} shift_enum_t;
		
		
		
		/*
*************************************************************************************
*/		
	 
		function integer freq_calc;
			input integer freq_requerida;
			integer result;
			begin
				result = (SYSTEM_CLK/freq_requerida)/2;
				freq_calc = result;
			end
		endfunction
		
/*
*************************************************************************************
*/
		
	endpackage
	`endif

/*
****************************************************************
* This module defines de top-block for the practice 
****************************************************************
*/
module Control_Unit
import nBitsPackage::*;
(
	// inputs for the control unit
	input Clk,
	input Rst,
	input Control_start Start,					// Signal that determines the Start of the proyect, by a push botton
	input Control_flag Flag_counter,			//This flag determines when the counter finished counting 
	
	// outputs from Control Unit						
	output Control_flag ASR_flag,			//Signal that sends the start process for the multiplication process 
	output Control_flag Rest_flag,		//Signal that send the flag of restart to the other modules
	output Control_ready Ready_flag,	   //Signal that indicates the state of the process 
	output Control_flag Enable_count		//Signal that enables the counter
);


State_e Actual_state;	

always_ff@(posedge Clk or negedge Rst) // Circuito Secuenicial en un proceso always.

	if (!Rst)
	
		Actual_state <= STATE_IDEL;
		
	else if (Start) begin
	
			case(Actual_state)
			
					STATE_IDEL:begin 
					
						if (Start == TRUE)
							Actual_state <= STATE_PROCESS; 
						else if (Flag_counter == TRUE)
								Actual_state <= STATE_IDEL;	
						else 
								Actual_state <= STATE_IDEL; 							
						end
						
					STATE_PROCESS:begin 
					
						if (Flag_counter==TRUE)
							Actual_state<=STATE_READY;
						else 
							Actual_state<=STATE_PROCESS;
						end
						
					STATE_READY:begin 
					
						if (Start==TRUE)
							Actual_state<=STATE_IDEL;
						else 
							Actual_state<=STATE_READY;							
						end
						
					default: Actual_state<=STATE_IDEL;
						
			 endcase
end


// CTO combinacional de salida.
always_comb begin
case(Actual_state)

    STATE_IDEL:begin    	
		Ready_flag = LOW;
		ASR_flag = HIGH; 
		Rest_flag = LOW;
		Enable_count = LOW;
	 end
	 
    STATE_PROCESS:begin
		Ready_flag = LOW;
		ASR_flag = LOW; 
		Rest_flag = LOW;
		Enable_count = HIGH;
	 end
	 
    STATE_READY:begin
		Ready_flag = HIGH;
		ASR_flag = LOW; 
		Rest_flag = LOW;
		Enable_count = LOW;
	 end
	 
endcase
end

endmodule
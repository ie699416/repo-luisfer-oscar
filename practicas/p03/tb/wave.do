onerror {resume}
quietly WaveActivateNextPane {} 0
add wave -noupdate -expand -group TB /p03_tb/clkFPGA_l
add wave -noupdate -expand -group TB /p03_tb/reset_pb_l
add wave -noupdate -expand -group TB /p03_tb/serial_input_RX_l
add wave -noupdate -expand -group TB /p03_tb/Data_tx_in
add wave -noupdate -expand -group TB /p03_tb/data_l
add wave -noupdate -expand -group TB /p03_tb/tx_enable
add wave -noupdate -expand -group TB /p03_tb/data_out_tx
add wave -noupdate -expand -group UART /p03_tb/uut/clkFPGA
add wave -noupdate -expand -group UART /p03_tb/uut/reset_pb_in
add wave -noupdate -expand -group UART /p03_tb/uut/serial_input_RX_in
add wave -noupdate -expand -group UART /p03_tb/uut/Tx_data
add wave -noupdate -expand -group UART /p03_tb/uut/Tx_enable
add wave -noupdate -expand -group UART /p03_tb/uut/data_o
add wave -noupdate -expand -group UART /p03_tb/uut/RX_parity_o
add wave -noupdate -expand -group UART /p03_tb/uut/TX_data
add wave -noupdate -group TX_Module /p03_tb/uut/RX/TX_UART_MODULE/clk
add wave -noupdate -group TX_Module /p03_tb/uut/RX/TX_UART_MODULE/rst
add wave -noupdate -group TX_Module /p03_tb/uut/RX/TX_UART_MODULE/en
add wave -noupdate -group TX_Module /p03_tb/uut/RX/TX_UART_MODULE/load
add wave -noupdate -group TX_Module /p03_tb/uut/RX/TX_UART_MODULE/transmitData_in
add wave -noupdate -group TX_Module /p03_tb/uut/RX/TX_UART_MODULE/serialData_TX_o
add wave -noupdate -group TX_Module /p03_tb/uut/RX/TX_UART_MODULE/Wire_count
add wave -noupdate -group TX_Module /p03_tb/uut/RX/TX_UART_MODULE/Start_counter
add wave -noupdate -group TX_Module /p03_tb/uut/RX/TX_UART_MODULE/Counter_flag
add wave -noupdate -group TX_Module /p03_tb/uut/RX/TX_UART_MODULE/Start_Reg
add wave -noupdate -group TX_Module /p03_tb/uut/RX/TX_UART_MODULE/Start_clk_115
add wave -noupdate -group TX_Module /p03_tb/uut/RX/TX_UART_MODULE/l_s_wire
add wave -noupdate -expand -group TX_FSM /p03_tb/uut/RX/TX_FSM/clk
add wave -noupdate -expand -group TX_FSM /p03_tb/uut/RX/TX_FSM/rst
add wave -noupdate -expand -group TX_FSM /p03_tb/uut/RX/TX_FSM/Counter_flag
add wave -noupdate -expand -group TX_FSM /p03_tb/uut/RX/TX_FSM/Start
add wave -noupdate -expand -group TX_FSM /p03_tb/uut/RX/TX_FSM/start_counter
add wave -noupdate -expand -group TX_FSM /p03_tb/uut/RX/TX_FSM/TX_enable
add wave -noupdate -expand -group TX_FSM /p03_tb/uut/RX/TX_FSM/Load_flag
add wave -noupdate -expand -group TX_FSM /p03_tb/uut/RX/TX_FSM/TX_flag
add wave -noupdate -expand -group TX_FSM /p03_tb/uut/RX/TX_FSM/UART_currentState
add wave -noupdate /p03_tb/uut/RX/Receive/clk
add wave -noupdate /p03_tb/uut/RX/Receive/rst
add wave -noupdate /p03_tb/uut/RX/Receive/en_SR
add wave -noupdate /p03_tb/uut/RX/Receive/en_PIPO
add wave -noupdate /p03_tb/uut/RX/Receive/serialData_RX_in
add wave -noupdate /p03_tb/uut/RX/Receive/parity_RX_o
add wave -noupdate /p03_tb/uut/RX/Receive/receivedData_o
add wave -noupdate /p03_tb/uut/RX/Receive/Q_l
TreeUpdate [SetDefaultTree]
WaveRestoreCursors {{Cursor 1} {1289707482 ps} 0}
quietly wave cursor active 1
configure wave -namecolwidth 247
configure wave -valuecolwidth 100
configure wave -justifyvalue left
configure wave -signalnamewidth 0
configure wave -snapdistance 10
configure wave -datasetprefix 0
configure wave -rowmargin 4
configure wave -childrowmargin 2
configure wave -gridoffset 0
configure wave -gridperiod 1
configure wave -griddelta 40
configure wave -timeline 0
configure wave -timelineunits ps
update
WaveRestoreZoom {0 ps} {2137040896 ps}

`timescale 10ns / 1ps
module p03_tb;

import nBitsPackage::*;

logic clkFPGA_l = 1'b1;
logic reset_pb_l;
logic serial_input_RX_l;	

	
p03 	uut (
		.clkFPGA(clkFPGA_l),
		.reset_pb_in(reset_pb_l),
		.serial_input_RX_in(serial_input_RX_l)
);


/*********************************************************/
initial // Clock generator
  begin
    forever #1 clkFPGA_l = !clkFPGA_l;
  end
/*********************************************************/
initial begin // reset generator


#0 reset_pb_l = 1'b0;

#0 serial_input_RX_l = 1'b1; 

#868 reset_pb_l = 1'b1;


/**
***************************************************
* 1st string
***************************************************
*/

/*0xFE*/

#868 serial_input_RX_l = 1'b1; // delay

#868 serial_input_RX_l = 1'b0; // start

#868 serial_input_RX_l = 1'b0; 

#868 serial_input_RX_l = 1'b1;

#868 serial_input_RX_l = 1'b1; 

#868 serial_input_RX_l = 1'b1;

#868 serial_input_RX_l = 1'b1; 

#868 serial_input_RX_l = 1'b1;

#868 serial_input_RX_l = 1'b1; 

#868 serial_input_RX_l = 1'b1;

#868 serial_input_RX_l = 1'b1; // parity

#868 serial_input_RX_l = 1'b1; // stop

#868 serial_input_RX_l = 1'b1; // IDLE


/*0x02 L = 2 */

#868 serial_input_RX_l = 1'b1; // delay

#868 serial_input_RX_l = 1'b0; // start

#868 serial_input_RX_l = 1'b0; 

#868 serial_input_RX_l = 1'b1;

#868 serial_input_RX_l = 1'b0; 

#868 serial_input_RX_l = 1'b0;

#868 serial_input_RX_l = 1'b0; 

#868 serial_input_RX_l = 1'b0;

#868 serial_input_RX_l = 1'b0; 

#868 serial_input_RX_l = 1'b0;

#868 serial_input_RX_l = 1'b1; // parity

#868 serial_input_RX_l = 1'b1; // stop

#868 serial_input_RX_l = 1'b1; // IDLE

/*0x01 CMD = 1 (N)*/

#868 serial_input_RX_l = 1'b1; // delay

#868 serial_input_RX_l = 1'b0; // start

#868 serial_input_RX_l = 1'b1; 

#868 serial_input_RX_l = 1'b0;

#868 serial_input_RX_l = 1'b0; 

#868 serial_input_RX_l = 1'b0;

#868 serial_input_RX_l = 1'b0; 

#868 serial_input_RX_l = 1'b0;

#868 serial_input_RX_l = 1'b0; 

#868 serial_input_RX_l = 1'b0;

#868 serial_input_RX_l = 1'b1; // parity

#868 serial_input_RX_l = 1'b1; // stop

#868 serial_input_RX_l = 1'b1; // IDLE

/*0x04 N = 4*/

#868 serial_input_RX_l = 1'b1; // delay

#868 serial_input_RX_l = 1'b0; // start

#868 serial_input_RX_l = 1'b0; 

#868 serial_input_RX_l = 1'b0;

#868 serial_input_RX_l = 1'b1; 

#868 serial_input_RX_l = 1'b0;

#868 serial_input_RX_l = 1'b0; 

#868 serial_input_RX_l = 1'b0;

#868 serial_input_RX_l = 1'b0; 

#868 serial_input_RX_l = 1'b0;

#868 serial_input_RX_l = 1'b1; // parity

#868 serial_input_RX_l = 1'b1; // stop

#868 serial_input_RX_l = 1'b1; // IDLE

/*0xEF*/

#868 serial_input_RX_l = 1'b1; // delay

#868 serial_input_RX_l = 1'b0; // start

#868 serial_input_RX_l = 1'b1; 

#868 serial_input_RX_l = 1'b1;

#868 serial_input_RX_l = 1'b1; 

#868 serial_input_RX_l = 1'b1;

#868 serial_input_RX_l = 1'b0; 

#868 serial_input_RX_l = 1'b1;

#868 serial_input_RX_l = 1'b1; 

#868 serial_input_RX_l = 1'b1;

#868 serial_input_RX_l = 1'b1; // parity

#868 serial_input_RX_l = 1'b1; // stop

#868 serial_input_RX_l = 1'b1; // IDLE

/**
***************************************************
* 2nd string
***************************************************
*/


/*0xFE*/

#868 serial_input_RX_l = 1'b1; // delay

#868 serial_input_RX_l = 1'b0; // start

#868 serial_input_RX_l = 1'b0; 

#868 serial_input_RX_l = 1'b1;

#868 serial_input_RX_l = 1'b1; 

#868 serial_input_RX_l = 1'b1;

#868 serial_input_RX_l = 1'b1; 

#868 serial_input_RX_l = 1'b1;

#868 serial_input_RX_l = 1'b1; 

#868 serial_input_RX_l = 1'b1;

#868 serial_input_RX_l = 1'b1; // parity

#868 serial_input_RX_l = 1'b1; // stop

#868 serial_input_RX_l = 1'b1; // IDLE


/*0x1 L = 1 */

#868 serial_input_RX_l = 1'b1; // delay

#868 serial_input_RX_l = 1'b0; // start

#868 serial_input_RX_l = 1'b1; 

#868 serial_input_RX_l = 1'b0;

#868 serial_input_RX_l = 1'b0; 

#868 serial_input_RX_l = 1'b0;

#868 serial_input_RX_l = 1'b0; 

#868 serial_input_RX_l = 1'b0;

#868 serial_input_RX_l = 1'b0; 

#868 serial_input_RX_l = 1'b0;

#868 serial_input_RX_l = 1'b1; // parity

#868 serial_input_RX_l = 1'b1; // stop

#868 serial_input_RX_l = 1'b1; // IDLE

/*0x01 CMD = 3 (START)*/

#868 serial_input_RX_l = 1'b1; // delay

#868 serial_input_RX_l = 1'b0; // start

#868 serial_input_RX_l = 1'b1; 

#868 serial_input_RX_l = 1'b1;

#868 serial_input_RX_l = 1'b0; 

#868 serial_input_RX_l = 1'b0;

#868 serial_input_RX_l = 1'b0; 

#868 serial_input_RX_l = 1'b0;

#868 serial_input_RX_l = 1'b0; 

#868 serial_input_RX_l = 1'b0;

#868 serial_input_RX_l = 1'b1; // parity

#868 serial_input_RX_l = 1'b1; // stop

#868 serial_input_RX_l = 1'b1; // IDLE


/*0xEF*/

#868 serial_input_RX_l = 1'b1; // delay

#868 serial_input_RX_l = 1'b0; // start

#868 serial_input_RX_l = 1'b1; 

#868 serial_input_RX_l = 1'b1;

#868 serial_input_RX_l = 1'b1; 

#868 serial_input_RX_l = 1'b1;

#868 serial_input_RX_l = 1'b0; 

#868 serial_input_RX_l = 1'b1;

#868 serial_input_RX_l = 1'b1; 

#868 serial_input_RX_l = 1'b1;

#868 serial_input_RX_l = 1'b1; // parity

#868 serial_input_RX_l = 1'b1; // stop

#868 serial_input_RX_l = 1'b1; // IDLE


/**
***************************************************
* 3rd string
***************************************************
*/

/*0xFE*/

#868 serial_input_RX_l = 1'b1; // delay

#868 serial_input_RX_l = 1'b0; // start

#868 serial_input_RX_l = 1'b0; 

#868 serial_input_RX_l = 1'b1;

#868 serial_input_RX_l = 1'b1; 

#868 serial_input_RX_l = 1'b1;

#868 serial_input_RX_l = 1'b1; 

#868 serial_input_RX_l = 1'b1;

#868 serial_input_RX_l = 1'b1; 

#868 serial_input_RX_l = 1'b1;

#868 serial_input_RX_l = 1'b1; // parity

#868 serial_input_RX_l = 1'b1; // stop

#868 serial_input_RX_l = 1'b1; // IDLE


/*0x17 L = 17 */

#868 serial_input_RX_l = 1'b1; // delay

#868 serial_input_RX_l = 1'b0; // start

#868 serial_input_RX_l = 1'b1; 

#868 serial_input_RX_l = 1'b0;

#868 serial_input_RX_l = 1'b0; 

#868 serial_input_RX_l = 1'b0;

#868 serial_input_RX_l = 1'b1; 

#868 serial_input_RX_l = 1'b0;

#868 serial_input_RX_l = 1'b0; 

#868 serial_input_RX_l = 1'b0;

#868 serial_input_RX_l = 1'b0; // parity

#868 serial_input_RX_l = 1'b1; // stop

#868 serial_input_RX_l = 1'b1; // IDLE

/*0x04 CMD = 1 (Matrix)*/

#868 serial_input_RX_l = 1'b1; // delay

#868 serial_input_RX_l = 1'b0; // start

#868 serial_input_RX_l = 1'b0; 

#868 serial_input_RX_l = 1'b0;

#868 serial_input_RX_l = 1'b1; 

#868 serial_input_RX_l = 1'b0;

#868 serial_input_RX_l = 1'b0; 

#868 serial_input_RX_l = 1'b0;

#868 serial_input_RX_l = 1'b0; 

#868 serial_input_RX_l = 1'b0;

#868 serial_input_RX_l = 1'b1; // parity

#868 serial_input_RX_l = 1'b1; // stop

#868 serial_input_RX_l = 1'b1; // IDLE

/*0x00 data0 = 0*/

#868 serial_input_RX_l = 1'b1; // delay

#868 serial_input_RX_l = 1'b0; // start

#868 serial_input_RX_l = 1'b0; 

#868 serial_input_RX_l = 1'b0;

#868 serial_input_RX_l = 1'b0; 

#868 serial_input_RX_l = 1'b0;

#868 serial_input_RX_l = 1'b0; 

#868 serial_input_RX_l = 1'b0;

#868 serial_input_RX_l = 1'b0; 

#868 serial_input_RX_l = 1'b0;

#868 serial_input_RX_l = 1'b1; // parity

#868 serial_input_RX_l = 1'b1; // stop

#868 serial_input_RX_l = 1'b1; // IDLE

/*0x01 data0 = 1*/

#868 serial_input_RX_l = 1'b1; // delay

#868 serial_input_RX_l = 1'b0; // start

#868 serial_input_RX_l = 1'b1; 

#868 serial_input_RX_l = 1'b0;

#868 serial_input_RX_l = 1'b0; 

#868 serial_input_RX_l = 1'b0;

#868 serial_input_RX_l = 1'b0; 

#868 serial_input_RX_l = 1'b0;

#868 serial_input_RX_l = 1'b0; 

#868 serial_input_RX_l = 1'b0;

#868 serial_input_RX_l = 1'b1; // parity

#868 serial_input_RX_l = 1'b1; // stop

#868 serial_input_RX_l = 1'b1; // IDLE

/*0x02 data0 = 2*/

#868 serial_input_RX_l = 1'b1; // delay

#868 serial_input_RX_l = 1'b0; // start

#868 serial_input_RX_l = 1'b0; 

#868 serial_input_RX_l = 1'b1;

#868 serial_input_RX_l = 1'b0; 

#868 serial_input_RX_l = 1'b0;

#868 serial_input_RX_l = 1'b0; 

#868 serial_input_RX_l = 1'b0;

#868 serial_input_RX_l = 1'b0; 

#868 serial_input_RX_l = 1'b0;

#868 serial_input_RX_l = 1'b1; // parity

#868 serial_input_RX_l = 1'b1; // stop

#868 serial_input_RX_l = 1'b1; // IDLE

/*0x03 data0 = 3*/

#868 serial_input_RX_l = 1'b1; // delay

#868 serial_input_RX_l = 1'b0; // start

#868 serial_input_RX_l = 1'b1; 

#868 serial_input_RX_l = 1'b1;

#868 serial_input_RX_l = 1'b0; 

#868 serial_input_RX_l = 1'b0;

#868 serial_input_RX_l = 1'b0; 

#868 serial_input_RX_l = 1'b0;

#868 serial_input_RX_l = 1'b0; 

#868 serial_input_RX_l = 1'b0;

#868 serial_input_RX_l = 1'b1; // parity

#868 serial_input_RX_l = 1'b1; // stop

#868 serial_input_RX_l = 1'b1; // IDLE

/*0x00 data0 = 0*/

#868 serial_input_RX_l = 1'b1; // delay

#868 serial_input_RX_l = 1'b0; // start

#868 serial_input_RX_l = 1'b0; 

#868 serial_input_RX_l = 1'b0;

#868 serial_input_RX_l = 1'b0; 

#868 serial_input_RX_l = 1'b0;

#868 serial_input_RX_l = 1'b0; 

#868 serial_input_RX_l = 1'b0;

#868 serial_input_RX_l = 1'b0; 

#868 serial_input_RX_l = 1'b0;

#868 serial_input_RX_l = 1'b1; // parity

#868 serial_input_RX_l = 1'b1; // stop

#868 serial_input_RX_l = 1'b1; // IDLE

/*0x01 data0 = 1*/

#868 serial_input_RX_l = 1'b1; // delay

#868 serial_input_RX_l = 1'b0; // start

#868 serial_input_RX_l = 1'b1; 

#868 serial_input_RX_l = 1'b0;

#868 serial_input_RX_l = 1'b0; 

#868 serial_input_RX_l = 1'b0;

#868 serial_input_RX_l = 1'b0; 

#868 serial_input_RX_l = 1'b0;

#868 serial_input_RX_l = 1'b0; 

#868 serial_input_RX_l = 1'b0;

#868 serial_input_RX_l = 1'b1; // parity

#868 serial_input_RX_l = 1'b1; // stop

#868 serial_input_RX_l = 1'b1; // IDLE

/*0x02 data0 = 2*/

#868 serial_input_RX_l = 1'b1; // delay

#868 serial_input_RX_l = 1'b0; // start

#868 serial_input_RX_l = 1'b0; 

#868 serial_input_RX_l = 1'b1;

#868 serial_input_RX_l = 1'b0; 

#868 serial_input_RX_l = 1'b0;

#868 serial_input_RX_l = 1'b0; 

#868 serial_input_RX_l = 1'b0;

#868 serial_input_RX_l = 1'b0; 

#868 serial_input_RX_l = 1'b0;

#868 serial_input_RX_l = 1'b1; // parity

#868 serial_input_RX_l = 1'b1; // stop

#868 serial_input_RX_l = 1'b1; // IDLE

/*0x03 data0 = 3*/

#868 serial_input_RX_l = 1'b1; // delay

#868 serial_input_RX_l = 1'b0; // start

#868 serial_input_RX_l = 1'b1; 

#868 serial_input_RX_l = 1'b1;

#868 serial_input_RX_l = 1'b0; 

#868 serial_input_RX_l = 1'b0;

#868 serial_input_RX_l = 1'b0; 

#868 serial_input_RX_l = 1'b0;

#868 serial_input_RX_l = 1'b0; 

#868 serial_input_RX_l = 1'b0;

#868 serial_input_RX_l = 1'b1; // parity

#868 serial_input_RX_l = 1'b1; // stop

#868 serial_input_RX_l = 1'b1; // IDLE

/*0x00 data0 = 0*/

#868 serial_input_RX_l = 1'b1; // delay

#868 serial_input_RX_l = 1'b0; // start

#868 serial_input_RX_l = 1'b0; 

#868 serial_input_RX_l = 1'b0;

#868 serial_input_RX_l = 1'b0; 

#868 serial_input_RX_l = 1'b0;

#868 serial_input_RX_l = 1'b0; 

#868 serial_input_RX_l = 1'b0;

#868 serial_input_RX_l = 1'b0; 

#868 serial_input_RX_l = 1'b0;

#868 serial_input_RX_l = 1'b1; // parity

#868 serial_input_RX_l = 1'b1; // stop

#868 serial_input_RX_l = 1'b1; // IDLE

/*0x01 data0 = 1*/

#868 serial_input_RX_l = 1'b1; // delay

#868 serial_input_RX_l = 1'b0; // start

#868 serial_input_RX_l = 1'b1; 

#868 serial_input_RX_l = 1'b0;

#868 serial_input_RX_l = 1'b0; 

#868 serial_input_RX_l = 1'b0;

#868 serial_input_RX_l = 1'b0; 

#868 serial_input_RX_l = 1'b0;

#868 serial_input_RX_l = 1'b0; 

#868 serial_input_RX_l = 1'b0;

#868 serial_input_RX_l = 1'b1; // parity

#868 serial_input_RX_l = 1'b1; // stop

#868 serial_input_RX_l = 1'b1; // IDLE

/*0x02 data0 = 2*/

#868 serial_input_RX_l = 1'b1; // delay

#868 serial_input_RX_l = 1'b0; // start

#868 serial_input_RX_l = 1'b0; 

#868 serial_input_RX_l = 1'b1;

#868 serial_input_RX_l = 1'b0; 

#868 serial_input_RX_l = 1'b0;

#868 serial_input_RX_l = 1'b0; 

#868 serial_input_RX_l = 1'b0;

#868 serial_input_RX_l = 1'b0; 

#868 serial_input_RX_l = 1'b0;

#868 serial_input_RX_l = 1'b1; // parity

#868 serial_input_RX_l = 1'b1; // stop

#868 serial_input_RX_l = 1'b1; // IDLE

/*0x03 data0 = 3*/

#868 serial_input_RX_l = 1'b1; // delay

#868 serial_input_RX_l = 1'b0; // start

#868 serial_input_RX_l = 1'b1; 

#868 serial_input_RX_l = 1'b1;

#868 serial_input_RX_l = 1'b0; 

#868 serial_input_RX_l = 1'b0;

#868 serial_input_RX_l = 1'b0; 

#868 serial_input_RX_l = 1'b0;

#868 serial_input_RX_l = 1'b0; 

#868 serial_input_RX_l = 1'b0;

#868 serial_input_RX_l = 1'b1; // parity

#868 serial_input_RX_l = 1'b1; // stop

#868 serial_input_RX_l = 1'b1; // IDLE

/*0x00 data0 = 0*/

#868 serial_input_RX_l = 1'b1; // delay

#868 serial_input_RX_l = 1'b0; // start

#868 serial_input_RX_l = 1'b0; 

#868 serial_input_RX_l = 1'b0;

#868 serial_input_RX_l = 1'b0; 

#868 serial_input_RX_l = 1'b0;

#868 serial_input_RX_l = 1'b0; 

#868 serial_input_RX_l = 1'b0;

#868 serial_input_RX_l = 1'b0; 

#868 serial_input_RX_l = 1'b0;

#868 serial_input_RX_l = 1'b1; // parity

#868 serial_input_RX_l = 1'b1; // stop

#868 serial_input_RX_l = 1'b1; // IDLE

/*0x01 data0 = 1*/

#868 serial_input_RX_l = 1'b1; // delay

#868 serial_input_RX_l = 1'b0; // start

#868 serial_input_RX_l = 1'b1; 

#868 serial_input_RX_l = 1'b0;

#868 serial_input_RX_l = 1'b0; 

#868 serial_input_RX_l = 1'b0;

#868 serial_input_RX_l = 1'b0; 

#868 serial_input_RX_l = 1'b0;

#868 serial_input_RX_l = 1'b0; 

#868 serial_input_RX_l = 1'b0;

#868 serial_input_RX_l = 1'b1; // parity

#868 serial_input_RX_l = 1'b1; // stop

#868 serial_input_RX_l = 1'b1; // IDLE

/*0x02 data0 = 2*/

#868 serial_input_RX_l = 1'b1; // delay

#868 serial_input_RX_l = 1'b0; // start

#868 serial_input_RX_l = 1'b0; 

#868 serial_input_RX_l = 1'b1;

#868 serial_input_RX_l = 1'b0; 

#868 serial_input_RX_l = 1'b0;

#868 serial_input_RX_l = 1'b0; 

#868 serial_input_RX_l = 1'b0;

#868 serial_input_RX_l = 1'b0; 

#868 serial_input_RX_l = 1'b0;

#868 serial_input_RX_l = 1'b1; // parity

#868 serial_input_RX_l = 1'b1; // stop

#868 serial_input_RX_l = 1'b1; // IDLE

/*0x03 data0 = 3*/

#868 serial_input_RX_l = 1'b1; // delay

#868 serial_input_RX_l = 1'b0; // start

#868 serial_input_RX_l = 1'b1; 

#868 serial_input_RX_l = 1'b1;

#868 serial_input_RX_l = 1'b0; 

#868 serial_input_RX_l = 1'b0;

#868 serial_input_RX_l = 1'b0; 

#868 serial_input_RX_l = 1'b0;

#868 serial_input_RX_l = 1'b0; 

#868 serial_input_RX_l = 1'b0;

#868 serial_input_RX_l = 1'b1; // parity

#868 serial_input_RX_l = 1'b1; // stop

#868 serial_input_RX_l = 1'b1; // IDLE

/*0xEF*/

#868 serial_input_RX_l = 1'b1; // delay

#868 serial_input_RX_l = 1'b0; // start

#868 serial_input_RX_l = 1'b1; 

#868 serial_input_RX_l = 1'b1;

#868 serial_input_RX_l = 1'b1; 

#868 serial_input_RX_l = 1'b1;

#868 serial_input_RX_l = 1'b0; 

#868 serial_input_RX_l = 1'b1;

#868 serial_input_RX_l = 1'b1; 

#868 serial_input_RX_l = 1'b1;

#868 serial_input_RX_l = 1'b1; // parity

#868 serial_input_RX_l = 1'b1; // stop

#868 serial_input_RX_l = 1'b1; // IDLE

/**
***************************************************
* 4th string
***************************************************
*/



/*0xFE*/

#868 serial_input_RX_l = 1'b1; // delay

#868 serial_input_RX_l = 1'b0; // start

#868 serial_input_RX_l = 1'b0; 

#868 serial_input_RX_l = 1'b1;

#868 serial_input_RX_l = 1'b1; 

#868 serial_input_RX_l = 1'b1;

#868 serial_input_RX_l = 1'b1; 

#868 serial_input_RX_l = 1'b1;

#868 serial_input_RX_l = 1'b1; 

#868 serial_input_RX_l = 1'b1;

#868 serial_input_RX_l = 1'b1; // parity

#868 serial_input_RX_l = 1'b1; // stop

#868 serial_input_RX_l = 1'b1; // IDLE


/*0x5 L = 5*/

#868 serial_input_RX_l = 1'b1; // delay

#868 serial_input_RX_l = 1'b0; // start

#868 serial_input_RX_l = 1'b1; 

#868 serial_input_RX_l = 1'b0;

#868 serial_input_RX_l = 1'b1; 

#868 serial_input_RX_l = 1'b0;

#868 serial_input_RX_l = 1'b0; 

#868 serial_input_RX_l = 1'b0;

#868 serial_input_RX_l = 1'b0; 

#868 serial_input_RX_l = 1'b0;

#868 serial_input_RX_l = 1'b0; // parity

#868 serial_input_RX_l = 1'b1; // stop

#868 serial_input_RX_l = 1'b1; // IDLE

/*0x05 CMD = 5 (Vector)*/

#868 serial_input_RX_l = 1'b1; // delay

#868 serial_input_RX_l = 1'b0; // start

#868 serial_input_RX_l = 1'b1; 

#868 serial_input_RX_l = 1'b0;

#868 serial_input_RX_l = 1'b1; 

#868 serial_input_RX_l = 1'b0;

#868 serial_input_RX_l = 1'b0; 

#868 serial_input_RX_l = 1'b0;

#868 serial_input_RX_l = 1'b0; 

#868 serial_input_RX_l = 1'b0;

#868 serial_input_RX_l = 1'b1; // parity

#868 serial_input_RX_l = 1'b1; // stop

#868 serial_input_RX_l = 1'b1; // IDLE

/*0x00 data0 = 0*/

#868 serial_input_RX_l = 1'b1; // delay

#868 serial_input_RX_l = 1'b0; // start

#868 serial_input_RX_l = 1'b0; 

#868 serial_input_RX_l = 1'b0;

#868 serial_input_RX_l = 1'b0; 

#868 serial_input_RX_l = 1'b0;

#868 serial_input_RX_l = 1'b0; 

#868 serial_input_RX_l = 1'b0;

#868 serial_input_RX_l = 1'b0; 

#868 serial_input_RX_l = 1'b0;

#868 serial_input_RX_l = 1'b1; // parity

#868 serial_input_RX_l = 1'b1; // stop

#868 serial_input_RX_l = 1'b1; // IDLE

/*0x01 data0 = 1*/

#868 serial_input_RX_l = 1'b1; // delay

#868 serial_input_RX_l = 1'b0; // start

#868 serial_input_RX_l = 1'b1; 

#868 serial_input_RX_l = 1'b0;

#868 serial_input_RX_l = 1'b0; 

#868 serial_input_RX_l = 1'b0;

#868 serial_input_RX_l = 1'b0; 

#868 serial_input_RX_l = 1'b0;

#868 serial_input_RX_l = 1'b0; 

#868 serial_input_RX_l = 1'b0;

#868 serial_input_RX_l = 1'b1; // parity

#868 serial_input_RX_l = 1'b1; // stop

#868 serial_input_RX_l = 1'b1; // IDLE

/*0x02 data0 = 2*/

#868 serial_input_RX_l = 1'b1; // delay

#868 serial_input_RX_l = 1'b0; // start

#868 serial_input_RX_l = 1'b0; 

#868 serial_input_RX_l = 1'b1;

#868 serial_input_RX_l = 1'b0; 

#868 serial_input_RX_l = 1'b0;

#868 serial_input_RX_l = 1'b0; 

#868 serial_input_RX_l = 1'b0;

#868 serial_input_RX_l = 1'b0; 

#868 serial_input_RX_l = 1'b0;

#868 serial_input_RX_l = 1'b1; // parity

#868 serial_input_RX_l = 1'b1; // stop

#868 serial_input_RX_l = 1'b1; // IDLE

/*0x03 data0 = 3*/

#868 serial_input_RX_l = 1'b1; // delay

#868 serial_input_RX_l = 1'b0; // start

#868 serial_input_RX_l = 1'b1; 

#868 serial_input_RX_l = 1'b1;

#868 serial_input_RX_l = 1'b0; 

#868 serial_input_RX_l = 1'b0;

#868 serial_input_RX_l = 1'b0; 

#868 serial_input_RX_l = 1'b0;

#868 serial_input_RX_l = 1'b0; 

#868 serial_input_RX_l = 1'b0;

#868 serial_input_RX_l = 1'b1; // parity

#868 serial_input_RX_l = 1'b1; // stop

#868 serial_input_RX_l = 1'b1; // IDLE

/*0xEF*/

#868 serial_input_RX_l = 1'b1; // delay

#868 serial_input_RX_l = 1'b0; // start

#868 serial_input_RX_l = 1'b1; 

#868 serial_input_RX_l = 1'b1;

#868 serial_input_RX_l = 1'b1; 

#868 serial_input_RX_l = 1'b1;

#868 serial_input_RX_l = 1'b0; 

#868 serial_input_RX_l = 1'b1;

#868 serial_input_RX_l = 1'b1; 

#868 serial_input_RX_l = 1'b1;

#868 serial_input_RX_l = 1'b1; // parity

#868 serial_input_RX_l = 1'b1; // stop

#868 serial_input_RX_l = 1'b1; // IDLE
	
end

/*********************************************************/


endmodule

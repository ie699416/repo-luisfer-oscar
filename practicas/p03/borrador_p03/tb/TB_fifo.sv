
module TB_fifo
import FIFO_pkg::*;
();

	logic clk_wr;
	logic clk_rd;
	logic rst;

	dataN_t  data_in;
	logic push;
	logic pop;

	dataN_t  data_out;
	logic empty;
	logic full;

	
T08_FIFO TB_t08
(	
	.clk_wr(clk_wr),
	.clk_rd(clk_rd),
	.rst(rst),
	.data_in(data_in),
	.push(push),
	.pop(pop),

	.data_out(data_out),
	.empty_flag(empty),
	.full_flag(full)
);



always begin
    #1 clk_wr <= ~clk_wr;
end
always begin
	#3 clk_rd <= ~clk_rd;
end

initial begin
	clk_wr = 1;  #1;
	clk_rd = 1;  #1;

	push   = 0; 
	pop    = 0; 

	rst    = 1;  #2;
	rst    = 0;  #3;
	rst    = 1;  #2;

	data_in = 1;
	push   = 1;  #2;
	push   = 0;  #4;

	data_in = 2;
	push   = 1;  #2;
	push   = 0;  #4;

	data_in = 3;
	push   = 1;  #2;
	push   = 0;  #4;

	data_in = 4;
	push   = 1;  #2;
	push   = 0;  #4;
	
	data_in = 5;
	push   = 1;  #2;
	push   = 0;  #4;
	
	data_in = 6;
	push   = 1;  #2;
	push   = 0;  #4;
	
	data_in = 7;
	push   = 1;  #2;
	push   = 0;  #4;
	
	data_in = 8;
	push   = 1;  #2;
	push   = 0;  #4;

	data_in = 9;
	push   = 1;  #2;
	push   = 0;  #4; 
	
	data_in = 10;
	push   = 1;  #2;
	push   = 0;  #4;
	
	data_in = 11;
	push   = 1;  #2;
	push   = 0;  #4;
	
	data_in = 12;
	push   = 1;  #2;
	push   = 0;  #4;
	
	data_in = 13;
	push   = 1;  #2;
	push   = 0;  #4;
	
	data_in = 14;
	push   = 1;  #2;
	push   = 0;  #4;
	
	data_in = 15;
	push   = 1;  #2;
	push   = 0;  #4;
	
	data_in = 16;
	push   = 1;  #2;
	push   = 0;  #4;
	
	data_in = 17;
	push   = 1;  #2;
	push   = 0;  #4;

	data_in = 18;
	push   = 1;  #2;
	push   = 0;  #4; 
	
	data_in = 19;
	push   = 1;  #2;
	push   = 0;  #4;
	
	data_in = 20;
	push   = 1;  #2;
	push   = 0;  #4;
	
	pop    = 1;  #6;
	pop    = 0;  #12;
	
	pop    = 1;  #6;
	pop    = 0;  #12;

	pop    = 1;  #6;
	pop    = 0;  #12;

	pop    = 1;  #6;
	pop    = 0;  #12;

	pop    = 1;  #6;
	pop    = 0;  #12;
	
	pop    = 1;  #6;
	pop    = 0;  #12;
	
	pop    = 1;  #6;
	pop    = 0;  #12;
	
	pop    = 1;  #6;
	pop    = 0;  #12;
	
	pop    = 1;  #6;
	pop    = 0;  #12;
	

	




end

endmodule


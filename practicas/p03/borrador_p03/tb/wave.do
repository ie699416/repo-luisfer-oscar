onerror {resume}
quietly WaveActivateNextPane {} 0
add wave -noupdate -expand -group Top_fifo -color Magenta /TB_fifo/TB_t08/clk_wr
add wave -noupdate -expand -group Top_fifo -color Magenta /TB_fifo/TB_t08/clk_rd
add wave -noupdate -expand -group Top_fifo -color Magenta /TB_fifo/TB_t08/rst
add wave -noupdate -expand -group Top_fifo -color Magenta /TB_fifo/TB_t08/data_in
add wave -noupdate -expand -group Top_fifo -color Magenta -format Literal /TB_fifo/TB_t08/push
add wave -noupdate -expand -group Top_fifo -color Magenta -format Literal /TB_fifo/TB_t08/pop
add wave -noupdate -expand -group Top_fifo -color Magenta /TB_fifo/TB_t08/data_out
add wave -noupdate -expand -group RAM -color Blue /TB_fifo/TB_t08/RAM/wr_enable
add wave -noupdate -expand -group RAM -color Blue /TB_fifo/TB_t08/RAM/rd_enable
add wave -noupdate -expand -group RAM -color Blue /TB_fifo/TB_t08/RAM/data_wr
add wave -noupdate -expand -group RAM -color Blue /TB_fifo/TB_t08/RAM/addr_wr
add wave -noupdate -expand -group RAM -color Blue /TB_fifo/TB_t08/RAM/addr_dr
add wave -noupdate -expand -group RAM -color Blue /TB_fifo/TB_t08/RAM/data_rd
add wave -noupdate -expand -group RAM -color Blue /TB_fifo/TB_t08/RAM/ram
add wave -noupdate -expand -group Pointer_WR -color Cyan /TB_fifo/TB_t08/pn_wr/push
add wave -noupdate -expand -group Pointer_WR -color Cyan /TB_fifo/TB_t08/pn_wr/WR_enable
add wave -noupdate -expand -group Pointer_WR -color Cyan /TB_fifo/TB_t08/pn_wr/addr_wr
add wave -noupdate -expand -group Pointer_WR /TB_fifo/TB_t08/pn_wr/full_flag
add wave -noupdate -expand -group Pointer_WR -color Cyan /TB_fifo/TB_t08/pn_wr/Flag_wr
add wave -noupdate -expand -group Pointer_WR -color Cyan /TB_fifo/TB_t08/pn_wr/queue
add wave -noupdate -expand -group Pointer_RD -color Green /TB_fifo/TB_t08/pn_rd/pop
add wave -noupdate -expand -group Pointer_RD -color Green /TB_fifo/TB_t08/pn_rd/empty_flag
add wave -noupdate -expand -group Pointer_RD -color Green /TB_fifo/TB_t08/pn_rd/RD_enable
add wave -noupdate -expand -group Pointer_RD -color Green /TB_fifo/TB_t08/pn_rd/addr_rd
add wave -noupdate -expand -group Pointer_RD -color Green /TB_fifo/TB_t08/pn_rd/Flag_rd
add wave -noupdate -expand -group Pointer_RD -color Green /TB_fifo/TB_t08/pn_rd/queue
add wave -noupdate -expand -group FULL -color Coral /TB_fifo/TB_t08/Full_module/addr_1
add wave -noupdate -expand -group FULL -color Coral /TB_fifo/TB_t08/Full_module/addr_2
add wave -noupdate -expand -group FULL -color Coral /TB_fifo/TB_t08/Full_module/flag_sync
add wave -noupdate -expand -group FULL -color Coral /TB_fifo/TB_t08/Full_module/addr_bin_sync
add wave -noupdate -expand -group FULL -color Coral /TB_fifo/TB_t08/Full_module/addr_gray_sync
add wave -noupdate -expand -group FULL -color Coral /TB_fifo/TB_t08/Full_module/addr_gray_comp
add wave -noupdate -expand -group EMPTY -color Yellow /TB_fifo/TB_t08/Empty_module/addr_1
add wave -noupdate -expand -group EMPTY -color Yellow /TB_fifo/TB_t08/Empty_module/addr_2
add wave -noupdate -expand -group EMPTY -color Yellow /TB_fifo/TB_t08/Empty_module/flag_sync
add wave -noupdate -expand -group EMPTY -color Yellow /TB_fifo/TB_t08/Empty_module/addr_bin_sync
add wave -noupdate -expand -group EMPTY -color Yellow /TB_fifo/TB_t08/Empty_module/addr_gray_sync
add wave -noupdate -expand -group EMPTY -color Yellow /TB_fifo/TB_t08/Empty_module/addr_gray_comp
add wave -noupdate /TB_fifo/TB_t08/Full_module/gray_bin_full/data_gray
add wave -noupdate /TB_fifo/TB_t08/Full_module/gray_bin_full/data_bin
add wave -noupdate /TB_fifo/TB_t08/Full_module/bin_gray_full/data_bin
add wave -noupdate /TB_fifo/TB_t08/Full_module/bin_gray_full/data_gray
add wave -noupdate /TB_fifo/TB_t08/Full_module/bin_gray_full/xor_l
TreeUpdate [SetDefaultTree]
WaveRestoreCursors {{Cursor 1} {140 ps} 0}
quietly wave cursor active 1
configure wave -namecolwidth 106
configure wave -valuecolwidth 50
configure wave -justifyvalue left
configure wave -signalnamewidth 1
configure wave -snapdistance 10
configure wave -datasetprefix 0
configure wave -rowmargin 4
configure wave -childrowmargin 2
configure wave -gridoffset 0
configure wave -gridperiod 1
configure wave -griddelta 40
configure wave -timeline 0
configure wave -timelineunits ps
update
WaveRestoreZoom {83 ps} {207 ps}

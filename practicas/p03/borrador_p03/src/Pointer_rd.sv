


module Pointer_rd
import FIFO_pkg::*;
(	
	input clk_rd,
	input rst,
	input         pop,
	input logic   empty_flag,

	output dataN_t addr_rd,
	output logic Flag_rd

);

addrN_t  queue = ZERON; //= ADDR_N - UNIT;

always_ff @(posedge clk_rd or negedge rst ) begin 

	if (rst == FALSE)begin
		queue    <= ZERON; 
	end
	else
		begin
		
			if(pop == TRUE && empty_flag == FALSE) 
				begin 
					queue    <= (queue + UNIT);
				end

			else 
				begin
					queue    <= queue ;
				end
		end
		
end

assign addr_rd = queue [DATA_N - UNIT:0];
assign Flag_rd = queue[ADDR_N - UNIT];

endmodule
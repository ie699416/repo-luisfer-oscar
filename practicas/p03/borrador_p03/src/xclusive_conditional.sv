

module xclusive_conditional_empty
import FIFO_pkg::*;
(
	input addrN_t  data_in_1,
	input addrN_t  data_in_2,

	output logic  flag_out
);


if( data_in_1 == data_in_2)

	assign flag_out = TRUE;
	
else

	assign flag_out = FALSE;

endmodule


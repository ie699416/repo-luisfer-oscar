

module gray2bin_empty
import FIFO_pkg::*;
(
	input  addrN_t data_gray,

	output addrN_t data_bin
);


genvar i;
    generate  
        for (i = ( ADDR_N - TWO ) ; i > ZERO ; i = ( i - UNIT ))begin: gray2bin	  
		 
            xclusive_xor XOR(
				.b_in(data_gray[i]),
				.a_in(data_bin[i + UNIT]),

				.xor_o(data_bin[i])
			);
        end: gray2bin
		  
    endgenerate
	 
	 
assign data_bin[ADDR_N - UNIT:0] = data_gray[ADDR_N - UNIT:0];
	 

endmodule


`ifndef MYPACKAGE_DONE
   `define MYPACKAGE_DONE
package FIFO_pkg;

    localparam  DATA_N  = 4;
    localparam  ADDR_N  = 5;
    localparam  DEPTH_N = 5;
	 
	 parameter TRUE = 1'b1;
	 parameter FALSE = 1'b0;
	 parameter HIGH = 1'b1;
	 parameter LOW = 1'b0;
	 parameter UNIT = 1'b1;
	 parameter ZERO = 1'b0;
	 parameter TWO = 2'b10;
	 parameter ZERON = {6{1'b0}};


    typedef logic [DATA_N-1:0]  dataN_t;
    typedef logic [ADDR_N-1:0]  addrN_t;

	 typedef struct{
	    logic  enable_ram_wr;
	    addrN_t addr_ram_wr;
	    ////////////////////////
	    logic  enable_ram_rd;
	    addrN_t addr_ram_rd;
	}FIFO_st;
     
	  
	typedef struct{
    	addrN_t addr_bin_sync;
    	addrN_t addr_gray_sync;
    	addrN_t addr_gray_comp;
    }addr_sync;
	 
	 typedef enum logic [1:0]{
        STATE_IDLE_WR = 2'b00,
		  STATE_WR 		 = 2'b01
		  
		} State_wr;
		
		typedef enum logic [1:0]{
        STATE_IDLE_RD    = 2'b00,
		  STATE_RD 		 = 2'b01
		  
		} State_rd;




endpackage

`endif
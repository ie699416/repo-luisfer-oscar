
/*
*************************************************************************************
*	Coder:	OSCAR MAISTERRA FLORES	 		[ ie699416]
*	Coder: 	LUIS FERNANDO MORENO ALONSO	[ ie693314] 
*	Date : 	June 8th, 2019
*  Name :   Bin to Gray 
*
*	Description: 	 
*		 this code decodifies the data in binary to data with the logarith gray
* 
*************************************************************************************
*/


module bin2gray_full
import FIFO_pkg::*;
(
	input  addrN_t data_bin,

	output addrN_t data_gray
);


    generate 
        genvar i;
        for ( i = ZERO ; i < ( ADDR_N - TWO ) ; i = ( i + UNIT ))begin: bin2gray
            xclusive_xor XOR(
				.b_in(data_bin[i]),
				.a_in(data_bin[i + UNIT]),

				.xor_o(data_gray[i])
			);
        end: bin2gray
    endgenerate

    assign data_gray[ADDR_N - UNIT:0] = data_bin[ADDR_N - UNIT:0];


endmodule


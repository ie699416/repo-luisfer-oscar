
module sync_addr_empty
import FIFO_pkg::*;
(
	input logic clk,
	input logic rst,

	input addrN_t addr_1,
	input addrN_t addr_2,

	output logic flag_sync
);

//addr_sync wires_struct;

dataN_t addr_bin_sync;
dataN_t addr_gray_sync;
dataN_t addr_gray_comp;


bin2gray bin_gray_empty(

	.data_bin(addr_1[DATA_N - UNIT:0]), 

	.data_gray(addr_bin_sync) 
);

sync_DFF DFF_empty(

	.clk(clk), 
	.rst(rst),
	.data_in(addr_bin_sync), 

	.data_out(addr_gray_sync) 
);

gray2bin gray_bin_empty(

    .data_gray(addr_gray_sync), 

    .data_bin(addr_gray_comp) 
);

xclusive_conditional_empty conditional_empty(
	.data_in_1({addr_1[DATA_N],addr_gray_comp[DATA_N - UNIT:0]}), 
	.data_in_2(addr_2), 

	.flag_out(flag_sync)
);

endmodule


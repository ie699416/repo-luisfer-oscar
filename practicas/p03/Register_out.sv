

module Register_out
import top_pkg::*;

(
	// inputs 
	input clk,
	input rst,
	input en,	
	input clear,
	input dataN_t data_in,		    		// M <- Multiplicand	}
	// outputs 
	output dataN_t data_o					// M <- value for adder	
	
);

dataN_t register;
dataN_t i;

always_ff@(posedge clk, negedge rst) 
	begin: S_register
		if (rst == FALSE)
			begin
				register <= ZEROS;
				i = ZEROS;
			end
		else 
			begin
				if (clear == TRUE)
					register <= ZERON;
					
				else if(en == TRUE)begin
						register[i] <= data_in;
						i = i + UNIT;
					end
				else
					register <= register[i];
					i = i + UNIT;
			end
			
	end:  S_register

	assign data_o = register;
	
	
endmodule



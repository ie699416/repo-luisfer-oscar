onerror {resume}
quietly WaveActivateNextPane {} 0
add wave -noupdate -group Top_P03 /TB_P03/clk_wr
add wave -noupdate -group Top_P03 /TB_P03/clk_rd
add wave -noupdate -group Top_P03 /TB_P03/rst
add wave -noupdate -group Top_P03 /TB_P03/data_in
add wave -noupdate -group Top_P03 /TB_P03/data_result
add wave -noupdate -group Register_1 /TB_P03/P03_tb/Register_1/en
add wave -noupdate -group Register_1 /TB_P03/P03_tb/Register_1/data_in
add wave -noupdate -group Register_1 /TB_P03/P03_tb/Register_1/data_o
add wave -noupdate -expand -group Control_unit /TB_P03/P03_tb/Enable_UART
add wave -noupdate -expand -group Control_unit /TB_P03/P03_tb/Control_unit_module/data_in
add wave -noupdate -expand -group Control_unit /TB_P03/P03_tb/Control_unit_module/Counter_flag
add wave -noupdate -expand -group Control_unit /TB_P03/P03_tb/Control_unit_module/Flag_Finish
add wave -noupdate -expand -group Control_unit /TB_P03/P03_tb/Control_unit_module/Flag_Result
add wave -noupdate -expand -group Control_unit /TB_P03/P03_tb/Control_unit_module/push_vector
add wave -noupdate -expand -group Control_unit /TB_P03/P03_tb/Control_unit_module/push_matrix
add wave -noupdate -expand -group Control_unit /TB_P03/P03_tb/Control_unit_module/push_result
add wave -noupdate -expand -group Control_unit /TB_P03/P03_tb/Control_unit_module/pop_vector
add wave -noupdate -expand -group Control_unit /TB_P03/P03_tb/Control_unit_module/pop_matrix
add wave -noupdate -expand -group Control_unit /TB_P03/P03_tb/Control_unit_module/pop_result
add wave -noupdate -expand -group Control_unit /TB_P03/P03_tb/Control_unit_module/flag_add_reg
add wave -noupdate -expand -group Control_unit /TB_P03/P03_tb/Control_unit_module/flag_mux_vector
add wave -noupdate -expand -group Control_unit /TB_P03/P03_tb/Control_unit_module/Start_counter
add wave -noupdate -expand -group Control_unit /TB_P03/P03_tb/Control_unit_module/Enable_register_L
add wave -noupdate -expand -group Control_unit /TB_P03/P03_tb/Control_unit_module/Enable_register_N
add wave -noupdate -expand -group Control_unit /TB_P03/P03_tb/Control_unit_module/Enable_mul
add wave -noupdate -expand -group Control_unit /TB_P03/P03_tb/Control_unit_module/Enable_sum
add wave -noupdate -expand -group Control_unit /TB_P03/P03_tb/Control_unit_module/L_out
add wave -noupdate -expand -group Control_unit /TB_P03/P03_tb/Control_unit_module/N_out
add wave -noupdate -expand -group Control_unit /TB_P03/P03_tb/Control_unit_module/data_out_vector
add wave -noupdate -expand -group Control_unit /TB_P03/P03_tb/Control_unit_module/data_out_matrix
add wave -noupdate -expand -group Control_unit /TB_P03/P03_tb/Control_unit_module/Actual_state
add wave -noupdate -group Process_module /TB_P03/P03_tb/Process_module/Vector
add wave -noupdate -group Process_module /TB_P03/P03_tb/Process_module/Matrix
add wave -noupdate -group Process_module /TB_P03/P03_tb/Process_module/push_vector
add wave -noupdate -group Process_module /TB_P03/P03_tb/Process_module/push_matrix
add wave -noupdate -group Process_module /TB_P03/P03_tb/Process_module/push_result
add wave -noupdate -group Process_module /TB_P03/P03_tb/Process_module/pop_vector
add wave -noupdate -group Process_module /TB_P03/P03_tb/Process_module/pop_matrix
add wave -noupdate -group Process_module /TB_P03/P03_tb/Process_module/pop_result
add wave -noupdate -group Process_module /TB_P03/P03_tb/Process_module/enable_mul
add wave -noupdate -group Process_module /TB_P03/P03_tb/Process_module/enable_sum
add wave -noupdate -group Process_module /TB_P03/P03_tb/Process_module/enable_mux_vector
add wave -noupdate -group Process_module /TB_P03/P03_tb/Process_module/enable_add_reg
add wave -noupdate -group Process_module /TB_P03/P03_tb/Process_module/data_out
add wave -noupdate -group Process_module /TB_P03/P03_tb/Process_module/empty_flag_vector
add wave -noupdate -group Process_module /TB_P03/P03_tb/Process_module/full_flag_vector
add wave -noupdate -group Process_module /TB_P03/P03_tb/Process_module/empty_flag_matrix
add wave -noupdate -group Process_module /TB_P03/P03_tb/Process_module/full_flag_matrix
add wave -noupdate -group Process_module /TB_P03/P03_tb/Process_module/empty_flag_result
add wave -noupdate -group Process_module /TB_P03/P03_tb/Process_module/full_flag_result
add wave -noupdate -group Process_module /TB_P03/P03_tb/Process_module/data_vector_wire
add wave -noupdate -group Process_module /TB_P03/P03_tb/Process_module/data_matrix_wire
add wave -noupdate -group Process_module /TB_P03/P03_tb/Process_module/Mux_data_o
add wave -noupdate -group Process_module /TB_P03/P03_tb/Process_module/Mux_data_add_o
add wave -noupdate -group Process_module /TB_P03/P03_tb/Process_module/Mul_vector
add wave -noupdate -group Process_module /TB_P03/P03_tb/Process_module/Mul_result
add wave -noupdate -group Process_module /TB_P03/P03_tb/Process_module/data_register
add wave -noupdate -group Process_module /TB_P03/P03_tb/Process_module/Sum_result
add wave -noupdate -group Counter /TB_P03/P03_tb/Counter_Num/enable
add wave -noupdate -group Counter /TB_P03/P03_tb/Counter_Num/N_counter
add wave -noupdate -group Counter /TB_P03/P03_tb/Counter_Num/Flag_counter
add wave -noupdate -group Counter /TB_P03/P03_tb/Counter_Num/Count_logic
add wave -noupdate -group FIFO_Vector /TB_P03/P03_tb/Process_module/FIFO_Vector/clk_wr
add wave -noupdate -group FIFO_Vector /TB_P03/P03_tb/Process_module/FIFO_Vector/clk_rd
add wave -noupdate -group FIFO_Vector /TB_P03/P03_tb/Process_module/FIFO_Vector/rst
add wave -noupdate -group FIFO_Vector /TB_P03/P03_tb/Process_module/FIFO_Vector/data_in
add wave -noupdate -group FIFO_Vector /TB_P03/P03_tb/Process_module/FIFO_Vector/push
add wave -noupdate -group FIFO_Vector /TB_P03/P03_tb/Process_module/FIFO_Vector/pop
add wave -noupdate -group FIFO_Vector /TB_P03/P03_tb/Process_module/FIFO_Vector/data_out
add wave -noupdate -group FIFO_Vector /TB_P03/P03_tb/Process_module/FIFO_Vector/empty_flag
add wave -noupdate -group FIFO_Vector /TB_P03/P03_tb/Process_module/FIFO_Vector/full_flag
add wave -noupdate -group FIFO_Vector /TB_P03/P03_tb/Process_module/FIFO_Vector/fifo_wires
add wave -noupdate -group FIFO_Vector /TB_P03/P03_tb/Process_module/FIFO_Vector/Flag_wr
add wave -noupdate -group FIFO_Vector /TB_P03/P03_tb/Process_module/FIFO_Vector/Flag_rd
add wave -noupdate -group FIFO_Vector /TB_P03/P03_tb/Process_module/FIFO_Vector/Flag_wr_full
add wave -noupdate -group FIFO_Vector /TB_P03/P03_tb/Process_module/FIFO_Vector/Flag_rd_empty
add wave -noupdate -group FIFO_Vector /TB_P03/P03_tb/Process_module/FIFO_Vector/Start_write
add wave -noupdate -group FIFO_Vector /TB_P03/P03_tb/Process_module/FIFO_Vector/Start_read
add wave -noupdate -group FIFO_Vector /TB_P03/P03_tb/Process_module/FIFO_Vector/addr_wr_pt
add wave -noupdate -group FIFO_Vector /TB_P03/P03_tb/Process_module/FIFO_Vector/addr_rd_pt
TreeUpdate [SetDefaultTree]
WaveRestoreCursors {{Cursor 1} {17 ps} 0}
quietly wave cursor active 1
configure wave -namecolwidth 172
configure wave -valuecolwidth 97
configure wave -justifyvalue left
configure wave -signalnamewidth 1
configure wave -snapdistance 10
configure wave -datasetprefix 0
configure wave -rowmargin 4
configure wave -childrowmargin 2
configure wave -gridoffset 0
configure wave -gridperiod 1
configure wave -griddelta 40
configure wave -timeline 0
configure wave -timelineunits ps
update
WaveRestoreZoom {0 ps} {89 ps}


module TB_process
import top_pkg::*;
();

	logic clk_wr;
	logic clk_rd;
	logic rst;

	dataN_t  data_vector_in;
	dataN_t  data_matrix_in;
	logic push_vector;
	logic pop_vector;
	logic push_matrix;
	logic pop_matrix;
	logic push_result;
	logic pop_result;
	
	logic Enable_mux;

	dataN_t  data_out;
	logic empty_flag_vector;
	logic full_flag_vector;
	logic empty_flag_matrix;
	logic full_flag_matrix;
	logic empty_flag_result;
	logic full_flag_result;

Process_top test_process
(	
	 .clk_wr(clk_wr),
	 .clk_rd(clk_rd),
	 .rst(rst),

	 .Vector(data_vector_in),
	 .Matrix(data_matrix_in),
	 .push_vector(push_vector),
	 .push_matrix(push_matrix),
	 .push_result(push_result),
	 .pop_vector(pop_vector),
	 .pop_matrix(pop_matrix),
	 .pop_result(pop_result),
	 .enable_mul(TRUE),
	 .enable_sum(TRUE),
	 .enable_mux_vector(Enable_mux),

	 .data_out(data_out),
	 .empty_flag_vector(empty_flag_vector),
	 .full_flag_vector(full_flag_vector),
	 .empty_flag_matrix(empty_flag_matrix),
	 .full_flag_matrix(full_flag_matrix),
	 .empty_flag_result(empty_flag_result),
	 .full_flag_result(full_flag_result)
		
);


always begin
    #1 clk_wr <= ~clk_wr;
end
always begin
	#3 clk_rd <= ~clk_rd;
end

initial begin
	clk_wr = 1;  #1;
	clk_rd = 1;  #1;

	push_vector = 0;
	pop_vector  = 0;
	push_matrix = 0;
	pop_matrix  = 0;
	push_result = 0;
	pop_result  = 0;
	
	Enable_mux  = 0;

	rst    = 1;  #2;
	rst    = 0;  #3;
	rst    = 1;  #2;

	Enable_mux  = 1;
	
	data_vector_in = 1;
	data_matrix_in = 1;
	push_vector   = 1;  #2;
	push_vector   = 0;  #4;
	push_matrix   = 1;  #2;
	push_matrix   = 0;  #4;
	push_result   = 1;  #2;
	push_result   = 0;  #4;

	data_vector_in = 2;
	data_matrix_in = 2;
	push_vector   = 1;  #2;
	push_vector   = 0;  #4;
	push_matrix   = 1;  #2;
	push_matrix   = 0;  #4;
	push_result   = 1;  #2;
	push_result   = 0;  #4;

	data_vector_in = 3;
	data_matrix_in = 3;
	push_vector   = 1;  #2;
	push_vector   = 0;  #4;
	push_matrix   = 1;  #2;
	push_matrix   = 0;  #4;
	push_result   = 1;  #2;
	push_result   = 0;  #4;

	data_vector_in = 4;
	data_matrix_in = 4;
	push_vector   = 1;  #2;
	push_vector   = 0;  #4;
	push_matrix   = 1;  #2;
	push_matrix   = 0;  #4;
	push_result   = 1;  #2;
	push_result   = 0;  #4;
	
	data_vector_in = 5;
	data_matrix_in = 5;
	push_vector   = 1;  #2;
	push_vector   = 0;  #4;
	push_matrix   = 1;  #2;
	push_matrix   = 0;  #4;
	push_result   = 1;  #2;
	push_result   = 0;  #4;
	
	data_vector_in = 6;
	data_matrix_in = 6;
	push_vector   = 1;  #2;
	push_vector   = 0;  #4;
	push_matrix   = 1;  #2;
	push_matrix   = 0;  #4;
	push_result   = 1;  #2;
	push_result   = 0;  #4;
	
	data_vector_in = 7;
	data_matrix_in = 7;
	push_vector   = 1;  #2;
	push_vector   = 0;  #4;
	push_matrix   = 1;  #2;
	push_matrix   = 0;  #4;
	push_result   = 1;  #2;
	push_result   = 0;  #4;
	
	data_vector_in = 8;
	data_matrix_in = 8;
	push_vector   = 1;  #2;
	push_vector   = 0;  #4;
	push_matrix   = 1;  #2;
	push_matrix   = 0;  #4;
	push_result   = 1;  #2;
	push_result   = 0;  #4;
	
	
	pop_vector    = 1;  #6;
	pop_vector    = 0;  #12;
	pop_matrix	  = 1;  #6;
	pop_matrix    = 0;  #12;
	pop_result    = 1;  #6;
	pop_result    = 0;  #12;
	
	pop_vector    = 1;  #6;
	pop_vector    = 0;  #12;
	pop_matrix	  = 1;  #6;
	pop_matrix    = 0;  #12;
	pop_result    = 1;  #6;
	pop_result    = 0;  #12;

	pop_vector    = 1;  #6;
	pop_vector    = 0;  #12;
	pop_matrix	  = 1;  #6;
	pop_matrix    = 0;  #12;
	pop_result    = 1;  #6;
	pop_result    = 0;  #12;

	pop_vector    = 1;  #6;
	pop_vector    = 0;  #12;
	pop_matrix	  = 1;  #6;
	pop_matrix    = 0;  #12;
	pop_result    = 1;  #6;
	pop_result    = 0;  #12;

	pop_vector    = 1;  #6;
	pop_vector    = 0;  #12;
	pop_matrix	  = 1;  #6;
	pop_matrix    = 0;  #12;
	pop_result    = 1;  #6;
	pop_result    = 0;  #12;
	
	pop_vector    = 1;  #6;
	pop_vector    = 0;  #12;
	pop_matrix	  = 1;  #6;
	pop_matrix    = 0;  #12;
	pop_result    = 1;  #6;
	pop_result    = 0;  #12;
	
	pop_vector    = 1;  #6;
	pop_vector    = 0;  #12;
	pop_matrix	  = 1;  #6;
	pop_matrix    = 0;  #12;
	pop_result    = 1;  #6;
	pop_result    = 0;  #12;
	
	pop_vector    = 1;  #6;
	pop_vector    = 0;  #12;
	pop_matrix	  = 1;  #6;
	pop_matrix    = 0;  #12;
	pop_result    = 1;  #6;
	pop_result    = 0;  #12;
	
	pop_vector    = 1;  #6;
	pop_vector    = 0;  #12;
	pop_matrix	  = 1;  #6;
	pop_matrix    = 0;  #12;
	pop_result    = 1;  #6;
	pop_result    = 0;  #12;
	

end

endmodule


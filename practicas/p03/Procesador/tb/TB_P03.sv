

module TB_P03
import P03_pkg::*;
();

	logic clk_wr;
	logic clk_rd;
	logic rst;
	logic Enable_uart;

	Data_N   data_in;
	Data_NN  data_result;

P03 P03_tb
(	
	 .clk_wr(clk_wr),
	 .clk_rd(clk_rd),
	 .rst(rst),
	 .Enable_UART(Enable_uart),
	
	 .data_in(data_in),
	
	 .Final_Result(data_result)
);


always begin
    #1 clk_wr <= ~clk_wr;
end
always begin
	#3 clk_rd <= ~clk_rd;
end

initial begin
	clk_wr = 1;  #1;
	clk_rd = 1;  #1;
	
	rst    = 1;  #2;
	rst    = 0;  #3;
	rst    = 1;  #2;

	/*Comand 0x01   */
	Enable_uart = LOW;#1
	Enable_uart = HIGH;#1
	data_in = 8'b11111110;#2; /* Dato a para inicar FE*/
	Enable_uart = LOW;#2
	#10;
	
	Enable_uart = HIGH;#1
	data_in = 8'b00000100;#2; /* Dato de tamaño L */
	Enable_uart = LOW;#2
	
	Enable_uart = HIGH;#1
	data_in = 8'b00000001;#2; /* Comando que se ejecutara */
	Enable_uart = LOW;#2
	
	Enable_uart = HIGH;#1
	data_in = 8'b00000010;#2; /* Comnado de N */
	Enable_uart = LOW;#2
	
	Enable_uart = HIGH;#1
	data_in = 8'b11101111;#2; /* Dato a para terminar EF*/
	Enable_uart = LOW;#2

	/*Comand 0x03   */
	data_in = 8'b11111110;#2; /* Dato a para inicar FE*/
	
	data_in = 8'b00000010;#2; /* Dato de tamaño L */
	
	data_in = 8'b00000011;#2;/* Comnado de Start */
	
	data_in = 8'b11101111;#3;/* Dato a para terminar EF*/
	
	/*Comand 0x04  */
	data_in = 8'b11111110;#3;/* Dato a para inicar FE*/
	
	data_in = 8'b00000100;#3;/* Ddato de tamaño L */
	
	data_in = 8'b00000100;#3;/* Comando que se ejecutara para la matriz */
	
	data_in = 8'b00000001;#3; /* Datos de la matriz*/
	data_in = 8'b00000010;#3;
	data_in = 8'b00000011;#3;
	data_in = 8'b00000100;#3;
	data_in = 8'b11101111;#3;/* Dato a para terminar EF*/
	
	
	/*Comand 0x05  */
	data_in = 8'b11111110;#3;/* Dato a para inicar FE*/
	
	data_in = 8'b00000010;#3;/* Ddato de tamaño L */
	
	data_in = 8'b00000101;#3;/* Comando que se ejecutara para el vector*/
	
	data_in = 8'b00000001;#3; /* Datos del vector */
	data_in = 8'b00000010;#3;
	
	data_in = 8'b11101111;#3;/* Dato a para terminar EF*/
	

end

endmodule


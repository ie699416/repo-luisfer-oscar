/*
*************************************************************************************
*	Coder:	OSCAR MAISTERRA FLORES	 		[ ie699416]
*	Coder: 	LUIS FERNANDO MORENO ALONSO	[ ie693314] 
*	Date : 	June 8th, 2019
*  Name :   N_register.sv  
*
*	Description: 	 
*		 This module operates the FF_D for N bits
* 
*************************************************************************************
*/

module N_register
import top_pkg::*;

(
	// inputs 
	input clk,
	input rst,
	input en,								
	input dataN_t data_in,		    		// M <- Multiplicand	
	// outputs 
	output dataN_t data_o					// M <- value for adder	
	
);


always_ff@(posedge clk, negedge rst) 
	begin: N_register
		if (rst == FALSE)
			begin
				data_o <= ZERON;
			end
		else 
			begin
				if(en == TRUE)
					data_o <= data_in;
				else
					data_o <= data_o;
			end
			
	end:  N_register

endmodule



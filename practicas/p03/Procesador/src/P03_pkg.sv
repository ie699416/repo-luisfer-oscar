
`ifndef TOPPACKAGE_DONE
   `define TOPPACKAGE_DONE
package P03_pkg;

    localparam  VECTOR_N  = 8;
	 localparam  MATRIX_N  = 8;
	 localparam  DATA_N  = 8;
	 localparam  DATA_R  = 16;
	
	 typedef logic [DATA_R-1:0]  Data_NN;
	 typedef logic [DATA_N-1:0]  Data_N;
	 
	 parameter FALSE = 1'b0;
	 parameter TRUE = 1'b1;
	 parameter HIGH = 1'b1;
	 parameter LOW = 1'b0;
	 parameter UNIT = 1'b1;
	 parameter ZERO = 1'b0;
	 parameter TWO = 2'b10;
	 parameter ZERON = {8{1'b0}};
	 

	

endpackage

`endif
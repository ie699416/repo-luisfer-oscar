


module P03
import P03_pkg::*;
(	
	input clk_wr,
	input clk_rd,
	input rst,
	input logic Enable_UART,
	
	input Data_N data_in,
	
	output Data_NN Final_Result
	
);

Data_N First_data_reg;
Data_N L_out_wire;
Data_N L_Counter_wire;
Data_N N_out_wire;
Data_N data_vector_wire;
Data_N data_matrix_wire;

logic push_vector_wire;
logic push_matrix_wire;
logic push_result_wire;
logic pop_vector_wire;
logic pop_matrix_wire;
logic pop_result_wire;
logic flag_add_reg_wire;
logic flag_mux_vector_wire;
logic Start_counter_wire;
logic Enable_reg_L_wire;
logic Enable_reg_N_wire;
logic Enable_mul_wire;
logic Enable_sum_wire;
logic Flag_counter_wire;
logic Flag_Finish_wire;
logic Flag_Result_wire;

N_register Register_1
(
	 .clk(clk_wr),
	 .rst(rst),
	 .en(TRUE),								
	 .data_in(data_in),		    		
	
	 .data_o(First_data_reg)					
	
);

Control_unit_top Control_unit_module
(	
	/////inputs
	 .clk(clk_wr),
	 .rst(rst),

	 .data_in(First_data_reg),
	 .Counter_flag(Flag_counter_wire),
	 .Flag_Finish(Flag_Finish_wire),
	 .Flag_Result(Flag_Result_wire),
	
	////outputs
	 .push_vector(push_vector_wire),
	 .push_matrix(push_matrix_wire),
	 .push_result(push_result_wire),
	 .pop_vector(pop_vector_wire),
	 .pop_matrix(pop_matrix_wire),
	 .pop_result(pop_result_wire),
	 .flag_add_reg(flag_add_reg_wire),
	 .flag_mux_vector(flag_mux_vector_wire),
	 .Start_counter(Start_counter_wire),
	 .Enable_register_L(Enable_reg_L_wire),
	 .Enable_register_N(Enable_reg_N_wire),
	 .Enable_mul(Enable_mul_wire),
	 .Enable_sum(Enable_sum_wire),
	 .Enable_data(Enable_UART),
	
	 .L_out(L_out_wire),
	 .N_out(N_out_wire),
	 .data_out_vector(data_vector_wire),
	 .data_out_matrix(data_matrix_wire)
	 
);

Process_top Process_module
(	
	//////////Inputs
	 .clk_wr(clk_wr),
	 .clk_rd(clk_rd),
	 .rst(rst),

	 .Vector(data_vector_wire),
	 .Matrix(data_matrix_wire),
	 .push_vector(push_vector_wire),
	 .push_matrix(push_matrix_wire),
	 .push_result(push_result_wire),
	 .pop_vector(pop_vector_wire),
	 .pop_matrix(pop_matrix_wire),
	 .pop_result(pop_result_wire),
	 .enable_mul(Enable_mul_wire),
	 .enable_sum(Enable_sum_wire),
	 .enable_mux_vector(flag_mux_vector_wire),
	 .enable_add_reg(flag_add_reg_wire),

	//////////Ouputs
	 .data_out(Final_Result),
	 .empty_flag_vector(),
	 .full_flag_vector(),
	 .empty_flag_matrix(Flag_Finish_wire),
	 .full_flag_matrix(),
	 .empty_flag_result(Flag_Result_wire),
	 .full_flag_result()
	
);


Counter_N Counter_Num
(
	// Input Ports
	 .clk(clk_wr),
	 .reset(rst),
	 .enable(Start_counter_wire),
	 .N_counter(L_Counter_wire),
	
	// Output Ports
	 .Flag_counter(Flag_counter_wire)
	
);

N_register Register_data_N
(
	 .clk(clk_wr),
	 .rst(rst),
	 .en(Enable_reg_N_wire),								
	 .data_in(N_out_wire),		    		
	
	 .data_o()					
	
);

N_register Register_data_L
(
	 .clk(clk_wr),
	 .rst(rst),
	 .en(Enable_reg_L_wire),								
	 .data_in(L_out_wire),		    		
	
	 .data_o(L_Counter_wire)					
	
);

endmodule

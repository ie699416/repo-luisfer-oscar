
module Sum
import top_pkg::*;
(	
	input clk,
	input rst,
	
	input Vector_N data_1,
	input Matrix_N data_2,

	output dataN_t Result
	
);

dataN_t temp_sum;

always_comb 
begin
		temp_sum = data_1 + data_2;

end
	
assign Result = temp_sum;

	
endmodule

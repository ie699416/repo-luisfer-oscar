/*
*************************************************************************************
*	Coder:	OSCAR MAISTERRA FLORES	 		[ ie699416]
*	Coder: 	LUIS FERNANDO MORENO ALONSO	[ ie693314] 
*	Date : 	June 8th, 2019
*  Name :   muxN_2_1.sv  
*
*	Description: Multiplexer parametric N bus value
*
* 
*************************************************************************************
*/

module muxN_2_1
import top_pkg::*;
(
	// Input Ports
	input Selector,
	input dataN_t data0_in,
	input dataN_t data1_in,
	
	// Output Ports
	
	output dataN_t mux_o

);


/*
*************************************************************************************
* Combiational processes 
*************************************************************************************
*/

always_comb 
	begin: MUX
		if (Selector == TRUE)
			mux_o = data1_in;
		else
			mux_o = data0_in;
	end: MUX// end always

endmodule


module Control_unit_rd
import FIFO_pkg::*;
(
	input clk,
	input rst,
	
	input logic Pop,
	input logic Flag_empty,	
		
	output logic Empty_flag,
	output logic Start_flag
	
);


State_rd Actual_state;	

always_ff@(posedge clk or negedge rst) 
	begin
		if (rst == FALSE)
			Actual_state <= STATE_IDLE_RD;			
		else 
			begin		
				case(Actual_state)				
						STATE_IDLE_RD:
							begin 						
								if (Pop == TRUE && Flag_empty == FALSE)
										Actual_state <= STATE_RD; 									
								else 
										Actual_state <= STATE_IDLE_RD; 							
							end	
						STATE_RD:
							begin 
								if (Pop == TRUE)	
									Actual_state <= STATE_RD;
								else if (Flag_empty == TRUE)
									Actual_state <= STATE_IDLE_RD; 									
								else 
									Actual_state <= STATE_IDLE_RD; 							
							end			
							
						default: Actual_state<=STATE_IDLE_RD;
							
				 endcase	
			end
	end

always_comb 
	begin
		case(Actual_state)
			 STATE_IDLE_RD:
				 begin 
					Empty_flag = HIGH;
					Start_flag = LOW;				
				 end
			STATE_RD:
				 begin 
					Empty_flag = LOW;
					Start_flag = HIGH;				
				 end
				
			 default:
				begin
					Empty_flag = HIGH;
					Start_flag = LOW;	
				end
	 
	endcase
end

endmodule

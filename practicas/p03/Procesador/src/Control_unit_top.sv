
module Control_unit_top
import top_pkg::*;
(	
	input clk,
	input rst,

	input Data_N data_in,
	input logic Counter_flag,
	input logic Flag_Finish,
	input logic Flag_Result,
	input logic Enable_data,
	
	output logic push_vector,
	output logic push_matrix,
	output logic push_result,
	output logic pop_vector,
	output logic pop_matrix,
	output logic pop_result,
	output logic flag_add_reg,
	output logic flag_mux_vector,
	output logic Start_counter,
	output logic Enable_register_L,
	output logic Enable_register_N,
	output logic Enable_mul,
	output logic Enable_sum,
	
	output Data_N L_out,
	output Data_N N_out,
	output Data_N data_out_vector,
	output Data_N data_out_matrix

	
);

State_decode Actual_state;	

always_ff@(posedge clk or negedge rst) 
	begin
		if (rst == FALSE)
			Actual_state <= STATE_IDEL;			
		else 
			begin		
				case(Actual_state)				
						STATE_IDEL:
							begin
							if(Enable_data == TRUE && data_in == CD_START)
								Actual_state <= STATE_L;
							else
								Actual_state <= STATE_IDEL;
							end
							
						STATE_L:
							begin 
								if(Enable_data == TRUE)
									Actual_state <= STATE_CMD;	
								else
									Actual_state <= STATE_IDEL;
							end
							
						STATE_CMD:
							begin 
								if(data_in == CD_END)
									Actual_state <= STATE_IDEL;
								else if(Enable_data == TRUE)begin						
									case(data_in)
										CMD_1:
											begin
												Actual_state <= STATE_N;	
											end
										CMD_2:
											begin								
												Actual_state <= STATE_RT;
											end
										CMD_3:
											begin								
												Actual_state <= STATE_START;
											end
										CMD_4:
											begin								
												Actual_state <= STATE_MATRIX;
											end	
										CMD_5:
											begin								
												Actual_state <= STATE_VECTOR;
											end		
										default:Actual_state <= Actual_state;
									endcase	
									end
								else
									Actual_state <= Actual_state;
							end
							
						STATE_N:
							begin
								if(Enable_data == TRUE && data_in == CD_END)							
									Actual_state <= STATE_IDEL;
								else 
									Actual_state <= STATE_N;
							end
							
						STATE_START:
							begin 
								if(Enable_data == TRUE && data_in == CD_END)								
									Actual_state <= STATE_IDEL;	
								else 
									Actual_state <= STATE_START;
							end
							
						STATE_MATRIX:
							begin 						
								if(Counter_flag == TRUE ||data_in == CD_END )
									Actual_state <= STATE_IDEL;
								else if(Enable_data == TRUE && Counter_flag == FALSE)
									Actual_state <= STATE_MATRIX;					
							end	

						STATE_VECTOR:
							begin 						
								if(Counter_flag == TRUE || data_in == CD_END)
									Actual_state <= STATE_PROCESS;
								else if(Enable_data == TRUE && Counter_flag == FALSE )
									Actual_state <= STATE_VECTOR;					
							end
							
						STATE_PROCESS:
							begin 						
								if(Flag_Finish == TRUE)
									Actual_state <= STATE_RESULT;
								else
									Actual_state <= STATE_SHOT;					
							end
						STATE_SHOT:
							begin 						
									Actual_state <= STATE_PROCESS;
							end
						STATE_RESULT:
							begin 						
								if(Flag_Result == TRUE)
									Actual_state <= STATE_IDEL;
								else
									Actual_state <= STATE_RESULT;					
							end
							
						default: Actual_state <= STATE_IDEL;
							
				 endcase	
			end
	end

always_comb 
	begin
		case(Actual_state)
			STATE_IDEL:
				begin 
					data_out_vector	= ZERON;
					data_out_matrix	= ZERON;
					L_out 				= ZERON;
					N_out					= ZERON;
					Enable_register_N = LOW; 
					Enable_register_L = LOW;
					Enable_mul			= LOW;
					Enable_sum			= LOW;
					Start_counter	   = LOW;
					push_vector		   = LOW;
					push_matrix		   = LOW;
					push_result			= LOW;
					flag_add_reg 		= LOW;	
					flag_mux_vector   = LOW;
					pop_matrix		   = LOW;
					pop_vector 			= LOW;
					pop_result			= LOW;
				end		
			STATE_L:
				begin 						
					data_out_vector	= ZERON;
					data_out_matrix	= ZERON;
					L_out 				= data_in;
					N_out					= ZERON;
					Enable_register_N = LOW; 
					Enable_register_L = HIGH;
					Enable_mul			= LOW;
					Enable_sum			= LOW;
					Start_counter	   = LOW;
					push_vector		   = LOW;
					push_matrix		   = LOW;
					push_result			= LOW;
					flag_add_reg 		= LOW;	
					flag_mux_vector   = LOW;
					pop_matrix		   = LOW;
					pop_vector 			= LOW;
					pop_result			= LOW;
				end
			STATE_CMD:
				begin 						
					data_out_vector	= ZERON;
					data_out_matrix	= ZERON;
					L_out 				= ZERON;
					N_out					= ZERON;
					Enable_register_N = LOW; 
					Enable_register_L = LOW;
					Enable_mul			= LOW;
					Enable_sum			= LOW;
					Start_counter	   = HIGH;
					push_vector		   = LOW;
					push_matrix		   = LOW;
					push_result			= LOW;
					flag_add_reg 		= LOW;	
					flag_mux_vector   = LOW;
					pop_matrix		   = LOW;
					pop_vector 			= LOW;
					pop_result			= LOW;
				end		
			STATE_N:
				begin 						
					data_out_vector	= ZERON;
					data_out_matrix	= ZERON;	
					L_out 				= ZERON;
					N_out					= data_in;
					Enable_register_N = HIGH; 
					Enable_register_L = LOW;
					Enable_mul			= LOW;
					Enable_sum			= LOW;
					Start_counter	   = HIGH;
					push_vector		   = LOW;
					push_matrix		   = LOW;
					push_result			= LOW;
					flag_add_reg 		= LOW;	
					flag_mux_vector   = LOW;
					pop_matrix		   = LOW;
					pop_vector 			= LOW;
					pop_result			= LOW;	
				end
			STATE_START:
				begin 						
					data_out_vector	= ZERON;
					data_out_matrix	= ZERON;	
					L_out 				= ZERON;
					N_out					= ZERON;
					Enable_register_N = LOW; 
					Enable_register_L = LOW;
					Enable_mul			= LOW;
					Enable_sum			= LOW;
					Start_counter	   = HIGH;
					push_vector		   = LOW;
					push_matrix		   = LOW;
					push_result			= LOW;
					flag_add_reg 		= LOW;	
					flag_mux_vector   = LOW;
					pop_matrix		   = LOW;
					pop_vector 			= LOW;
					pop_result			= LOW;			
				end
			STATE_MATRIX:
				begin
					data_out_vector	= ZERON;
					data_out_matrix	= data_in;	
					L_out 				= ZERON;
					N_out					= ZERON;
					Enable_register_N = LOW; 
					Enable_register_L = LOW;
					Enable_mul			= LOW;
					Enable_sum			= LOW;
					Start_counter	   = HIGH;
					push_vector		   = LOW;
					push_matrix		   = HIGH;
					push_result			= LOW;
					flag_add_reg 		= LOW;	
					flag_mux_vector   = LOW;
					pop_matrix		   = LOW;
					pop_vector 			= LOW;
					pop_result			= LOW;
				end
			STATE_VECTOR:
				begin 	
					data_out_vector	= data_in;	
					data_out_matrix	= ZERON;
					L_out 				= ZERON;
					N_out					= ZERON;
					Enable_register_N = LOW; 
					Enable_register_L = LOW;
					Enable_mul			= LOW;
					Enable_sum			= LOW;
					Start_counter	   = HIGH;
					push_vector		   = HIGH;
					push_matrix		   = LOW;
					push_result			= LOW;
					flag_add_reg 		= HIGH;	
					flag_mux_vector   = HIGH;
					pop_matrix		   = LOW;
					pop_vector 			= LOW;
					pop_result			= LOW;
				end
				
			STATE_PROCESS:
				begin 						
					data_out_vector	= ZERON;
					data_out_matrix	= ZERON;
					L_out 				= ZERON;
					N_out					= ZERON;
					Enable_register_N = LOW; 
					Enable_register_L = LOW;
					Enable_mul			= HIGH;
					Enable_sum			= HIGH;
					Start_counter	   = LOW;
					push_vector		   = LOW;
					push_matrix		   = LOW;
					push_result			= HIGH;
					flag_add_reg 		= LOW;	
					flag_mux_vector   = LOW;
					pop_matrix		   = HIGH;
					pop_vector 			= HIGH;
					pop_result			= LOW;
				end
			STATE_SHOT:
				begin 						
					data_out_vector	= ZERON;
					data_out_matrix	= ZERON;
					L_out 				= ZERON;
					N_out					= ZERON;
					Enable_register_N = LOW; 
					Enable_register_L = LOW;
					Enable_mul			= HIGH;
					Enable_sum			= HIGH;
					Start_counter	   = LOW;
					push_vector		   = LOW;
					push_matrix		   = LOW;
					push_result			= LOW;
					flag_add_reg 		= LOW;	
					flag_mux_vector   = LOW;
					pop_matrix		   = LOW;
					pop_vector 			= LOW;
					pop_result			= LOW;
				end
			STATE_RESULT:
				begin 						
					data_out_vector	= ZERON;
					data_out_matrix	= ZERON;
					L_out 				= ZERON;
					N_out					= ZERON;
					Enable_register_N = LOW; 
					Enable_register_L = LOW;
					Enable_mul			= HIGH;
					Enable_sum			= HIGH;
					Start_counter	   = LOW;
					push_vector		   = LOW;
					push_matrix		   = LOW;
					push_result			= LOW;
					flag_add_reg 		= LOW;	
					flag_mux_vector   = LOW;
					pop_matrix		   = LOW;
					pop_vector 			= LOW;
					pop_result			= HIGH;
				end
			default: 
				begin 
					data_out_vector	= ZERON;
					data_out_matrix	= ZERON;
					L_out 				= ZERON;
					N_out					= ZERON;
					Enable_register_N = LOW; 
					Enable_register_L = LOW;
					Enable_mul			= LOW;
					Enable_sum			= LOW;
					Start_counter	   = LOW;
					push_vector		   = LOW;
					push_matrix		   = LOW;
					push_result			= LOW;
					flag_add_reg 		= LOW;	
					flag_mux_vector   = LOW;
					pop_matrix		   = LOW;
					pop_vector 			= LOW;
					pop_result			= LOW;
				end		
					
	endcase
end

endmodule





module Mul
import top_pkg::*;
(	
	input clk,
	input rst,
	
	input Vector_N data_Vector,
	input Matrix_N data_Matrix,

	output dataNN_t Result,
	output Vector_N vector
	
);

dataNN_t temp_mul;

always_comb 
begin

		temp_mul = data_Vector * data_Matrix;
		
end
	
assign Result = temp_mul;
assign vector = data_Vector;
	
	
endmodule

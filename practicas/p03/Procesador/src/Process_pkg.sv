
`ifndef MYPACKAGE_DONE
   `define MYPACKAGE_DONE
package Process_pkg;

    localparam  VECTOR_N  = 8;
	 localparam  MATRIX_N  = 8;
	 localparam  DATA_N    = 8;
	 
	 parameter TRUE = 1'b1;

	 typedef logic [VECTOR_N-1:0]  Vector_N;
	 typedef logic [MATRIX_N-1:0]  Matrix_N;
	 
	 
	 parameter TRUE = 1'b1;
	 parameter FALSE = 1'b0;
	 parameter HIGH = 1'b1;
	 parameter LOW = 1'b0;
	 parameter UNIT = 1'b1;
	 parameter ZERO = 1'b0;
	 parameter TWO = 2'b10;
	 parameter ZERON = {7{1'b0}};


    typedef logic [DATA_N-1:0]  dataN_t;

endpackage

`endif

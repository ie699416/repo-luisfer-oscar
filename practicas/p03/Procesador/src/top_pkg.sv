
`ifndef OTHERPACKAGE_DONE
   `define OTHERPACKAGE_DONE
package top_pkg;

    localparam  VECTOR_N  = 8;
	 localparam  MATRIX_N  = 8;
	 localparam  DATA_N  = 8;
	 localparam  DATA_R  = 16;
	 
	 
	 parameter TRUE = 1'b1;

	 typedef logic [VECTOR_N-1:0]  Vector_N;
	 typedef logic [MATRIX_N-1:0]  Matrix_N;
	 typedef logic [DATA_R-1:0]  dataNN_t;
	 typedef logic [DATA_N-1:0]  Data_N;
	 
	 parameter FALSE = 1'b0;
	 parameter HIGH = 1'b1;
	 parameter LOW = 1'b0;
	 parameter UNIT = 1'b1;
	 parameter ZERO = 1'b0;
	 parameter TWO = 2'b10;
	 parameter ZERON = {8{1'b0}};

	 parameter CD_START = 8'b11111110;
	 parameter CD_END   = 8'b11101111;
	 
	 parameter CMD_1 = 8'b00000001;
	 parameter CMD_2 = 8'b00000010;
	 parameter CMD_3 = 8'b00000011;
	 parameter CMD_4 = 8'b00000100;
	 parameter CMD_5 = 8'b00000101;


    typedef logic [DATA_N-1:0]  dataN_t;
	 typedef logic [DATA_R-1:0]  dataR_t;
	 
	 	 typedef enum logic [3:0]{
        STATE_IDEL	    = 4'b0000,
		  STATE_START 		 = 4'b0001,
		  STATE_L			 = 4'b0010,
		  STATE_CMD			 = 4'b0011,
		  STATE_N			 = 4'b0100,
		  STATE_VECTOR		 = 4'b0101,
		  STATE_MATRIX		 = 4'b0110,
		  STATE_PROCESS	 = 4'b0111,
		  STATE_RT			 = 4'b1000,
		  STATE_SHOT		 = 4'b1001,
		  STATE_RESULT		 = 4'b1010		  
		  
		} State_decode;
		
		

endpackage

`endif

/*
*************************************************************************************
*	Coder:	OSCAR MAISTERRA FLORES	 		[ ie699416]
*	Coder: 	LUIS FERNANDO MORENO ALONSO	[ ie693314] 
*	Date : 	June 8th, 2019
*  Name :   xclusive_or.sv  
*
*	Description: 	 
*		 This module operates the logical xOr operation between single bits.
* 
*************************************************************************************
*/

module xclusive_xor
(
	input b_in,
	input a_in,
	output logic xor_o
);

assign xor_o = b_in ^ a_in ;
endmodule 
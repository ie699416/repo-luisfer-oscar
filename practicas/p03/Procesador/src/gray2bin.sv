

module gray2bin
import FIFO_pkg::*;
(
	input  dataN_t data_gray,

	output dataN_t data_bin
);


 dataN_t xor_l;
genvar i;
    generate  
        for ( i = ZERO   ; i < (DATA_N - UNIT) ; i = ( i + UNIT ))
			  begin: gray2bin	  
				if(i < (DATA_N - TWO))
					begin
						xclusive_xor XOR(
						.b_in(data_gray[i]),
						.a_in(xor_l[i + UNIT]),

						.xor_o(xor_l[i])
						);
					end				
				else
				  begin				 
						xclusive_xor XOR(
						.b_in(data_gray[i]),
						.a_in(data_gray[i + UNIT]),

						.xor_o(xor_l[i])
						);
				  
				  end
				end: gray2bin
			
		  
    endgenerate
	 
	 
assign data_bin = {data_gray[DATA_N - UNIT], xor_l[DATA_N - TWO : 0]};	 

endmodule

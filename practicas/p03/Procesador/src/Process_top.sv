


module Process_top
import top_pkg::*;
(	
	input clk_wr,
	input clk_rd,
	input rst,

	input Vector_N Vector,
	input Matrix_N Matrix,
	input logic push_vector,
	input logic push_matrix,
	input logic push_result,
	input logic pop_vector,
	input logic pop_matrix,
	input logic pop_result,
	input logic enable_mul,
	input logic enable_sum,
	input logic enable_mux_vector,
	input logic enable_add_reg,

	output dataNN_t data_out,
	output logic empty_flag_vector,
	output logic full_flag_vector,
	output logic empty_flag_matrix,
	output logic full_flag_matrix,
	output logic empty_flag_result,
	output logic full_flag_result
	
	
);

Vector_N data_vector_wire;
Matrix_N data_matrix_wire;
dataNN_t Mux_data_o;
dataN_t Mux_data_add_o;
dataN_t Mul_vector;

dataNN_t Mul_result;
dataN_t data_register;
dataNN_t Sum_result;


T08_FIFO FIFO_Vector
(	
	.clk_wr(clk_wr),
	.clk_rd(clk_rd),
	.rst(rst),

	.data_in(Mux_data_o),
	.push(push_vector),
	.pop(pop_vector),

	.data_out(data_vector_wire),
	.empty_flag(empty_flag_vector),
	.full_flag(full_flag_vector)
);


T08_FIFO FIFO_Matrix
(	
	.clk_wr(clk_wr),
	.clk_rd(clk_rd),
	.rst(rst),

	.data_in(Matrix),
	.push(push_matrix),
	.pop(pop_matrix),

	.data_out(data_matrix_wire),
	.empty_flag(empty_flag_matrix),
	.full_flag(full_flag_matrix)
);

Mul Multiplication
(	
	 .clk(clk_wr),
	 .rst(rst),
	
	 .data_Vector(data_vector_wire),
	 .data_Matrix(data_matrix_wire),

	 .Result(Mul_result),
	 .vector(Mul_vector)
	
);

muxN_2_1 Mux_mul_fifo
(
	 .Selector(enable_mux_vector),
	 .data0_in(Mul_vector),
	 .data1_in(Vector),
	
	 .mux_o(Mux_data_o)

);


N_register Register_result
(
	 .clk(clk_wr),
	 .rst(rst),
	 .en(TRUE),								
	 .data_in(Sum_result),		    		
	
	 .data_o(data_register)					
	
);

muxN_2_1 Mux_adder_reg
(
	 .Selector(enable_add_reg),
	 .data0_in(data_register),
	 .data1_in(ZERON),
	
	 .mux_o(Mux_data_add_o)

);


Sum add_result
(	
	 .clk(clk_wr),
	 .rst(rst),
	
	 .data_1(Mul_result),
	 .data_2(Mux_data_add_o),

	 .Result(Sum_result)
	
);

T08_FIFO FIFO_Result
(	
	.clk_wr(clk_wr),
	.clk_rd(clk_rd),
	.rst(rst),

	.data_in(Sum_result),
	.push(push_result),
	.pop(pop_result),

	.data_out(data_out),
	.empty_flag(empty_flag_result),
	.full_flag(full_flag_result)
);



endmodule

module Counter_N
import P03_pkg::*;
(
	// Input Ports
	input clk,
	input reset,
	input enable,
	input Data_N N_counter,
	
	// Output Ports
	
	output logic Flag_counter
	
);

Data_N Count_logic;

always_ff@(posedge clk or negedge reset)
	begin: counter
		if (reset == FALSE)
			Count_logic <= ZERON;
		else 
			begin
				if(enable == TRUE) 				
					Count_logic <= Count_logic + UNIT;						
				else
					Count_logic <= ZERON;			
			end
	end: counter
	
always_comb
begin

	if(N_counter != Count_logic)
		Flag_counter = FALSE;
	else
		Flag_counter = TRUE;

end	


endmodule



module FIFO
import FIFO_pkg::*;
(	
	input clk_wr,
	input clk_rd,
	input rst,

	input dataN_t data_in,
	input logic  push,
	input logic  pop,

	output dataN_t data_out,
	output logic empty_flag,
	output logic full_flag
);


FIFO_st fifo_wires;
logic Flag_wr;
logic Flag_rd;
logic Flag_wr_full;
logic Flag_rd_empty;
logic Start_write;
logic Start_read;
dataN_t addr_wr_pt;
dataN_t addr_rd_pt;

Mem_RAM RAM(

	.clk_wr(clk_wr),   
	.clk_rd(clk_rd),   
	.wr_enable(fifo_wires.enable_ram_wr),    
	.rd_enable(fifo_wires.enable_ram_rd),    
	.data_wr(data_in),  
	.addr_wr({Flag_wr,addr_wr_pt}),  
	.addr_dr({Flag_rd,addr_rd_pt}),  

	.data_rd(data_out)  
);

Control_unit_wr CU_WR
(
	.clk(clk_wr),
	.rst(rst),
	
	.Start_wr(push),
	.Flag_full(full_flag),	
		
	.Full_flag(Flag_wr_full),
	.Start_flag(Start_write)
	
);

Pointer_wr pn_wr
(	
	.clk_wr(clk_wr),
	.rst(rst),
	.push(Start_write),
	.full_flag(Flag_wr_full),

	.WR_enable(fifo_wires.enable_ram_wr),
	.addr_wr(addr_wr_pt),
	.Flag_wr(Flag_wr)

);

Control_unit_rd CU_RD
(
	.clk(clk_rd),
	.rst(rst),
	
	.Start_rd(pop),
	.Flag_empty(empty_flag),	
		
	.Empty_flag(Flag_rd_empty),
	.Start_flag(Start_read)
	
);


Pointer_rd pn_rd
(	
	.clk_rd(clk_rd),
	.rst(rst),
	.pop(Start_read),
	.empty_flag(Flag_rd_empty),

	.RD_enable(fifo_wires.enable_ram_rd),
	.addr_rd(addr_rd_pt),
	.Flag_rd(Flag_rd)

);

sync_addr_full Full_module(
	.clk(clk_wr),
	.rst(rst),
	.addr_1({Flag_rd,addr_rd_pt}), 
	.addr_2({Flag_wr,addr_wr_pt}), 
	
	.flag_sync(full_flag)
);

sync_addr_empty Empty_module(
	.clk(clk_rd),
	.rst(rst),
	.addr_1({Flag_wr,addr_rd_pt}), 
	.addr_2({Flag_rd,addr_wr_pt}),

	.flag_sync(empty_flag)
);


endmodule

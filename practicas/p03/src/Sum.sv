
module Sum
import top_pkg::*;
(		
	input dataNN_t data_1_fromMul,
	input dataS_t data_2_fromRegister,

	output dataS_t Result
	
);

dataS_t temp_sum;

always_comb 
begin
		temp_sum = ({({3{ZERO}}),data_1_fromMul}) + data_2_fromRegister;

end
	
assign Result = temp_sum;

	
endmodule

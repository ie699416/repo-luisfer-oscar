module PISO
import nBitsPackage::*;
 (
input  clk,    
input  rst,     
input  enable,   
input  Load,    
input  data8_t Data_in,   
output Data_out     
);

logic data_l;

logic [8:0]  Register_load;

logic [8:0]  Register_tx;

assign Register_load  = ({Data_in, 1'b1 }) ;

always_ff@(posedge clk or negedge rst)
	begin
		if(rst == FALSE)
        Register_tx  <= 10'b0000000000 ;
		else if (Load)
			begin 
				Register_tx  <= Register_load;
				
			end
		else if (enable == TRUE) 			
			Register_tx  <= {Register_tx[7:0], 1'b0};
		else
			Register_tx <= Register_tx;

	end
	
always_comb
	begin
		if (Load == TRUE)
			data_l = 1'b0;
		else
			data_l = Register_tx[8];
	end

assign Data_out  =  data_l;
endmodule

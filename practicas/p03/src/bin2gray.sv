
/*
*************************************************************************************
*	Coder:	OSCAR MAISTERRA FLORES	 		[ ie699416]
*	Coder: 	LUIS FERNANDO MORENO ALONSO	[ ie693314] 
*	Date : 	June 8th, 2019
*  Name :   Bin to Gray 
*
*	Description: 	 
*		 this code decodifies the data in binary to data with the logarith gray
* 
*************************************************************************************
*/


module bin2gray
import FIFO_pkg::*;
(
	input  dataN_t data_bin,

	output dataN_t data_gray
);

 dataN_t xor_l;
    generate 
        genvar i;
        for ( i = ZERO ; i < ( DATA_N - UNIT ) ; i = ( i + UNIT ))begin: bin2gray
			
            xclusive_xor XOR(
				.b_in(data_bin[i]),
				.a_in(data_bin[i + UNIT]),

				.xor_o(xor_l[i])
			);
        end: bin2gray
    endgenerate

    assign data_gray = {data_bin[DATA_N - UNIT],xor_l[DATA_N - TWO:0]};


endmodule


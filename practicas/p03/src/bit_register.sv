/*
*************************************************************************************
*	Coder:	OSCAR MAISTERRA FLORES	 		[ ie699416]
*	Coder: 	LUIS FERNANDO MORENO ALONSO	[ ie693314] 
*	Date : 	June 8th, 2019
*  Name :   bit_register.sv  
*
*	Description: 	 
*		 This module operates the FF_D for one bit
* 
*************************************************************************************
*/

module bit_register
import nBitsPackage::*;

(
	// inputs 
	input clk,
	input rst,
	input clr,
	input en,								
	input data_in,		    				// One bit reg
	// outputs 
	output logic data_o					// One bit reg	
);

always_ff@(posedge clk, negedge rst, posedge clr) 
	begin: bit_register
		if (rst == FALSE)
			begin
				data_o <= ZERO;
			end
		else if (clr == TRUE)
			begin
				data_o <= ZERO;
			end
		else 
			begin
				if(en == TRUE)
					data_o <= data_in;
				else
					data_o <= data_o;
			end
			
	end:  bit_register

endmodule



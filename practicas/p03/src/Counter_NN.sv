module Counter_NN
import P03_pkg::*;
(
	// Input Ports
	input clk,
	input rst,
	input en,
	input clear,	
	input Data_N NN_counter,
	
	// Output Ports
	
	output logic Flag_counter
	
);

Data_NN Count_logic;

always_ff@(posedge clk or negedge rst)
	begin: counter
		if (rst == FALSE)
			Count_logic <= ZERONN;
		else 
			begin
				if(en == TRUE)
					begin
						if (Flag_counter == HIGH)
							begin								
								Count_logic <= ZERONN;
							end
						else					
							Count_logic <= Count_logic + UNIT;
					end
				else if (clear == TRUE)
					Count_logic <= ZERONN;	
				else
					Count_logic <= Count_logic;							
			end
	end: counter
//--------------------------------------------------------------------------------------------

always_comb
begin

	if(DATA_R - UNIT == Count_logic)
		Flag_counter = HIGH;
	else
		Flag_counter = LOW;

end	



endmodule

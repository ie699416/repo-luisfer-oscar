/*
*************************************************************************************
*	Coder:	OSCAR MAISTERRA FLORES	 		[ ie699416]
*	Coder: 	LUIS FERNANDO MORENO ALONSO	[ ie693314] 
*	Date : 	June 8th, 2019
*  Name :   Queue_N_register.sv  
*
*	Description: 	 
*		 This module operates the FF_D for N bits
* 
*************************************************************************************
*/

module Queue_N_register
import top_pkg::*;

(
	// inputs 
	input clk,
	input rst,
	input en,								
	input dataN_t data_in,		    		// M <- Multiplicand	
	// outputs 
	output dataN_t data_o					// M <- value for adder	
	
);

dataN_t QUEUE_shift [63:0];

/*Init values*/

assign QUEUE_shift [0] = data_in;
assign data_o = QUEUE_shift[20];



genvar i;
/*
*************************************************************************************
* Instances definition  
*************************************************************************************
*/


generate
	for (i = ZERO; i<63 ;i++)
		begin:QUEUE_
			N_register Register_data_L
			(
				 .clk(clk),
				 .rst(rst),
				 .en(en),								
				 .data_in(QUEUE_shift[i]),	
				 .data_o(QUEUE_shift[i+1])						
			);
		end
endgenerate



endmodule



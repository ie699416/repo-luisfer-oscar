module Module_N
import top_pkg::*;
(	
	input Data_N data_in,
	output Data_N data_o
	
);

always_comb 
	begin
		case(N_decode'(data_in))
			N_ONE:
				begin 
					data_o = 3;
				end
			N_TWO:
				begin 
					data_o = 6;
				end
			N_THREE:
				begin 
					data_o = 11;
				end
			N_FOUR:
				begin 
					data_o = 18;
				end
			N_FIVE:
				begin 
					data_o = 27;
				end
			N_SIX:
				begin 
					data_o = 38;
				end
				
			N_SEVEN:
				begin 
					data_o = 51;
				end
				
			N_EIGHT:
				begin 
					data_o = 66;
				end
			default:
				
				begin 
					data_o = 0;
				end
		
		endcase
	end
endmodule
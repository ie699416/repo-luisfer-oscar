
module Control_unit_top
import top_pkg::*;
(	
	input clk,
	input rst,

	input Data_N data_in,
	input logic Counter_flag,
	input logic Flag_Finish,
	input logic Flag_Result,
	input logic Enable_data,
	input logic Flag_FE,
	input logic Flag_N,
	input logic Flag_rs,
	input logic Flag_start,
	input logic Flag_vector,
	input logic Flag_matrix,	
	input logic Flag_EF,
	input logic Flag_tx,
	
	output logic push_vector,
	output logic push_matrix,
	output logic push_result,
	output logic pop_vector,
	output logic pop_matrix,
	output logic pop_result,
	output logic flag_add_reg,
	output logic flag_mux_vector,
	output logic Start_counter_L,
	output logic Start_counter_N,
	output logic Enable_register_L,
	output logic Enable_register_N,
	output logic Enable_mul,
	output logic Enable_sum,
	output logic Enable_vector,
	output logic Enable_matrix,
	output logic flag_mux_push_vector,
	output logic End_tx,	
	output Data_N L_out,
	output Data_N N_out,
	output Data_N data_out_vector,
	output Data_N data_out_matrix

	
);

State_decode Actual_state;	

always_ff@(posedge clk or negedge rst) 
	begin
		if (rst == FALSE)
			Actual_state <= STATE_IDEL;			
		else 
			begin		
				case(Actual_state)				
						STATE_IDEL:
							begin
							if(Enable_data == TRUE && Flag_FE == TRUE)
								Actual_state <= STATE_L;
							else
								Actual_state <= Actual_state;
							end
							
						STATE_L:
							begin 
								if(Enable_data == TRUE)
									Actual_state <= STATE_CMD;	
								else
									Actual_state <= Actual_state;
							end
							
						STATE_CMD:
							begin 
								if(Flag_EF == TRUE)
									Actual_state <= STATE_IDEL;
								else 
									begin
										if(Enable_data == TRUE)
											begin													
												if(Flag_N == TRUE)
													Actual_state <= STATE_N;	
													
												else if(Flag_rs == TRUE)
													Actual_state <= STATE_RT;
													
												else if(Flag_start == TRUE)
													Actual_state <= STATE_START;	
													
												else if(Flag_matrix == TRUE)		
													Actual_state <= STATE_MATRIX;
													
												else if(Flag_vector == TRUE)
													Actual_state <= STATE_VECTOR;		
													
												else
													Actual_state <= Actual_state;
													
												end
										else
											Actual_state <= Actual_state;	
									end
								
							end
							
						STATE_N:
							begin
								if(Enable_data == TRUE && Flag_EF == TRUE)							
									Actual_state <= STATE_IDEL;
								else 
									Actual_state <= Actual_state;
							end
							
						STATE_START:
							begin 
								if(Enable_data == TRUE && Flag_EF == TRUE)								
									Actual_state <= STATE_IDEL;	
								else 
									Actual_state <= Actual_state;
							end
							
						STATE_MATRIX:
							begin 						
								if(Counter_flag == TRUE && Flag_EF == TRUE )
									Actual_state <= STATE_IDEL;
								else if(Enable_data == TRUE && Counter_flag == FALSE)
									Actual_state <= Actual_state;					
							end	

						STATE_VECTOR:
							begin 						
								if(Counter_flag == TRUE && Flag_EF == TRUE)
									Actual_state <= STATE_PROCESS;
								else if(Enable_data == TRUE && Counter_flag == FALSE )
									Actual_state <= Actual_state;					
							end
							
						STATE_PROCESS:
							begin 						
								if(Flag_Result == TRUE)
									Actual_state <= STATE_RESULT;
								else
									Actual_state <= Actual_state;					
							end
						STATE_RESULT:
							begin 						
								if(Flag_Finish == TRUE)
									Actual_state <= STATE_TX;
								else
									Actual_state <= Actual_state;					
							end
						STATE_TX:
							begin
								if(Flag_tx == TRUE)
									Actual_state <= STATE_IDEL;
								else
									Actual_state <= Actual_state;
							end
							
						default: Actual_state <= STATE_IDEL;
							
				 endcase	
			end
	end

always_comb 
	begin
		case(Actual_state)
			STATE_IDEL:
				begin 
					data_out_vector	= ZERON;
					data_out_matrix	= ZERON;
					L_out 				= ZERON;
					N_out					= ZERON;
					Enable_register_N = LOW; 
					Enable_register_L = LOW;
					Enable_mul			= LOW;
					Enable_sum			= LOW;
					Enable_vector		= LOW;
					Enable_matrix		= LOW;
					Start_counter_L	= LOW;
					Start_counter_N	= LOW;
					push_vector		   = LOW;
					push_matrix		   = LOW;
					push_result			= LOW;
					flag_add_reg 		= LOW;	
					flag_mux_vector   = LOW;
					flag_mux_push_vector= LOW;
					pop_matrix		   = LOW;
					pop_vector 			= LOW;
					pop_result			= LOW;
					End_tx				= LOW;
				end		
			STATE_L:
				begin 						
					data_out_vector	= ZERON;
					data_out_matrix	= ZERON;
					L_out 				= data_in;
					N_out					= ZERON;
					Enable_register_N = LOW; 
					Enable_register_L = HIGH;
					Enable_mul			= LOW;
					Enable_sum			= LOW;
					Enable_vector		= LOW;
					Enable_matrix		= LOW;
					Start_counter_L	= LOW;
					Start_counter_N	= LOW;
					push_vector		   = LOW;
					push_matrix		   = LOW;
					push_result			= LOW;
					flag_add_reg 		= LOW;	
					flag_mux_vector   = LOW;
					flag_mux_push_vector= LOW;
					pop_matrix		   = LOW;
					pop_vector 			= LOW;
					pop_result			= LOW;
					End_tx				= LOW;
				end
			STATE_CMD:
				begin 						
					data_out_vector	= ZERON;
					data_out_matrix	= ZERON;
					L_out 				= ZERON;
					N_out					= ZERON;
					Enable_register_N = LOW; 
					Enable_register_L = LOW;
					Enable_mul			= LOW;
					Enable_sum			= LOW;
					Enable_vector		= LOW;
					Enable_matrix		= LOW;
					Start_counter_L	= HIGH;
					Start_counter_N	= LOW;
					push_vector		   = LOW;
					push_matrix		   = LOW;
					push_result			= LOW;
					flag_add_reg 		= LOW;	
					flag_mux_vector   = LOW;
					flag_mux_push_vector= LOW;
					pop_matrix		   = LOW;
					pop_vector 			= LOW;
					pop_result			= LOW;
					End_tx				= LOW;
				end		
			STATE_N:
				begin 						
					data_out_vector	= ZERON;
					data_out_matrix	= ZERON;	
					L_out 				= ZERON;
					N_out					= data_in;
					Enable_register_N = HIGH; 
					Enable_register_L = LOW;
					Enable_mul			= LOW;
					Enable_sum			= LOW;
					Enable_vector		= LOW;
					Enable_matrix		= LOW;
					Start_counter_L	= HIGH;
					Start_counter_N	= LOW;
					push_vector		   = LOW;
					push_matrix		   = LOW;
					push_result			= LOW;
					flag_add_reg 		= LOW;	
					flag_mux_vector   = LOW;
					flag_mux_push_vector= LOW;
					pop_matrix		   = LOW;
					pop_vector 			= LOW;
					pop_result			= LOW;
					End_tx				= LOW;	
				end
			STATE_START:
				begin 						
					data_out_vector	= ZERON;
					data_out_matrix	= ZERON;	
					L_out 				= ZERON;
					N_out					= ZERON;
					Enable_register_N = LOW; 
					Enable_register_L = LOW;
					Enable_mul			= LOW;
					Enable_sum			= LOW;
					Enable_vector		= LOW;
					Enable_matrix		= LOW;
					Start_counter_L	= HIGH;
					Start_counter_N	= LOW;
					push_vector		   = LOW;
					push_matrix		   = LOW;
					push_result			= LOW;
					flag_add_reg 		= LOW;	
					flag_mux_vector   = LOW;
					flag_mux_push_vector= LOW;
					pop_matrix		   = LOW;
					pop_vector 			= LOW;
					pop_result			= LOW;
					End_tx				= LOW;			
				end
			STATE_MATRIX:
				begin
					data_out_vector	= ZERON;
					data_out_matrix	= data_in;	
					L_out 				= ZERON;
					N_out					= ZERON;
					Enable_register_N = LOW; 
					Enable_register_L = LOW;
					Enable_mul			= LOW;
					Enable_sum			= LOW;
					Enable_vector		= LOW;
					Enable_matrix		= HIGH;
					Start_counter_L	= HIGH;
					Start_counter_N	= LOW;
					push_vector		   = LOW;
					push_matrix		   = HIGH;
					push_result			= LOW;
					flag_add_reg 		= LOW;	
					flag_mux_vector   = LOW;
					flag_mux_push_vector= LOW;
					pop_matrix		   = LOW;
					pop_vector 			= LOW;
					pop_result			= LOW;
					End_tx				= LOW;
				end
			STATE_VECTOR:
				begin 	
					data_out_vector	= data_in;	
					data_out_matrix	= ZERON;
					L_out 				= ZERON;
					N_out					= ZERON;
					Enable_register_N = LOW; 
					Enable_register_L = LOW;
					Enable_mul			= LOW;
					Enable_sum			= LOW;
					Enable_vector		= HIGH;
					Enable_matrix		= LOW;
					Start_counter_L	= HIGH;
					Start_counter_N	= LOW;
					push_vector		   = HIGH;
					push_matrix		   = LOW;
					push_result			= LOW;
					flag_add_reg 		= HIGH;	
					flag_mux_vector   = HIGH;
					flag_mux_push_vector= LOW;
					pop_matrix		   = LOW;
					pop_vector 			= LOW;
					pop_result			= LOW;
					End_tx				= LOW;
				end
				
			STATE_PROCESS:
				begin 						
					data_out_vector	= ZERON;
					data_out_matrix	= ZERON;
					L_out 				= ZERON;
					N_out					= ZERON;
					Enable_register_N = LOW; 
					Enable_register_L = LOW;
					Enable_mul			= HIGH;
					Enable_sum			= HIGH;
					Enable_vector		= LOW;
					Enable_matrix		= LOW;
					Start_counter_L	= LOW;
					Start_counter_N	= HIGH;
					push_vector		   = LOW;
					push_matrix		   = LOW;
					push_result			= HIGH;
					flag_add_reg 		= LOW;	
					flag_mux_vector   = LOW;
					flag_mux_push_vector= HIGH;
					pop_matrix		   = HIGH;
					pop_vector 			= HIGH;
					pop_result			= LOW;
					End_tx				= LOW;
				end
				
			STATE_RESULT:
				begin 						
					data_out_vector	= ZERON;
					data_out_matrix	= ZERON;
					L_out 				= ZERON;
					N_out					= ZERON;
					Enable_register_N = LOW; 
					Enable_register_L = LOW;
					Enable_mul			= HIGH;
					Enable_sum			= HIGH;
					Enable_vector		= LOW;
					Enable_matrix		= LOW;
					Start_counter_L	= LOW;
					Start_counter_N	= LOW;
					push_vector		   = LOW;
					push_matrix		   = LOW;
					push_result			= LOW;
					flag_add_reg 		= LOW;	
					flag_mux_vector   = LOW;
					flag_mux_push_vector= LOW;
					pop_matrix		   = LOW;
					pop_vector 			= LOW;
					pop_result			= HIGH;
					End_tx				= LOW;
				end
				
			STATE_TX:
				begin 						
					data_out_vector	= ZERON;
					data_out_matrix	= ZERON;
					L_out 				= ZERON;
					N_out					= ZERON;
					Enable_register_N = LOW; 
					Enable_register_L = LOW;
					Enable_mul			= LOW;
					Enable_sum			= LOW;
					Enable_vector		= LOW;
					Enable_matrix		= LOW;
					Start_counter_L	= LOW;
					Start_counter_N	= LOW;
					push_vector		   = LOW;
					push_matrix		   = LOW;
					push_result			= LOW;
					flag_add_reg 		= LOW;	
					flag_mux_vector   = LOW;
					flag_mux_push_vector= LOW;
					pop_matrix		   = LOW;
					pop_vector 			= LOW;
					pop_result			= LOW;
					End_tx				= HIGH;
				end
			default: 
				begin 
					data_out_vector	= ZERON;
					data_out_matrix	= ZERON;
					L_out 				= ZERON;
					N_out					= ZERON;
					Enable_register_N = LOW; 
					Enable_register_L = LOW;
					Enable_mul			= LOW;
					Enable_sum			= LOW;
					Enable_vector		= LOW;
					Enable_matrix		= LOW;
					Start_counter_L	= LOW;
					Start_counter_N	= LOW;
					push_vector		   = LOW;
					push_matrix		   = LOW;
					push_result			= LOW;
					flag_add_reg 		= LOW;	
					flag_mux_vector   = LOW;
					flag_mux_push_vector= LOW;
					pop_matrix		   = LOW;
					pop_vector 			= LOW;
					pop_result			= LOW;
					End_tx				= LOW;
				end		
					
	endcase
end

endmodule




module xclusive_conditional
import FIFO_pkg::*;
(
	input addrN_t  data_in_1,
	input addrN_t  data_in_2,

	output logic  flag_out
);

always_comb
	begin
		if( data_in_1 == data_in_2)
			flag_out = TRUE;			
		else
			flag_out = FALSE;
	end

endmodule


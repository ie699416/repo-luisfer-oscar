
module CLK_1000
import nBitsPackage::*;

(
	
	// Input Ports
	input clk_FPGA,
	input rst,
	input en, 
	// Output Ports
	output logic CLK_1000
	
);

data9_t Count_logic; // Max count 436

logic MaxValue_Bit = LOW;


always_ff@(posedge clk_FPGA or negedge rst) 
	begin
		if (rst == FALSE)
			begin
					Count_logic <= ZERO9;
					CLK_1000 <= LOW;						
			end
		else
			begin
				if(en == TRUE)
					begin
						if(MaxValue_Bit == HIGH)
							begin							
								CLK_1000 <= ~CLK_1000;
								Count_logic <= ZERO9;		
							end
						else 
							Count_logic <= Count_logic + UNIT;
					end
				else
					begin
						CLK_1000 <= LOW;
						Count_logic <= ZERO9;			
					end
			end
	end
	
	
	
always@(*)
			begin
				if(Count_logic == ONEK_COUNT_VALUE - UNIT)
					MaxValue_Bit = HIGH;
				else
					MaxValue_Bit = LOW;
			end




endmodule 

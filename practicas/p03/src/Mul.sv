


module Mul
import top_pkg::*;
(		
	input Vector_N data_Vector,
	input Matrix_N data_Matrix,

	output dataNN_t Result	
);

dataNN_t temp_mul;

always_comb 
begin

		temp_mul = data_Vector * data_Matrix;
		
end
	
assign Result = temp_mul;
	
	
endmodule

/*
*************************************************************************************
*	Coder:	OSCAR MAISTERRA FLORES	 		[ ie699416]
*	Coder: 	LUIS FERNANDO MORENO ALONzO	[ ie693314] 
*	Date:          Jun 4th, 2019	
*  Name:          Counter_data.sv      
*************************************************************************************
*/

module Counter_data
import nBitsPackage::*;
(
	// Input Ports
	input clk,
	input rst,
	input en,
	input clear,
	
	// Output Ports
	output logic flag,
	output dataN_t countValue_o
	
);

logic MaxValue_Bit;
dataCount_t Count_logic;

always_ff@(posedge clk or negedge rst)
	begin: counter
		if (rst == FALSE)
			Count_logic <= ZERON;
		else 
			begin
				if(en == TRUE)
					begin
						if (MaxValue_Bit == HIGH)
							begin								
								Count_logic <= ZERON;
							end
						else					
							Count_logic <= Count_logic + UNIT;
					end
				else if (clear == TRUE)
					Count_logic <= ZERON;	
				else
					Count_logic <= Count_logic;							
			end
	end: counter
//--------------------------------------------------------------------------------------------

always_comb
	begin
		if(Count_logic ==  N )
			begin
				flag = HIGH;				
				MaxValue_Bit = LOW;
			end
		else if (Count_logic ==  N + 1)
			begin				
				flag = LOW;				
				MaxValue_Bit = LOW; 
			end
		else		
			begin				
				flag = LOW;				
				MaxValue_Bit = LOW; 
			end                               
	end


assign countValue_o = Count_logic;

endmodule

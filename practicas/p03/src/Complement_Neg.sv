/*
*************************************************************************************
*	Coder:	OSCAR MAISTERRA FLORES	 		[ ie699416]
*	Coder: 	LUIS FERNANDO MORENO ALONSO	[ ie693314] 
*	Date:          Jun 8th, 2019
*  Name:          Complement_Neg.sv
*  Description:   The process below will perform the two's complement only by the 
*						flag request. 
*
*************************************************************************************
*/
module Complement_Neg
import nBitsPackage::*;
(
	input complement_flag_in,
	input data19_t	data_in,
	output data19_t comp2_o
	
);


/*
*************************************************************************************
* Combiational processes 
*************************************************************************************
*/

always_comb 
	begin: comp2_always		
		if(complement_flag_in == TRUE)
			comp2_o = (~data_in) + UNIT;
		else
			comp2_o = data_in;
	end: comp2_always

endmodule 
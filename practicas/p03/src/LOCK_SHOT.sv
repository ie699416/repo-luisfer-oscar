module LOCK_SHOT
import FIFO_pkg::*;
(
    input clk,
    input rst,	 
	
	input logic shot,
	input logic flag_EF,	
	output logic lock
	
);


State_lock Actual_state;	

always_ff@(posedge clk or negedge rst) 
	begin
		if (rst == FALSE)
			Actual_state <= STATE_IDLE_LOCK;			
		else 
			begin		
				case(Actual_state)				
						STATE_IDLE_LOCK:
							begin 						
								if (shot == TRUE)
										Actual_state <= STATE_LOCK; 									
								else 
										Actual_state <= STATE_IDLE_LOCK; 							
							end	
						STATE_LOCK:
							begin 
								if (flag_EF == FALSE)	
									Actual_state <= STATE_IDLE_LOCK;
								else
									Actual_state <= STATE_LOCK; 							
							end			
							
						default: Actual_state<=STATE_IDLE_LOCK;
							
				 endcase	
			end
	end

always_comb 
	begin
		case(Actual_state)
			STATE_IDLE_LOCK:
				  
					lock = LOW;
				
			STATE_LOCK:
				  
					lock = HIGH;			
				 				
			 default:
				
					lock = LOW;
				
	 
	endcase
end

endmodule

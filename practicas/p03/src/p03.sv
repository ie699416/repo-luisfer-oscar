/*
*************************************************************************************
*	Coder:	OSCAR MAISTERRA FLORES	 		[ ie699416]
*	Coder: 	LUIS FERNANDO MORENO ALONSO	[ ie693314] 
*	Date:          Jul 3th, 2019
*  Name:          p03.sv
*  Description:   First approach
*
*************************************************************************************
*/
module p03
import nBitsPackage::*;
(
	//inputs
	input clkFPGA,
	input reset_pb_in,
	input pop,
	input serial_input_RX_in	// PIN_G12 	
);

logic RX_interrupt_l;
logic CLK_115200_16X_l;
logic CLK_1000_l;
logic push_l;
logic Register_data_N_PUSH_l;
logic TX_interrupt_PUSH_l;

data8_t receivedData_l;
data8_t TX_DATA_FROM_Q;
data19_t comp2_l;
data19_t data_from_FIFO_result_l;

Data_N First_data_reg;
Data_N L_out_wire;
Data_N L_Counter_wire;
Data_N N_Counter_wire;
Data_N N_out_wire;
Data_N data_vector_wire;
Data_N data_matrix_wire;
Data_N counter6_countlogic_l;

data8_t sign_Ascii_l;
data8_t Uni_Ascii_l;
data8_t Dec_Ascii_l;
data8_t Cen_Ascii_l;
data8_t Uni_Mill_Ascii_l;
data8_t Dec_Mill_Ascii_l;

data8_t demux_l;


decoder_en sign_l;
decoder_en Dec_Mill_l;
decoder_en Uni_Mill_l;
decoder_en Cen_l;
decoder_en Dec_l;
decoder_en Uni_l;

logic push_vector_wire;
logic push_matrix_wire;
logic push_result_wire;
logic pop_vector_wire;
logic pop_matrix_wire;
logic pop_result_wire;
logic flag_add_reg_wire;
logic flag_mux_vector_wire;
logic Start_counter_L_wire;
logic Start_counter_N_wire;
logic Enable_reg_L_wire;
logic Enable_reg_N_wire;
logic Enable_mul_wire;
logic Enable_sum_wire;
logic Enable_vector_wire;
logic Enable_matrix_wire;
logic Flag_counter_wire;
logic Flag_Finish_wire;
logic Flag_Result_wire;
logic Vector_push;
logic Matrix_push;
logic counterNN_flag;
logic TX_interrupt_l;
logic ALL_BYTES;

logic Wire_FE;
logic Wire_N;
logic Wire_rs;
logic Wire_start;
logic Wire_matrix;
logic Wire_vector;
logic Wire_EF;

/* 
*************************************************************************************
* Instances definition
*************************************************************************************
*/

CLK_115200_16X clk_UART
(
	.clk_FPGA(clkFPGA),
	.rst(reset_pb_in),
	.en(HIGH), 						//Always enable
	.CLK_115200_16X(CLK_115200_16X_l)	
);

CLK_1000 clk_POP
(
	.clk_FPGA(clkFPGA),
	.rst(reset_pb_in),
	.en(HIGH), 						//Always enable
	.CLK_1000(CLK_1000_l)	
);


UART RX
(
    .CLK_115200_16X_l(CLK_115200_16X_l),
	 .rst(reset_pb_in),
	 
	 // RX input signals
	 .serialData_RX_in(serial_input_RX_in),					// RX serial input	 
	 .clearInterrupt_in(),					// RX INT signal clear
	 
	 // TX input signals	 
	 .transmit_in(TX_CONTROL_FLAG_l), 							// TX external control start flag
    .transmitData_in(TX_DATA_FROM_Q),			// TX parallel input
	 
	 // RX output signals
	 .receivedData_o(receivedData_l),				// RX parallel output
	 .RX_parityError_o(RX_parity_o),					// RX error
	 .RX_interrupt_o(RX_interrupt_l),						// RX received	1 byte
	 
	// TX output signals
	 .TX_interrupt_o(TX_interrupt_l),
	 .serialData_TX_o()					// TX serial output
	 
);


Module_CMD CMD_decode
(	
	 .data_in(receivedData_l),

	 .Flag_FE(Wire_FE),
	 .Flag_N(Wire_N),
	 .Flag_rs(Wire_rs),
	 .Flag_start(Wire_start),
	 .Flag_matrix(Wire_matrix),
	 .Flag_vector(Wire_vector),
	 .Flag_EF(Wire_EF)
	
);

PUSH_SHOT PUSH
(
    .clk(CLK_115200_16X_l),
    .rst(reset_pb_in),	 
	 .UART_dataReady_in(RX_interrupt_l),
	 .allowPush_Flag_in(push_vector_wire | push_matrix_wire ), 
	 .push_o(push_l)
	 
);

xclusive_AND AND_Vector
(
	 .a_in(push_l),
	 .b_in(Enable_vector_wire),
	 .and_o(Vector_push)
);

xclusive_AND AND_Matrix
(
	 .a_in(push_l),
	 .b_in(Enable_matrix_wire),
	 .and_o(Matrix_push)
);


Control_unit_top Control_unit_module
(	
	/////inputs
	 .clk(CLK_115200_16X_l),
	 .rst(reset_pb_in),

	 .data_in(),
	 .Counter_flag(Flag_counter_wire),
	 .Flag_Finish(empty_result),
	 .Flag_Result(counterNN_flag),
	 .Enable_data(RX_interrupt_l),
	 .Flag_FE(Wire_FE),
	 .Flag_N(Wire_N),
	 .Flag_rs(Wire_rs),
	 .Flag_start(Wire_start),
	 .Flag_matrix(Wire_matrix),
	 .Flag_vector(Wire_vector),
	 .Flag_EF(Wire_EF),
	 .Flag_tx(ALL_BYTES),
	 
	////outputs
	 .push_vector(push_vector_wire),
	 .push_matrix(push_matrix_wire),
	 .push_result(),
	 .pop_vector(pop_vector_wire),
	 .pop_matrix(pop_matrix_wire),
	 .pop_result(pop_result_wire),
	 .flag_add_reg(flag_add_reg_wire),
	 .flag_mux_vector(flag_mux_vector_wire),
	 .Start_counter_L(Start_counter_L_wire),
	 .Start_counter_N(Start_counter_N_wire),
	 .Enable_register_L(Enable_reg_L_wire),
	 .Enable_register_N(Enable_reg_N_wire),
	 .Enable_mul(Enable_mul_wire),
	 .Enable_sum(Enable_sum_wire),
	 .Enable_vector(Enable_vector_wire),
	 .Enable_matrix(Enable_matrix_wire),
	 .flag_mux_push_vector(flag_mux_push_vector_wire),
	 .End_tx(TX_CONTROL_FLAG_l),	
	 .L_out(L_out_wire),
	 .N_out(N_out_wire),
	 .data_out_vector(data_vector_wire),
	 .data_out_matrix(data_matrix_wire)
	 
);

Process_top Process_module
(	
	//////////Inputs
	 .clk_wr(CLK_115200_16X_l),
	 .clk_rd(CLK_1000_l),
	 .rst(reset_pb_in),

	 .Vector(receivedData_l),
	 .Matrix(receivedData_l),
	 .N_value(N_Counter_wire), 
	 .push_vector(Vector_push),
	 .push_matrix(Matrix_push),
	 .pop_vector(pop_vector_wire),
	 .pop_matrix(pop_matrix_wire),
	 .pop_result(pop_result_wire),
	 .enable_mul(Enable_mul_wire),
	 .enable_sum(Enable_sum_wire),
	 .enable_mux_vector(flag_mux_vector_wire),
	 .enable_mux_push_vector(flag_mux_push_vector_wire),
	 .enable_add_reg(flag_add_reg_wire),
	 .flag_EF(Wire_EF),

	//////////Ouputs
	 .data_out(data_from_FIFO_result_l),
	 .empty_flag_vector(empty_vector),
	 .full_flag_vector(full_vector),
	 .empty_flag_matrix(empty_matrix),
	 .full_flag_matrix(full_matrix),
	 .empty_flag_result(empty_result),
	 .full_flag_result(full_result),
	 .counterNN_flag(counterNN_flag)
	
);

Complement_Neg comp2
(
	.complement_flag_in(data_from_FIFO_result_l[18]),
	.data_in(data_from_FIFO_result_l),
	.comp2_o(comp2_l)
);

Conv_SIGN_BCD SIGN
(
	.sign_in(data_from_FIFO_result_l[18]),
	.sign_o(sign_l)

);


DD_BCD BCD_Result
(
	// Input Ports
	.A_in(comp2_l),
	.Dec_Mill_o(Dec_Mill_l),
	.Uni_Mill_o(Uni_Mill_l),
	.Cen_o(Cen_l),
	.Dec_o(Dec_l),
	.Uni_o(Uni_l)
);

Conv_ASCII Uni_Ascii
(
	.digit(Uni_l),
	.UART_ascii(Uni_Ascii_l)

);

Conv_ASCII Dec_Ascii
(
	.digit(Dec_l),
	.UART_ascii(Dec_Ascii_l)

);

Conv_ASCII Cen_Ascii
(
	.digit(Cen_l),
	.UART_ascii(Cen_Ascii_l)

);

Conv_ASCII Uni_Mill_Ascii
(
	.digit(Uni_Mill_l),
	.UART_ascii(Uni_Mill_Ascii_l)

);

Conv_ASCII Dec_Mill_Ascii
(
	.digit(Dec_Mill_l),
	.UART_ascii(Dec_Mill_Ascii_l)

);

Conv_ASCII sign_Ascii
(
	.digit(sign_l),
	.UART_ascii(sign_Ascii_l)

);


Counter_6 Counter_6
(
	// Input Ports
	 .clk(CLK_115200_16X_l),
	 .rst(reset_pb_in),
	 .en(pop_result_wire),
	 .clear(CLK_1000_l),	
	// Output Ports
	 .enableRegister_flag(enableRegister_flag_l),     // RegisterUART_TX enable
	 .Count_logic(counter6_countlogic_l)
);

Demux6_UART demux
(
	.decoder_in(counter6_countlogic_l), //counter6_countlogic
	.dataSign_in(sign_Ascii_l),
	.dataDec_Mill_in(Dec_Mill_Ascii_l),
	.dataUni_Mill_in(Uni_Mill_Ascii_l),
	.dataCen_in(Cen_Ascii_l),
	.dataDec_in(Dec_Ascii_l),
	.dataUni_in(Uni_Ascii_l),
	.data_o(demux_l)
);

Counter_L Counter_L
(
	// Input Ports
	 .clk(CLK_115200_16X_l),
	 .rst(reset_pb_in),
	 .en(Start_counter_L_wire && RX_interrupt_l),
	 .clear(~Start_counter_L_wire),
	 .N_counter(L_Counter_wire),
	
	// Output Ports
	 .Flag_counter(Flag_counter_wire)
);

PUSH_SHOT Register_data_N_PUSH
(
    .clk(CLK_115200_16X_l),
    .rst(reset_pb_in),	 
	 .UART_dataReady_in(~Enable_reg_N_wire),
	 .allowPush_Flag_in(HIGH), 
	 .push_o(Register_data_N_PUSH_l)
	 
);


N_register Register_data_N
(
	 .clk(CLK_115200_16X_l),
	 .rst(reset_pb_in),
	 .en(Register_data_N_PUSH_l),								
	 .data_in(receivedData_l),		    		
	
	 .data_o(N_Counter_wire)					
	
);

N_register Register_data_L
(
	 .clk(CLK_115200_16X_l),
	 .rst(reset_pb_in),
	 .en(Enable_reg_L_wire),								
	 .data_in(receivedData_l),		    		
	
	 .data_o(L_Counter_wire)					
	
);
PUSH_SHOT TX_data_N_PUSH
(
    .clk(CLK_115200_16X_l),
    .rst(reset_pb_in),	 
	 .UART_dataReady_in(~TX_interrupt_l),
	 .allowPush_Flag_in(HIGH), 
	 .push_o(TX_interrupt_PUSH_l)
	 
);



Queue_N_register Q

(
	.clk(CLK_115200_16X_l),
	.rst(reset_pb_in),
	.en(enableRegister_flag_l | TX_interrupt_PUSH_l),								
	.data_in(demux_l),		    		
	.data_o(TX_DATA_FROM_Q)					
	
);

Counter_data_63_TX counter_TX // 
(
	.clk(CLK_115200_16X_l),
	.rst(rst),
	.clear(LOW),
	.en(TX_interrupt_PUSH_l),
	.flag(ALL_BYTES),
	.countValue_o()	
);




/*
*************************************************************************************
*/
	
endmodule


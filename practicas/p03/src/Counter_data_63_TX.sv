/*
*************************************************************************************
*	Coder:	OSCAR MAISTERRA FLORES	 		[ ie699416]
*	Coder: 	LUIS FERNANDO MORENO ALONzO	[ ie693314] 
*	Date:          Jun 4th, 2019	
*  Name:          Counter_data_11_TX.sv      
*************************************************************************************
*/

module Counter_data_63_TX
import nBitsPackage::*;
(
	// Input Ports
	input clk,
	input rst,
	input en,
	input clear,
	
	// Output Ports
	output logic flag,
	output dataN_t countValue_o
	
);

logic MaxValue_Bit;
logic [7:0] Count_logic;

always_ff@(posedge clk or negedge rst)
	begin: counter
		if (rst == FALSE)
			Count_logic <= 8'b00000000;
		else 
			begin
				if (MaxValue_Bit == HIGH)
					begin								
						Count_logic <= ZERON;
					end
				else if(en == TRUE)			
						Count_logic <= Count_logic + UNIT;
						
				else
					Count_logic <= Count_logic;							
			end
	end: counter
//--------------------------------------------------------------------------------------------

always_comb
	begin
		if(Count_logic ==  63 )
			begin
				flag = HIGH;				
				MaxValue_Bit = LOW;
			end
		else if (Count_logic ==  64)
			begin				
				flag = LOW;				
				MaxValue_Bit = HIGH; 
			end
		else		
			begin				
				flag = LOW;				
				MaxValue_Bit = LOW; 
			end                               
	end

assign countValue_o = Count_logic;

endmodule

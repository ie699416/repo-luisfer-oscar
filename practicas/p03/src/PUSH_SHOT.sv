
module PUSH_SHOT
import nBitsPackage::*;
(
    input clk,
    input rst,	 
	 input UART_dataReady_in,
	 input allowPush_Flag_in, 
	 output logic push_o
	 
);


/*state enum definition*/
PUSH_STATE_en PUSH_currentState;

/*
*************************************************************************************
* Continuous assign
*************************************************************************************
*/




always_ff@(posedge clk or negedge rst ) 
	begin
		if(rst == FALSE)
			PUSH_currentState <= P_IDLE;
		else 
			begin
				case (PUSH_currentState)
					P_IDLE: 
						begin
							if(UART_dataReady_in == TRUE)
								PUSH_currentState <= NOT_PUSH;
							else 
								PUSH_currentState <= P_IDLE;										
						end
						
					NOT_PUSH:
						begin
							if(UART_dataReady_in == FALSE)
								PUSH_currentState <= PUSH;
							else
								PUSH_currentState <= NOT_PUSH;
						end
						
					PUSH:
						begin
								PUSH_currentState <=P_IDLE;
						end	
						
					default:
						PUSH_currentState <= P_IDLE;
				endcase
			end
	end


/*
*************************************************************************************
* Combinational processes
*************************************************************************************
*/



always@(*)
begin
	case(PUSH_currentState)
		P_IDLE: 
			begin
						push_o = LOW;
			end
			
		NOT_PUSH:	
			begin
						push_o = LOW;						
			end
			
		PUSH:
			begin
				if (allowPush_Flag_in == TRUE)
					begin
						push_o = HIGH;					
					end
				else 
					begin
						push_o = LOW;					
					end					
			end


		default: 
			begin
						push_o = LOW;
			end
	endcase

end


endmodule

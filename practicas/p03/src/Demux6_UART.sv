/*
*************************************************************************************
*	Coder:	OSCAR MAISTERRA FLORES	 		[ ie699416]
*	Coder: 	LUIS FERNANDO MORENO ALONSO	[ ie693314] 
*	Date : 	June 8th, 2019
*  Name :   muxN_2_1.sv  
*
*	Description: Multiplexer parametric N bus value
*
* 
*************************************************************************************
*/


module Demux6_UART
import P03_pkg::*;
(
	// Input Ports
	input Data_N decoder_in,
	input Data_N dataSign_in,	
	input Data_N dataDec_Mill_in,	
	input Data_N dataUni_Mill_in,	
	input Data_N dataCen_in,
	input Data_N dataDec_in,	
	input Data_N dataUni_in,
	output Data_N data_o

);


always@(*) 
	begin: demux
		case (decoder_in )	
			8'b000000000: 
				begin
					data_o = dataSign_in;			
				end
			8'b000000001:
				begin
					data_o = dataDec_Mill_in;	
				end
			8'b000000010: 
				begin
					data_o = dataUni_Mill_in;
				end
			8'b000000011:
				begin
					data_o = dataCen_in;
				end
			8'b000000100: 
				begin
					data_o = dataDec_in;
				end
			8'b000000101: 
				begin
					data_o = dataUni_in;
				end
			default:
				begin
					data_o = data_o;
				end
		
		
		endcase
			
			
	end: demux// end always

endmodule


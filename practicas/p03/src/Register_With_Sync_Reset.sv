//////////////////////////////////////////////////////////////////////////////////
// Company: ITESO
// Engineer: Gustavo Araiza Obeso ie699209@iteso.mx
// 			 
// Create Date:  25/04/2019  
// Design Name:  
// Module Name:  Register with reset
// Project Name: Sequential multiplier
// Target Devices: FPGA ALTERA DE2-115
// Tool versions: Quartus Prime
//
// Revision: 
// Revision 0.01 - File Created
// Additional Comments: 
////////////////////////////////////////////////////////////////////////////////////
module Register_With_Sync_Reset

import nBitsPackage::*;

(
	input clk,
	input reset,
	input enable,
	input Sync_Reset,					//Flush
	input data9_t Data_Input,
	output parity_RX_o,	
	output data8_t Data_Output
	
);

data9_t Data_reg;

always_ff@(posedge clk or negedge reset) begin: REGISTER
	if(reset == FALSE)
		Data_reg <= ZERO9;
	else
		if(Sync_Reset == TRUE) 
				Data_reg <= ZERO9;
		else
			if(enable == TRUE)
				Data_reg <= Data_Input;
			else
				Data_reg <= Data_reg;
				
end: REGISTER

assign Data_Output = Data_reg[WORD_LENGTH-1:0];
assign parity_RX_o = Data_reg[WORD_LENGTH]; 

endmodule

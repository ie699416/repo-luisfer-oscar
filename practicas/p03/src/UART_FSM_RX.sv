

module UART_FSM_RX
import nBitsPackage::*;
(
    input clk,
    input rst,	 
	 input UART_start_in,
	 input counter_flag_in,				 // timer for 16 counts, 115200 baud rate
	 input counter_data_in,				 // counter for 8 bits 
	 output RX_control_t halfCount_flag,
    output RX_control_t receiving_flag,
	 output RX_control_t data_ready_flag,
	 output RX_control_t waiting_flag,
	 output RX_control_t timer_flag
	 
);


/*state enum definition*/
UART_STATE_en UART_currentState;

/*
*************************************************************************************
* Continuous assign
*************************************************************************************
*/




always_ff@(posedge clk or negedge rst ) 
	begin
		if(rst == FALSE)
			UART_currentState <= IDLE;
		else 
			begin
				case (UART_currentState)
					IDLE: 
						begin
							if(UART_start_in == TRUE)
								UART_currentState <= STARTBIT;
							else 
								UART_currentState <= IDLE;
						end
					STARTBIT:
						begin
							if(counter_flag_in == TRUE)
								UART_currentState<=PROCESS;
							else 
								UART_currentState <= STARTBIT;
						end			
							
					PROCESS:
						begin
							if(counter_flag_in == TRUE)
								UART_currentState <= PARITY;
							else
								UART_currentState <= UART_currentState;
						end
					PARITY:
						begin
							if(counter_flag_in == TRUE)
								UART_currentState <= STOPBIT;
							else
								UART_currentState <= PARITY;
						end
					STOPBIT :
						begin
							if(counter_flag_in == TRUE)
								UART_currentState <= IDLE;
							else
								UART_currentState <= STOPBIT;
						end
					default:
						UART_currentState <= IDLE;
				endcase
			end
	end


/*
*************************************************************************************
* Combinational processes
*************************************************************************************
*/



always@(*)
begin
	case(UART_currentState)
		IDLE: 
			begin
				halfCount_flag = LOW;
				receiving_flag = LOW;
				data_ready_flag = LOW; 
				waiting_flag = HIGH; 
				timer_flag = LOW;
			end
		STARTBIT:
			begin
				halfCount_flag = HIGH;
				receiving_flag = LOW;
				data_ready_flag = LOW; 
				waiting_flag = LOW;  
				timer_flag = HIGH;
			end
		PROCESS:
			begin
				halfCount_flag = LOW;
				receiving_flag = HIGH;
				data_ready_flag = LOW; 
				waiting_flag = LOW; 
				timer_flag = HIGH;
			end
		PARITY:
			begin
				halfCount_flag = LOW;
				receiving_flag = HIGH;
				data_ready_flag = LOW; 
				waiting_flag = HIGH; 
				timer_flag = HIGH;
			end
		STOPBIT:	
			begin
				halfCount_flag = LOW;
				receiving_flag = LOW;
				data_ready_flag = HIGH; 
				waiting_flag = HIGH; 
				timer_flag = HIGH;
			end
		default: 
			begin
				halfCount_flag = LOW;
				receiving_flag = LOW;
				data_ready_flag = LOW; 
				waiting_flag = HIGH; 
				timer_flag = LOW;
			end
	endcase

end


endmodule

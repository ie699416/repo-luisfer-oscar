



module Mem_RAM
import FIFO_pkg::*;
(
	input clk_wr,   
	input clk_rd,   

	input wr_enable,    
	input rd_enable,    
	input dataN_t data_wr,  
	input addrN_t addr_wr,  
	input addrN_t addr_dr,  

	output dataN_t data_rd  
);


logic [DATA_N-1:0]  ram [DEPTH_N-1:0];


always_ff@( posedge clk_wr )
 begin
		if( wr_enable )begin
			 ram[addr_wr] <= data_wr;
		end
end

always_ff@( posedge clk_rd ) begin
	if( rd_enable )begin
	    data_rd <= ram[addr_dr];
    end
end



endmodule

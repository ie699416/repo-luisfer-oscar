

module xclusive_conditional_empty
import FIFO_pkg::*;
(
	input addrN_t  data_in_1,
	input addrN_t  data_in_2,

	output logic  flag_out
);

logic temp_flag;

always_comb
begin
	if( data_in_1 == data_in_2)

		 temp_flag = TRUE;
		
	else

		 temp_flag = FALSE;
end

	assign flag_out = temp_flag;


endmodule


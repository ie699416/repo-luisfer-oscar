
`ifndef MYNPACKAGE_DONE
   `define MYNPACKAGE_DONE
	 package nBitsPackage; // package name

		parameter WORD_LENGTH = 8;
		parameter N = 8;
		parameter MSB = 3;
		parameter  DATA_N  = 8;
		parameter FPGA_CCLK =50000000;
		parameter BD_115200 = 115200; 		
		parameter ONEK  = 100000;
		parameter BD_COUNT_VALUE = FPGA_CCLK/BD_115200/2 ;	//	217
		parameter HALF_BD_COUNT_VALUE = BD_COUNT_VALUE/2; 	// 108
		parameter SAMP_16X_COUNT_VALUE = BD_COUNT_VALUE/16; //	13		
		parameter ONEK_COUNT_VALUE = FPGA_CCLK/ONEK/2; //	13
		parameter SAMP_HALF_COUNT = 8; 
		parameter SAMP_FULL_COUNT = 16; 
		
		parameter ZERO8 = {WORD_LENGTH{1'b0}};
		parameter ZERO4 = {4{1'b0}};
		parameter ZERO9 = {9{1'b0}};
		parameter ZERO11 = {11{1'b0}};	
		parameter UNIT = 1'b1;	
		parameter HIGH = 1'b1;
		parameter LOW  = 1'b0;
		parameter TRUE = 1'b1;
		parameter FALSE = 1'b0;
		parameter ZERO = 1'b0;		
		parameter CERO = 1'b0;
		parameter ZERON = {N{1'b0}};
		parameter TOP_VAL	= {N{1'b1}};
		
			
		typedef logic [WORD_LENGTH-1:0] data8_t;
		typedef logic [WORD_LENGTH:0] data9_t;
		
		typedef logic [18:0] data19_t;
		typedef logic [1:0] data2_t;		
		typedef logic [10:0] data11_t;
		
		typedef logic [3:0] dataNum_t;
		typedef logic [5:0] dataCount_t;
		typedef logic [N-1:0] dataN_t;
		typedef logic [DATA_N-1:0]  Data_N;
		typedef logic RX_control_t;
		
		typedef enum logic [3:0] {IDLE, STARTBIT,PROCESS, PARITY, STOPBIT} UART_STATE_en;	
		typedef enum logic [2:0] {P_IDLE, NOT_PUSH, PUSH} PUSH_STATE_en;
		
		typedef enum logic [3:0] {
		ZERO_d, ONE, TWO, THREE, FOUR, FIVE, SIX, SEVEN, EIGHT, NINE, A, B, C, D, SIGN, DEFAULT
		} decoder_en;
		
		
	endpackage
`endif
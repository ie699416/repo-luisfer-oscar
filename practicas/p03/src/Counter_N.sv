module Counter_N
import P03_pkg::*;
(
	// Input Ports
	input clk,
	input rst,
	input en,
	input clear,	
	input Data_N N_counter,
	
	// Output Ports
	
	output logic Flag_counter
	
);

Data_N Count_logic;

always_ff@(posedge clk or negedge rst)
	begin: counter
		if (rst == FALSE)
			Count_logic <= ZERON;
		else 
			begin
				if(en == TRUE)
					begin
						if (Flag_counter == HIGH)
							begin								
								Count_logic <= ZERON;
							end
						else					
							Count_logic <= Count_logic + UNIT;
					end
				else if (clear == TRUE)
					Count_logic <= ZERON;	
				else
					Count_logic <= Count_logic;							
			end
	end: counter
//--------------------------------------------------------------------------------------------

always_comb
begin

	if(N_counter - UNIT == Count_logic)
		Flag_counter = HIGH;
	else
		Flag_counter = LOW;

end	



endmodule

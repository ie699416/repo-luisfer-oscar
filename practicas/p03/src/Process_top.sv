


module Process_top
import top_pkg::*;
(	
	input clk_wr,
	input clk_rd,
	input rst,

	input Vector_N Vector,
	input Matrix_N Matrix,
	input Vector_N N_value, 
	input logic push_vector,
	input logic push_matrix,
	input logic pop_vector,
	input logic pop_matrix,
	input logic pop_result,
	input logic enable_mul,
	input logic enable_sum,
	input logic enable_mux_vector,
	input logic enable_mux_push_vector,
	input logic enable_add_reg,
	input logic flag_EF,

	output dataS_t data_out,
	output logic empty_flag_vector,
	output logic full_flag_vector,
	output logic empty_flag_matrix,
	output logic full_flag_matrix,
	output logic empty_flag_result,
	output logic full_flag_result,
	output logic counterNN_flag
	
	
);

logic pop_l_vector;
logic pop_l_lock;
logic push_result_wire;
logic shot_load_l;
logic phase_reg_l;
logic push_result_shot_l;
dataN_t NN_value_l;

dataS_t data_vector_wire;
dataS_t data_matrix_wire;
dataS_t Mux_data_o;
dataS_t Mux_data_add_o;
dataN_t Mul_vector;

dataNN_t Mul_result;
dataS_t data_register;
dataS_t Sum_result;


PUSH_SHOT pop_clk_1k
(
    .clk(clk_wr),
    .rst(rst),	 
	 .UART_dataReady_in(clk_rd),
	 .allowPush_Flag_in(enable_mux_push_vector), 
	 .push_o(pop_l_lock)
	 
);

LOCK_SHOT lock
(
   .clk(clk_wr),
   .rst(rst),	 
	.shot(pop_l_lock),
	.flag_EF(flag_EF),	
	.lock(pop_l_vector)
	
);

Counter_N Counter_N
(
	// Input Ports
	 .clk(clk_rd),
	 .rst(rst),
	 .en(pop_l_vector),
	 .clear(),
	 .N_counter(N_value),
	
	// Output Ports
	 .Flag_counter(phase_reg_l)
	
);



bit_register phase_reg
(
	.clk(clk_rd),
	.rst(rst),
	.clr(LOW),
	.en(HIGH),											// always enable					
	.data_in(phase_reg_l),		    				// One bit reg
	.data_o(push_result_wire)						// One bit reg	
);


mux_bit_2_1 Mux_push_vector
(
	 .Selector(enable_mux_push_vector),
	 .data0_in(push_vector),
	 .data1_in(pop_l_vector & pop_l_lock),
	
	 .mux_o(Mux_push_vec)

);


T08_FIFO FIFO_Vector
(	
	.clk_wr(clk_wr),
	.clk_rd(clk_rd),
	.rst(rst),

	.data_in({({11{ZERO}}),Mux_data_o}), // 19 - 8 
	.push(Mux_push_vec),
	.pop(pop_vector),

	.data_out(data_vector_wire),
	.empty_flag(empty_flag_vector),
	.full_flag(full_flag_vector)
);


T08_FIFO FIFO_Matrix
(	
	.clk_wr(clk_wr),
	.clk_rd(clk_rd),
	.rst(rst),

	.data_in({({11{ZERO}}),Matrix}),
	.push(push_matrix),
	.pop(pop_matrix),
	.data_out(data_matrix_wire),
	.empty_flag(empty_flag_matrix),
	.full_flag(full_flag_matrix)
);

Mul Multiplication
(	
	
	 .data_Vector(data_vector_wire[7:0]),
	 .data_Matrix(data_matrix_wire[7:0]),
	 .Result(Mul_result) // 16 bits
	
);

muxN_2_1 Mux_mul_fifo
(
	 .Selector(enable_mux_vector),
	 .data0_in(data_vector_wire[7:0]),
	 .data1_in(Vector), // 8bits
	
	 .mux_o(Mux_data_o) // Bits

);


S_register Register_result
(
	 .clk(clk_rd),
	 .rst(rst),
	 .en(enable_mux_push_vector),		
	 .clear(push_result_wire),
	 .data_in(Mux_data_add_o),	    		
	
	 .data_o(Sum_result)					
	
);

SHOT_LOAD shot_load
(
    .clk(clk_rd),
    .rst(rst),	 
	 .data_in(enable_mux_push_vector),
	 .allow_Flag_in(enable_mux_push_vector), 
	 .push_o(shot_load_l)
	 
);

muxS_2_1 Mux_adder_reg
(
	 .Selector(push_result_wire | shot_load_l),
	 .data0_in(data_register),
	 .data1_in(ZEROS),	
	 .mux_o(Mux_data_add_o)

);


Sum add_result
(		 
	 .data_1_fromMul(Mul_result),
	 .data_2_fromRegister(Sum_result),
	 .Result(data_register)
	
);

PUSH_SHOT push_result_shot
(
    .clk(clk_wr),
    .rst(rst),	 
	 .UART_dataReady_in(phase_reg_l),
	 .allowPush_Flag_in(enable_mux_push_vector), 
	 .push_o(push_result_shot_l)
	 
);

T08_FIFO FIFO_Result 
(	
	.clk_wr(clk_wr),
	.clk_rd(clk_rd),
	.rst(rst),

	.data_in(data_register),
	.push(push_result_shot_l),
	.pop(pop_result),

	.data_out(data_out),
	.empty_flag(empty_flag_result),
	.full_flag()
);


Module_N NN_value
(	
	.data_in(N_value),
	.data_o(NN_value_l)
	
);



Counter_N Counter_NN
(
	// Input Ports
	 .clk(clk_rd),
	 .rst(rst),
	 .en(pop_l_vector),
	 .clear(LOW),
	 .N_counter(NN_value_l),
	
	// Output Ports
	 .Flag_counter(counterNN_flag)
	
);




endmodule


module SHOT_LOAD
import nBitsPackage::*;
(
    input clk,
    input rst,	 
	 input data_in,
	 input allow_Flag_in, 
	 output logic push_o
	 
);


/*state enum definition*/
PUSH_STATE_en PUSH_currentState;

/*
*************************************************************************************
* Continuous assign
*************************************************************************************
*/




always_ff@(posedge clk or negedge rst ) 
	begin
		if(rst == FALSE)
			PUSH_currentState <= P_IDLE;
		else 
			begin
				case (PUSH_currentState)
					P_IDLE: 
						begin
							if(data_in == TRUE)
								PUSH_currentState <= PUSH;
							else 
								PUSH_currentState <= P_IDLE;										
						end
												
					PUSH:
						begin
								PUSH_currentState <=NOT_PUSH;
						end	
						
					NOT_PUSH:
						begin
							if(data_in == FALSE)
								PUSH_currentState <= P_IDLE;
							else
								PUSH_currentState <= NOT_PUSH;
						end
						
					default:
						PUSH_currentState <= P_IDLE;
				endcase
			end
	end


/*
*************************************************************************************
* Combinational processes
*************************************************************************************
*/



always@(*)
begin
	case(PUSH_currentState)
		P_IDLE: 
			begin
						push_o = LOW;
			end
			
		PUSH:
			begin
				if (allow_Flag_in == TRUE)
					begin
						push_o = HIGH;					
					end
				else 
					begin
						push_o = LOW;					
					end					
			end
		
		NOT_PUSH:	
			begin
						push_o = LOW;						
			end


		default: 
			begin
						push_o = LOW;
			end
	endcase

end


endmodule

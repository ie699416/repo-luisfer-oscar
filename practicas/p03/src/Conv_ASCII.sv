module Conv_ASCII
import nBitsPackage::*;
(
	// Input Ports
	input decoder_en digit,
	// Output Ports
	output  data8_t UART_ascii

);


//Case encargado de mostrar el numero correspondiente de la entra al display
always_comb
begin
//Solamente tomamos los casos desde 0 hasta 9
	case (digit)
	
		ZERO_d 	: UART_ascii = 8'b00110000; 
      ONE 		: UART_ascii = 8'b00110001; 
      TWO 		: UART_ascii = 8'b00110010; 
      THREE		: UART_ascii = 8'b00110011; 
		FOUR 		: UART_ascii = 8'b00110100; 
      FIVE 		: UART_ascii = 8'b00110101; 
      SIX 		: UART_ascii = 8'b00110110; 
      SEVEN 	: UART_ascii = 8'b00110111; 
      EIGHT 	: UART_ascii = 8'b00111000; 
      NINE 		: UART_ascii = 8'b00111001; 
		SIGN 		: UART_ascii = 8'b00110101; 
      default  : UART_ascii = 8'b01011111; // Enter
	
	
    endcase
end



endmodule
module Module_CMD
import top_pkg::*;
(	
	input Data_N data_in,
	output logic Flag_FE,
	output logic Flag_N,
	output logic Flag_rs,
	output logic Flag_start,
	output logic Flag_vector,
	output logic Flag_matrix,	
	output logic Flag_EF
	
);

always_comb 
	begin
		case(data_in)
			CMD_FE_start:
				begin 
					Flag_FE		= TRUE;
					Flag_N		= FALSE;
					Flag_start  = FALSE;
					Flag_vector = FALSE;
					Flag_matrix = FALSE;
					Flag_rs     = FALSE;
					Flag_EF		= FALSE;
				end
			CMD_N:
				begin 
					Flag_FE		= FALSE;
					Flag_N		= TRUE;
					Flag_start  = FALSE;
					Flag_vector = FALSE;
					Flag_matrix = FALSE;
					Flag_rs     = FALSE;
					Flag_EF		= FALSE;
				end
			CMD_rs:
				begin
					Flag_FE		= FALSE;
					Flag_N		= FALSE;
					Flag_rs     = TRUE;
					Flag_start  = FALSE;
					Flag_matrix = FALSE;
					Flag_vector = FALSE;
					Flag_EF		= FALSE;
				end
			CMD_Start:
				begin
					Flag_FE		= FALSE;
					Flag_N		= FALSE;
					Flag_rs     = FALSE;
					Flag_start  = TRUE;
					Flag_matrix = FALSE;
					Flag_vector = FALSE;
					Flag_EF		= FALSE;
				end
			CMD_Matrix:
				begin
					Flag_FE		= FALSE;
					Flag_N		= FALSE;
					Flag_rs     = FALSE;
					Flag_start  = FALSE;
					Flag_matrix = TRUE;
					Flag_vector = FALSE;
					Flag_EF		= FALSE;
				end
			CMD_Vector:
				begin
					Flag_FE		= FALSE;
					Flag_N		= FALSE;
					Flag_rs     = FALSE;
					Flag_start  = FALSE;
					Flag_matrix = FALSE;
					Flag_vector = TRUE;
					Flag_EF		= FALSE;
				end
				
			CMD_EF_end:
				begin
					Flag_FE		= FALSE;
					Flag_N		= FALSE;
					Flag_rs     = FALSE;
					Flag_start  = FALSE;
					Flag_matrix = FALSE;
					Flag_vector = FALSE;
					Flag_EF		= TRUE;
				end
			default:
				begin 
					Flag_FE		= FALSE;
					Flag_N		= FALSE;
					Flag_rs     = FALSE;
					Flag_start  = FALSE;
					Flag_matrix = FALSE;
					Flag_vector = FALSE;
					Flag_EF		= FALSE;
				end
		
		endcase
	end
endmodule
		
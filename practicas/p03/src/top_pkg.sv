
`ifndef OTHERPACKAGE_DONE
   `define OTHERPACKAGE_DONE
package top_pkg;

    localparam  VECTOR_N  = 8;
	 localparam  MATRIX_N  = 8;
	 localparam  DATA_N  = 8;
	 localparam  DATA_R  = 16;
	 localparam  DATA_S  = 19;
	 
	 
	 parameter TRUE = 1'b1;

	 typedef logic [VECTOR_N-1:0]  Vector_N;
	 typedef logic [MATRIX_N-1:0]  Matrix_N;
	 typedef logic [DATA_R-1:0]  dataNN_t;
	 typedef logic [DATA_N-1:0]  Data_N;
	 
	 
	 parameter FALSE = 1'b0;
	 parameter HIGH = 1'b1;
	 parameter LOW = 1'b0;
	 parameter UNIT = 1'b1;
	 parameter ZERO = 1'b0;
	 parameter TWO = 2'b10;
	 parameter ZERON = {8{1'b0}};	 
	 parameter ZEROS = {DATA_S{1'b0}};

	 parameter CMD_FE_start = 8'b11111110;
	 parameter CMD_EF_end   = 8'b11101111;
	 
	 parameter CMD_N =  8'b00000001;
	 parameter CMD_rs = 8'b00000010;
	 parameter CMD_Start = 8'b00000011;
	 parameter CMD_Vector = 8'b00000101;
	 parameter CMD_Matrix = 8'b00000100;


    typedef logic [DATA_N-1:0]  dataN_t;
	 typedef logic [DATA_R-1:0]  dataR_t;
	 typedef logic [DATA_S-1:0]  dataS_t;
	 
	 
	 	 typedef enum logic [3:0]{
        STATE_IDEL	    = 4'b0000,
		  STATE_START 		 = 4'b0001,
		  STATE_L			 = 4'b0010,
		  STATE_CMD			 = 4'b0011,
		  STATE_N			 = 4'b0100,
		  STATE_VECTOR		 = 4'b0101,
		  STATE_MATRIX		 = 4'b0110,
		  STATE_PROCESS	 = 4'b0111,
		  STATE_RT			 = 4'b1000,
		  STATE_RESULT		 = 4'b1001,
		  STATE_TX 			 = 4'b1010
		  
		} State_decode;
		
		 typedef enum logic [DATA_N -1:0]{
        N_ONE	  	 		 = 8'b00000001,
		  N_TWO 		 		= 8'b00000010,
		  N_THREE			 = 8'b00000011,
		  N_FOUR			 = 8'b00000100,
		  N_FIVE			 = 8'b00000101,
		  N_SIX				 = 8'b00000110,
		  N_SEVEN			 = 8'b00000111,
		  N_EIGHT			 = 8'b00001000	
		  	
		  
		} N_decode;
		
endpackage

`endif

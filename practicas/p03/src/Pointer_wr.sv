



module Pointer_wr
import FIFO_pkg::*;
(	
	input clk_wr,
	input rst,

	//input DATA_N data_addr,
	input         push,
	input logic   full_flag,

	output dataN_t addr_wr,
	output logic Flag_wr

);

addrN_t  queue = ZERON; //= ADDR_N - UNIT;

always_ff @(posedge clk_wr or negedge rst ) begin 

	if (rst == FALSE)begin
		queue   <= ZERON;
	end
	else
		begin
			if(push == TRUE && full_flag == FALSE) 
				begin 
					queue    <= (queue + UNIT);
				end

			else 
				begin
					queue    <= queue ;
				end
		end

	
end

assign addr_wr = queue [DATA_N - UNIT:0];
assign Flag_wr = queue[ADDR_N - UNIT];

endmodule

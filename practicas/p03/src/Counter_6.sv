
module Counter_6
import P03_pkg::*;
(
	// Input Ports
	input clk,
	input rst,
	input en,
	input clear, 
	
	// Output Ports
	
	output logic enableRegister_flag,
	output Data_N Count_logic
	
	
);


logic Flag_counter;


always_ff@(posedge clk or negedge rst)
	begin: counter
		if (rst == FALSE)
			Count_logic <= ZERON;
		else 
			begin
				if (clear == TRUE)
					Count_logic <= ZERON;
				else if(en == TRUE)
					begin
						if (Flag_counter == HIGH)
							begin								
								Count_logic <= Count_logic;
							end
						else					
							Count_logic <= Count_logic + UNIT;
					end					
				else
					Count_logic <= Count_logic;							
			end
	end: counter
//--------------------------------------------------------------------------------------------

always_comb
begin

	if(6 == Count_logic)
		Flag_counter = HIGH;
	else
		Flag_counter = LOW;

end	


always_comb
begin

	if(Flag_counter == LOW & clear == LOW & en == HIGH)
		enableRegister_flag = HIGH;
	else
		enableRegister_flag = LOW;

end	



endmodule
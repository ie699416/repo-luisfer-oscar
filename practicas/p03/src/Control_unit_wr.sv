

module Control_unit_wr
import FIFO_pkg::*;
(
	input clk,
	input rst,
	
	input logic Push,
	input logic Flag_full,	
		
	output logic Full_flag,
	output logic Start_flag
	
);


State_wr Actual_state;	

always_ff@(posedge clk or negedge rst) 
	begin
		if (rst == FALSE)
			Actual_state <= STATE_IDLE_WR;			
		else 
			begin		
				case(Actual_state)				
						STATE_IDLE_WR:
							begin 						
								if (Push == TRUE && Flag_full == FALSE)
									Actual_state <= STATE_WR; 									
								else 
									Actual_state <= STATE_IDLE_WR; 							
							end	
						STATE_WR:
							begin 						
								if (Flag_full == TRUE)
									Actual_state <= STATE_IDLE_WR; 	
								else if( Push == TRUE)
									Actual_state <= STATE_WR; 	
								else 
									Actual_state <= STATE_IDLE_WR; 							
							end			
							
						default: Actual_state<=STATE_IDLE_WR;
							
				 endcase	
			end
	end

always_comb 
	begin
		case(Actual_state)
			 STATE_IDLE_WR:
				 begin 
					Full_flag = LOW;
					Start_flag = LOW;				
				 end
			STATE_WR:
				 begin 
					Full_flag = LOW;
					Start_flag = HIGH;				
				 end
				
			 default:
				begin
					Full_flag = HIGH;
					Start_flag = LOW;	
				end
	 
	endcase
end

endmodule

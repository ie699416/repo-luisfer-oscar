module UART_FSM_TX
import nBitsPackage::*;
(
    input clk,
    input rst,	 
	 input Counter_flag,
	 input counter_data_TX,
	 input logic Start,
	 input logic Counter_stop_flag,
	 input logic Counter_parity_flag,
	 
	 //////////
	 output logic start_counter,
	 output logic TX_flag,
	 output logic Load_flag,
    output logic data_ready_flag_TX, // 	 output logic stop_flag
	 output logic parity_flag,
	 output logic ENDPARITY_flag,
	 output logic ENDTX_flag

	 
	 
);


/*state enum definition*/
UART_STATE_en UART_currentState; 


always_ff@(posedge clk or negedge rst ) 
	begin
		if(rst == FALSE)
		 UART_currentState <= IDLE;
		else 
			begin
				case (UART_currentState)
					IDLE: 
						begin
							if(Start == TRUE)
								UART_currentState <= STARTBIT;
							else 
								UART_currentState <= IDLE;
						end
					STARTBIT:
						begin
							if(Counter_flag == TRUE)
								UART_currentState <= PROCESS;
							else
								UART_currentState <= UART_currentState;
						end
					PROCESS:
						begin
							if(counter_data_TX == TRUE)
								UART_currentState <= PARITY;
							else
								UART_currentState <= UART_currentState;
						end
					PARITY:
						begin
							if(Counter_parity_flag  == TRUE)
								UART_currentState <= STOPBIT;
							else
								UART_currentState <= UART_currentState;
						end
					STOPBIT:
						begin
							if(Counter_flag == TRUE)
							UART_currentState <= IDLE;
							else
								UART_currentState <= UART_currentState;
						end
					default:
						UART_currentState <= IDLE;
				endcase
			end
	end


/*
*************************************************************************************
* Combinational processes
*************************************************************************************
*/


always@(*)
begin
	case(UART_currentState)
		IDLE: 
			begin
				TX_flag 			= LOW;
				start_counter  = LOW;  
				Load_flag 		= LOW;
				data_ready_flag_TX = LOW; 
				parity_flag = LOW;
				ENDPARITY_flag = LOW;
				ENDTX_flag = LOW;
			end
		STARTBIT:
			begin
				TX_flag 			= FALSE;
				start_counter  = TRUE; 
				data_ready_flag_TX = LOW;
				Load_flag 		= TRUE;
				parity_flag = LOW;
				ENDPARITY_flag = HIGH;				
				ENDTX_flag = HIGH;
			end
		PROCESS:
			begin
				TX_flag 			= TRUE;
				start_counter  = TRUE; 
				data_ready_flag_TX = LOW;
				Load_flag 		= LOW;
				parity_flag = LOW;	
				ENDPARITY_flag = HIGH;				
				ENDTX_flag = HIGH;
			end
		PARITY:
			begin
				TX_flag 			= LOW;
				start_counter 	= HIGH;  	
				data_ready_flag_TX = LOW;
				Load_flag 		= LOW;
				parity_flag = HIGH;
				ENDPARITY_flag = HIGH;	
				ENDTX_flag = HIGH;
			end
		STOPBIT:	
			begin
				TX_flag 			= LOW;
				start_counter 	= HIGH; 
				data_ready_flag_TX = HIGH;
				Load_flag 		= LOW;
				parity_flag = LOW;
				ENDPARITY_flag = LOW;					
				ENDTX_flag = HIGH;
			end
		default: 
			begin
				TX_flag 			= LOW;
				start_counter 	= LOW; 
				data_ready_flag_TX = LOW;
				Load_flag 		= LOW;	
				parity_flag = LOW;	
				ENDPARITY_flag = LOW;				
				ENDTX_flag = LOW;
			end
	endcase

end


endmodule

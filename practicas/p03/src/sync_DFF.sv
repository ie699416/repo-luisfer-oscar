
module sync_DFF
import FIFO_pkg::*; 
(
	input clk, 
	input rst,
	input dataN_t data_in, 
	
	output dataN_t data_out
);

dataN_t data_w;

Dual_FF FF_D_1(

	.clk(clk),
	.rst(rst),
	.data_in(data_in),

	.data_out(data_w)
);


Dual_FF FF_D_2(

	.clk(clk),
	.rst(rst),
	.data_in(data_w),

	.data_out(data_out)
);


endmodule


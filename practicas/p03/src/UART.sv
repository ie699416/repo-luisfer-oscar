/*
*************************************************************************************
*	Coder:	OSCAR MAISTERRA FLORES	 		[ ie699416]
*	Coder: 	LUIS FERNANDO MORENO ALONSO	[ ie693314] 
*	Date:          Jul 3th, 2019
*  Name:          UART.sv
*  Description:   First approach
*
*************************************************************************************
*/

module UART
import nBitsPackage::*;
(
    input CLK_115200_16X_l,
	 input rst,
	 
	 // RX input signals
	 input serialData_RX_in,					// RX serial input	 
	 input clearInterrupt_in,					// RX INT signal clear
	 
	 // TX input signals	 
	 input transmit_in, 							// TX external control start flag
    input data8_t transmitData_in,			// TX parallel input
	 
	 // RX output signals
	 output data8_t receivedData_o,			// RX parallel output
	 output RX_parityError_o,					// RX error
	 output logic RX_interrupt_o,	
	 
	// TX output signals
	
	 output logic TX_interrupt_o,						// TX transfered	1 byte
	 output serialData_TX_o						// TX serial output	 
);


logic halfCount_flag_l;
logic receiving_flag_l;
logic counter_flag_l;
logic waiting_flag_l;
logic data_ready_flag_l;
logic counter_data_l;
logic timer_flag_l;
logic parity_flag_l;

logic Start_counter_tx;
logic Flag_counter_tx;
logic data_ready_flag_TX;
logic counter_data_TX_l;
logic ENDPARITY_flag_l;
logic ENDTX_flag_l;
logic Counter_parity_flag_l;

/* 
*************************************************************************************
* Instances definition
*************************************************************************************
*/


UART_RX Receive(
	.clk(CLK_115200_16X_l),
	.rst(rst),
	.en_SR(counter_flag_l),
	.en_PIPO(data_ready_flag_l),
	.serialData_RX_in(serialData_RX_in),
	.RX_int(RX_interrupt_o),
	.parity_RX_o(RX_parityError_o),
	.receivedData_o(receivedData_o)
);

UART_FSM_RX RX_control
(
	.clk(CLK_115200_16X_l),
	.rst(rst),
	.UART_start_in(serialData_RX_in),
	.counter_flag_in(counter_flag_l),
	.counter_data_in(counter_data_l),
	.halfCount_flag(halfCount_flag_l),
	.receiving_flag(receiving_flag_l),
	.data_ready_flag(data_ready_flag_l),
	.waiting_flag(waiting_flag_l),
	.timer_flag(timer_flag_l)
);


Counter_bt counter_16_ts
(
	.clk(CLK_115200_16X_l),
	.rst(rst),
	.clear(data_ready_flag_l),
	.halfCount_flag_in(halfCount_flag_l),
	.en(timer_flag_l),
	.flag(counter_flag_l),
	.countValue_o()	
);

Counter_data counter_8_data
(
	.clk(CLK_115200_16X_l),
	.rst(rst),
	.clear(data_ready_flag_l),
	.en(receiving_flag_l & counter_flag_TX_l),
	.flag(counter_data_l),
	.countValue_o()	
);


UART_TX TX_UART_MODULE
(
     .clk(CLK_115200_16X_l),
     .rst(rst),
	  .en(counter_flag_TX_l),						// Enable 
	  .tx_active(transmit_in),
	  .parity(parity_flag_l),
	  .stop(data_ready_flag_TX_l),
	  .load(load_data),
     .transmitData_in(transmitData_in),	 
     .serialData_TX_o(serialData_TX_o)
);

UART_FSM_TX TX_FSM
(
     .clk(CLK_115200_16X_l),
     .rst(rst),	 
	  .Counter_flag(counter_flag_TX_l),
	  .counter_data_TX(counter_data_TX_l),
	  .Start(transmit_in),
	  .Counter_parity_flag(Counter_parity_flag_l),
	 
	 //////////
	  .start_counter(Start_counter_tx),
	  .TX_flag(TX_flag_l),
	  .Load_flag(load_data),
	  .data_ready_flag_TX(data_ready_flag_TX_l),
	  .parity_flag(parity_flag_l),
	  .ENDTX_flag(ENDTX_flag_l),
	  .ENDPARITY_flag(ENDPARITY_flag_l)
	 
);

Counter_bt counter_16_ts_TX
(
	.clk(CLK_115200_16X_l),
	.rst(rst),
	.clear(LOW),
	.halfCount_flag_in(LOW),
	.en(Start_counter_tx),
	.flag(counter_flag_TX_l),
	.countValue_o()	
);



Counter_data counter_8_data_tx
(
	.clk(CLK_115200_16X_l),
	.rst(rst),
	.clear(data_ready_flag_TX_l),
	.en(TX_flag_l & counter_flag_TX_l),
	.flag(counter_data_TX_l),
	.countValue_o()	
);

Counter_data_9_TX counter_PARITY_data_TX // 
(
	.clk(CLK_115200_16X_l),
	.rst(rst),
	.clear(data_ready_flag_TX_l),
	.en(counter_flag_TX_l),
	.flag(Counter_parity_flag_l),
	.countValue_o()	
);




assign TX_interrupt_o = data_ready_flag_TX_l; 



endmodule 
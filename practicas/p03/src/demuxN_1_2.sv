/*
*************************************************************************************
*	Coder:	OSCAR MAISTERRA FLORES	 		[ ie699416]
*	Coder: 	LUIS FERNANDO MORENO ALONSO	[ ie693314] 
*	Date : 	June 8th, 2019
*  Name :   muxN_2_1.sv  
*
*	Description: Multiplexer parametric N bus value
*
* 
*************************************************************************************
*/

module demuxN_1_2
import P03_pkg::*;
(
	// Input Ports
	input Selector,
	input Data_N data_in,

	
	// Output Ports
	output Data_N data_out_1,
	output Data_N data_out_2

);


always_comb 
	begin: MUX
		if (Selector == TRUE)
			data_out_1 = data_in;
		else
			data_out_2 = data_in;
			
	end: MUX// end always

endmodule


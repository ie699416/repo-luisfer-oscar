/*
*************************************************************************************
*	Coder:	OSCAR MAISTERRA FLORES	 		[ ie699416]
*	Coder: 	LUIS FERNANDO MORENO ALONzO	[ ie693314] 
*	Date:          Jun 4th, 2019	
*  Name:          Counter_bt.sv      
*************************************************************************************
*/

module Counter_bt
import nBitsPackage::*;
(
	// Input Ports
	input clk,
	input rst,
	input en,	
	input clear,
	input halfCount_flag_in,
	
	// Output Ports
	output flag,
	output dataN_t countValue_o
	
);

logic MaxValue_Bit;
dataCount_t Count_logic;

	always_ff@(posedge clk or negedge rst) begin: counter
		if (rst == FALSE)
			Count_logic <= ZERON;
		else 
			begin
				if(en == TRUE)
					begin
						if (MaxValue_Bit == HIGH)
							begin								
								Count_logic <= ZERON;
							end
						else					
							Count_logic <= Count_logic + UNIT;
					end
				else if (clear == TRUE)
					Count_logic <= ZERON;	
				else
					Count_logic <= Count_logic;	
				
			end
	end: counter

//--------------------------------------------------------------------------------------------

always_comb
	begin
		if(halfCount_flag_in == TRUE)
			begin
				if(Count_logic ==  SAMP_HALF_COUNT)
					MaxValue_Bit = HIGH;
				else
					MaxValue_Bit = LOW;                                
			end
		else
			begin		
				if(Count_logic ==  SAMP_FULL_COUNT                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                               )
					MaxValue_Bit = HIGH;
				else
					MaxValue_Bit = LOW;
			end
	end

assign flag = MaxValue_Bit;
assign countValue_o = Count_logic;

endmodule

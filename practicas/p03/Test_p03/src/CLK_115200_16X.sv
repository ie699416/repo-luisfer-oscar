
module CLK_115200_16X
import nBitsPackage::*;

(
	
	// Input Ports
	input clk_FPGA,
	input rst,
	input en, 
	// Output Ports
	output logic CLK_115200_16X
	
);

data9_t Count_logic; // Max count 436

logic MaxValue_Bit = LOW;


always_ff@(posedge clk_FPGA or negedge rst) 
	begin
		if (rst == FALSE)
			begin
					Count_logic <= ZERO9;
					CLK_115200_16X <= LOW;						
			end
		else
			begin
				if(en == TRUE)
					begin
						if(MaxValue_Bit == HIGH)
							begin							
								CLK_115200_16X <= ~CLK_115200_16X;
								Count_logic <= ZERO9;		
							end
						else 
							Count_logic <= Count_logic + UNIT;
					end
				else
					begin
						CLK_115200_16X <= LOW;
						Count_logic <= ZERO9;			
					end
			end
	end
	
	
	
always@(*)
			begin
				if(Count_logic == SAMP_16X_COUNT_VALUE - UNIT)
					MaxValue_Bit = HIGH;
				else
					MaxValue_Bit = LOW;
			end




endmodule 

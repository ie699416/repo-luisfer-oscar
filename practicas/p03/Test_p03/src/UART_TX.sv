/*
*************************************************************************************
*	Coder:	OSCAR MAISTERRA FLORES	 		[ ie699416]
*	Coder: 	LUIS FERNANDO MORENO ALONSO	[ ie693314] 
*	Date:          Jul 3th, 2019
*  Name:          UART.sv
*  Description:   First approach
*
*************************************************************************************
*/

module UART_TX
import nBitsPackage::*;(
    input clk,
    input rst,
	 input en,						// Enable 
	 input tx_active,
	 input parity,
	 input stop,
	 input load,
    input data8_t transmitData_in,	 
    output logic serialData_TX_o
);

/*
*************************************************************************************
* Logics definitions
*************************************************************************************
*/

logic serialData_TX_l;

PISO
TX_register
 (
  .clk(clk),    
  .rst(rst),     
  .enable(en),   
  .Load(load),    
  .Data_in(transmitData_in),   
  .Data_out(serialData_TX_l)     
);


always_comb
	begin		
		if(tx_active == TRUE)
			begin
				if (parity == TRUE)
					serialData_TX_o = 1'b0;
				else if (stop == TRUE)
					serialData_TX_o = 1'b1;
				else
					serialData_TX_o = serialData_TX_l;
			end
		else
			serialData_TX_o = 1'b1;
	end
	
	



                                                                                                                                              
endmodule 
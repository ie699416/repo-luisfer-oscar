/*
*************************************************************************************
*	Coder:	OSCAR MAISTERRA FLORES	 		[ ie699416]
*	Coder: 	LUIS FERNANDO MORENO ALONSO	[ ie693314] 
*	Date:          Jul 3th, 2019
*  Name:          p03.sv
*  Description:   First approach
*
*************************************************************************************
*/
module p03
import nBitsPackage::*;
(
	//inputs
	input clkFPGA,
	input reset_pb_in,
	input serial_input_RX_in,	// PIN_G12
	input data8_t Tx_data,
	input Tx_enable,
	
	output data8_t data_o,
	output logic RX_parity_o,
	output logic TX_data
);

logic CLK_115200_16X_l;

/* 
*************************************************************************************
* Instances definition
*************************************************************************************
*/

CLK_115200_16X clk_UART
(
	.clk_FPGA(clkFPGA),
	.rst(reset_pb_in),
	.en(HIGH), 						//Always enable
	.CLK_115200_16X(CLK_115200_16X_l)	
);

UART UART
(
    .CLK_115200_16X_l(CLK_115200_16X_l),
	 .rst(reset_pb_in),
	 
	 // RX input signals
	 .serialData_RX_in(serial_input_RX_in),					// RX serial input	 
	 .clearInterrupt_in(),					// RX INT signal clear
	 
	 // TX input signals	 
	 .transmit_in(Tx_enable), 							// TX external control start flag
    .transmitData_in(Tx_data),			// TX parallel input
	 
	 // RX output signals
	 .receivedData_o(data_o),				// RX parallel output
	 .RX_parityError_o(RX_parity_o),					// RX error
	 .RX_interrupt_o(),						// RX received	1 byte
	 
	// TX output signals
	 .serialData_TX_o(TX_data)					// TX serial output
	 
);


/*
*************************************************************************************
*/
	
endmodule

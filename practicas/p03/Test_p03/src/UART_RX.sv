/*
*************************************************************************************
*	Coder:	OSCAR MAISTERRA FLORES	 		[ ie699416]
*	Coder: 	LUIS FERNANDO MORENO ALONSO	[ ie693314] 
*	Date:          Jul 3th, 2019
*  Name:          UART.sv
*  Description:   First approach
*
*************************************************************************************
*/

module UART_RX
import nBitsPackage::*;
(
    input clk,
    input rst,	 
    input en_SR,
	 input en_PIPO,
    input serialData_RX_in,
	 output logic RX_int, 
	 output parity_RX_o,
    output data8_t receivedData_o
);

/*
*************************************************************************************
* Logics definitions
*************************************************************************************
*/



data9_t Q_l;

/*

*************************************************************************************
* Instances definitions
*************************************************************************************
*/



shiftRegister_Q UART_Register
(  
	.clk(clk),
	.rst(rst),
	.EN(en_SR),
	.D_in(serialData_RX_in),
	.Q_o(Q_l)
);

Register_With_Sync_Reset INT_register
(
	.clk(clk), 
	.reset(rst),
	.enable(en_PIPO & en_SR),
	.Sync_Reset(FALSE),					//Flush
	.Data_Input(Q_l),
	.parity_RX_o(parity_RX_o),
	.Data_Output(receivedData_o)
);

assign RX_int = en_PIPO & en_SR;

	 
	 
endmodule

//////////////////////////////////////////////////////////////////////////////////
// Company: ITESO
// Engineer: Gustavo Araiza Obeso ie699209@iteso.mx
// Engineer: Oscar Maisterra Flores ie699416@iteso.mx
// 			 
// Create Date:  21/04/2019  
// Design Name:  
// Module Name:  shift register de rx
// Project Name: Sequential multiplier
// Target Devices: FPGA ALTERA DE2-115
// Tool versions: Quartus Prime
//
// Revision: 
// Revision 0.01 - File Created
// Additional Comments: 
////////////////////////////////////////////////////////////////////////////////////
module shiftRegister_Q
import nBitsPackage::*;
(
    input clk,
    input rst,
    input EN,
    input D_in,
    output data9_t Q_o
);



always_ff @(posedge clk, negedge rst)
	begin
		if (rst == FALSE)
			Q_o <= ZERO9;
		else
			if (EN == TRUE)
				begin
				/*Shift right operation, new bit enters as MSB*/
				Q_o <= {D_in,Q_o[8:1]};
				end
			else
				begin
					Q_o <= Q_o;
				end
	end



endmodule

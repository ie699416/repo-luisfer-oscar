onerror {resume}
quietly WaveActivateNextPane {} 0
add wave -noupdate -expand -group TB /p03_tb/clkFPGA_l
add wave -noupdate -expand -group TB /p03_tb/reset_pb_l
add wave -noupdate -expand -group TB /p03_tb/serial_input_RX_l
add wave -noupdate -expand -group TB /p03_tb/Data_tx_in
add wave -noupdate -expand -group TB /p03_tb/data_l
add wave -noupdate -expand -group TB /p03_tb/tx_enable
add wave -noupdate -expand -group TB /p03_tb/data_out_tx
add wave -noupdate -expand -group UART /p03_tb/uut/clkFPGA
add wave -noupdate -expand -group UART /p03_tb/uut/reset_pb_in
add wave -noupdate -expand -group UART /p03_tb/uut/serial_input_RX_in
add wave -noupdate -expand -group UART /p03_tb/uut/Tx_data
add wave -noupdate -expand -group UART /p03_tb/uut/Tx_enable
add wave -noupdate -expand -group UART /p03_tb/uut/data_o
add wave -noupdate -expand -group UART /p03_tb/uut/RX_parity_o
add wave -noupdate -expand -group UART /p03_tb/uut/TX_data
add wave -noupdate /p03_tb/uut/UART/TX_UART_MODULE/clk
add wave -noupdate /p03_tb/uut/UART/TX_UART_MODULE/rst
add wave -noupdate /p03_tb/uut/UART/TX_UART_MODULE/en
add wave -noupdate /p03_tb/uut/UART/TX_UART_MODULE/load
add wave -noupdate /p03_tb/uut/UART/TX_UART_MODULE/transmitData_in
add wave -noupdate /p03_tb/uut/UART/TX_UART_MODULE/serialData_TX_o
add wave -noupdate /p03_tb/uut/UART/TX_FSM/clk
add wave -noupdate /p03_tb/uut/UART/TX_FSM/rst
add wave -noupdate /p03_tb/uut/UART/TX_FSM/Counter_flag
add wave -noupdate /p03_tb/uut/UART/TX_FSM/Start
add wave -noupdate /p03_tb/uut/UART/TX_FSM/start_counter
add wave -noupdate /p03_tb/uut/UART/TX_FSM/TX_flag
add wave -noupdate /p03_tb/uut/UART/TX_FSM/Load_flag
add wave -noupdate /p03_tb/uut/UART/TX_FSM/data_ready_flag_TX
add wave -noupdate /p03_tb/uut/UART/TX_FSM/UART_currentState
add wave -noupdate /p03_tb/uut/UART/counter_16_ts_TX/clk
add wave -noupdate /p03_tb/uut/UART/counter_16_ts_TX/rst
add wave -noupdate /p03_tb/uut/UART/counter_16_ts_TX/en
add wave -noupdate /p03_tb/uut/UART/counter_16_ts_TX/clear
add wave -noupdate /p03_tb/uut/UART/counter_16_ts_TX/halfCount_flag_in
add wave -noupdate /p03_tb/uut/UART/counter_16_ts_TX/flag
add wave -noupdate /p03_tb/uut/UART/counter_16_ts_TX/countValue_o
add wave -noupdate /p03_tb/uut/UART/counter_16_ts_TX/MaxValue_Bit
add wave -noupdate /p03_tb/uut/UART/counter_16_ts_TX/Count_logic
add wave -noupdate /p03_tb/uut/UART/counter_PARITY_data_TX/clk
add wave -noupdate /p03_tb/uut/UART/counter_PARITY_data_TX/rst
add wave -noupdate /p03_tb/uut/UART/counter_PARITY_data_TX/en
add wave -noupdate /p03_tb/uut/UART/counter_PARITY_data_TX/clear
add wave -noupdate /p03_tb/uut/UART/counter_PARITY_data_TX/flag
add wave -noupdate /p03_tb/uut/UART/counter_PARITY_data_TX/countValue_o
add wave -noupdate /p03_tb/uut/UART/counter_PARITY_data_TX/MaxValue_Bit
add wave -noupdate /p03_tb/uut/UART/counter_PARITY_data_TX/Count_logic
add wave -noupdate /p03_tb/uut/UART/counter_8_data_tx/clk
add wave -noupdate /p03_tb/uut/UART/counter_8_data_tx/rst
add wave -noupdate /p03_tb/uut/UART/counter_8_data_tx/en
add wave -noupdate /p03_tb/uut/UART/counter_8_data_tx/clear
add wave -noupdate /p03_tb/uut/UART/counter_8_data_tx/flag
add wave -noupdate /p03_tb/uut/UART/counter_8_data_tx/countValue_o
add wave -noupdate /p03_tb/uut/UART/counter_8_data_tx/MaxValue_Bit
add wave -noupdate /p03_tb/uut/UART/counter_8_data_tx/Count_logic
TreeUpdate [SetDefaultTree]
WaveRestoreCursors {{Cursor 1} {217842606 ps} 0}
quietly wave cursor active 1
configure wave -namecolwidth 318
configure wave -valuecolwidth 100
configure wave -justifyvalue left
configure wave -signalnamewidth 0
configure wave -snapdistance 10
configure wave -datasetprefix 0
configure wave -rowmargin 4
configure wave -childrowmargin 2
configure wave -gridoffset 0
configure wave -gridperiod 1
configure wave -griddelta 40
configure wave -timeline 0
configure wave -timelineunits ps
update
WaveRestoreZoom {41099719 ps} {524152647 ps}
